# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from datetime import datetime

class PostponeOpWizard(models.TransientModel):
	_name = 'postpone.op.wizard'
	
	date_production = fields.Date(string=u'Nueva Fecha de Producción')
	order_id = fields.Many2one('glass.order',string=u'Órd. de Prod.',readonly=True)
	reason = fields.Text('Motivo de reprogramación')
	
	def set_values_dates_op(self):
		self.ensure_one()
		order = self.order_id
		old_date = order.date_production
		order._set_order_dates(force_prod_date=self.date_production)

		old_date = '/'.join(reversed(old_date.split('-')))
		new_date = '/'.join(reversed(order.date_production.split('-')))
		motive =  u'<br>Motivo de Reprogramación: %s.'%self.reason if self.reason else ''

		message = u'%s ha reprogramado la fecha de producción de la O.P. %s de la fecha %s a la fecha %s.%s'%(self.env.user.name,order.name,old_date,new_date,motive)

		sender = self.env['send.email.event'].create({
			'subject': u'Reprogramación de Órden de Producción: %s'%order.name,
			'message': message
		})
		res = sender.send_emails(motive='reprograming_op') 
		return res