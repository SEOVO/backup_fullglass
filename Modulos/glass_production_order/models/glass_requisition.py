# -*- coding: utf-8 -*-
from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
from odoo.tools.float_utils import float_round

class GlassRequisition(models.Model):
	_name = 'glass.requisition'
	_order_by = 'id desc'

	def _get_default_config(self):
		config = self.env['glass.order.config'].search([],limit=1)
		if not config:
			raise UserError(u'No ha encontrado la configuración de producción.')
		return config.id

	name = fields.Char(u'Orden de Requisición',default="/")
	table_number=fields.Char('Nro. de Mesa',required=True)
	date_order = fields.Date('Fecha',default=lambda self: fields.Date.context_today(self))
	required_area = fields.Float('M2 Requeridos',compute="totals",digits=(20,4))
	required_mp   = fields.Float('M2 Materia Prima',compute="totals",digits=(20,4))
	returned_area = fields.Float('M2 Devueltos',compute="totals",digits=(20,4))
	used_area     = fields.Float('M2 Usados',compute="totals",digits=(20,4))
	merma = fields.Float('Merma',compute="totals",digits=(20,4))
	
	state = fields.Selection([
		('draft','Borrador'),
		('confirm','Confirmada'),
		('process','Procesada'), # done / Finalizada , por motivos x se quedó así
		('done','Finalizada'),
		('cancel','Cancelada')],string='Estado',default='draft',required=True)

	move_mp_ids  = fields.One2many('stock.move','requisition_mp_id',string=u'Operaciones de Materia Prima',readonly=True)
	move_rt_ids  = fields.One2many('stock.move','requisition_rt_id',string=u'Operaciones de Retazos',readonly=True)
	move_drt_ids = fields.One2many('stock.move','requisition_drt_id',string=u'Operaciones de devolución de retazos',readonly=True)
	move_srv_raw_ids = fields.One2many('stock.move', 'requisition_srv_raw_id', 
		string=u'Operaciones de materia prima para servicios', readonly=True)

	count_picking_mp  = fields.Integer('Total pick MP',compute="_compute_count_pick")
	count_picking_rt  = fields.Integer('Total pick RT',compute="_compute_count_pick")
	count_picking_drt = fields.Integer('Total pick DRT',compute="_compute_count_pick")
	count_picking_srv_raw = fields.Integer('Total pick Servicios',compute="_compute_count_pick")
	
	production_order_ids = fields.Many2many('glass.order','glass_requisition_production_rel','requisition_id','order_id',u'ordenes de Produccion',compute="getprdorders")
	
	glass_lot_ids = fields.One2many('glass.lot','requisition_id',string='Lotes',readonly=True,
	states={'draft': [('readonly', False)],'confirm': [('readonly', False)]})
	product_id = fields.Many2one('product.product','Producto General')
	config_id = fields.Many2one('glass.order.config','Config Param',default=_get_default_config)

	# IMPORTANTE: Esta vaina no es lo más idóneo, arreglar cuando tengan tiempo XDXD, no es para nada escalable, pero ya que el requerimiento de producir en 2 sedes se hizo al final y no hubo tiempo para replantear y se hizo de esta forma...:(
	# IMPORTANTE tener en cuenta que éste campo se usa en la logística de insulados
	location = fields.Selection([('prod_aqp','Prod. Arequipa'),('prod_lima','Prod. Lima')],string='Sede',required=True)
	
	_sql_constraints = [
		('table_location_uniq', 'UNIQUE (table_number,location)', u'La combinación de Nro de mesa y Sede debe ser única.'),]

	@api.multi
	@api.depends('table_number','name')
	def name_get(self):
		result = []
		dict_locs = {'prod_aqp':'AQP','prod_lima':'LIMA',}
		for req in self:
			name = '%s - %s (%s)'%(req.table_number,req.name,dict_locs.get(req.location,''))
			result.append((req.id, name))
		return result

	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		domain = []
		if name:
			domain = ['|',('table_number', operator, name), ('name',operator,name)]
		requisitions = self.search(domain + args, limit=limit)
		return requisitions.name_get()

	def _compute_count_pick(self):
		for req in self:
			req.count_picking_mp  = len(req.move_mp_ids.mapped('picking_id'))
			req.count_picking_rt  = len(req.move_rt_ids.mapped('picking_id'))
			req.count_picking_drt = len(req.move_drt_ids.mapped('picking_id'))
			req.count_picking_srv_raw = len(req.move_srv_raw_ids.mapped('picking_id'))

	# boton de pickings
	def action_view_delivery_glass(self):
		action = self.env.ref('stock.action_picking_tree_all').read()[0]
		get_pickings = self._context.get('get_pickings')
		pickings = self.env['stock.picking']
		if get_pickings=='mat_prima':
			pickings = self.move_mp_ids.mapped('picking_id')
		elif get_pickings=='scraps':
			pickings = self.move_rt_ids.mapped('picking_id')
		elif get_pickings=='return_scraps':
			pickings = self.move_drt_ids.mapped('picking_id')
		elif get_pickings == 'mat_prima_services':
			pickings = self.move_srv_raw_ids.mapped('picking_id')
		if len(pickings) > 1:
			action['domain'] = [('id', 'in', pickings.ids)]
		elif pickings:
			action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
			action['res_id'] = pickings.id
		return action

	def get_allowed_materials(self):
		allowed = self.config_id.requisition_materials_ids.filtered(lambda x: x.product_id.id == self.product_id.id) 
		if not allowed:
			raise UserError(u'No se ha encontrado la lista de materiales para el producto %s en este tipo de requisicion.\nConfigure su lista de materiales en: Produccion->Parametros->Orden de Requisicion->Materiales de Requisición.'%self.product_id.name)
		elif not allowed.materials_ids:
			raise UserError('La lista de materiales permitidos para %s esta vacia'%self.product_id.name )
		else:
			return allowed.materials_ids.ids
			
	def confirm(self):
		self.ensure_one()
		conf = self.config_id
		if not conf.seq_requisi:
			raise UserError(u'No ha configurado la secuencia de Requisiciones')

		lots = self.glass_lot_ids
		products = lots.mapped('product_id')
		if len(products) == 0:
			raise UserError(u'No ha agregado Lotes en la Orden de Requisición.')
		if len(products) > 1:
			raise UserError(u'El producto de los Lotes agregados debe ser único')

		lots.mapped('line_ids').write({'requisicion':True}) # TODO Debería jalarse de la req. asociada a su lote??
		self.write({
			'name':conf.seq_requisi.next_by_id(),
			'state':'confirm',
			'product_id':products.id,
		})

	@api.model
	def create(self,vals):
		if 'table_number' in vals:
			vals['table_number']=self.get_table_name(vals['table_number'])
		return super(GlassRequisition,self).create(vals)

	@api.multi
	def write(self,vals):
		if 'glass_lot_ids' in vals:
			new_vals = []
			for v in vals['glass_lot_ids']: # prevenir que se elimine el registro de lote:
				if v[0]==2: new_vals.append([3]+v[1:])
				else: new_vals.append(v)
			vals['glass_lot_ids'] = new_vals
		if 'table_number' in vals:
			vals['table_number'] = self.get_table_name(vals['table_number'])
		return super(GlassRequisition,self).write(vals)
		 
	@api.model
	def get_table_name(self,table):
		if not table.isnumeric():
			raise UserError(u'El valor ingresado "%s" no es válido,\nIngrese un número entero.'%table)
		if len(str(table)) > 6:
			raise UserError(u'El valor ingresado "%s" excede el límite permitido (6 cifras).'%s)
		# A partir del 13/04/2021, el prefijo de mesa pasará de "M" a "m"
		#return 'M' + table.rjust(6, '0')
		return 'm' + table.rjust(6, '0')

	def cancel(self):
		self.ensure_one()
		moves = self.move_mp_ids | self.move_rt_ids | self.move_drt_ids
		if any(move.state == 'done' for move in moves):
			raise UserError(u'No es posible cancelar ésta Orden de Requisición debido que ya registra movimientos de almacén')
		pickings = moves.mapped('picking_id')
		pickings.action_cancel()
		self.glass_lot_ids.mapped('line_ids').write({'requisicion':False})
		self.write({'glass_lot_ids':[(5,)],'state':'cancel'})
		return True 

	@api.multi
	def unlink(self):
		for req in self:
			if req.state!='draft':
				raise UserError(u'No es posible eliminar una Requisición que fue Confirmada')
		return super(GlassRequisition,self).unlink()

	def getprdorders(self):
		for req in self:
			req.production_order_ids = req.glass_lot_ids.mapped('line_ids.order_prod_id')

	def totals(self):
		for req in self:
			n,m,j,tpmp,tprt,tpdrt=0,0,0,0,0,0
			for l in req.glass_lot_ids:
				n+=l.total_area
			for mp in req.move_mp_ids.filtered(lambda x:x.state=='done'):
				m += mp.product_uom_qty/mp.product_uom.factor
			for rt in req.move_rt_ids.filtered(lambda x:x.state=='done'):
				m += rt.product_uom_qty/rt.product_uom.factor
			for drt in req.move_drt_ids.filtered(lambda x:x.state=='done'): 
				j += drt.product_uom_qty/drt.product_uom.factor

			req.required_area = n
			req.required_mp   = m
			req.merma         = (m-j)-n
			req.returned_area = j
			req.used_area     = m-j

	def process(self):
		for req in self:
			if req.state != 'confirm':
				continue
			if not req.move_mp_ids and not req.move_rt_ids:
				raise UserError(u'Error:\nLa Orden de requisición %s debe tener por lo menos un Albarán de Materias primas o de Retazos.'%req.name)
			totalsolicitado = sum(req.glass_lot_ids.mapped('total_area'))
			merma = req.merma
			for l in req.glass_lot_ids:
				psol = (l.total_area * 100)/totalsolicitado
				mermaequi = merma * (psol/100)
				for sl in l.line_ids:
					g = (sl.area * 100)/l.total_area
					mermaline = mermaequi * (g/100)
					sl.merma  = mermaline

			# TODO procesar picking de servicios
			req.action_create_services_transfer()
			req.state = 'done'

	def action_create_services_transfer(self):
		self.ensure_one()
		# Crear picking se materiales para servicios:
		# heredado en módulo de fichas  maestras
		return {}

	# MATERIAS PRIMAS:
	def add_material(self):
		modes = ('mat_prima','scraps','return_scraps')
		ctx = dict(self._context or {})
		mode = ctx.get('mode',False)
		if mode not in modes: return

		type_pick,tras_motive,rt_loc = self._get_pick_params(mode)

		year = datetime.now().year
		allowed = self.get_allowed_materials()
		ids = ','.join(map(str,allowed))
		location = type_pick.default_location_src_id # source location
		line_values = []

		if mode=='mat_prima':
			view = 'requisition_mat_prima_wizard_view_form'
			action_name = u'Solicitud de Materia Prima'
			# NOTE Reestablecer la condicionla de rango de fecha en caso vayan a cambiar de base de datos
			# y quieran excluir los registro del año anterior
			# AND vst_k.date >= '%d-01-01' 
			# AND vst_k.date <= '%d-12-31' 
			# AND vst_kf.date >= '%d-01-01'
			# AND vst_kf.date <= '%d-12-31'
			query = """
				SELECT * FROM (
				SELECT 
				vst_k.product_id,
				pu.id AS uom_id,
				(COALESCE(sum(stock_disponible),0.0)) * pu.factor as available
				FROM vst_kardex_onlyfisico_total vst_k
				JOIN product_template pt ON pt.id = vst_k.product_tmpl_id 
				JOIN product_uom pu ON pu.id = pt.uom_id
				WHERE 
				product_id IN (%s) 
				AND ubicacion = %d 
				AND pu.plancha = true
				AND vst_k.date >= '%d-01-01' 
				AND vst_k.date <= '%d-12-31' 
				GROUP BY vst_k.product_id,pu.id,pu.factor)T
				WHERE T.available > 0.0
				""" % (ids, location.id, year, year)
			self._cr.execute(query)
			results = self._cr.dictfetchall()
			line_values = [(0,0,{
				'product_id': r['product_id'],
				'uom_id': r['uom_id'],
				'qty_available': r['available'],
			}) for r in results]

		elif mode=='scraps':
			view = 'requisition_scraps_wizard_view_form'
			action_name = u'Solicitud de Retazos'
			query = """
				SELECT * FROM
				(SELECT 
				vst_kf.product_id AS product_id,
				vst_kf.unidad AS uom_id,
				round(sum(vst_kf.stock_disponible) * pu.factor,4) AS available
				FROM vst_kardex_onlyfisico_total_poliglass vst_kf
				JOIN product_uom pu on pu.id = vst_kf.unidad
				WHERE 
				vst_kf.product_id IN (%s)
				AND vst_kf.ubicacion =  %d
				AND vst_kf.date >= '%d-01-01'
				AND vst_kf.date <= '%d-12-31'
				GROUP BY 
				vst_kf.product_id,
				vst_kf.unidad,
				pu.factor
				ORDER BY 
				vst_kf.product_id,
				vst_kf.unidad)T
				WHERE T.available > 0.0
				""" % (ids, rt_loc.id, year, year)
			self._cr.execute(query)
			results = self._cr.dictfetchall()
			line_values = [(0,0,{
				'product_id': r['product_id'],
				'uom_id': r['uom_id'],
				'qty_available': r['available'],
			}) for r in results]

		elif mode=='return_scraps':
			view = 'requisition_return_scraps_wizard_view_form'
			action_name = u'Devolución de Retazos'
			prods = self.move_mp_ids.mapped('product_id') | self.move_rt_ids.mapped('product_id')
			ctx['dom_scraps_prods'] = [('id','in',prods.ids)]

		wizard = self.env['requisition.material.wizard'].create({
			'line_ids':line_values,
			'requisition_id':self.id,
			'mode':mode,
		})
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'res_id':wizard.id,
			'name': action_name,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_id': self.env.ref('%s.%s'%(module,view)).id,
			'view_mode': 'form',
			'target': 'new',
			'context':ctx
		}

	def _get_pick_params(self,type_op):
		## mat prima , retazos y dev de ratazos
		conf = self.config_id
		tp,tm,loc = False,False,False
		if type_op == 'mat_prima':
			if self.location=='prod_aqp':
				tp,tm,loc = conf.picking_type_mp,conf.traslate_motive_mp,conf.location_retazo
			elif self.location=='prod_lima':
				tp,tm,loc =  conf.picking_type_mp_lima,conf.traslate_motive_mp_lima,conf.location_retazo_lima
		elif type_op == 'scraps':
			if self.location=='prod_aqp':
				tp,tm,loc = conf.picking_type_rt,conf.traslate_motive_rt,conf.location_retazo
			elif self.location=='prod_lima':
				tp,tm,loc = conf.picking_type_rt_lima,conf.traslate_motive_rt_lima,conf.location_retazo_lima
		elif type_op == 'return_scraps':
			if self.location=='prod_aqp':
				tp,tm,loc = conf.picking_type_drt,conf.traslate_motive_drt,conf.location_retazo
			elif self.location=='prod_lima':
				tp,tm,loc = conf.picking_type_drt_lima,conf.traslate_motive_drt_lima,conf.location_retazo_lima

		elif type_op == 'mat_prima_services':
			if self.location=='prod_aqp':
				tp,tm,loc = conf.pick_type_def_serv_mp_aqp, conf.traslate_motive_mp, conf.location_retazo
			elif self.location=='prod_lima':
				tp,tm,loc = conf.pick_type_def_serv_mp_lima, conf.traslate_motive_mp_lima, conf.location_retazo_lima

		if not all((tp,tm,loc)):
			raise UserError(u'No se ha encontrado todos los parámetros necesarios para realizar ésta operación')
		return tp,tm,loc
