# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError,ValidationError
import re

# NOTE Estas etapas sólo deberían registrarse de forma explícita, mas no registrarse desde el control de la producción,
# tampoco deberían considerarse como etapas pendientes, 
# ya que definen si se produjeron o si son posteriores a la producción
FORBIDDEN_STAGES = {'producido', 'ingresado', 'entregado', 'rotura'}


class GlassStage(models.Model):
	_name = 'glass.stage'
	_description = u'Etapa/Proceso de cristal'

	name = fields.Char('Nombre', required=True, index=True)
	#is_service = fields.Boolean('Etapa de Servicio', default=False)
	description = fields.Text(u'Descripción')

	_sql_constraints = [('uniq_name','UNIQUE (name)',u'El nombre de la etapa debe ser único')]

	@api.constrains('name')
	def _verify_name(self):
		for stage in self:
			if not re.match(r'^[a-z0-9_]*$',stage.name):
				raise ValidationError(u'El nombre de etapa debe contener sólo caracteres alfanuméricos y en minúscula, además no debe tener espacios.')