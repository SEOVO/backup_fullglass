# -*- coding: utf-8 -*-
from odoo import fields, models,api, _
from odoo.exceptions import UserError,ValidationError
from datetime import datetime
from odoo.addons.glass_production_order.models.sale_order import TYPE_ORDERS
from odoo.addons.glass_production_order.models.glass_stage import FORBIDDEN_STAGES

class GlassLot(models.Model):
	_name='glass.lot'
	_inherit = ['mail.thread']
	_order="id desc"

	name = fields.Char('Lote', default='/')
	state = fields.Selection([
		('draft','Nuevo'),
		('cancel','Cancelado'),
		('done','Emitido')], string='Estado', track_visibility='onchange', default='draft')
	type_lot = fields.Selection(TYPE_ORDERS, string='Tipo de lote', default='production', readonly=True, copy=False)
	date = fields.Date('Fecha',default=datetime.now())
	product_id = fields.Many2one('product.product','Producto')
	total_area = fields.Float(u'Área Total',compute="_compute_totals",digits=(20,4))
	crystal_count = fields.Integer(u'Número de cristales',compute="_compute_totals")
	line_ids = fields.One2many('glass.lot.line','lot_id',string='Detalle',domain=['|',('active','=',False),('active','=',True)])
	user_id = fields.Many2one('res.users','Responsable')
	optimafile = fields.Binary('Archivo OPTIMA')
	file_name = fields.Char(string='File Name', compute='_get_file_name')
	requisition_id = fields.Many2one('glass.requisition', string=u"Requisición")

	@api.depends('name')
	def _get_file_name(self):
		ext = self.env['glass.order.config'].search([],limit=1).optimization_ext
		for record in self:
			record.file_name = '%s.%s'%(record.name.rjust(7,'0'),(ext or 'ext'))

	@api.multi
	@api.depends('name', 'product_id')
	def name_get(self):
		result = []
		for lote in self:
			name = lote.name + ' ' + lote.product_id.name
			result.append((lote.id, name))
		return result

	@api.multi
	def unlink(self):
		for rec in self:
			if rec.state in ('cancel','done'):
				raise UserError(u'No se puede eliminar en los estados: Cancelado o Emitido')
		return super(GlassLot,self).unlink()

	@api.one
	def optimize_lot(self):
		for line in self.line_ids:
			line.order_line_id.order_id.state="process"
		return True

	def cancel_lot(self):
		self.ensure_one()
		if self.requisition_id:
			raise UserError(u'No es posible cancelar el lote: %s debido a que se encuentra en la requisición: %s.'%(self.name,self.requisition_id.name))
		lines = self.line_ids
		g_lines = lines.mapped('order_line_id')
		lines.unlink()
		g_lines.write({'is_used':False})
		g_lines.mapped('order_id')._refresh_state()
		self.state='cancel'
		return True

	@api.multi
	def _compute_totals(self): # consulta directa para mejorar el performance
		self._cr.execute(""" 
			SELECT 
			gll.lot_id,
			COUNT(gll.*)::text || ';' || SUM(gll.area)::text 
			FROM glass_lot_line gll
			WHERE gll.lot_id IN %s
			GROUP BY gll.lot_id; """,(tuple(self.ids),))
		results = dict(self._cr.fetchall())
		for lot in self:
			result = results.get(lot.id,'0;0.0').split(';')
			lot.crystal_count = result[0]
			lot.total_area = result[1]

	@api.one
	def validate_lot(self):
		config_data = self.env['glass.order.config'].search([])[0]
		if self.name=='/':
			newname = config_data.seq_lot.next_by_id()
			self.update({'name':newname})
		self.write({'state':'done'})
		for line in self.line_ids:
			line.order_line_id.is_used = True
		return True

class GlassLotLine(models.Model):
	_name='glass.lot.line'
	
	_rec_name="search_code"
	_order = 'nro_cristal_fl'

	lot_id = fields.Many2one('glass.lot','Lote',readonly=True)
	product_id=fields.Many2one('product.product','Producto',readonly=True)
	calc_line_id = fields.Many2one('glass.sale.calculator.line', copy=False, index=True, readonly=True)
	order_line_id = fields.Many2one('glass.order.line', index=True, readonly=True)
	nro_cristal = fields.Char(u"Número de Cristal", index=True, size=10,readonly=True)
	nro_cristal_fl = fields.Float(related='order_line_id.crystal_number_fl',store=True,readonly=True)
	order_prod_id = fields.Many2one('glass.order',related='order_line_id.order_id',string="OP",store=True,readonly=True)
	order_date_prod = fields.Date('Fecha OP',related='order_prod_id.date_production')
	search_code = fields.Char(u'Código de búsqueda',readonly=True,index=True)
	base1 = fields.Integer("Base1 (L 4)",related='calc_line_id.base1',store=True)
	base2 = fields.Integer("Base2 (L 2)",related='calc_line_id.base2',store=True)
	altura1 = fields.Integer("Altura1 (L 1)",related='calc_line_id.height1',store=True)
	altura2 = fields.Integer("Altura2 (L 3)",related='calc_line_id.height2',store=True)
	area = fields.Float(u'Área M2',digits=(12,4))
	measures = fields.Char('Medidas', related='calc_line_id.measures',store=True, readonly=True)
	page_number = fields.Integer(u"Nro. Pág.")
	
	# optimizado = fields.Boolean('Optimizado',
	# 	compute='_compute_checked_stages', store=True)
	# corte = fields.Boolean('Corte',
	# 	compute='_compute_checked_stages', store=True)
	# lavado = fields.Boolean('Lavado',
	# 	compute='_compute_checked_stages', store=True)
	# horno = fields.Boolean('Horno',
	# 	compute='_compute_checked_stages', store=True)
	# templado = fields.Boolean('Templado',
	# 	compute='_compute_checked_stages', store=True)
	# insulado = fields.Boolean('Insulado',
	# 	compute='_compute_checked_stages', store=True)
	# producido = fields.Boolean('Producido',
	# 	compute='_compute_checked_stages', store=True)
	# ingresado = fields.Boolean('Ingresado',
	# 	compute='_compute_checked_stages', store=True)
	# entregado = fields.Boolean('Entregado',
	# 	compute='_compute_checked_stages', store=True)
	# comprado = fields.Boolean('Comprado',
	# 	compute='_compute_checked_stages', store=True)
	# # Etapas de servicio:
	# pulido = fields.Boolean('Pulido',
	# 	compute='_compute_checked_stages', store=True)
	# entalle = fields.Boolean('Entalle',
	# 	compute='_compute_checked_stages', store=True)
	# arenado = fields.Boolean('Arenado',
	# 	compute='_compute_checked_stages', store=True)
	# corte_laminado = fields.Boolean(u'Corte laminado', 
	# 	compute='_compute_checked_stages', store=True)
	# corte_lavado_laminado = fields.Boolean(u'Corte Lavado laminado', 
	# 	compute='_compute_checked_stages', store=True)
	# perforacion = fields.Boolean(u'Perforación', 
	# 	compute='_compute_checked_stages', store=True)
	# pegado = fields.Boolean(u'Pegado', 
	# 	compute='_compute_checked_stages', store=True)
	# biselado = fields.Boolean(u'Biselado', 
	# 	compute='_compute_checked_stages', store=True)

	optimizado = fields.Boolean('Optimizado', readonly=True)
	corte = fields.Boolean('Corte', readonly=True)
	lavado = fields.Boolean('Lavado', readonly=True)
	horno = fields.Boolean('Horno', readonly=True)
	templado = fields.Boolean('Templado', readonly=True)
	insulado = fields.Boolean('Insulado', readonly=True)
	producido = fields.Boolean('Producido', readonly=True)
	ingresado = fields.Boolean('Ingresado', readonly=True)
	entregado = fields.Boolean('Entregado', readonly=True)
	comprado = fields.Boolean('Comprado', readonly=True)
	# Etapas de servicio:
	pulido = fields.Boolean('Pulido', readonly=True)
	entalle = fields.Boolean('Entalle', readonly=True)
	arenado = fields.Boolean('Arenado', readonly=True)
	corte_laminado = fields.Boolean(u'Corte laminado', readonly=True)
	corte_lavado_laminado = fields.Boolean(u'Corte Lavado laminado', readonly=True)
	perforacion = fields.Boolean(u'Perforación', readonly=True)
	pegado = fields.Boolean(u'Pegado', readonly=True)
	biselado = fields.Boolean(u'Biselado', readonly=True)

	# rotura
	is_break = fields.Boolean('Roto',
		compute='_compute_break_crystal',store=True)
	
	active = fields.Boolean('Activo', default=True)
	stage_ids = fields.One2many('glass.stage.record', 'lot_line_id', string='Etapas')
	pending_stages_string = fields.Char('Etapas pendientes (Ref)', 
		compute='_compute_pending_stages_string', readonly=True,
		help='Etapas pendientes excluyendo ingresado y entregado, campo de ayuda visual solamente')
	# all_pending_stages = fields.Char('Etapas pendientes', 
	# 	compute='_compute_checked_stages', store=True, readonly=True)
	all_pending_stages = fields.Char('Etapas pendientes', readonly=True)
	descuadre = fields.Char("Descuadre", size=7)
	requisicion = fields.Boolean("Requisición",compute='_get_requisition',store=True)
	laminado = fields.Boolean("Laminado",readonly=True)
	# esto no debería estar aqui per weno-....
	#transferido = fields.Boolean('Transferido',readonly=True)
	location_transfer_id = fields.Many2one('stock.location',string=u'Ubicación de transferencia',readonly=True)
	in_packing_list = fields.Boolean(related='order_line_id.in_packing_list') 
	is_service = fields.Boolean(related='order_line_id.is_service', string='Es servicio', store=True, readonly=True) # ?
	type_prod = fields.Char('tipoproducto') # ?
	image_glass = fields.Binary("imagen", related="calc_line_id.image") #?
	merma = fields.Float('Merma',digits=(12, 4)) #?

	_sql_constraints = [('uniq_search_code', 'UNIQUE (search_code)', u'El código de búsqueda debe ser único!')]

	# @api.depends('stage_ids.done')
	# def _compute_checked_stages(self):
	# 	static_stg = ('optimizado','corte','pulido','lavado','entalle','horno','templado','producido','insulado','ingresado','entregado','arenado','comprado')
	# 	for line in self:
	# 		done_stages = line.stage_ids.filtered('done')
	# 		for stg in static_stg:
	# 			line[stg] = bool(done_stages.filtered(lambda s: s.stage_id.name==stg))


	# OLD VERSION TODO REMOVE, usar sólo para subsanar data
	#@api.depends('stage_ids.done')
	def _compute_checked_stages(self):
		vals = {}
		#self.refresh()
		static_stg = ('optimizado', 'corte', 'pulido', 'lavado', 'entalle', 'horno', 'templado', 'producido', 
					'insulado', 'ingresado', 'entregado', 'arenado', 'comprado', 'corte_laminado', 
					'corte_lavado_laminado', 'perforacion', 'pegado', 'biselado')
		
		if self.ids:
			self._cr.execute("""
			SELECT 
			gsr.lot_line_id AS lot_line_id,
			ARRAY_AGG(CASE WHEN COALESCE(gsr.done, false) = false THEN 'pend_' || gsr.name ELSE gsr.name END) AS all_stages
			FROM 
			glass_stage_record gsr 
			WHERE 
			gsr.lot_line_id IN %s 
			AND gsr.name IN %s 
			GROUP BY gsr.lot_line_id; """, (tuple(self.ids), tuple(static_stg)))

			vals = dict(self._cr.fetchall())

		for line in self:
			dict_stages = dict.fromkeys(static_stg, False)
			done_stages, pending_stages = [], []
			for r in vals.get(line.id, []):
				if r[:5] == 'pend_':
					pending_stages.append(r[5:])
				else:
					done_stages.append(r)
			dict_stages.update(dict.fromkeys(done_stages, True))
			dict_stages['all_pending_stages'] = ','.join(pending_stages)
			line.write(dict_stages)


	def _compute_pending_stages_string(self):
		for line in self:
			pending = [st.stage_id.name for st in line.stage_ids if not st.done and st.stage_id.name not in ('producido', 'entregado')]
			# Agg etapas que todos deberían tener en algún momento:
			if not line.producido:
				pending.append('producido')
			# NOTE tener cuidado de cambiar el separador (' ,'), se usa éste campo para otros controles.
			line.pending_stages_string = ', '.join(pending)

	@api.depends('stage_ids.break_stage_id')
	def _compute_break_crystal(self):
		for line in self:
			line.is_break = bool(line.stage_ids.filtered(lambda x: x.stage_id.name == 'rotura' and x.done))

	@api.depends('lot_id.requisition_id')
	def _get_requisition(self):
		for line in self:
			line.requisicion = bool(line.lot_id.requisition_id)

	def retire_lot_line(self):
		self.ensure_one()
		if self.corte:
			raise UserError(u'No se puede retirar si a pasado la etapa de corte')
		rid = self.lot_id.id
		g_line = self.order_line_id
		self.unlink()
		g_line.write({
			#'last_lot_line':self.id,
			'glass_break':False,
			'lot_line_id':False,
			'is_used':False,
			'retired_user':self.env.uid,
			'retired_date':datetime.now(),
			#'state':''
		})
		g_line.order_id._refresh_state()
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name': u'Lotes de producción',
			'res_id':rid,
			'type': 'ir.actions.act_window',
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'glass.lot',
			'view_id': self.env.ref('%s.view_glass_lot_form'%module).id,
		} 

	@api.constrains('stage_ids')
	def _verify_unique_stages(self):
		for rec in self:
			names = [stage.stage_id.name for stage in rec.stage_ids]
			if len(names) > len(set(names)):
				raise ValidationError('Existen etapas duplicadas para el cristal %s Número: %s'%(rec.product_id.name,rec.nro_cristal))

	@api.model
	def create(self, values):
		# create required stages
		t = super(GlassLotLine, self).create(values)
		# Debemos restar las etapas restrictivas, ya que causan problemas al ser posteriores al producido,
		# dichas etapas ya serán registradas en su respectivo momento
		req_stages = list(set(t._get_requested_stages()) - FORBIDDEN_STAGES)
		if any(req_stages):
			stages = self.env['glass.stage'].search([('name', 'in', req_stages)])
			#t.stage_ids = [(0, 0, line._prepare_record_stage_vals(stage)) for stage in stages]
			added_stages = []
			for stage in stages:
				t._add_stage(stage)
				added_stages.append(stage.name)
			t.all_pending_stages = ','.join(added_stages)
		return t

	# TODO mejorar performance de creación de líneas de lote:
	# prop1 : la asignación de etpas hacerla desde otro método (_get_requested_stages_new)
	# y hacer la consulta de etapas solicitadas por todas las líneas 
	# y generarlas de forma masiva también

	def _get_requested_stages(self):
		#self.ensure_one() Debe ser sólo 1 , sólo por motivos de performance queda comentado
		# get requested stages from calculator
		# etapas definidas en calculadora ( no es buena idea ponerlo en estático pero
		# dado que hay prisa queda asi esa vaina):
		stages = ['corte', 'pulido', 'entalle', 'lavado', 'arenado', 'biselado', 'perforacion'] # tomo en cuenta q todos deben cortarse y lavarse
		if not self.calc_line_id.polished_id:
			stages.remove('pulido')
		# if not self.calc_line_id.arenado:
		if not self.calc_line_id.arenado_id:
			stages.remove('arenado')
		if not self.calc_line_id.biselado_id:
			stages.remove('biselado')
		if not self.calc_line_id.entalle > 0:
			stages.remove('entalle')
		if not self.calc_line_id.perforacion > 0:
			stages.remove('perforacion')
		# if not self.calc_line_id.lavado:
		# 	stages.remove('lavado')
		return stages

	# solo agrega una etapa por realizarse
	def _add_stage(self,stage):
		for line in self:
			line.write({'stage_ids': [(0, 0, line._prepare_record_stage_vals(stage))]})
	
	def register_stage(self, stage, break_info=False, raise_error=True):
		"""break_info debe ser un dict con posibles keys en break_motive,break_stage_id,break_note
		IMPORTANTE: Si es una rotura no llamar directamente a éste método, ya que no completa la información y proceso 
		necesario para una rotura, hacerlo con el método break_crystal de la order_line_id asociada"""
		StageObj = self.env['glass.stage']
		StageRecord = self.env['glass.stage.record']

		static_stg = ('optimizado', 'corte', 'pulido', 'lavado', 'entalle', 'horno', 'templado', 'producido', 
					'insulado', 'ingresado', 'entregado', 'arenado', 'comprado', 'corte_laminado', 
					'corte_lavado_laminado', 'perforacion', 'pegado', 'biselado')

		stage = stage if isinstance(stage, models.Model) else StageObj.search([('name', '=', stage)])
		stage_name = stage.name
		#stage = stage if type(stage) in (str, unicode) else stage.name

		#assert stage.id, #u'No se encontró la etapa solicitada.'
		bad_items = []
		ctx = self._context

		for line in self:

			# Excepción para producidos:
			# ya que la etapa de producido puede darse sin ser pedida explícitamente, puede
			# que se intente registar manualmente y de error por etapa ya registrada.
			if stage_name == 'producido' and line.producido:
				continue
			
			all_pending_stages = set()
			current_pending_stages = line.all_pending_stages.split(',')
			line_write_vals = {}
			stage_vals = []
			
			if not line.active:
				raise UserError(u'No es posible grabar etapas en un cristal roto/inactivo.')
			#if not ctx.get('force_register') and stage.name not in line._get_requested_stages():
			# TODO FIXME, considerar las etapas de _get_requested_stages sólo para la creación del cristal
			# ya que es posible que la ficha tenga modificaciones una vez determinadas las
			# etapas requeridas  y genere problemas. en su lugar usar mejor el campo de all_pending_stages
			#if not ctx.get('force_register') and stage_name not in line._get_requested_stages():
			if not ctx.get('force_register') and stage_name not in current_pending_stages:
				#bad_items.append(u'Etapa de %s no fue solicitada para el cristal: %s.'%(stage.name,line.search_code))
				bad_items.append(u'Etapa de %s no fue solicitada para el cristal: %s.' % (stage_name, line.search_code))
				continue
			# ver si la etapa ya existe y hace falta realizarla
			else:
				#existing = line.stage_ids.filtered(lambda x: x.stage_id.id == stage.id)
				existing = StageRecord
				# Es posble que en la ficha se haya solicitado la etapa de producido, 
				# entonces sólo deberíamos actualizar dicho registro 
				#producido_stg_waiting = False 
				for st in line.stage_ids:
					st_name = st.stage_id.name
					if st_name == stage_name:
						existing = st
					if st_name != stage_name and not st.done: # Excluir la etapa a registrar
						all_pending_stages.add(st.stage_id.name)
					#if st_name == 'producido' and not st.done:
					#	producido_stg_waiting = st

				if existing.done:
					bad_items.append(u'Etapa: %s Cristal: %s. Motivo: etapa ya fue registrada.' % (stage_name, line.search_code))
					continue
				elif existing:
					existing.write({'done': True, 'user_id': self.env.uid, 'date': datetime.now()})
				else:
					# solo si se fuerza el registro
					new_vals = self._prepare_record_stage_vals(stage, done=True)
					if break_info and stage_name == 'rotura':
						line.active = False
						new_vals.update(break_info)
					#new_vals['lot_line_id'] = line.id
					#StageRecord.create(new_vals)
					stage_vals.append((0, 0, new_vals))

				if hasattr(line, stage_name):
					line_write_vals[stage_name] = True
				# Dar como producido si no tiene etapas pendientes y si fue templado
				# TODO Pendiente de validación:
				#if not all_pending_stages and stage_name != 'producido' and (stage_name == 'templado' or line.templado) \
				#	and not line.is_service and not line.from_insulado and not line.producido:
				# Si el cristal tiene ficha maestra, ovbiar la obligatoriedad de la etapa de templado
				if not all_pending_stages and stage_name != 'producido' and\
					(stage_name == 'templado' or line.templado or line.is_service or line.mtf_template_id) and\
					not line.from_insulado and not line.producido:
					
					#if producido_stg_waiting:
					#	producido_stg_waiting.write({'done': True, 'user_id': self.env.uid, 'date': datetime.now()})
					#else:
					producido_stg = self.env.ref('glass_production_order.18', False)
					assert producido_stg and producido_stg.name == 'producido', 'Wrong value for "producido" stage'
					stage_vals.append((0, 0, self._prepare_record_stage_vals(producido_stg, done=True)))
					
					line_write_vals['producido'] = True
					line.order_line_id.write({
						'is_used': True, # sólo lo colocamos por si acaso :)
						'state': 'ended',
					})
					
				line.write(dict(line_write_vals, stage_ids=stage_vals, all_pending_stages=','.join(all_pending_stages)))

		if bad_items:
			error_msg = u'No fue posible registrar la(s) etapa(s), Detalle:\n'+'\n'.join(bad_items)
			if raise_error:
				raise UserError(error_msg)
			else:
				return {'status': 'ERROR', 'message': error_msg}
		return {'status': 'OK'}

	@api.model
	def _prepare_record_stage_vals(self, stage, done=False):
		return {
			'user_id': self.env.uid,
			'stage_id': stage.id,
			'date': datetime.now(),
			'done': done,
		}

	@api.model
	def make_search_code(self,**kwargs):
		search_code = '' 
		order_name = kwargs.get('order_name',False)
		lot_name = kwargs.get('lot_name',False)
		crystal_num = kwargs.get('crystal_number',False)

		if not all((order_name,lot_name,crystal_num)):
			raise UserError(u'No se han ingresado todos los parámetros necesarios')

		if '.' in order_name: # factura adelantada:
			splited = order_name.split('.') # debería tener 2 items
			try:
				search_code+=str(splited[1])
				order_name = splited[0]
			except IndexError:
				raise UserError(u'Ocurrió un error al generar el código de cristal debido a un formato incorrecto en el nombre de OP.')
		try:
			# TMP FIX xD:
			if not order_name[:1].isnumeric():
				order_name = order_name[1:]
			search_code += str(int(order_name)).rjust(5,'0')
		except ValueError:
			raise UserError(u'El nombre de OP: %s no corresponde a producción, es posible que se trate de una órden de distribución.'%order_name)
		search_code += str(crystal_num).rjust(4,'0')
		search_code += str(int(lot_name)).rjust(6,'0')
		return search_code

	@api.model
	def _cron_remove_calc_image(self):
		"""cron para remover el campo image de la línea de calculadora asociada a cristales entregados (todos los cristales
		asociados a la línea de calculadora deben estar entregados),
		al ser el campo image de tipo binario consume demasiado espacio en DB, y en caso de requerirlo de nuevo 
		puede ser generado con el método 'draw_crystal' de glass.calculator.line
		"""
		sql = """
			UPDATE glass_sale_calculator_line SET image = NULL WHERE id IN (
				SELECT id 
				FROM glass_sale_calculator_line gc 
				JOIN (
					SELECT split_part(T1.num,'_',2)::integer AS calc_line_id 
					FROM (SELECT count(*)::text || '_' ||calc_line_id AS num 
						FROM glass_lot_line WHERE (entregado = true OR is_break = true) GROUP BY calc_line_id
						)T1
					JOIN (SELECT count(*)::text || '_' ||calc_line_id AS num 
						FROM glass_lot_line GROUP BY calc_line_id
						)T2
					ON T1.num = T2.num)X
				ON gc.id = X.calc_line_id
				WHERE gc.image IS NOT NULL
				-- agregar las calculadoras de tengan una antiguedad superior a 6 meses aprox.
				UNION
				SELECT id
				FROM glass_sale_calculator_line gc
				WHERE gc.image IS NOT NULL 
				AND EXTRACT(DAY FROM CURRENT_DATE::timestamp - gc.create_date::timestamp) > 180.0
				);
		"""
		self._cr.execute(sql)