# -*- coding: utf-8 -*-

from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError
from datetime import datetime
from datetime import timedelta
import base64,os

class glass_pdf_file(models.Model):
	_name='glass.pdf.file'

	pdf_file = fields.Binary('Croquis')
	pdf_name = fields.Char('Archivo')
	file_name = fields.Char('Archivo')
	is_used = fields.Boolean('Usado',default=False)
	op_id = fields.Many2one('glass.order',u'Orden de Producción')
	sale_id = fields.Many2one('sale.order','Pedido de venta',required=True,ondelete='cascade')
	is_editable = fields.Boolean('Editable',default=True)
	path_pdf = fields.Char(string='Ruta del Pdf')
	_rec_name="pdf_name"

	@api.multi
	def unlink(self):
		for rec in self:
			if rec.op_id and rec.op_id.state != 'returned':
				raise UserError(u'No es posible eliminar\nLa Orden de producción %s utiliza este archivo.'%rec.op_id.name)
			if rec.path_pdf and os.path.exists(rec.path_pdf):
				os.remove(rec.path_pdf)
			else:
				print('Path file does not exist !')	
		return super(glass_pdf_file,self).unlink()

	@api.one
	def save_pdf(self):
		self.editable_croquis=False
		return True

	def show_sketch(self):
		vals = {}
		wizard = self.env['add.sketch.file']
		try:
			with open(self.path_pdf,"rb") as pdf_file:
				vals = {'sketch': pdf_file.read().encode("base64")}
		except TypeError as e:
			print(u'Path does not exist!')
			vals = {'message': 'Archivo Croquis removido o no encontrado!',}
		except IOError as e:
			print(u'Pdf file not found or not available!')
			vals = {'message': 'Archivo Croquis removido o no encontrado!',}
		wizard = wizard.create(vals)
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name':'Ver Croquis',
			'res_id':wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_id':self.env.ref('%s.only_view_sketch_file_view_form'%module).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
		}

# NOTE FROZEN, sólo aumentar ítems, no quitar los existentes
TYPE_ORDERS = [
	('production',u'Producción'),
	('services','Servicios')]

TYPE_SALES = TYPE_ORDERS + [('distribution',u'Distribución')]

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	op_ids = fields.One2many('glass.order','sale_order_id',string=u'Ordenes de producción',readonly=True,copy=False)
	op_count = fields.Integer(u'Ordenes de producción',compute="_get_glass_orders",copy=False)
	op_control = fields.Char('Control de OP',compute='_get_state_op')
	#op_ids = fields.One2many('glass.order',string=u'Ordenes de producción',compute="getops",copy=False)
	op_returned_ids = fields.One2many('glass.order',string=u'OP devueltas',compute="_get_glass_orders",copy=False)
	op_returned_count = fields.Integer(u'Nro OP devueltas',compute="_get_glass_orders",copy=False)
	files_ids = fields.One2many('glass.pdf.file','sale_id','Croquis',copy=False)
	reference_order = fields.Char(string='Referencia OP',copy=False,track_visibility='onchange')
	count_files = fields.Integer('Files',compute='_get_count_files')
	type_sale = fields.Selection(TYPE_SALES, string='Tipo de venta')
	#date_order = fields.Datetime(string='Fecha de Pedido', required=True, readonly=True, index=True, states={'draft': [('readonly', False)], 'sent': [('readonly', False)]}, copy=False, default=fields.Datetime.now,track_visibility='onchange') # original
	date_order = fields.Datetime(string='Fecha de Pedido', required=True, 
		readonly=True, index=True, copy=False, default=fields.Datetime.now,track_visibility='onchange')
	op_date_production = fields.Date(string=u'Fecha de producción', 
		compute='_get_glass_orders', readonly=True)

	# picking de servicios / de ubicación clientes a almacén de terceros:
	picking_services_ids = fields.One2many('stock.picking', 
		inverse_name='sale_services_id', string='Pickings materiales de cliente', readonly=True)
	count_picking_services = fields.Integer('Nro. Pickings servicios',compute='_get_count_picking_services')


	@api.depends('files_ids')
	def _get_count_files(self):
		for rec in self:
			rec.count_files = len(rec.files_ids)

	@api.depends('picking_services_ids')
	def _get_count_picking_services(self):
		for sale in self:
			sale.count_picking_services = len(sale.picking_services_ids)

	def add_sketch_file(self):
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name':'Agregar Archivo Croquis',
			'view_id': self.env.ref('%s.add_sketch_file_view_form'%module).id,
			'type': 'ir.actions.act_window',
			'res_model': 'add.sketch.file',
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context':{'sale_order_id':self.id}
		}

	@api.model
	def create(self, values):
		t = super(SaleOrder, self).create(values)
		type_sale = values.get('type_sale', False)
		seq = self.env['ir.sequence'].search([('code', '=', 'sale.order')])
		
		if type_sale == 'production':
			prod_seq = self.env['glass.order.config'].search([], limit=1).seq_sale_prod
			if not prod_seq:
				raise UserError(u'No ha configurado la secuencia para ventas de producción')
			t.name = prod_seq.next_by_id()
			seq.write({'number_next_actual': seq.number_next_actual-seq.number_increment})
		
		elif type_sale == 'services':
			srv_seq = self.env['glass.order.config'].search([], limit=1).seq_sale_services_id
			if not srv_seq:
				raise UserError(u'No ha configurado la secuencia para ventas de servicios')
			t.name = srv_seq.next_by_id()
			seq.write({'number_next_actual': seq.number_next_actual-seq.number_increment})
		return t
	
	def _get_state_op(self):
		# TODO para la huevada de los servicios con OP, refactorizar ésta vaina...
		for order in self:
			have_op = bool(order.op_count > 0)
			services = order.order_line.filtered(lambda x: x.product_id.type=='service')
			calculators = order.order_line.mapped('calculator_id')
			if calculators and len(services) != len(order.order_line):
				prod = calculators.filtered(lambda x: x.qty_invoiced_rest == 0.0)
				#if len(prod)==len(calculators) and ops: # todo producido
				if len(prod)==len(calculators) and have_op: # todo producido
					order.op_control = 'OP GENERADA'
				else:
					order.op_control = 'OP PARCIAL' if have_op else 'OP PENDIENTE'
			else:
				order.op_control = 'NO APLICA' # si no tiene calculadora y todas las lineas son servicios

	def _get_glass_orders(self):
		for order in self:
			ops = order.op_ids
			valid_ops = ops.filtered(lambda o: o.state != 'returned')
			returned = ops - valid_ops
			order.op_count = len(ops - returned)
			order.op_returned_count = len(returned)
			order.op_returned_ids = returned.ids
			prod_dates = valid_ops.mapped('date_production')
			if prod_dates and not order.before_invoice:
				order.op_date_production = max(prod_dates)
			else:
				order.op_date_production = False

	def loadproductionwizard(self):
		self.ensure_one()
		module = __name__.split('addons.')[1].split('.')[0]
		Wizard = self.env['build.glass.order.wizard']

		action = {
			'res_model': Wizard._name,
			'type': 'ir.actions.act_window',
			'view_mode': 'form',
			'target': 'new',
		}

		context = dict(self.env.context or {}, default_sale_id=self.id)

		selected_file_id = False
		available_files = self.files_ids.filtered(lambda x: not x.is_used and x.is_editable)
		context['file_domain'] = [('id', 'in', available_files.ids)]

		if self.type_sale == 'production':
			selected_file_id = available_files[0].id if available_files else False
			act_name = u'Generar Órden de Producción.'
			view_name = 'build_glass_order_wizard_form_view'

		elif self.type_sale == 'services':
			act_name = u'Generar Órden de Servicio.'
			view_name = 'build_glass_order_wizard_services_form_view'
		else:
			raise UserError('Esta operación sólo está permitida para lso pedidos de producción y servicios')
		
		wiz = Wizard.create(self._prepare_build_glass_order_wizard_vals(self, selected_file_id))

		action.update({
			'name': act_name,
			'view_id': self.env.ref('%s.%s' % (module, view_name)).id,
			'res_id': wiz.id,
			'context': context,
		})

		return action


	@api.model
	def _prepare_build_glass_order_wizard_vals(self, sale_order, selected_file_id=False):
		return {
			'sale_id': sale_order.id,
			'selected_file_id': selected_file_id,
			}
	
	def action_view_service_pickings(self):
		self.ensure_one()
		action = self.env.ref('stock.action_picking_tree_all').read()[0]
		
		# TODO get default picking_type from glass parameters
		action['context'] = dict(
			self.env.context,
			search_default_picking_type=1,
			default_sale_services_id=self.id, 
			default_partner_id=self.partner_id.id,
			default_origin=self.name)

		pickings = self.picking_services_ids
		if len(pickings) > 1:
			action['domain'] = [('id', 'in', pickings.ids)]
		elif pickings:
			action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
			action['res_id'] = pickings.id
		return action

	def show_op_list(self):
		self.ensure_one()
		action = self.env.ref('glass_production_order.glass_order_action').read()[0]
		returned = self.op_returned_ids
		ops = returned if self._context.get('returned_ops',False) else self.op_ids - returned
		if len(ops) > 1:
			action['domain'] = [('id', 'in', ops.ids)]
		elif ops:
			action['views'] = [(self.env.ref('glass_production_order.view_glass_order_form').id,'form')]
			action['res_id'] = ops.id
		return action

	def validate_sale_pay_terms(self,operation,config=None):
		#validando restricciones de terminos de pago:
		self.ensure_one()
		config = config or self.env['glass.order.config'].search([],limit=1)
		excluded_ids = config.partner_excluded_ids
		is_excluded = bool(self.partner_id in excluded_ids)
		if is_excluded: return True

		invoices = self.invoice_ids.filtered(lambda x: x.state!='cancel')
		if not invoices:
			raise UserError(u'No se completar esta operación cuando no se tiene una factura vigente')

		pay_term = self.payment_term_id
		if pay_term:
			param = self.env['config.payment.term'].search([('operation','=',operation)])
			param = param.filtered(lambda p: pay_term.id in p.payment_term_ids.ids)
			if len(param) > 1:
				raise UserError(u'La condición de pago actual se ha encontrado en múltiples ítems de la configuración de términos de pago.')
			if param and param.minimal > 0: # solo puede estar en una conf
				invoice = invoices[0] # FIXME simpre será sólo una factura??
				payed = invoice.amount_total - invoice.residual
				percent = (float(payed)/invoice.amount_total) * 100
				if percent < param.minimal:
					raise UserError(u'No es posible completar ésta operación\nEl porcentaje mínimo para el plazo de pago elegido es del {} %.'.format(str(param.minimal)))
			#else:
			#	raise UserError(u'No han configurado las condiciones para el Plazo de pago para la operación requerida')
		return True

	@api.multi
	def action_cancel(self):
		if self._context.get('force_action_cancel'):
			return super(SaleOrder,self).action_cancel()
		for order in self:
			if order.op_count > 0:
				raise UserError(u'No es posible cancelar un pedido de venta que tiene Órdenes de producción generadas')
			if order.invoice_ids.filtered(lambda i: i.state in ('open','paid')):
				raise UserError(u'No es posible cancelar un pedido de venta que tiene facturas vigentes.')
		return super(SaleOrder,self).action_cancel()

	@api.multi
	def unlink(self):
		for sale in self:
			if sale.op_count > 0:
				raise UserError(u"No se puede eliminar un pedido de venta si generó Órdenes de Producción")
		return super(SaleOrder,self).unlink()


class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'
	
	def get_glass_order_lines(self):
		self.ensure_one()
		return self.calculator_id._get_glass_order_ids().mapped('line_ids')

	@api.multi
	def unlink(self):
		for line in self:
			if line.order_id.state in ('sale','done'):
				raise UserError(u'No es posible eliminar una línea si el estado de su pedido es distinto a borrador')
			if line.calculator_id and line.calculator_id._get_glass_order_ids():
				raise UserError(u'No es posible eliminar la línea de pedido cuando su calculadora tiene Órdenes de Producción')
			line.calculator_id.unlink()
		return super(SaleOrderLine,self).unlink()