# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime

class StockMove(models.Model):
	_inherit='stock.move'

	glass_order_line_ids = fields.Many2many('glass.order.line','glass_order_line_stock_move_rel','stock_move_id','glass_order_line_id',string='Glass Order Lines',copy=False)
	show_detail_button  = fields.Boolean('Mostrar', compute='_get_show_detail_button')

	# glass.requisition_fields
	requisition_mp_id  = fields.Many2one('glass.requisition',string=u'Requisión MP',readonly=True,copy=False)
	requisition_rt_id  = fields.Many2one('glass.requisition',string=u'Requisión RT',readonly=True,copy=False)
	requisition_drt_id = fields.Many2one('glass.requisition',string=u'Requisión DRT',readonly=True,copy=False)

	# req. servicios
	requisition_srv_raw_id = fields.Many2one('glass.requisition',
		string=u'Requisión mat. servicios', readonly=True, copy=False, help=u'Requisición de material para servicios')

	@api.depends('glass_order_line_ids','state')
	def _get_show_detail_button(self):
		for rec in self:
			rec.show_detail_button = bool(rec.glass_order_line_ids and rec.state == 'done')

	# Obtiene los del detalle de los cristales que conforman 
	# el stock_move de salida (para entrega a clientes)
	# return dict : [{move_id:values},{move_2_id:vals}] 
	def get_results(self,extra_domain=[],limit=False):
		GlassLotLine = self.env['glass.lot.line']
		stages = ('templado','producido','ingresado','entregado','in_packing_list')
		values = []
		for move in self.filtered('procurement_id.sale_line_id.calculator_id'):
			calculator = move.procurement_id.sale_line_id.calculator_id
			dict_vals = calculator._get_dict_crystals(extra_domain=extra_domain,only_exists=True,limit=limit)
			for key,value in dict_vals.items():
				for k,v in value.items():
					if not any(v): continue
					# NOTE v = [lot_line.ids]
					lot_lines = GlassLotLine.browse(v) # en insulados puede ser mas de un cristal
					vals = {
						# NOTE en insulados todos deberían ser de la misma OP
						# no debería afectar sacar la data del primar item, ya q en isulados las áreas 
						# y dimensiones debería corresponderse...
						'move_id':move.id,
						'product_id':calculator.product_id.id,# aplica para templados e insulados
						'order_id': lot_lines[0].order_prod_id.id,
						'base1':lot_lines[0].base1,
						'base2':lot_lines[0].base2,
						'height1':lot_lines[0].altura1,
						'height2':lot_lines[0].altura2,
						'area':lot_lines[0].area,
						'crystal_num':str(k),
						'order_name':lot_lines[0].order_prod_id.name,
						'crystal_num_fl':float(k),
						'lot_line_ids':[(6,0,lot_lines.ids)]
					}
					# NOTE para el caso de insulados: sólo si ambos cristales cumplen la etapa 
					# se considera la etapa del cristal final como concluída:
					for st in stages:
						vals[st] = bool(all(l[st] for l in lot_lines))
					values.append((0,0,vals))
					if limit > 0: limit-=1
		return values
		
	# Detalle de los cristales que conforman el stock_move,
	def get_detail_lines_entered_to_stock(self):
		self.ensure_one()
		wizard = self.env['detail.crystals.entered.wizard'].create({
			'detail_lines':[(0,0,{
				'move_id':self.id,
				'glass_line_id':l.id,
				'lot_line_id':l.lot_line_id and l.lot_line_id.id or l.last_lot_line.id,
			}) for l in self.glass_order_line_ids]
		})
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name': 'Detalle de Cristales',
			'res_id': wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'view_id': self.env.ref('%s.show_detail_lines_entered_stock_wizard'%module).id,
			'target': 'new',
		}

	# Devolución de requisiciones
	def return_requisition_stock(self):
		"""Por ahora la devolución deberá ser por el total, queda pendiente el desarrollo para devoluciones parciales"""
		self.ensure_one()

		if self.state != 'done':
			raise UserError(u'El movimiento debe estar realizado')
		
		mode = False
		if self.requisition_mp_id:
			requisition = self.requisition_mp_id
			mode = 'mat_prima'
		elif self.requisition_rt_id:
			requisition = self.requisition_rt_id
			mode = 'scraps'
		elif self.requisition_drt_id:
			requisition = self.requisition_drt_id
			mode = 'return_scraps'

		if not requisition:
			raise UserError(u'El Movimiento debe estar asociado a una requisición de material')

		if requisition.state != 'confirm':
			raise UserError(u'La devolución de materiales sólo es posible para requisiciones en estado "Confirmado".')
		
		pick_src = self.picking_id
		pick_type = pick_src.picking_type_id.return_picking_type_id.id or pick_src.picking_type_id.id

		# FIXME mejorar esta mrd y sacar de los parámetros:
		if pick_src.einvoice_12.code == '99':
			code = '10'
		elif pick_src.einvoice_12.code == '10':
			code = '99'
		else:
			code = '11'
		traslate_motive = self.env['einvoice.catalog.12'].search([('code','=',code)],limit=1)

		return_pick = pick_src.copy({
			'move_lines': [],
			'picking_type_id': pick_type,
			'state': 'draft',
			'origin': pick_src.name,
			'location_id': pick_src.location_dest_id.id,
			'location_dest_id': pick_src.location_id.id,
			'einvoice_12': traslate_motive.id,
		})

		return_move = self.copy({
			'product_id': self.product_id.id,
			'product_uom_qty': self.product_uom_qty,
			'picking_id': return_pick.id,
			'state': 'draft',
			'location_id': pick_src.location_dest_id.id,
			'location_dest_id': pick_src.location_id.id,
			'picking_type_id': pick_type,
			'warehouse_id': pick_src.picking_type_id.warehouse_id.id,
			'origin_returned_move_id': self.id,
			'procure_method': 'make_to_stock',
		})

		return_pick.action_confirm()
		return_pick.action_assign()
		if return_pick.state!='assigned':
			return_pick.force_assign()

		if mode == 'mat_prima':
			#transferencia normal de planchas en la uom del producto
			for op in return_pick.pack_operation_ids:
				op.write({'qty_done':op.product_qty})
			return_pick.action_done()
		elif mode in ('scraps','return_scraps'):
			self.env['scraps.entry.wizard'].sudo().transfer_scraps_picking(return_pick,force_assign=True)

		self.write({
			'requisition_mp_id':False,
			'requisition_rt_id':False,
			'requisition_drt_id':False,
		})

		return {
			'name':return_pick.name,
			'res_id':return_pick.id,
			'type': 'ir.actions.act_window',
			'res_model': return_pick._name,
			'view_mode': 'form',
		}
