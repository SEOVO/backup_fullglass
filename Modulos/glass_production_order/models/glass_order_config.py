# -*- coding: utf-8 -*-

from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError, ValidationError
from odoo.addons.glass_production_order.wizard.glass_production_control import FORBIDDEN_STAGES

class GlassOrderConfig(models.Model):
	_name = 'glass.order.config'
	name = fields.Char(u'Parámetros',default=u"Parámetros",required=True)
	seq_sale_prod = fields.Many2one('ir.sequence',u'Secuencia S.O. de producción')
	seq_sale_services_id = fields.Many2one('ir.sequence',u'Secuencia S.O. de servicios')
	seq_def_delivery_services_id = fields.Many2one('ir.sequence', 
		string=u'Sec. por defec. para entreg. serv.', help=u'Secuencia por defecto para guías de remisión de servicios')
	seq_lot=fields.Many2one('ir.sequence',u'Secuencia para los lotes')
	
	optimization_ext = fields.Char(u'Extención')
	optimization_path = fields.Char(u'Ruta de archivo exportado')
	# horno
	seq_furnace = fields.Many2one('ir.sequence',u'Secuencia lote de horno')
	furnace_area = fields.Float(u'Área de Horno (M2)',digist=(12,4))
	seq_furnace_out = fields.Many2one('ir.sequence',u'Secuencia lote salida de horno')
	# ordenrequisicion
	seq_requisi = fields.Many2one('ir.sequence',u'Secuencia Orden de Requisición')
	
	# prod terminado:
	picking_type_pt = fields.Many2one('stock.picking.type',u'Operación producto terminado') # No usar
	picking_type_pt_ids = fields.Many2many('stock.picking.type',string=u'Operaciones de producto terminado')# Tipos de op para ingreso de prod.
	traslate_motive_pt = fields.Many2one('einvoice.catalog.12','Motivo traslado producto terminado')
	
	# devoluciones de prod terminado desde apt:
	# estas mrds al parecer tbn va a ser m2m para el las devoluciones de Lima tmr Lima otra vez jodiendo...tmr lo que me imaginaba grrr...
	picking_type_return_pt = fields.Many2one('stock.picking.type',string=u'Pick Type Return Apt') # Borrar
	picking_type_return_apt_ids = fields.Many2many('stock.picking.type','config_pick_type_return_apt_rel',string=u'Tipos de Pick. devoluciones a producción')
	traslate_motive_return_pt = fields.Many2one('einvoice.catalog.12','Motivo Traslado Apt')

	picking_type_mp = fields.Many2one('stock.picking.type',string=u'Operación consumo materia prima ')
	picking_type_rt = fields.Many2one('stock.picking.type',string=u'Operación consumo retazos')
	picking_type_drt = fields.Many2one('stock.picking.type',string=u'Operación devolución retazos')
	
	# Tipos de Picking para requisiciones de Lima:
	# esta mrd es una solución temporal, si hay más almacenes es necesario reestructurar 
	# étos parámetros y también el modelos de requisiciones
	
	## Almacén de prod.
	picking_type_mp = fields.Many2one('stock.picking.type',u'Operación cons. materia prima ')
	picking_type_rt = fields.Many2one('stock.picking.type',u'Operación cons. retazos')
	picking_type_drt = fields.Many2one('stock.picking.type',u'Operación devolución retazos')

	traslate_motive_mp = fields.Many2one('einvoice.catalog.12','Motivo traslado cons. materia prima ')
	traslate_motive_rt = fields.Many2one('einvoice.catalog.12','Motivo traslado cons. retazos')
	traslate_motive_drt = fields.Many2one('einvoice.catalog.12',u'Motivo traslado devolución retazos')


	#Retazos 
	entry_scraps_pt_ids  = fields.Many2many('stock.picking.type','config_pick_type_entry_scraps_rel',string=u'Operaciones ingreso de retazos')
	traslate_reason_scraps_id = fields.Many2one('einvoice.catalog.12','Motivo de tras. ing. retazos') 
	
	import_scraps_pt_ids = fields.Many2many('stock.picking.type','config_pick_type_import_scraps_rel',string=u'Operaciones importación de retazos')
	traslate_import_scraps = fields.Many2one('einvoice.catalog.12','Motivo de tras. importación retazos')

	## Almacén de Lima.
	picking_type_mp_lima = fields.Many2one('stock.picking.type',u'Operación cons. materia prima Lima')
	picking_type_rt_lima = fields.Many2one('stock.picking.type',u'Operación cons. retazos Lima')
	picking_type_drt_lima = fields.Many2one('stock.picking.type',u'Operación devolución retazos Lima')

	traslate_motive_mp_lima = fields.Many2one('einvoice.catalog.12','Motivo cons. materia prima Lima')
	traslate_motive_rt_lima = fields.Many2one('einvoice.catalog.12','Motivo cons. retazos Lima')
	traslate_motive_drt_lima = fields.Many2one('einvoice.catalog.12',u'Motivo devolución retazos Lima')


	# operaciones para picking de requisición de servicios
	# pick_type_allow_services_picks_ids = fields.Many2many('stock.picking.type', 
	# 	relation='config_pick_type_services_picks_rel',
	# 	column1='config_id', column2='pick_type_id', 
	# 	string=u'Tipos de picking permitidos para requisiciones de servicios')

	pick_type_def_serv_mp_lima = fields.Many2one('stock.picking.type', 
		string=u'Operación cons. materia prima Lima', 
		help=u'Tipo de picking por defecto para requisiciones de servicios (Lima)')
	pick_type_def_serv_mp_aqp = fields.Many2one('stock.picking.type', 
		string=u'Operación cons. materia prima Arequipa',
		help=u'Tipo de picking por defecto para requisiciones de servicios (Arequipa)')

	# Destino de consumo / desconsumo
	pick_type_consumer_dest_id = fields.Many2one('stock.picking.type', 
		string='Tipo de picking',
		help=u'Operación para Destino de consumo/desconsumo')
	partner_consumer_dest = fields.Many2one('res.partner', 
		string='Partner por def. Destino de consumo/desconsumo')
	traslate_motive_consumer_dest = fields.Many2one('einvoice.catalog.12', 
		string='Tipo de operación', 
		help=u'Mot. de tras. Destino de consumo/desconsumo')

	limit_ids=fields.One2many('glass.production.limit','config_id','Plazos de Producción')
	userstage = fields.One2many('glass.production.user.stage','config_id','Usuarios - Etapas')
	dateexceptions_ids = fields.One2many('glass.date.exception','config_id','Fechas excluidas')
	uom_categ_id = fields.Many2one('product.uom.categ', string=u'Categoría de unidad por defecto')
	nro_cristales_guia = fields.Integer(string='Nro. de Cristales por guia', default=100)
	compare_attribute = fields.Many2one('product.atributo',string=u'Atributo de Comparación')
	motive_event_send_email_ids = fields.One2many('motive.event.send.email','config_id',string='Motivos de Envio de Emails')
	requisition_materials_ids = fields.One2many('requisition.material','config_id',string='Materiales de Requisicion')
	break_stage_id = fields.Many2one('glass.stage',string='Etapa de rotura')
	default_pulido = fields.Many2one('sale.pulido.proforma',string="Pulido por defecto")
	
	location_retazo = fields.Many2one('stock.location',u'Ubicación de retazos AQP')
	location_retazo_lima = fields.Many2one('stock.location',u'Ubicación de retazos Lima')

	apt_location_id = fields.Many2one('stock.location',u'Ubicación APT')
	path_remission_guides = fields.Char('Path guías de remisión')
	
	path_remission_guides_ind = fields.Char(u'Path guías de rem. Independencia')
	path_remission_guides_pque = fields.Char(u'Path guías de rem. Pque Ind.')
	path_remission_guides_lim = fields.Char(u'Path guías de rem. Lima')

	street_picking_ind = fields.Char(u'Dirección Independencia')
	street_picking_pque = fields.Char(u'Dirección Pque Ind.')
	street_picking_lim = fields.Char(u'Dirección Lima')
	
	path_glass_order_pdf = fields.Char(string='Ruta de GlassOrder Pdf')
	path_glass_lines_pdf = fields.Char(string='Ruta de GlassOrderLine Pdf')

	partner_excluded_ids = fields.Many2many('res.partner',string=u'Partners excluídos',help=u'Partners excluídos de facturación y control de términos de pago.')

	hour_limit_op = fields.Integer(u'Hora límite producción OP', 
		default=12, help=u'Hora límite para definir como día hábil de producción')
	minute_limit_op = fields.Integer(u'Minuto límite producción OP', 
		default=30, help=u'Minuto límite para definir como día hábil de producción')

	@api.constrains('hour_limit_op', 'minute_limit_op')
	def _verify_hour_minute_limit_op(self):
		for conf in self:
			if conf.hour_limit_op and (conf.hour_limit_op < 1 or conf.hour_limit_op > 23):
				raise exceptions.ValidationError('La hora debe estar entre 1 y 23')
			if conf.minute_limit_op and (conf.minute_limit_op < 1 or conf.minute_limit_op > 59):
				raise exceptions.ValidationError('La hora debe estar entre 1 y 23')

	@api.constrains('nro_cristales_guia')
	def _verify_nro_cristales(self):
		for rec in self:
			if rec.nro_cristales_guia < 1:
				raise exceptions.ValidationError(u'Valor Incorrecto:\nEl Número de cristales por guia debe ser mayor a 0')

	@api.model
	def _get_parameter(self):
		param = self.search([], limit=1)
		if not param:
			raise UserError('No se ha encontrado un registor para los parámetros de producción')
		return param

	@api.model
	def create(self, values):
		exist = self.search([])
		if len(exist) != 0:
			raise exceptions.Warning(u'No es posible crear o duplicar parámetros de configuración si ya existe uno')
		return super(GlassOrderConfig, self).create(values)

	def execute_script_1(self):
		Uom = self.env['product.uom']
		bad_uoms = Uom.search([('name','ilike',' - R'),('category_id','=',6)])
		prods = self.env['product.product'].search([('uom_id','in',bad_uoms.ids)])
		for pr in prods:
			uo = Uom.search([('name','=',pr.uom_id.name.replace(' - R','')),('ancho','=',pr.uom_id.ancho),('alto','=',pr.uom_id.alto)])
			if uo:
				print('uom ',uo.id)
				self._cr.execute("""UPDATE product_template SET uom_id = %d WHERE id = %d;"""%(uo.id,pr.product_tmpl_id.id))
			else:
				pr.uom_id.name = pr.uom_id.name.replace(' - R','')

		prods = self.env['product.product'].search([('unidad_kardex','in',bad_uoms.ids)])
		for pr in prods:
			uo = Uom.search([('name','=',pr.unidad_kardex.name.replace(' - R','')),('ancho','=',pr.unidad_kardex.ancho),('alto','=',pr.unidad_kardex.alto)])
			if uo:
				print('uom ',uo.id)
				self._cr.execute("""UPDATE product_template SET unidad_kardex = %d WHERE id = %d;"""%(uo.id,pr.product_tmpl_id.id))
			else:
				pr.unidad_kardex.name = pr.unidad_kardex.name.replace(' - R','')

		prods = self.env['product.product'].search([('uom_po_id','in',bad_uoms.ids)])
		for pr in prods:
			uo = Uom.search([('name','=',pr.uom_po_id.name.replace(' - R','')),('ancho','=',pr.uom_po_id.ancho),('alto','=',pr.uom_po_id.alto)])
			if uo:
				print('uom ',uo.id)
				self._cr.execute("""UPDATE product_template SET uom_po_id = %d WHERE id = %d;"""%(uo.id,pr.product_tmpl_id.id))
			else:
				pr.uom_po_id.name = pr.uom_po_id.name.replace(' - R','')
		#force
		self._cr.execute("""
			UPDATE product_template SET uom_po_id = 74 WHERE id = 754;
			UPDATE product_template SET uom_id = 74 WHERE id = 754;
			UPDATE product_template SET uom_po_id = 72 WHERE id = 752;
			UPDATE product_template SET uom_id = 72 WHERE id = 752;
			""")
		# correr de nuevo
		bad_uoms.unlink()

		
		# lines = self.env['glass.pdf.file'].search([('pdf_file','!=',False)])
		# from datetime import datetime
		# import base64
		# sub_items = [lines[i:i+100] for i in range(0,len(lines),100)]
		# for array in sub_items:
		# 	for item in array:
		# 		print('line in:', str(item.id))
		# 		path = self.path_glass_order_pdf
		# 		file, pdf = None,None
		# 		if item.op_id:
		# 			name = 'order_'+item.op_id.name+'_'+ str(datetime.now()).replace(':','').replace(' ','_').replace('-','_')+'.pdf'
		# 			path += name
		# 			file = open(path,'wb')
		# 			pdf = item.op_id.sketch if item.op_id.sketch else item.pdf_file
		# 			file.write(base64.b64decode(pdf))
		# 			file.close()
		# 			item.op_id.write({'croquis_path':path})
		# 			item.write({'path_pdf':path})
		# 		else:
		# 			if not item.sale_id:
		# 				continue
		# 			name = 'order_'+item.sale_id.name+'_'+ str(datetime.now()).replace(':','').replace(' ','_').replace('-','_')+'.pdf'
		# 			path += name
		# 			file = open(path,'wb')
		# 			file.write(base64.b64decode(item.pdf_file))
		# 			file.close()
		# 			item.write({'path_pdf':path})
		# 		print('line path:', str(item.id)+item.path_pdf)
		# ordernar records ancho y alto para encontrar mejor las unidades de medida:
	@api.multi
	def execute_script_2(self):
		records = self.env['glass.scrap.move'].search([])
		for rec in records:
			if rec.width > rec.height:
				rec.width,rec.height = rec.height,rec.width
		#return
		# segunda parte
		records = self.env['glass.scrap.record'].search([])
		Uom = self.env['product.uom']
		Link = self.env['stock.move.operation.link']
		for rec in records:
			width  = rec.scrap_move_id.width
			height = rec.scrap_move_id.height
			uo = Uom.search([('ancho','=',width),('alto','=',height)])
			if len(uo) > 1:
				raise UserError('unidad duplicada '+uo[0].name)
			if not uo:
				uo = Uom.create({
					'ancho':width,
					'alto':height,
					'plancha':True,
					'name':'%dx%dmm'%(width,height),
					'rounding':0.0001,
					'category_id':6,
				})
				uo.recalculate_uom_plancha_factor()
			sql = """UPDATE stock_move SET product_uom = %d, product_uom_qty = %f where id = %d;"""%(uo.id,rec.pieces,rec.move_id.id)
			self._cr.execute(sql)
			links = Link.search([('move_id','=',rec.move_id.id)])
			links.operation_id.write({
				'product_qty': rec.pieces,
				'qty_done':rec.pieces,
				'product_uom_id':uo.id,
				})
		print('enjoy!')

	@api.multi
	def execute_script_3(self):
		reqs = self.env['glass.requisition'].search([])
		for req in reqs:
			mp = req.picking_mp_ids.mapped('move_lines').ids
			if any(mp):
				req.move_mp_ids = [(6,0,mp)]
			rt = req.picking_rt_ids.mapped('move_lines').ids
			if any(rt):
				req.move_rt_ids = [(6,0,rt)]
			drt = req.picking_drt_ids.mapped('move_lines').ids
			if any(drt):
				req.move_drt_ids = [(6,0,drt)]


	def _get_op_dates(self,categories,in_obra,destinity_order,send2partner,with_entalle):
		# TODO calcular los días de producción según parámetros
		pass


class GlassProductionLimit(models.Model):
	_name="glass.production.limit"

	config_id = fields.Many2one('glass.order.config','main_id', required=True)

	motive_limit = fields.Selection([
		('templado','Templado'),
		('laminado','Laminado'),
		('insulado','Insulado'),
		('cmetalica',u'C. Metálica'),
		('pac','PAC'),
		('crudo','Crudo'),
		('service', 'Servicios')
		],string='Aplicar en:')

	category_ids = fields.Many2many('product.category',string='Categorías',help=u'Categorías de producto que usarán ésta parámetro')

	piezas = fields.Integer(u'Reposición Piezas')
	zero_2_4_5 = fields.Integer('0-4.5m2')
	four_dot_six_2_50 = fields.Integer('4.6-50m2')
	fiftyone_2_100 = fields.Integer('51-100m2')
	onehundred1_2_200 = fields.Integer('101-200m2')
	more_2_200 = fields.Integer('+200m2')
	obras = fields.Integer('Obras')
	entalle = fields.Integer('Entalle')
	local_send = fields.Integer('Entrega AQP')
	external_send = fields.Integer('Embalaje y Transporte a Lima')
	send2partner= fields.Integer('Entrega Lima')

	_sql_contraints = [('motive_limit_uniq','UNIQUE (motive_limit, config_id)',u'La aplicación del límite debe ser única')]


	# TODO control de categorías
	# @api.constrains('category_ids', 'config_id')
	# def _check_category_ids(self):
	# 	for rec in self:
	# 		if self.search_count([('config_id', '=', rec.config_id.id), ('')])


class GlassProductionUserStage(models.Model):
	_name="glass.production.user.stage"

	user_id = fields.Many2one('res.users','Usuario')
	#stage = fields.Selection([('corte','Corte'),('pulido','Pulido'),('entalle','Entalle'),('lavado','Lavado')],'Etapa')
	#stage_id = fields.Many2one('glass.stage',string='Etapa')
	
	stage_id = fields.Many2one('glass.stage', 
		domain=[('name', 'not in', list(FORBIDDEN_STAGES))],
		string='Etapa por defecto')
	stage_ids = fields.Many2many('glass.stage', 
		relation="glass_config_user_stage_prod_control_rel", 
		domain=[('name', 'not in', list(FORBIDDEN_STAGES))], string='Etapas')

	config_id = fields.Many2one('glass.order.config','Mainid')
	order_prod = fields.Integer('Orden de Proceso')

	@api.onchange('stage_ids')
	def _onchange_stage_ids(self):
		return {
			'domain': {'stage_id': [('id', 'in', self.stage_ids.ids)]}
		}


class GlassDateException(models.Model):
	_name="glass.date.exception"

	date = fields.Date('Fecha Excluida')
	config_id = fields.Many2one('glass.order.config','Mainid')


