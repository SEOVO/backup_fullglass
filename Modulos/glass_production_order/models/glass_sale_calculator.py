# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError,ValidationError
from itertools import groupby,chain

class GlassSaleCalculator(models.Model):
	_inherit = 'glass.sale.calculator'

	is_locked = fields.Boolean('Bloqueado',compute='_compute_locked')

	@api.multi
	def write(self,values):
		if self.is_locked and self._context.get('force_write') is None:
			raise UserError(u'No es posible editar una calculadora en estado bloqueado.')
		return super(GlassSaleCalculator,self).write(values)

	def save_calculator(self):
		if self.is_locked:
			raise UserError(u'No es posible grabar una calculadora en estado bloqueado.')
		return super(GlassSaleCalculator,self).save_calculator()

	@api.constrains('line_ids')
	def _verify_line_ids(self):
		"""Verificar si hay cristales con números duplicados en la calculadora.
		   Se agrupa por orden de producción ya que si es factura adelantada un cristal
		   puede repetir el número, siempre que esté en otra orden de producción.
		   Comment by Leo de Oz
		"""
		key_func = lambda x: x.glass_order_id
		for rec in self:
			data = rec.line_ids.sorted(key=key_func)
			for key,group in groupby(data,key_func):
				crystal_nums = list(chain(*[x.get_crystal_numbers() for x in list(group)]))
				duplicates = set(i for i in crystal_nums if crystal_nums.count(i)>1)
				if duplicates:
					duplicates = ', '.join(map(str,duplicates))
					raise UserError(u'Existen cristales con números duplicados en su calculadora.\nDUPLICADOS: %s.'%duplicates)

	@api.depends('order_id.state','line_ids.glass_order_id','order_id.before_invoice','qty_invoiced_rest')
	def _compute_locked(self):
		for rec in self:
			op_ids = rec._get_glass_order_ids()
			if rec.order_id.state=='draft' and not op_ids:
				rec.is_locked = False
			else:
				if rec.order_id.before_invoice:
					if not rec._get_lines_to_produce() and rec.qty_invoiced_rest <= 0:
						rec.is_locked = True
					else:
						rec.is_locked = False
				else:
					rec.is_locked = True

	def _get_glass_order_ids(self):
		"""Método para heredar en la calculadora de insulados"""
		self.ensure_one()
		return self.line_ids.mapped('glass_order_id')

	def _get_dict_crystals(self, extra_domain=[], only_exists=False, limit=False):
		"""Método para heredar en la calculadora de insulados"""
		# return: dict {'calc_line_id':{'crystal_number':[lot_line_ids]}}
		self.ensure_one()
		LotLine = self.env['glass.lot.line'].sudo()
		lines = self.line_ids
		values = dict.fromkeys(lines.ids)

		data = LotLine.search_read([('calc_line_id','in', lines.ids)] + extra_domain, ['calc_line_id','id', 'nro_cristal'], limit=limit)
		dict_lines = dict([('%s_%s' % (l['calc_line_id'][0], l['nro_cristal']), [l['id']]) for l in data])

		for line in lines:
			sub_dict = {}
			for num in line.get_crystal_numbers():
				lot_line_ids = dict_lines.get('%d_%d'%(line.id,num),[])
				if only_exists and not any(lot_line_ids): continue
				sub_dict[str(num)] = lot_line_ids
			values[line.id] = sub_dict
		return values

	def _get_lines_to_produce(self):
		"""Obtener las líneas de calculadora para generar O.P.
		método heredable para agregar lógica adicional, por defecto retornamos las líneas de calculadora
		que no tienen 'glass_order_id'"""
		self.ensure_one()
		return self.line_ids.filtered(lambda l: not l.glass_order_id)


class GlassSaleCalculatorLine(models.Model):
	_inherit = 'glass.sale.calculator.line'

	glass_order_id = fields.Many2one('glass.order',string=u'Órden de producción')
	# crystal stages
	lavado = fields.Boolean('Lavado') # TODO remover Esta huevada quedó de la versión anterior, seguirá??'
	arenado = fields.Boolean('Arenado')# OLD field, usar arenado_id en su lugar, se mantiene por motivos de compatibilidad

	#biselado = fields.Integer('Biselados') # Nro de biselados

	def _get_ref_order_id(self):
		"""IMPORTANTE: éste método devuelve la OP de la línea de calculadora,NO se debe usar 
		en procesos que no sean para fines de reportes u operaciones referenciales.
		-> método destinado herencia"""
		self.ensure_one()
		return self.glass_order_id

	@api.model
	def default_get(self,default_fields):
		res = super(GlassSaleCalculatorLine,self).default_get(default_fields)
		default_pulido = self.env['glass.order.config'].search([],limit=1).default_pulido
		if default_pulido:
			res.update({'polished_id': default_pulido.id})
		return res

	def unlink(self):
		if self.filtered('glass_order_id'):
			raise UserError(u'No es posible eliminar una línea de calculadora cuando tiene un OP asociada')
		return super(GlassSaleCalculatorLine,self).unlink()
