# -*- coding: utf-8 -*-
from odoo import fields, models,api
from odoo.exceptions import UserError
from datetime import datetime

class StockPicking(models.Model):
	_inherit = 'stock.picking'

	sale_picking = fields.Boolean() # # para checar que la etrega es por cristales
	prod_picking = fields.Boolean(u'Albarán de producción',compute='_compute_prod_picking')
	driver_delivery = fields.Char('Conductor')
	apt_in = fields.Boolean('Apt') # si es albaran de ingreso a apt XDXD
	consumer_destination = fields.Char('Destino de consumo/desconsumo')
	sale_services_id = fields.Many2one('sale.order', string=u'Orden de venta Servicios')

	def action_assign(self):
		# Prevenir la reserva y entrega de albaranes de ventas, si no son hechas usando el wizard de entrega de cristales
		if self.filtered('prod_picking') and not self._context.get('glass_force_assign'):
			raise UserError(u'No es posible reservar un albarán de entrega manual de cristales')
		else:
			return super(StockPicking, self).action_assign()

	def _compute_prod_picking(self):
		# checar si el albarán entrega stock por cristales
		for pick in self:
			calcs = pick.move_lines.mapped('procurement_id.sale_line_id.calculator_id')
			#FIXME si tiene calculadoras diferente de services (insulados y comunes, si hay otra reparar)
			pick.prod_picking = bool(calcs.filtered(lambda c: c.type_calculator!='service' and c.line_ids))

	@api.model
	def action_picking_consumer_dest(self):
		conf = self.env['glass.order.config']._get_parameter()
		context = dict(self._context or {})
		context.update({
			'default_partner_id': conf.partner_consumer_dest.id,
			'default_picking_type_id': conf.pick_type_consumer_dest_id.id,
			'default_einvoice_12': conf.traslate_motive_consumer_dest.id,
			'default_fecha_kardex': fields.Date.today(),
		})
		return {
			'name':'Albarán de consumo/desconsumo',
			'type': 'ir.actions.act_window',
			'res_model': self._name,
			'view_mode': 'form',
			'view_id': self.env.ref('stock.view_picking_form').id,
			'context': context,
		}

	def delivery_crystals(self):
		self.ensure_one()
		sale_order = self.move_lines.mapped('procurement_id.sale_line_id.order_id')
		sale_order.validate_sale_pay_terms('delivery_products')
		ops = sale_order.mapped('op_ids').filtered(lambda o: o.state != 'returned')
		
		wiz = self.env['glass.delivery.picking.crystals'].create({
			'picking_id':self.id,
			'glass_order_ids':[(6, 0, ops.ids)],
		})

		if ops: 
			wiz.get_crystals_list()
		return {
			'name':'Entrega de cristales',
			'res_id':wiz.id,
			'type': 'ir.actions.act_window',
			'res_model': wiz._name,
			'view_id': self.env.ref('glass_production_order.glass_delivery_picking_crystals_view_form').id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': dict(self._context or {}, ops_domain=[('id', 'in', ops.ids)]),
			}

	# esta mrd del reopen debería quitarse...
	def action_revert_done(self):
		for pick in self:
			if pick.move_lines.mapped('glass_order_line_ids'):
				raise UserError(u'Se ha encontrado que el albarán %s contiene cristales en su demanda inicial, utilice la devolución por cristales para este tipo de extornos.' % pick.name)

			if any((m.requisition_mp_id or m.requisition_rt_id or m.requisition_drt_id or m.requisition_srv_raw_id) for m in pick.move_lines):
				raise UserError(u"""La devolución de albaranes de asociados a una órden de requisición 
									deben devolverse desde la misma órden.""")
		return super(StockPicking, self).action_revert_done()
	
	@api.multi
	def do_new_transfer(self):
		if self.filtered('prod_picking') and not self._context.get('delivery_transfer'):
			raise UserError(u'No es posible validar directamente un albarán de entrega manual de cristales')
		return super(StockPicking,self).do_new_transfer()
