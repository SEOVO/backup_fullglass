# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
import time
from StringIO import StringIO
import base64,os,tempfile,sys,random
from PyPDF2 import PdfFileWriter, PdfFileReader
from uuid import uuid1

class GlassOrder(models.Model):
	_name = 'glass.order'
	_inherit = ['mail.thread']
	_order = "date_order desc"

	name = fields.Char(u'Orden de producción', default='/', readonly=True, index=True)
	sale_order_id = fields.Many2one('sale.order', string='Pedido de venta', readonly=True)
	invoice_id = fields.Many2one('account.invoice','Documento', readonly=True)
	partner_id = fields.Many2one('res.partner', 'Cliente', related="sale_order_id.partner_id", readonly=True, store=True)
	delivery_department = fields.Char(u'Departamento', related="sale_order_id.partner_shipping_id.state_id.name", store=True)
	delivery_province = fields.Char(u'Provincia', related="sale_order_id.partner_shipping_id.province_id.name", store=True)
	delivery_street = fields.Char(u'Dirección Entrega', related="sale_order_id.partner_shipping_id.street", store=True)
	date_sale_order = fields.Datetime('Fecha de pedido de venta',related='sale_order_id.date_order')
	type_sale = fields.Selection(related="sale_order_id.type_sale", string="Tipo de venta", store=True)
	comercial_area = fields.Selection([
		('distribucion',u'Distribución'),
		('obra','Obra'),
		('proyecto','Proyecto')], string=u'Área Comercial')
	obra = fields.Char('Obra')
	date_order = fields.Datetime(u'Fecha Emisión', required=True, readonly=True)
	date_production = fields.Date(u'Fecha de Producción', track_visibility='onchange')
	date_send = fields.Date(u'Fecha de Despacho', track_visibility='onchange')
	date_delivery = fields.Date(u'Fecha de Entrega', track_visibility='onchange')
	warehouse_id = fields.Many2one('stock.warehouse', related='sale_order_id.warehouse_id', 
		string=u'Almacén Despacho', store=True, readonly=True)
	seller_id = fields.Many2one('res.users', 
		string='Vendedor', related='sale_order_id.user_id', store=True, readonly=True)
	corrected = fields.Boolean('Corregido')
	observ = fields.Text('Observaciones')
	state = fields.Selection([
		('draft','Generada'),
		('confirmed','Emitida'),
		('process','En Proceso'),
		('ended','Finalizada'),
		('delivered','Despachada'),
		('returned','Devuelta'),], string='Estado', default='draft', index=True, track_visibility='onchange')
	sale_lines = fields.One2many(related='sale_order_id.order_line')
	line_ids = fields.One2many('glass.order.line', 'order_id', string=u'Líneas a producir')
	total_area = fields.Float(u'Metros',compute="_gettotals",digits=(20,4))
	total_peso = fields.Float("Peso",compute="_gettotals",digits=(20,4))
	total_pzs = fields.Float("Total Pzs",compute="_gettotals")
	#sketch = fields.Binary('Croquis')
	croquis_path = fields.Char(string='Ruta de Croquis')
	file_name = fields.Char("File Name")
	reference_order = fields.Char(related='sale_order_id.reference_order',store=True)
	editable_croquis = fields.Boolean('editar croquis',default=True)
	invoice_count = fields.Integer(string='# of Invoices', related='sale_order_id.invoice_count', readonly=True)
	invoice_ids = fields.Many2many("account.invoice", string='Invoices', 
		related="sale_order_id.invoice_ids", readonly=True,track_visibility='onchange')
	invoice_status = fields.Selection(related='sale_order_id.invoice_status',string=u'Estado de Facturación', store=True,readonly=True)
	destinity_order = fields.Selection([
		('local','En la ciudad'),
		('external','En otra ciudad')],u'Lugar de entrega',default="local",readonly=True)
	send2partner=fields.Boolean(u'Entregar en ubicación del cliente',default=False,readonly=True)
	in_obra = fields.Boolean('Entregar en Obra',readonly=True)

	@api.multi
	def unlink(self):
		for order in self:
			if order.state in ('draft',):
				super(GlassOrder,self).unlink()
			else:
				raise UserError(u'No se puede eliminar una Orden de Producción en los estados: Emitida, En proceso, Finalizada o Despachada')

	@api.one
	def optimizar(self):
		self.state="process"
		return True

	@api.one
	def save_pdf(self):
		self.editable_croquis=False
		return True	

	def _refresh_state(self):
		for order in self.filtered(lambda o: o.state!='returned'):
			lines = order.line_ids.filtered(lambda l: l.state!='cancelled')
			if all(l.state=='instock' for l in lines):
				order.state = 'ended'
			elif all(l.state=='send2partner' for l in lines):
				order.state = 'delivered'
			elif any(l.lot_line_id for l in lines):
				order.state = 'process'
			elif all(not l.lot_line_id for l in lines):
				order.state = 'confirmed'

	@api.multi
	def action_view_invoice(self):
		invoices = self.mapped('invoice_ids')
		action = self.env.ref('account.action_invoice_tree1').read()[0]
		if len(invoices) > 1:
			action['domain'] = [('id', 'in', invoices.ids)]
		elif len(invoices) == 1:
			action['views'] = [(self.env.ref('account.invoice_form').id, 'form')]
			action['res_id'] = invoices.ids[0]
		else:
			action = {'type': 'ir.actions.act_window_close'}
		return action

	def remove_order(self):
		self.ensure_one()
		module = __name__.split('addons.')[1].split('.')[0]
		wizard = self.env['glass.remove.order'].create({'order_id':self.id})
		return {
			'name': u'Devolver Órden de Producción',
			'res_id':wizard.id,
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': wizard._name,
			'view_id': self.env.ref('%s.glass_remove_order_form_view'%module).id,
			'type': 'ir.actions.act_window',
			'target': 'new',
		}

	@api.one
	def _gettotals(self):
		ta=tp=n=0
		lines = self.line_ids.filtered(lambda x: x.state != 'cancelled')
		if len(lines) > 0:
			ta = sum(lines.mapped('area'))
			tp = sum(lines.mapped('peso'))
		self.total_area = ta
		self.total_peso = tp
		self.total_pzs  = len(lines)

	def view_change_sketch_wizard(self):
		self.ensure_one()
		vals = {}
		wizard = self.env['add.sketch.file']
		if self.croquis_path:
			try:
				with open(self.croquis_path, "rb") as pdf_file:
					vals = {'sketch': pdf_file.read().encode("base64")}
			except TypeError as e:
				vals = {'message': 'Archivo Croquis removido o no encontrado!',}
			except IOError as e:
				vals = {'message': 'Archivo Croquis removido o no encontrado!',}
		else:
			vals = {'message': 'No se ha cargado un archivo croquis',}
		
		wizard = wizard.create(vals)

		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.change_sketch_file_view_form' % module)
		return {
			'name':'Ver Croquis',
			'res_id':wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': 'add.sketch.file',
			'view_id':view.id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context':{'glass_order_id': self.id}
		}

	def show_sketch(self):
		self.ensure_one()
		if not self.croquis_path:
			raise UserError('La OP %s no tiene cargado un fichero de croquis, puede adjuntarlo desde el botón "Modificar Croquis"' % self.name)
		# NOTE al haber frecuentes conflictos con ngrok, se optó por colocar la base URL estáticamente.
		# Tener en cuenta que si cambian a un dominio u otra dirección IP, no podrán visualizar los croquis.
		# Lo más adecuado será entonces usar el parámetro de sistema web.base.url de nuevo.
		#base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		base_url = 'http://192.168.12.177:8069'
		path = self.env['main.parameter'].search([],limit=1).download_directory
		file_name = 'sketch_op.pdf'
		path += file_name
		try:
			with open(self.croquis_path,"rb") as pdf_file:
				with open(path,'wb') as file_new:
					file_new.write(pdf_file.read())
			random_key = '%d%d' % (random.randrange(1, 100000), int(time.time()))
			url = '%s/import_base_it/static/%s?t=%s' % (base_url, file_name, random_key)
		except TypeError as e:
			raise UserError(u'No se encontró la ruta del PDF')
		except IOError as e:
			raise UserError(u'No se encontró el archivo PDF o éste fue removido')
		return {
			"type": "ir.actions.act_url",
			"url": url,
			"target": "new",
		}

	def _set_order_dates(self,force_prod_date=False):
		"""Recomputar fechas de OP
		@Param force_prod_date : string-date string en formato de fecha
		"""
		conf = self.env['glass.order.config'].search([],limit=1)
		date_exceptions = conf.dateexceptions_ids.mapped('date')

		dt = fields.Datetime

		lim_hour = (conf.hour_limit_op or 12), (conf.minute_limit_op or 30)
		
		def validate_date(date): # valida si la fecha no es hábil : Método recursivo
			# Si domingo, se van al lunes
			if date.weekday() == 6: date += timedelta(days=1)
			# validar si cae en una fecha prohibida
			if str(date)[:10] in date_exceptions:
				date += timedelta(days=1)
				return validate_date(date)
			else:
				return date

		def get_date(start_date, days):
			end_date = start_date
			if days <= 0:
				return end_date
			while days > 0:
				end_date = validate_date(end_date + timedelta(days=1))
				days -= 1
			return end_date

		#get_date_tz = lambda self,date: dt.context_timestamp(self, dt.from_string(date))

		for order in self:
			# NOTE para determinar qué parámetro de fechas usar, nos basamos en la categoría 
			# del primer cristal que encuentre, puede no ser lo más correcto ,pero ya que
			if not order.line_ids:
				continue

			#categ_ids = set(order.line_ids.mapped('product_id.categ_id').ids)
			# get main product
			# set product_ categs:
			categ_ids = set()
			for gol in order.line_ids:
				# TODO FIXME mejorar para permitir herencia, obtener el producto original
				from_insulado = getattr(gol.calc_line_id, 'from_insulado', False) # módulo de plantillas está instalado
				if from_insulado:
					categ_ids.add(gol.mtf_template_id.product_id.categ_id.id)
				else:
					categ_ids.add(gol.product_id.categ_id.id)

			lim_config = False
			
			for cl in conf.limit_ids:
				if categ_ids & set(cl.category_ids.ids): # Con el primer idiota que se cruce por el camino...
					lim_config = cl
					break

			if not lim_config:
				raise UserError(u"No se ha encontrado la configuración de plazos de producción adecuado para la OP %s." % order.name)

			days_prod = days_send = 0 # days for production
			# FIXME por ahora le resto la diferencia horaria nomás, mejorar...
			date_order = dt.from_string(order.date_order) - timedelta(hours=5)

			if force_prod_date:
				force_prod_date = dt.from_string(force_prod_date).date()
				if force_prod_date < date_order.date():
					raise UserError(u'La fecha de reprogramación debe ser superior a la fecha de emisión')
				dateprod = validate_date(force_prod_date)
			else:
				area = order.total_area
				with_entalle = any(l.entalle for l in order.line_ids)
				if area <= 4.5: days_prod = lim_config.zero_2_4_5
				if area < 51 and area > 4.5: days_prod = lim_config.four_dot_six_2_50
				if area < 101 and area > 50: days_prod = lim_config.fiftyone_2_100
				if area < 201 and area > 101: days_prod = lim_config.onehundred1_2_200
				if area > 200: days_prod = lim_config.more_2_200
				if order.in_obra: days_prod = days_prod + lim_config.obras	
				if with_entalle: days_prod = days_prod + lim_config.entalle
				dateprod = date_order.date()

				if (date_order.hour < lim_hour[0]) or (date_order.hour == lim_hour[0] and date_order.minute < lim_hour[1]):
					days_prod -= 1

				dateprod = get_date(dateprod, days_prod)
			
			if order.destinity_order == 'local':
				days_send = days_send + lim_config.local_send
			if order.destinity_order == 'external':
				days_send = days_send + lim_config.external_send
			
			datesend = get_date(dateprod, days_send)
			datedeli = datesend if order.destinity_order == 'local' else get_date(datesend, lim_config.send2partner)
			order.write({
				'date_production': dateprod,
				'date_send': datesend,
				'date_delivery': datedeli,
				})

	@api.model
	def create(self,values):
		order = super(GlassOrder,self).create(values)
		order.with_context(mail_notrack=True)._set_order_dates()
		return order

class GlassOrderLine(models.Model):
	_name = 'glass.order.line'
	_order = 'date_production,order_id,crystal_number_fl'
	
	order_id = fields.Many2one('glass.order', string=u'Órd. de prod.', 
			required=True, readonly=True ,index=True)
	product_id = fields.Many2one('product.product', string='Producto', readonly=True)

	@api.depends('crystal_number')
	def _compute_crystal_number_fl(self):
		for line in self:
			line.crystal_number_fl = float(line.crystal_number)

	crystal_number = fields.Char('Nro. Cristal', readonly=True, index=True)
	crystal_number_fl = fields.Float(string='Nro de Cristal',
		compute='_compute_crystal_number_fl', digits=(3,1), store=True) # para ordenamiento y optima, no considerar para otros casos.
	calc_line_id = fields.Many2one('glass.sale.calculator.line', copy=False, readonly=True, index=True)
	base1 = fields.Integer("Base1 (L 4)",related="calc_line_id.base1",readonly=True)
	base2 = fields.Integer("Base2 (L 2)",related="calc_line_id.base2",readonly=True)
	altura1 = fields.Integer("Altura1 (L 1)",related="calc_line_id.height1",readonly=True)
	altura2 = fields.Integer("Altura2 (L 3)",related="calc_line_id.height2",readonly=True)

	area = fields.Float(u"Área M2",readonly=True,digits=(20,4))
	
	descuadre = fields.Char("Descuadre",size=7,related="calc_line_id.descuadre",readonly=True)
	polished_id = fields.Many2one(related="calc_line_id.polished_id",string="Pulido",readonly=True)
	#arenado_id = fields.Many2one(related="calc_line_id.arenado_id", string='Arenado', readonly=True)
	#biselado_id = fields.Many2one(related="calc_line_id.biselado_id", string='Arenado', readonly=True)
	entalle = fields.Integer("Entalle",related="calc_line_id.entalle",readonly=True)
	plantilla = fields.Boolean("Plantilla",related="calc_line_id.template",readonly=True)
	page_number = fields.Integer(u"Nro. Pág.",related="calc_line_id.page_number",readonly=True)
	embalado = fields.Boolean("Embalado",related="calc_line_id.packed",readonly=True)
	image = fields.Binary("Embalado",related="calc_line_id.image",readonly=True)
	measures = fields.Char('Medidas',related='calc_line_id.measures',store=True, readonly=True)
	is_service = fields.Boolean(string='Es servicio', readonly=True, default=False)
	glass_break = fields.Boolean('Roto')
	glass_repo = fields.Boolean(u'Reposición') # ???
	stock_move_ids = fields.Many2many('stock.move',
		relation='glass_order_line_stock_move_rel',
		column1='glass_order_line_id', column2='stock_move_id',
		string='Stock Moves', copy=False)
	search_code = fields.Char(u'Código de búsqueda',related="lot_line_id.search_code")
	peso = fields.Float('Peso',digits=(20,4))
	lot_id = fields.Many2one('glass.lot','Lote')
	lot_line_id = fields.Many2one('glass.lot.line', string=u'Línea de Lote', index=True)
	pending_stages_string = fields.Char(related='lot_line_id.pending_stages_string', 
		readonly=True)
	last_lot_line = fields.Many2one('glass.lot.line',u'Última línea de Lote')
	is_used = fields.Boolean('Usado')
	image_page_number = fields.Char(u'Dirección')
	partner_id = fields.Many2one('res.partner', 'Cliente', related="order_id.partner_id",readonly=True)
	date_production = fields.Date('F. Produc.', related="order_id.date_production")
	warehouse_id = fields.Many2one('stock.warehouse', related='order_id.warehouse_id', 
		string=u'Almacén Despacho', store=True, readonly=True)
	seller_id = fields.Many2one('res.users', 
		string='Vendedor', related='order_id.seller_id', store=True, readonly=True)
	state = fields.Selection([
		('process','En Proceso'),
		('ended','Producido'),
		('instock','Ingresado'),
		('send2partner','Entregado'),
		('cancelled','Anulado')],'Estado', index=True, default='process')
	#image_page = fields.Binary('image pdf')
	retired_user = fields.Many2one('res.users',string='Retirado por')
	retired_date = fields.Date('Fecha de retiro')
	reference_order  =  fields.Char('Referencia OP', related='order_id.reference_order')
	in_packing_list = fields.Boolean('Packing List')

	#locacion temporal como auxiliar para agregar un location a locations
	location_tmp = fields.Many2one('custom.glass.location',string='Ubicacion') 
	# modelo a consultar
	locations =  fields.Many2many('custom.glass.location','glass_line_custom_location_rel','glass_line_id','custom_location_id',string='Ubicaciones')
	

	def _build_pdf_page(self):
		"""Obtener página del pdf de la op en función al la página de calculadora"""
		
		path_lines = self.env['glass.order.config'].search([],limit=1).path_glass_lines_pdf
		if not path_lines:
			raise UserError(u'No se ha encontrado la ruta de generación de PDF para líneas de OP')
		if not os.path.exists(path_lines):
			raise UserError(u'La ruta %s no es una ruta de sistema operativo válida'%path_lines)
		
		orders = self.mapped('order_id')
		for order in orders:
			if not order.croquis_path:
				continue
			
			lines = self.filtered(lambda l:l.order_id==order)
			dict_paths = {}
			
			try:
				file = open(order.croquis_path,"rb")
				opened_pdf = PdfFileReader(file)
			except IOError as e:
				raise UserError(u'¡Archivo PDF removido,no encontrado o sin permisos de acceso!')
			for line in lines:
				p_number = line.page_number
				if p_number and p_number not in dict_paths.keys(): # all should have page_number???
					try:
						# build pdf page from op pdf file
						output = PdfFileWriter()
						output.addPage(opened_pdf.getPage(p_number-1))
						# name format = op_name + page_num + uuid() code
						pdf_name = '%s_%d_%s.pdf'%(order.name,p_number,str(uuid1()))
						pdf_path = path_lines+pdf_name
						with open(pdf_path,"wb") as output_pdf:
							output.write(output_pdf)
						dict_paths[p_number] = pdf_path
					except IOError as e:
						file.close()
						raise UserError(u'¡Archivo PDF para generar OP no encontrado!')
					except Exception as e:
						file.close()
						raise UserError(u'Ocurrió un error al generar los ficheros PDF, es posible que no haya asignado correctamente los números de página en su(s) calculadora(s)')
				# try remove old pdf:
				if line.image_page_number and os.path.exists(line.image_page_number):
					os.remove(line.image_page_number)
				line.image_page_number = dict_paths.get(p_number,False)

	def remove_line(self):
		if self.is_used or self.lot_line_id:
			raise UserError(u'No se puede retirar este cristal.\nEl cristal ya se encuentran en los lotes de producción.')
		self.write({
			'state':'cancelled',
			'retired_user':self.env.uid,
			'retired_date':datetime.now().date(),
			'locations':False,
		})

	#def break_crystal(self,stage,motive,note=None,validate_stage=True):
	def break_crystal(self, stage, break_dict_info, validate_stage=True):
		"""Registro de rotura de cristales 
		Param: stage str o instancia de glass.stage -> Ultima etapa exitosa del cristal
		Param: validate_stage: bool -> verificar si el cristal registró la última etapa exitosa
		Param: break_dict_info: str -> Dict con la info de la rotura, formato {line_id: {'motive':'..','note','..'}}
		"""
		StageObj = self.env['glass.stage']
		stage = stage if isinstance(stage,StageObj.__class__) else StageObj.search([('name','=',stage)])
		apt_returned = self._context.get('apt_returned',False)
		if not stage:
			raise UserError('Etapa de rotura no válida')
		msg = u'El usuario %s ha registrado la rotura de los siguientes cristales:</br><ul>'%(self.env.user.name)
		for line in self:
			lot_line = line.lot_line_id
			if not lot_line:
				raise UserError(u'No es posible registrar rotura para un cristal que aún no tiene lote de producción')
			# Si ya fué entregado a cliente, ya fuentes...
			if lot_line.entregado:
				raise UserError(u'No es posible registrar rotura para un cristal que ya fué entregado al cliente.')
			# Si los servicios marcan el ingreso al momento d edespachar no habría problema con ésto
			if lot_line.ingresado and not apt_returned:
				raise  UserError(u'El cristal %s registra un ingreso a ATP, debe ser devuelto y registrar su rotura desde dicha ubicación'%line.search_code)
			if validate_stage:
				record = lot_line.stage_ids.filtered(lambda s: s.stage_id.id==stage.id and s.done)
				if not record:
					raise UserError(u'El cristal a no ha pasado por la etapa seleccionada (%s)!\nSeleccione la última etapa exitosa del cristal.'%stage.name)
			line.write({
				'last_lot_line':lot_line.id,
				'glass_break':True,
				'state':'process',
				'lot_line_id':False,
				'is_used':False,})
			motive = break_dict_info[line.id]['motive']
			note = break_dict_info[line.id].get('note','')
			break_info = {
				'break_motive': motive,
				'break_stage_id': stage.id,
				'break_note': note,
			}
			msg += u"""<li>
				<strong>Órd. Prod./Serv.:</strong>&nbsp;%s&nbsp;
				<strong>Lote:</strong>&nbsp;%s&nbsp;
				<strong>Nro. de cristal:</strong>&nbsp;%s&nbsp;
				<strong>Motivo:</strong>&nbsp;%s&nbsp;
				<strong>Nota:</strong>&nbsp;%s&nbsp;
				</li>"""%(line.order_id.name,lot_line.lot_id.name,line.crystal_number,motive,note)

			lot_line.with_context(force_register=True).register_stage('rotura',break_info)
		
		sender = self.env['send.email.event'].create({
			'subject': u'Rotura de cristales',
			'message': msg + '</ul>',
		})
		sender.send_emails(motive='break_crystal') 

	def showimg(self):
		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.view_glass_order_line_image' % module)
		data = {
			'name': _('Imagen'),
			'context': self._context,
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'glass.order.line',
			'view_id': view.id,
			'res_id':self.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
		}
		return data

	@api.multi
	def write(self, values):
		t = super(GlassOrderLine,self).write(values)
		if 'state' in values and values['state'] != 'cancelled':
			self.refresh()
			self.mapped('order_id')._refresh_state()
		return t

	@api.model
	def create(self, values):
		product = self.env['product.product'].browse(values['product_id'])
		values['peso'] = product.weight * values['area']
		return super(GlassOrderLine,self).create(values)

	@api.multi
	def unlink(self):
		for rec in self:
			if rec.image_page_number and os.path.exists(rec.image_page_number):
				os.remove(rec.image_page_number)
		return super(GlassOrderLine,self).unlink()
