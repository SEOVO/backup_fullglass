# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class GlassStageRecord(models.Model):
	_name='glass.stage.record'
	_description = 'Registro de etapa'

	user_id = fields.Many2one('res.users',u'Usuario responsable',required=True)
	stage_id = fields.Many2one('glass.stage',string=u'Etapa', required=True, ondelete='restrict', index=True)
	name = fields.Char('Etapa', related='stage_id.name', store=True, readonly=True, index=True)
	lot_line_id  =  fields.Many2one('glass.lot.line',u'Línea de lote',required=True,ondelete='cascade', index=True)
	date = fields.Datetime('Fecha y hora de registro',required=True, readonly=True)
	done = fields.Boolean('Realizado', default=False, readonly=True)

	# solo para registrar detalles en caso sea rotura
	break_motive = fields.Char('Motivo de rotura')
	break_stage_id = fields.Many2one('glass.stage',string=u'Etapa de rotura')
	break_note   = fields.Text(u'Observación de Rotura')

	def name_get(self):
		result = []
		for rec in self:
			done_str = 'Realizado' if rec.done else 'NO Realizado'
			name = '%s (%s)' % (rec.name.upper(), done_str)
			result.append((rec.id, name))
		return result

	def unlink(self):
		res = super(GlassStageRecord, self).unlink()
		self.mapped('lot_line_id')._compute_checked_stages()
		return res

	def write(self, vals):
		res = super(GlassStageRecord, self).write(vals)
		if 'done' in vals and not vals['done']:
			self.mapped('lot_line_id')._compute_checked_stages()
		return res
