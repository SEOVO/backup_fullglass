# -*- coding: utf-8 -*-
from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
from odoo.addons.glass_production_order.models.sale_order import TYPE_ORDERS

BREAK_STAGES = ('corte', 'pulido', 'entalle', 'lavado', 'templado', 'horno')

class GlassListMainWizard(models.TransientModel):
	_name='glass.list.main.wizard'
	_rec_name = 'titulo'
	titulo = fields.Char(default=u'Seguimiento de Producción')
	order_id = fields.Many2one('glass.order', string=u'Órden de Prod.', 
		domain="[('type_sale', '=', type_orders), ('state','!=','returned')]")
	lote_id = fields.Many2one('glass.lot','Lote', 
		domain="[('state','=','done'), ('type_lot', '=', type_orders)]")
	requisition_id = fields.Many2one('glass.requisition',string="Mesa")
	line_ids = fields.One2many('glass.list.wizard','main_id','Lineas')
	filter_field = fields.Selection([
		('all','Todos'),
		('pending','Pendientes'),
		('produced','Producidos'),
		('to inter','Por ingresar'),
		('to deliver','Por Entregar'),
		('expired','Vencidos')],string='Filtro',default='all')
	search_param = fields.Selection([
		('glass_order', u'Orden de Producción'),
		('lot','Lote'),
		('requisition', u'Mesa/Requisición')], default='glass_order',string=u'Búsqueda por')

	type_orders = fields.Selection(TYPE_ORDERS, string=u'Tipo de órdenes', required=True, default='production')
	show_breaks = fields.Boolean('Mostrar Rotos', default=True)
	count_total_crystals = fields.Integer('Nro cristales', compute='_get_count_crystals')
	total_area_breaks = fields.Float('Total Rotos M2', compute='_get_total_area_breaks', digits=(20, 4))
	count_total_breaks = fields.Integer('Nro Rotos', compute='_get_count_breaks')
	percentage_breaks = fields.Float('Porcentage de rotos', compute='_get_percentage_breaks', digits=(20, 4)) 
	total_area = fields.Float('Total M2',compute='_get_total_area', digits=(20,4))
	tot_optimizado = fields.Integer('Optimizado')
	tot_corte = fields.Integer('Corte')
	tot_pulido = fields.Integer('Pulido')
	tot_entalle = fields.Integer('Entalle')
	tot_lavado = fields.Integer('Lavado')
	tot_horno = fields.Integer('Horno')
	tot_templado = fields.Integer('Templado')
	tot_insulado = fields.Integer('Insulado')
	tot_comprado = fields.Integer('Comprado')
	tot_ingresado = fields.Integer('Ingresado')
	tot_entregado = fields.Integer('Entregado')
	tot_requisicion = fields.Integer(u'Requisición')
	tot_arenado = fields.Integer(u'Arenado')
	tot_biselado = fields.Integer(u'Biselado')
	tot_perforacion = fields.Integer(u'Perforación')
	tot_corte_laminado = fields.Integer('Corte laminado')
	tot_corte_lavado_laminado = fields.Integer('Corte lavado laminado')
	tot_perforacion = fields.Integer('Perforación')
	tot_pegado = fields.Integer('Pegado')
	tot_biselado = fields.Integer('Biselado')

	@api.depends('line_ids','total_area_breaks')
	def _get_total_area(self):
		for record in self:
			record.total_area = sum(record.line_ids.mapped('area'))-record.total_area_breaks

	@api.depends('line_ids','count_total_breaks')
	def _get_count_crystals(self):
		for record in self:
			record.count_total_crystals = len(record.line_ids) - record.count_total_breaks

	@api.depends('line_ids')
	def _get_total_area_breaks(self):
		for record in self:
			line_ids = record.line_ids.mapped('lot_line_id').filtered(lambda x: x.is_break)
			record.total_area_breaks = sum(line_ids.mapped('area'))

	@api.depends('line_ids')
	def _get_count_breaks(self):
		for record in self:
			record.count_total_breaks = len(record.line_ids.filtered(lambda x: x.glass_break))

	@api.depends('total_area','total_area_breaks')
	def _get_percentage_breaks(self):
		for record in self:
			if record.total_area > 0:
				record.percentage_breaks=(record.total_area_breaks/record.total_area)*100
			else:
				record.percentage_breaks=0
					
	def build_list(self):
		self.ensure_one()
		self.line_ids = [(5,)]
		line_values, orders = [],[]
		lines_without_lot=[] # lineas sin lote de produccion

		if self.search_param == 'requisition' and self.type_orders != 'production':
			raise UserError('La búsqueda por mesa/requisición no está disponible para los servicios.')

		if self.requisition_id and self.search_param == 'requisition':
			lot_lines = self.requisition_id.with_context(active_test=False).mapped('glass_lot_ids.line_ids')
			orders = self._get_data(lot_lines)

		elif self.order_id and self.search_param == 'glass_order':
			lineas = self.order_id.line_ids
			lot_lines=(lineas.with_context(active_test=False).filtered('lot_line_id')).mapped('lot_line_id')
			orders = self._get_data(lot_lines)
			lines_without_lot=lineas.filtered(lambda x: not x.lot_line_id and x.state != 'cancelled')
			# en este caso sacamos los cristales rotos manualmente:
			if self.show_breaks:
				glass_breaks = self.env['glass.lot.line'].with_context(active_test=False).search([('order_line_id','in',lineas.ids),('is_break','=',True)])
				orders += glass_breaks

		elif self.lote_id and self.search_param == 'lot':
			orders = self._get_data(self.lote_id.with_context(active_test=False).line_ids)
		
		if len(orders)==0 and len(lines_without_lot)==0:
			raise exceptions.Warning(u'No se ha encontrado información.')

		if len(lines_without_lot) > 0 and self.filter_field in ('all', 'pending'):
			for line in lines_without_lot:
				line_values.append((0, 0, {
					'order_line': line.id,
					'decorator': 'without_lot',
					'order_name':line.order_id.name,
					'crystal_number_fl':line.crystal_number_fl,
					}))
		for line in orders:
			line_values.append((0, 0, {
					'decorator': 'break' if line.is_break else 'default',
					'order_line':line.order_line_id.id,
					'lot_line_id':line.id,
					'order_name':line.order_prod_id.name,
					'crystal_number_fl':line.nro_cristal_fl,
					}))
		line_values = sorted(line_values,key=lambda x: (x[2]['order_name'],x[2]['crystal_number_fl']),reverse=False)
		
		#orders = filter(lambda x: not x.is_break, orders)

		tot_fields = ('tot_optimizado', 'tot_corte', 'tot_pulido', 'tot_entalle', 'tot_lavado', 'tot_horno',
			 'tot_templado', 'tot_insulado', 'tot_comprado', 'tot_ingresado', 'tot_entregado', 'tot_requisicion', 
			 'tot_corte_laminado', 'tot_corte_lavado_laminado', 'tot_perforacion', 'tot_pegado', 'tot_biselado', 
			 'tot_arenado', 'tot_biselado', 'tot_perforacion')

		vals = dict.fromkeys(tot_fields, 0)

		for item in orders:
			if item.is_break:
				continue
			for t_field in tot_fields:
				if item[t_field[4:]]:
					vals[t_field] += 1

		vals['line_ids'] = line_values
		
		# vals={
		# 	'tot_optimizado' : len(list(filter(lambda x: x.optimizado, orders))),
		# 	'tot_corte'      : len(list(filter(lambda x: x.corte, orders))),
		# 	'tot_pulido'     : len(list(filter(lambda x: x.pulido, orders))),
		# 	'tot_entalle'    : len(list(filter(lambda x: x.entalle, orders))),
		# 	'tot_lavado'     : len(list(filter(lambda x: x.lavado, orders))),
		# 	'tot_horno'      : len(list(filter(lambda x: x.horno, orders))),
		# 	'tot_templado'   : len(list(filter(lambda x: x.templado, orders))),
		# 	'tot_insulado'   : len(list(filter(lambda x: x.insulado, orders))),
		# 	'tot_comprado'   : len(list(filter(lambda x: x.comprado, orders))),
		# 	'tot_ingresado'  : len(list(filter(lambda x: x.ingresado, orders))),
		# 	'tot_entregado'  : len(list(filter(lambda x: x.entregado, orders))),
		# 	'tot_requisicion': len(list(filter(lambda x: x.requisicion, orders))),
		# 	'line_ids'       :line_values,
		# } 
		self.write(vals)
		return True

	# Optimizar mejor esta wada...
	def _get_data(self, lot_lines):
		if self.type_orders == 'services':
			lot_lines = lot_lines.filtered(lambda x: x.is_service)
		if self.filter_field:
			if self.filter_field == 'all':
				pass
			elif self.filter_field == 'pending':
				lot_lines = lot_lines.filtered(lambda x: not x.producido)
			elif self.filter_field == 'produced':
				lot_lines = lot_lines.filtered(lambda x: x.producido)
			elif self.filter_field == 'to inter':
				lot_lines = lot_lines.filtered(lambda x: x.producido and not x.ingresado)
			elif self.filter_field == 'to deliver':
				lot_lines = lot_lines.filtered(lambda x: x.ingresado and not x.entregado)
			elif self.filter_field == 'expired':
				now = (datetime.now() - timedelta(hours=5)).date()
				lot_lines = lot_lines.filtered(lambda x: datetime.strptime(x.order_prod_id.date_delivery,"%Y-%m-%d").date() < now and not x.producido)
		if not self.show_breaks:
			lot_lines = lot_lines.filtered(lambda x: not x.is_break)
		return list(set(lot_lines))

class GlassListWizard(models.TransientModel):
	_name='glass.list.wizard'

	main_id = fields.Many2one('glass.list.main.wizard','Main')
	lot_line_id = fields.Many2one('glass.lot.line','Linealote')
	order_line  = fields.Many2one('glass.order.line','Lineapedido')
	order_id      = fields.Many2one(related='order_line.order_id')
	order_name = fields.Char('O.P.')
	#order_id      = fields.Many2one('glass.order')
	crystal_number = fields.Char(related='order_line.crystal_number')
	crystal_number_fl = fields.Float('Nro cristal')
	#crysta_number = fields.Integer('Nro Cristal')
	base1=fields.Integer(related='order_line.base1')
	base2=fields.Integer(related='order_line.base2')
	altura1=fields.Integer(related='order_line.altura1')
	altura2=fields.Integer(related='order_line.altura2')
	descudre=fields.Char(related='order_line.descuadre')
	nro_pagina=fields.Integer(related='order_line.page_number')
	repos=fields.Boolean(related='order_line.glass_repo')
	estado = fields.Selection(related='order_line.state')
	partner_id = fields.Many2one(related='order_line.partner_id')
	croquis=fields.Char(related='order_line.image_page_number')
	area = fields.Float('Area',related='order_line.area')
	optimizado=fields.Boolean(related='lot_line_id.optimizado') 
	requisicion=fields.Boolean(related='lot_line_id.requisicion') 
	corte=fields.Boolean(related='lot_line_id.corte')
	pulido=fields.Boolean(related='lot_line_id.pulido')
	entalle=fields.Boolean(related='lot_line_id.entalle')
	lavado=fields.Boolean(related='lot_line_id.lavado') 
	templado=fields.Boolean(related='lot_line_id.templado')
	horno = fields.Boolean(related='lot_line_id.horno')
	insulado=fields.Boolean(related='lot_line_id.insulado') 
	producido=fields.Boolean(related='lot_line_id.producido') 
	comprado=fields.Boolean(related='lot_line_id.comprado')
	ingresado=fields.Boolean(related='lot_line_id.ingresado') 
	entregado=fields.Boolean(related='lot_line_id.entregado')
	arenado = fields.Boolean(related='lot_line_id.arenado')
	glass_break=fields.Boolean(related='lot_line_id.is_break')
	lot_id = fields.Many2one(related='lot_line_id.lot_id')

	corte_laminado = fields.Boolean(related='lot_line_id.corte_laminado')
	corte_lavado_laminado = fields.Boolean(related='lot_line_id.corte_lavado_laminado')
	perforacion = fields.Boolean(related='lot_line_id.perforacion')
	pegado = fields.Boolean(related='lot_line_id.pegado')
	biselado = fields.Boolean(related='lot_line_id.biselado')
	
	pending_stages_string = fields.Char(related='lot_line_id.pending_stages_string', 
		string='Etapas pendientes', readonly=True)
	measures = fields.Char(related='order_line.measures', 
		string='Medidas', readonly=True)
	# nombre corto:
	display_name_lot = fields.Char(string='Lote', related='lot_id.name')
	product_name = fields.Char('Producto',related='order_line.product_id.name')
	display_name_partner=fields.Char(string='Cliente',compute='_get_display_name_partner')
	# Campo auxiliar para mostrar las lineas que no tienen lote de produccion:
	decorator = fields.Selection([
		('default','default'), 
		('break','break'), 
		('without_lot','without_lot')],
		default='default')
	
	@api.depends('partner_id')
	def _get_display_name_partner(self):
		for rec in self:
			rec.display_name_partner = rec.partner_id.name[:14] if rec.partner_id.name else ''

	@api.multi
	def show_detail_tracing_line(self):
		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.show_detail_tracing_line_wizard_form' % module, False)
		wizard = self.env['show.detail.tracing.line.wizard'].create({'lot_line_id':self.lot_line_id.id})
		return{
			'name': 'Detalle de Seguimiento',
			'res_id': wizard.id,
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': wizard._name,
			'view_id': view.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
		} 

	def break_crystal(self):
		module = __name__.split('addons.')[1].split('.')[0]
		lot_line = self.lot_line_id
		done_stage = lot_line.stage_ids.filtered(lambda s: s.done and s.stage_id.name in BREAK_STAGES).sorted(key=lambda s: s.date,reverse=True)
		wiz = self.env['glass.respos.wizard'].create({
			'lot_line_id': lot_line.id,
			'stage_id': done_stage and next(iter(done_stage)).stage_id.id or False, # last success stage
			})
		return {
			'name': _('Rotura de Cristales'),
			'res_id':wiz.id,
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': wiz._name,
			'view_id': self.env.ref('%s.view_glass_respos_wizard_form' % module).id,
			'type': 'ir.actions.act_window',
			'target': 'new',
		} 

	@api.multi
	def show_croquis(self):
		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.view_glass_list_image_form' % module)
		path = self.env['main.parameter'].search([])[0].download_directory
		
		file = None
		if self.croquis:
			file = open(self.croquis,'rb')
			cont_t = file.read()
			file.close()
			file_new = open(path + 'croquis.pdf','wb')
			file_new.write(cont_t)
			file_new.close()
		else:
			import PyPDF2
			writer = PyPDF2.PdfFileWriter()
			writer.insertBlankPage(width=500, height=500, index=0)
			with open(path+'croquis.pdf', "wb") as outputStream: 
				writer.write(outputStream) #write pages to new PDF

		return {
			'name': _('Croquis'),
			'view_type': 'form',
			'view_mode': 'form',
			'res_model': 'glass.list.wizard',
			'view_id': view.id,
			'type': 'ir.actions.act_window',
			'target': 'new',
			'res_id':self.id,
		} 

class GlassReposWizard(models.TransientModel):
	_name='glass.respos.wizard'
	motive = fields.Selection([
		('Error entalle','Error entalle'), 
		('Error medidas','Error medidas'), 
		('Vidrio rayado','Vidrio rayado'), 
		('Vidrio roto','Vidrio roto'), 
		('Planimetria','Planimetria'), 
		('Error ventas','Error ventas'), 
		('Materia prima','Materia prima')])
	lot_line_id = fields.Many2one('glass.lot.line',string=u'Línea de lote')
	stage_id = fields.Many2one('glass.stage', string='Etapa', domain=[('name', 'in', BREAK_STAGES)])
	note = fields.Text(u'Observación')
	
	def makerepo(self):
		glass_line = self.lot_line_id.order_line_id
		validate_stage = not glass_line.is_service
		break_info = {glass_line.id:{'motive': self.motive,'note': self.note or ''}}
		return glass_line.break_crystal(self.stage_id, break_info, validate_stage=validate_stage)