# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import base64,decimal
import sys
from odoo.exceptions import UserError
import pprint
from odoo.exceptions import ValidationError

class GlassServiceRemissionGuide(models.Model):
	_name = 'glass.service.remission_guide'
	_description = u'Guía de remisión de servicios'

	name = fields.Char('Nombre', readonly=True)
	invoice_id = fields.Many2one('account.invoice', string='Factura', 
		domain="[('partner_id', '=', 'partner_id'), ('state', 'not in', ['draft', 'cancel'])]")
	transporter_id = fields.Many2one('res.partner', string="Transportista")
	sale_order_id = fields.Many2one('sale.order', string='Pedido de venta', readonly=True)
	partner_id = fields.Many2one(related='sale_order_id.partner_id', string='Partner')
	send_agency = fields.Boolean(string=u'Envío a Agencia')
	first_route = fields.Char('Primer Tramo')
	second_route = fields.Char('Segundo Tramo')

	# campos de guía de remisión
	serie_guia_id = fields.Many2one('ir.sequence', string=u'Serie de guía', readonly=True)
	numberg = fields.Char(string=u'Nro de guía')
	marca = fields.Char('Marca', size=12)
	placa = fields.Char('Placa', size=12)
	nro_const = fields.Char(u'Número de constancia de inscripción', size=12)
	licencia = fields.Char(u'Licencia de Conducir N°(5)', size=12)
	nombre = fields.Char('Nombre')
	ruc = fields.Char('Ruc', size=100)
	tipo = fields.Char('Tipo', size=12)
	nro_comp = fields.Char(u'Número de comprobante', size=12)
	nro_guia = fields.Char(u'Número de guía', size=12)
	fecha_traslado = fields.Datetime(string='Fecha de traslado')
	punto_partida = fields.Char('Punto de partida', size=100)
	punto_llegada = fields.Char('Punto de llegada', size=100)
	driver_delivery = fields.Char('Conductor')
	line_ids = fields.One2many('glass.service.remission_guide.line', 'rg_guide_id', string='Líneas de detalle')
	

	def _next_number_remission_guide(self):
		seq = self.serie_guia_id
		next_number = seq.number_next_actual
		return (seq.prefix or '') + '0' * (seq.padding - len(str(next_number))) + str(next_number)

	def action_set_number_remission_guide(self):
		self.ensure_one()
		name_guide = self._next_number_remission_guide()
		self.serie_guia_id.next_by_id()
		self.write({'name': name_guide, 'numberg': name_guide})
		return {}

	@api.onchange('send_agency')
	def on_change_send_agency(self):
		partner_id = self.partner_id.parent_id or self.partner_id
		if partner_id and self.send_agency:
			street_partner = partner_id.street or ''
			province = '/' + partner_id.province_id.name if partner_id.province_id else ''
			district = '/' + partner_id.district_id.name if partner_id.district_id else ''
			self.second_route = street_partner + district + province

	# def get_print_wizard(self):
	# 	module = __name__.split('addons.')[1].split('.')[0]
	# 	wizard = self.env['print.picking.guides.wizard'].create({'sale_order_id': self.id})
	# 	return {
	# 		'name': 'Elegir Sede',
	# 		'type': 'ir.actions.act_window',
	# 		'res_id': wizard.id,
	# 		'res_model': wizard._name,
	# 		'view_id': self.env.ref('%s.print_picking_guides_wizard_view_form' % module).id,
	# 		'view_mode': 'form',
	# 		'view_type': 'form',
	# 		'target': 'new',
	# 		'context': dict(self._context or {}, called_method='action_print_rem_guide_services')
	# 		}

class GlassServiceRemissionGuideLine(models.Model):
	_name = 'glass.service.remission_guide.line'
	_description = u'Línea de Guía de remisión de servicios'

	rg_guide_id = fields.Many2one('glass.service.remission_guide', string=u'Guía de remisión')
	glass_line_id = fields.Many2one('glass.order.line', 
		domain="[('is_service', '=', True)]", 
		string=u'Línea de órden de servicio', 
		required=True, readonly=True)
	measures = fields.Char(related='glass_line_id.measures', readonly=True)
	product_id = fields.Many2one(related='glass_line_id.product_id', string='Producto')
	#product_qty = fields.Float('Cantidad', digits=(10, 4)) # Area
	area = fields.Float(string=u'Área', related='glass_line_id.area', readonly=True)
	uom_id = fields.Many2one(related='glass_line_id.calc_line_id.calculator_id.sale_uom_id', 
		readonly=True, string='Unidad de medida')