# -*- coding: utf-8 -*-
from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta

class GlassFurnaceOut(models.Model):
	_name='glass.furnace.out'
	_inherit = ['mail.thread']
	_order="date_out desc"

	name = fields.Char('Lote de Horno Salida', default="/")
	nro_crystal = fields.Integer('Nro. Cristales')
	total_m2 = fields.Float('Total M2',digits=(12,4))
	e_percent = fields.Float('% de Eficiencia', digits=(12,4))
	state = fields.Selection([
		('draft','Borrador'),
		('process','En Proceso'),
		('done','Finalizado')],'Estado', default='draft')
	line_ids=fields.One2many('glass.furnace.line.out','main_id','Detalle')
	user_ingreso = fields.Many2one('res.users', string='Usuario Ingreso')
	date_ingreso = fields.Datetime('Fecha Ingreso')
	date_out = fields.Datetime('Fecha Salida')
	user_out = fields.Many2one('res.users', string='Usuario Salida')
	show_button = fields.Boolean('Mostrar Limpiar', compute='_get_show_button')

	@api.depends('line_ids')
	def _get_show_button(self):
		for rec in self:
			bad_lines = rec.line_ids.filtered(lambda x: x.lot_line_id.is_break)
			rec.show_button = bool(len(bad_lines) > 0)

	def remove_bad_lines(self):
		self.line_ids.filtered(lambda x: x.lot_line_id.is_break).unlink()
		return True

	#salida de horno
	def furnace_out(self):
		module = __name__.split('addons.')[1].split('.')[0]
		if not self.env.user.has_group('%s.group_furnace_out'%module):
			raise UserError(u'Ud. No tiene autorización para realizar ésta acción.')

		if self.filtered(lambda r: r.state != 'process'):
			raise UserError(u'Sólo se permite la salida de horno en lotes con estado "En proceso"')

		lines = self.mapped('line_ids')
		breaks = lines.mapped('lot_line_id').filtered(lambda x: x.is_break)
		breaks = '\n'.join(['-> %s - %s : Cristal Roto'%(cr.search_code,cr.nro_cristal) for cr in breaks])
		if breaks:
			raise UserError(u'No es posible procesar los siguientes cristales\n%s\nHaga click en el botón limpiar para remover los cristales rotos.'%breaks)
		for line in lines:
			self._register_info(line.lot_line_id)
			
		self.write({'state':'done','user_out':self.env.uid,'date_out':datetime.now()})
		return {'msg_success_notify':u'Se ha realizado la salida de horno exitosamente.'}
	
	def _register_info(self, line):
		"""heredable"""
		# param: line (glass.lot.line obj)
		
		# por si le dan templado por el control de producción
		if not line.templado:
			line.with_context(force_register=True).register_stage('templado')
		linked_services = set(line.calc_line_id._get_linked_services())
		pending_stages = {ps.strip() for ps in (line.all_pending_stages and line.all_pending_stages.split(',') or [])}
		
		if not (linked_services & pending_stages) and not line.is_service: 
			# si no tiene servicios vinculados pendientes, dar como producido
			# Dar por producido si es un cristal común (templado y sin servicios vinculados)
			line.with_context(force_register=True).register_stage('producido') 
		# NOTE Un cristal común siempre se marca como producido al salir de templado???
		#if not line.all_pending_stages:
			# Es posible que al registrar el templado ya se haya registrado esta etapa también, en cuyo caso no se registrará de nuevo
		#	line.with_context(force_register=True).register_stage('producido')
		line.is_used = True #??
		line.order_line_id.state = 'ended'
	
	def reset_all_labels(self):
		self.line_ids.reset_label()

class GlassFurnaceIn(models.TransientModel):
	_name = 'glass.furnace.in'
	next_name = fields.Char('Nombre') 
	line_in_ids = fields.One2many('glass.furnace.line.in','main_id')
	search_code = fields.Char('Producto', store=False)
	crystal_num = fields.Integer('Nro de cristales',compute='_get_crystal_num')
	#message_erro = fields.Char('*',default='')
	message_erro = fields.Text('*',default='')

	def get_element(self):
		wizard = self.create({})
		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.view_glass_furnace_in_form' % module)
		return {
			'name':'Ingreso a Horno',
			'res_id':wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_id': view.id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			}

	@api.model
	def default_get(self, default_fields):
		res = super(GlassFurnaceIn,self).default_get(default_fields)
		conf = self.env['glass.order.config'].search([],limit=1)
		res.update({
			'next_name':conf and conf.seq_furnace.number_next_actual or '/',
		})
		return res

	@api.depends('line_in_ids')
	def _get_crystal_num(self):
		for rec in self:
			rec.crystal_num = len(rec.line_in_ids)

	@api.onchange('search_code')
	def onchangecode(self):
		if self.search_code and self._origin.id:
			line = self.env['glass.lot.line'].search([
				('search_code', '=', self.search_code), 
				('is_service', '=', False)], limit=1)
			self.search_code,self.message_erro = '',''
			if line:
				if line.templado:
					sql = """SELECT gfo.name AS fur_name, 
						gfo.date_ingreso AS fur_in, 
						gfo.date_out AS fur_out 
						FROM 
						glass_furnace_line_out gflo
						JOIN glass_furnace_out gfo ON gfo.id = gflo.main_id 
						JOIN glass_lot_line gll ON gll.id = gflo.lot_line_id
						WHERE gflo.lot_line_id = %s
						AND gfo.state = 'done' LIMIT 1
						"""
					self._cr.execute(sql,(line.id,))
					data = self._cr.dictfetchall()
					fur_name = in_date = out_date = 'no definido'
					if data: # Siempre debería encontrar, pero es posible que haya lotes eliminados
						dt = fields.Datetime
						date_tz = lambda self,date: dt.to_string(dt.context_timestamp(self,dt.from_string(date)))
						fur_name = data[0]['fur_name']
						in_date  = date_tz(self,data[0]['fur_in'])
						out_date = date_tz(self,data[0]['fur_out'])
					self.message_erro = u"El cristal ya fue registrado como templado en el Lote %s\nFecha de Ingreso: %s\nFecha de salida: %s"%(fur_name,in_date,out_date)
					return
				# if not line.lavado:
				# 	self.message_erro="El cristal no tiene etapa LAVADO"
				# 	return
				if line.calc_line_id.entalle and not line.entalle:
					self.message_erro="El cristal no ha pasado por la etapa de ENTALLE solicitada."
					return
				this = self.browse(self._origin.id)
				if line.id in this.line_in_ids.mapped('lot_line_id').ids:
					self.message_erro="El cristal seleccionado ya fue registrado en este ingreso a horno"
					return
				new_line = self.env['glass.furnace.line.in'].create({
					'main_id': this.id,
					'lot_line_id':line.id,
					'order_number':len(this.line_in_ids) + 1
				})
			else:
				self.message_erro=u"Código de búsqueda no encontrado!"
				return
		else:
			return
		return {'value':{'line_in_ids':this.line_in_ids.ids}}

	def generate_furnace_lot(self):
		conf = self.env['glass.order.config'].search([],limit=1)
		if not conf:
			raise UserError(u'No se encontraron los valores de configuración de producción')
		if not self.line_in_ids:
			raise UserError('No ha ingresado Cristales.')
		breaks = self.line_in_ids.filtered(lambda x: x.lot_line_id.is_break)
		furnace = self.line_in_ids.filtered(lambda x: x.lot_line_id.horno)
		msg = ''
		for bad in breaks:
			msg+='-> %s - %s :Roto\n'%(bad.lot_line_id.search_code,bad.lot_line_id.nro_cristal)
		for bad in furnace:
			msg+='-> %s - %s :En Horno\n'%(bad.lot_line_id.search_code,bad.lot_line_id.nro_cristal)
		if msg != '':
			raise UserError('Los siguientes cristales no pueden procesarse:\n'+msg)
		
		area = sum(self.line_in_ids.mapped('area'))

		new_furnace_lot = self.env['glass.furnace.out'].create({
			'date_out':datetime.now(),
			'user_ingreso':self.env.uid,
			'date_ingreso':datetime.now(),
			'e_percent':(area*100)/conf.furnace_area,
			'name':conf.seq_furnace.next_by_id(),
			'state':'process',
			'total_m2':area,
			'nro_crystal':self.crystal_num,
		})

		for i,line in enumerate(self.line_in_ids,1):
			self.env['glass.furnace.line.out'].create({
				'main_id': new_furnace_lot.id,
				'order_number':i,
				'lot_id':line.lot_line_id.lot_id.id,
				'lot_line_id':line.lot_line_id.id,
				'crystal_number':line.lot_line_id.nro_cristal,
				'base1':line.lot_line_id.base1,
				'base2':line.lot_line_id.base2,
				'altura1':line.lot_line_id.altura1,
				'altura2':line.lot_line_id.altura2,
				'area':line.lot_line_id.area,
				'partner_id':line.lot_line_id.order_prod_id.partner_id.id,
				'obra':line.lot_line_id.order_prod_id.obra,
			})
			line.lot_line_id.with_context(force_register=True).register_stage('horno')
		if self._context.get('new_element'):
			return self.get_element()
		else:
			module = __name__.split('addons.')[1].split('.')[0]
			return {
				'name':new_furnace_lot.name,
				'res_id':new_furnace_lot.id,
				'type': 'ir.actions.act_window',
				'res_model': new_furnace_lot._name,
				'view_id': self.env.ref('%s.view_glass_furnace_out_form'%module).id,
				'view_mode': 'form',
				'view_type': 'form',
				}
			

class GlassFurnaceLineIn(models.TransientModel):
	_name = 'glass.furnace.line.in'
	main_id = fields.Many2one('glass.furnace.in')
	lot_line_id = fields.Many2one('glass.lot.line')
 	order_id = fields.Many2one(related='lot_line_id.order_prod_id',string='Ord. de Producc.')
 	order_number = fields.Integer('Nro de Orden')
	crystal_number = fields.Char(related='lot_line_id.nro_cristal')
	base1 = fields.Integer(related='lot_line_id.base1')
	base2 = fields.Integer(related='lot_line_id.base2')
	altura1 = fields.Integer(related='lot_line_id.altura1')
	altura2 = fields.Integer(related='lot_line_id.altura2')
	area = fields.Float(related='lot_line_id.area')
	
class GlassFurnaceLineOut(models.Model):
	_name='glass.furnace.line.out'

	#_rec_name="lot_line_id"
	main_id = fields.Many2one('glass.furnace.out')
	lot_id = fields.Many2one('glass.lot','Lote')
	lot_line_id = fields.Many2one('glass.lot.line',u'Línea de lote')
	order_number = fields.Integer(u'Nro. Orden')
	crystal_number = fields.Char('Nro. Cristal')
	base1 = fields.Integer("Base1 (L 4)")
	base2 = fields.Integer("Base2 (L 2)")
	altura1 = fields.Integer("Altura1 (L 1)")
	altura2 = fields.Integer("Altura2 (L 3)")
	area = fields.Float(u'Área M2',digits=(20,4))
	partner_id = fields.Many2one('res.partner',string='Cliente',)
	obra = fields.Char(string='Obra')
	etiqueta = fields.Boolean(string='Etiqueta')
	is_used = fields.Boolean(string='Usado', default=False)
	is_break = fields.Boolean(related='lot_line_id.is_break',string='Roto')

	def reset_label(self):
		self.write({'etiqueta':False})