# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime
from datetime import timedelta

class ProductProduct(models.Model):
	_inherit='product.template'
	
	type_materia_prod = fields.Char(u"Código Tipo Material")
	optima_trim = fields.Integer('Optima Trim', default=15)

class ProductProduct(models.Model):
	_inherit='product.product'
	
	@api.multi
	def name_get(self):
		aresult=[]
		result=super(ProductProduct,self).name_get()
		for r in result:
			pt = self.browse(r[0])
			if any(pt):
				if pt.uom_id.plancha:
					cad=r[1]+" ["+pt.uom_id.name+"]"
					aresult.append((pt.id,cad))
				else:
					aresult.append(r)
		return aresult

	@api.model
	def name_search(self, name, args=None, operator='ilike', limit=100):
		args = args or []
		domain = []
		if name:
			domain = ['|','|',('name', operator, name),('default_code', operator, name),('type_materia_prod', operator, name)]
		products = self.search(domain + args, limit=limit)
		return products.name_get()


class ProductCategory(models.Model):
	_inherit = 'product.category'

	prevent_extra_measure_optima = fields.Boolean('Prevenir medida extra en Optima', default=False,
		help='Si está marcado, los cristales cuyo producto pertenezca a ésta categoría no registrarán una medida extra de 3mm en los ficheros de óptima.')
