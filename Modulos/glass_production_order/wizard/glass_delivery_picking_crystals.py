# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round,float_compare
from decimal import Decimal,ROUND_HALF_UP

class GlassDeliveryPickingCrystals(models.TransientModel):
	_name = 'glass.delivery.picking.crystals'

	def _get_max_items(self):
		max_items = self.env['glass.order.config'].search([],limit=1).nro_cristales_guia
		if not max_items:
			raise UserError(u'No se han encontrado los valores de configuración necesarios para esta operacion (Nro. cristales por guía)')
		return max_items

	glass_order_ids = fields.Many2many('glass.order', string='Ord. de Prod.', 
		domain=lambda self: self._context.get('ops_domain', [('state','not in',('draft','returned'))]))
	picking_id = fields.Many2one('stock.picking', string='Picking')
	sale_order_id = fields.Many2one('sale.order', string='Pedido de venta') # picking_id y sale_order_id son excluyentes
	line_ids = fields.One2many('glass.delivery.picking.crystals.line','main_id')
	show_only_allowed = fields.Boolean(u'Mostrar sólo Aptos',default=True)
	max_items = fields.Integer(u'Límite de cristales por guía',default=_get_max_items,readonly=True)
	only_max_items = fields.Boolean(u'Obtener sólo el máximo permitido',default=True)
	selected_items_count = fields.Integer(u'Ítems seleccionados', compute='_get_selected_items_count', readonly=True)
	serie_guia_id = fields.Many2one('ir.sequence', string=u'Serie de guía')


	@api.depends('line_ids.check')
	def _get_selected_items_count(self):
		for wiz in self:
			wiz.selected_items_count = len(wiz.line_ids.filtered('check'))

	def get_crystals_list(self):
		self.line_ids.unlink()
		domain = self._context.get('extra_domain',[])
		if self.glass_order_ids:
			domain.append(('order_prod_id','in',self.glass_order_ids.ids))
		if self.show_only_allowed:
			domain+=[('ingresado','=',True),('entregado','=',False),('in_packing_list','=',False)]
		values = self.picking_id.move_lines.get_results(extra_domain=domain,limit=self.max_items if self.only_max_items else False)
		values = sorted(values,key=lambda l: (l[2]['order_name'],l[2]['crystal_num_fl']),reverse=False)
		self.write({'line_ids':values})
		return {"type": "ir.actions.do_nothing",}

	def delivery(self):
		self.ensure_one()

		lines = self.line_ids.filtered(lambda x: x.check)
		if len(lines) > 0:
			breaks       = lines.mapped('lot_line_ids').filtered(lambda x: x.is_break)
			# TODO mejorar para evitar tanta iteración
			sended       = lines.filtered(lambda x: x.entregado)
			not_entried  = lines.filtered(lambda x: not x.ingresado)
			packing_list = lines.filtered(lambda x: x.in_packing_list)
			msg = ''
			for item in breaks:
				msg+='%s - %s Motivo: Roto\n'%(item.order_id.name,item.crystal_num)
			for item in sended:
				msg+='%s - %s Motivo: Ya Entregado\n'%(item.order_id.name,item.crystal_num)
			for item in not_entried:
				msg+='%s - %s Motivo: No ingresado\n'%(item.order_id.name,item.crystal_num)
			for item in packing_list:
				msg+='%s - %s Motivo: En Packing List\n'%(item.order_id.name,item.crystal_num)
			if msg != '':
				raise UserError(u'No es posible procesar los siguientes cristales:\n'+msg)
		else:
			lines = self.line_ids.filtered(lambda x: not x.entregado and x.ingresado and not x.in_packing_list)
		
		if not lines:
			raise UserError(u'No hay líneas que cumplan los requisitos para procesarse (ingresadas y no entregadas).')
		lines = lines[:self.max_items] #NOTE no remover hata que se regularice el límite en insulado

		picking = self.picking_id.with_context(glass_force_assign=True,delivery_transfer=True)
		if picking.state=='draft':
			picking.action_confirm()
		if picking.state=='confirmed':
			picking.action_assign()
		
		#Sit = self.env['stock.immediate.transfer']
		Sbc = self.env['stock.backorder.confirmation']
		if picking.state != 'assigned':
			picking.force_assign()
		
		ops_to_transfer = self.env['stock.pack.operation']
		if picking.state == 'assigned':
			moves = lines.mapped('move_id')
			rounding = 0.0001
			for move in moves:
				pack_operation = move.linked_move_operation_ids.mapped('operation_id') # FIXME debería haber sólo una operación relacionada
				filt = lines.filtered(lambda l: l.move_id.id == move.id)
				quantity = float_round(sum(filt.mapped('area')),precision_rounding=rounding)
				#pack_operation = self._get_operation(picking,move.product_id)
				compare = float_compare(quantity,pack_operation.product_qty,precision_rounding=rounding)
				if compare==1:
					# dado que todo está disponible, la cantidad en stock_move no debería superar a la del producto
					quantity = pack_operation.product_qty 

				# esta vaina es para subsanar las diferencias en calculadora, retirar cuando las 
				# ordenes generadas antes del 18 de enero se hayan entregado, ya no tiene sentido mantener esta verificación...
				calc = move.procurement_id.sale_line_id.calculator_id
				a_cobrada = calc.total_sold_area
				a_invoiced = calc.qty_invoiced
				diff = float(Decimal(str(a_cobrada-a_invoiced)).quantize(Decimal('0.0001'), rounding=ROUND_HALF_UP))
				if diff > 0.0:
					quantity = quantity - diff

				pack_operation.write({'qty_done':quantity})
				loc_id = move.location_id.id
				order_lines_ids = self.env['glass.order.line']
				for line in filt:
					lot_lines = line.lot_line_ids
					g_lines = lot_lines.mapped('order_line_id')
					
					# validación temporal, por si algún gil quiere entregar cristales q no fueron ingresados a la ubicación origen de éste alb.
					# Hace hacer un mejor análisis para ver en qué casos no aplicaría ésta validación, pero eso ya es chamba del q retome 
					# éste proyecto xD
					if not all(loc_id in gl.stock_move_ids.mapped('location_dest_id').ids or gl.lot_line_id.from_insulado for gl in g_lines):
						raise UserError(u'No fue posible realizar la transferencia debido a que existen cristales cuyo ingreso no fue hecho al lugar de origen de ésta entrega.')
					
					g_lines.write({'state':'send2partner'})
					lot_lines.with_context(force_register=True).register_stage('entregado')
					order_lines_ids |= g_lines

				if order_lines_ids:
					move.write({'glass_order_line_ids':[(6, 0, order_lines_ids.ids)]})
				else:
					raise UserError(u'No se encontraron líneas de cristales para el movimiento "%s".' % (move.name or ''))
				ops_to_transfer |= pack_operation

			(picking.pack_operation_product_ids - ops_to_transfer).unlink()
			action = picking.do_new_transfer()
			if type(action) is dict and action['res_model']==Sbc._name:
				ctx = action['context']
				sbc = Sbc.with_context(ctx).create({'pick_id': picking.id})
				sbc.process()
				backorder_pick = self.env['stock.picking'].search([('backorder_id', '=', picking.id)])
				backorder_pick.do_unreserve()
			return {'type': 'ir.actions.act_window_close'}
		else:
			raise UserError('No se pudo realizar la reservación de los productos')

	def validate_lines(self):
		lines = self.line_ids.filtered(lambda x: x.check)
		if len(lines) > 0:
			breaks       = lines.mapped('lot_line_ids').filtered(lambda x: x.is_break)
			sended       = lines.filtered(lambda x: x.entregado)
			not_entried  = lines.filtered(lambda x: not x.ingresado)
			packing_list = lines.filtered(lambda x: x.in_packing_list)
			msg = ''
			for item in breaks:
				msg+='%s - %s Motivo: Roto\n'%(item.order_id.name,item.crystal_num)
			for item in sended:
				msg+='%s - %s Motivo: Ya Entregado\n'%(item.order_id.name,item.crystal_num)
			for item in not_entried:
				msg+='%s - %s Motivo: No ingresado\n'%(item.order_id.name,item.crystal_num)
			for item in packing_list:
				msg+='%s - %s Motivo: En Packing List\n'%(item.order_id.name,item.crystal_num)
			if msg != '':
				raise UserError(u'No es posible procesar los siguientes cristales:\n'+msg)
		else:
			lines = self.line_ids.filtered(lambda x: not x.entregado and x.ingresado and not x.in_packing_list)
		
		if not lines:
			raise UserError(u'No hay líneas que cumplan los requisitos para procesarse (ingresadas y no entregadas).')

class GlassDeliveryPickingCrystals(models.TransientModel):
	_name = 'glass.delivery.picking.crystals.line'

	check     = fields.Boolean(string='Seleccion')
	main_id = fields.Many2one('glass.delivery.picking.crystals')
	move_id = fields.Many2one('stock.move')
	order_id = fields.Many2one('glass.order',u'Órden de Prod.')
	order_name = fields.Char(u'O.P.')
	crystal_num_fl = fields.Float('Nro Cristal')
	base1   = fields.Integer('Base 1')
	base2   = fields.Integer('Base 2')
	height1 = fields.Integer('Altura 1')
	height2 = fields.Integer('Altura 2')
	measures = fields.Char('Medidas')
	crystal_num =  fields.Char('Num. Cristal')
	product_id = fields.Many2one('product.product','Producto')
	area = fields.Float(u'Área',digits=(12,4))
	producido = fields.Boolean('Producido')
	ingresado = fields.Boolean('Ingresado')
	entregado = fields.Boolean('Entregado') 
	in_packing_list = fields.Boolean('Packing List')
	# Servicios
	#pulido = fields.Boolean('Pulido')
	#entalle = fields.Boolean('Entalle')
	#arenado = fields.Boolean('Arenado')
	#corte_laminado = fields.Boolean('Corte Laminado')
	#corte_lavado_laminado = fields.Boolean('Corte_lavado Laminado')
	#perforacion = fields.Boolean('Perforacion')
	#pegado = fields.Boolean('Pegado')
	#biselado = fields.Boolean('Biselado')

	# NOTE en teoría los critales insulados ya no deberían 
	# afectar aquí pero es posible q luego les de la gana de querer ver los cristales 
	# que conforman un insulado, por eso queda esto como M2M
	lot_line_ids = fields.Many2many('glass.lot.line',string=u'Líneas de lote')