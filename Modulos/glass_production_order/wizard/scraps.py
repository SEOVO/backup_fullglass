# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError


class ScrapsEntryWizard(models.TransientModel):
	_inherit = 'scraps.entry.wizard'

	@api.model
	def default_get(self,default_fields):
		res = super(ScrapsEntryWizard,self).default_get(default_fields)
		traslate_motive_drt = self.env['glass.order.config'].search([],limit=1).traslate_motive_drt
		if traslate_motive_drt:
			res.update({'traslate_motive': traslate_motive_drt.id})
		return res

	@api.multi
	def get_element(self):
		res = super(ScrapsEntryWizard,self).get_element()
		entry_scraps_pt_ids = self.env['glass.order.config'].search([],limit=1).entry_scraps_pt_ids.ids
		ctx = dict(self._context or {},dom_picking_type=[('id','in',entry_scraps_pt_ids)])
		res.update({'context':ctx})
		return res

	def process_scraps(self):
		conf = self.env['glass.order.config'].search([],limit=1)
		allowed = (conf.location_retazo | conf.location_retazo_lima).ids
		if self.picking_type.default_location_dest_id.id not in allowed:
			raise UserError('El tipo de picking que intenta usar no tiene por destino una ubicación de retazos.')
		return super(ScrapsEntryWizard,self).process_scraps()

class detalle_simple_fisico_total_d_wizard_poliglass(models.TransientModel):
	_inherit = 'detalle.simple.fisico.total.d.wizard.poliglass'

	@api.multi
	def get_element(self):
		res = super(detalle_simple_fisico_total_d_wizard_poliglass,self).get_element()
		conf = self.env['glass.order.config'].search([],limit=1)
		ids = (conf.location_retazo | conf.location_retazo_lima).ids # TODO esta vaina ya debería ser M2M, otra vez Lima jodiéndola grr
		ctx = dict(self._context or {},dom_scrap_locations=[('id','in',ids)])
		res.update({'context':ctx})
		return res

