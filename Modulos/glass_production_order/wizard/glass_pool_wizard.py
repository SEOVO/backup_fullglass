# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime
import base64
import codecs
import os,string
from odoo.addons.glass_production_order.models.sale_order import TYPE_ORDERS

class GlassPoolWizard(models.TransientModel):
	_name='glass.pool.wizard'

	line_order_ids = fields.Many2many('glass.order.line', 
		relation='glass_lines_wizard_rel', column1='wizard_id', column2='line_order_id', 
		string=u'Líneas de Cristales/Servicios')
	line_ids = fields.One2many('glass.pool.wizard.line','wizard_id')
	product_id = fields.Many2one('product.product',string=u'Producto Seleccionado')
	nextlotnumber = fields.Char('Lote')
	qty_lines = fields.Integer('Cantidad selecc.',compute="getarealines")
	area_lines = fields.Float(u'Área selecc. M2',compute="getqtyarea",digits=(20,4))
	area_rest = fields.Float(u'Área Total M2',digits=(20,4),help=u'Area total de cristal pendiente de lote')
	qty_rest = fields.Integer(u'Cantidad Total',help=u'Cantidad de cristales pendientes de lote')
	user_id=fields.Many2one('res.users',string='Responsable',default=lambda self: self.env.uid)
	show_button = fields.Boolean('Show button',default=True)
	state = fields.Selection([
		('draft','Borrador'),
		('confirm','Confirmado')], string='Estado', default='draft')
	type_pool = fields.Selection(TYPE_ORDERS, required=True, readonly=True)


	def get_new_element(self):

		type_pool = self._context.get('default_type_pool', 'production')

		vals = {
			'type_pool': type_pool,
		}

		ext_sql = ' AND COALESCE(gol.is_service, false) = false '

		if type_pool == 'services':
			ext_sql = ' AND gol.is_service = true '

		query = """
			SELECT 
			count(gol.*) AS quantity,
			gol.product_id AS product_id,
			sum(gol.area) AS area
			FROM glass_order_line gol
			WHERE 
			gol.state = 'process'
			AND (gol.is_used = false OR gol.is_used is null)
			AND gol.lot_line_id is null
			AND gol.calc_line_id is not null -- temporal, retirar cuando se regularice la data
			%s
			GROUP BY gol.product_id""" % ext_sql
		
		self._cr.execute(query)
		
		results = self._cr.dictfetchall()
		config_data = self.env['glass.order.config'].search([], limit=1)
		if not config_data:
			raise UserError(u'No se encontraron los valores de configuración de producción')		
		vals.update({
			'nextlotnumber': config_data.seq_lot.number_next_actual,
			'area_rest': sum(map(lambda r: r['area'],results)),
			'qty_rest': sum(map(lambda r: r['quantity'],results)),
			'line_ids': [(0, 0, {
				'product_id': line['product_id'],
				'area': line['area'] if line['area'] else 0.0,
				'qty': line['quantity'],
				}) for line in results],
		})

		wizard = self.create(vals)

		return {
			'name': self._context.get('pool_title', u'Pool de Producción'),
			'res_id': wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
		}

	def confirm(self):
		selected = self.line_ids.filtered('selected')
		if len(selected)==0:
			raise UserError(u'Debe seleccionar una línea para poder confirmar.')
		if len(selected)>1:
			raise UserError(u'Ha seleccionado más de una línea\nElija sólo una.')
		self.write({
			'state':'confirm',
			'product_id':selected.product_id.id,
			'line_ids':[(6,0,[selected.id])] # clean lines
		})
		return {"type": "ir.actions.do_nothing",}

	@api.onchange('line_order_ids')
	def onchange_lines(self):
		areat=0.00
		for line in self.line_order_ids:
			areat=areat+line.area
		self.area_lines = areat
		self.qty_lines=len(self.line_order_ids)

		catidadsel = 0
		areasel = 0
		for line in self.line_order_ids:
			catidadsel=catidadsel+1
			areasel=areasel+line.area
		for line in self.line_ids:
			if line.selected:
				line.cant_rest=line.qty-catidadsel
				line.area_rest=line.area-areasel

	@api.one
	def getqtyarea(self):
		areat=float(0.00)
		for line in self.line_order_ids:
			areat=areat+float(line.area)
		self.area_lines = areat

	@api.one
	def getarealines(self):
		self.qty_lines=len(self.line_order_ids)

	def addlot(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		if not self.line_order_ids:
			raise UserError(u'No ha seleccionado cristales para generar un lote.')
		config_data = self.env['glass.order.config'].search([],limit=1)
		newname = config_data.seq_lot.next_by_id()
		Lot = self.env['glass.lot']
		LotLine = self.env['glass.lot.line']

		newlot = Lot.create({
			'name':newname,
			'date':datetime.now(),
			'product_id':self.product_id.id,
			'user_id':self.user_id.id,
			'state':'done',
			'type_lot': self.type_pool,
		})

		cad_optima = ""
		cad_optima =cad_optima +("Lote"+newlot.name.rjust(7,'0')).ljust(32,' ')
		fecha_s=str(newlot.date[8:10]).ljust(2,'0')+str(newlot.date[5:7]).ljust(2,'0')+str(newlot.date[:4])

		cad_optima =cad_optima +fecha_s
		cad_optima =cad_optima +''.ljust(8,' ')
		cad_optima =cad_optima +str(self.qty_lines).rjust(8,'0')
		cad_optima =cad_optima +'V7'+'\r\n'
		cad_optima =cad_optima +str(self.qty_lines).rjust(8,'0')+'\r\n'
		n=1
		g_lines = self.line_order_ids.sorted(lambda l: (l.order_id.name,l.crystal_number_fl))
		for line in g_lines:
			args = {
				'order_name': line.order_id.name,
				'lot_name': newname,
				'crystal_number': line.crystal_number,}
				
			search_code = LotLine.make_search_code(**args)
			
			new_line = LotLine.create({
				'product_id':line.product_id.id,
				'nro_cristal':line.crystal_number,
				'calc_line_id':line.calc_line_id.id,
				'descuadre':line.descuadre,
				'page_number':line.page_number,
				'lot_id':newlot.id,
				'order_line_id':line.id,
				'search_code':search_code,
				'area':line.area,
			}) 
			line.write({
				'is_used':True,
				'lot_line_id':new_line.id,
			})
			#line.order_id.state="process"
			new_line.with_context(force_register=True).register_stage('optimizado')
			desc='        '
			molval = '000003.0' if not line.descuadre and not line.product_id.categ_id.prevent_extra_measure_optima else '000000.0'
			caddes=''
			data_desc = False # String con los datos de descuadre para optima
			if line.descuadre:
				desc='########'
				#molval='000000.0'
				adesc=line.descuadre.split(',')
				caddes = caddes+'/'
				if adesc[0]=='1':
					caddes = caddes+str(line.base1)
				if adesc[0]=='2':
					caddes = caddes+str(line.base2)
				if adesc[0]=='3':
					caddes = caddes+str(line.altura1)
				if adesc[0]=='4':
					caddes = caddes+str(line.altura2)
				if len(adesc)>1: 
					if adesc[1]=='1':
						caddes = caddes+'x'+str(line.base1)
					if adesc[1]=='2':
						caddes = caddes+'x'+str(line.base2)
					if adesc[1]=='3':
						caddes = caddes+'x'+str(line.altura1)
					if adesc[1]=='4':
						caddes = caddes+'x'+str(line.altura2)
					data_desc = self.get_descuadre_2(line.order_id.name,line.product_id.product_tmpl_id.optima_trim,line.crystal_number,adesc,line.base1,line.base2,line.altura1,line.altura2)
				elif len(adesc) == 1:
					data_desc = self.get_descuadre_1(line.order_id.name,line.product_id.product_tmpl_id.optima_trim,line.crystal_number,adesc[0],line.base1,line.base2,line.altura1,line.altura2)

			x=line.base1
			if line.base2>x:
				x=line.base2
			ax = str(x).split('.')
			cadx=ax[0].rjust(4,'0')
			if len(ax)>1:
				cadx=cadx+'.'+ax[1].ljust(3,'0')
			else:
				cadx=cadx+'.000'
			y=line.altura1
			if line.altura2>y:
				y=line.altura2
			ay = str(y).split('.')
			cady=ay[0].rjust(4,'0')
			if len(ay)>1:
				cady=cady+'.'+ay[1].ljust(3,'0')
			else:
				cady=cady+'.000'
			cade1=''.rjust(32,' ')
			cade = '      '
			if line.entalle:
				cade='E     '
				cade1='E'.ljust(32,' ')
			#medidas
			cmedida=""
			if desc=='########':
				if line.base1!=line.base2:
					cmedida=cmedida+str(line.base1)+'/'+str(line.base2)
				else:
					cmedida=cmedida+str(line.base1)
				cmedida=cmedida+"x"
				if line.altura1!=line.altura2:
					cmedida=cmedida+str(line.altura1)+'/'+str(line.altura2)
				else:
					cmedida=cmedida+str(line.altura1)
			else:
				cmedida=str(line.base1)+"x"+str(line.altura1)

			#fecha_prod_a=self.line_order_ids[0].order_id.date_production ???
			fecha_prod_a = line.order_id.date_production
			fecha_prod=(str(fecha_prod_a[:4]+'.'+str(fecha_prod_a[5:7])+'.'+fecha_prod_a[8:10])).ljust(32,' ')

			cad_optima =cad_optima+(line.product_id.type_materia_prod.ljust(64,' ') if line.product_id.type_materia_prod else ''.ljust(64,' ')) #codmaterial
			cad_optima =cad_optima+str(n).rjust(5,'0') # numpos
			cad_optima =cad_optima+line.partner_id.name[:12].ljust(12,' ') #partner cliente
			cad_optima =cad_optima+line.order_id.name.ljust(12,' ') #ORDEN
			cad_optima =cad_optima+desc #si tine descuadre
			cad_optima =cad_optima+'    ' #extension
			cad_optima =cad_optima+molval #moliendavalor
			cad_optima =cad_optima+'000' #prioridadd de pezxas
			cad_optima =cad_optima+'Y' #si se rorta
			cad_optima =cad_optima+'00000001' # numero de corte
			cad_optima =cad_optima+'000000.0' # maquinadoblado
			cad_optima =cad_optima+cadx # tamaño x
			cad_optima =cad_optima+cady # tamaño y
			cad_optima =cad_optima+'        ' # espaciador

			cr_num = line.crystal_number
			cr_num = cr_num+'.0' if '.' not in cr_num else cr_num
			
			cad_optima =cad_optima+(cr_num).ljust(32,' ') #notas adicionales
			cad_optima =cad_optima+''.ljust(5,'0') # nro etiuqetas
			cad_optima =cad_optima+''.ljust(5,'0') # piezas
			cad_optima =cad_optima+''.ljust(5,'0') # preferencia

			cad_optima =cad_optima+cade # entalle
			cad_optima =cad_optima+'        '# fenetrega
			cad_optima =cad_optima+'000' #cod hoyo
			cad_optima =cad_optima+'000000.0' #distancia borde cristal
			# old cad_optima =cad_optima+(str(line.crystal_number)+".0").ljust(32,' ') # texto add 1
			cad_optima =cad_optima+(cr_num).ljust(32,' ') # texto add 1
			# aqui me voy a ver lo de qr de la alemana conuar desde aqui
			cad_optima =cad_optima+search_code.ljust(32,' ') # texto add 2
			cad_optima =cad_optima+line.product_id.name[:32].ljust(32,' ') # text add 3
			cad_optima =cad_optima+line.partner_id.name[:32].ljust(32,' ') # change: nombre completo del cliente recortado a 32 chars
			cad_optima =cad_optima+''.rjust(32,' ') # t a 5
			cad_optima =cad_optima+(line.polished_id.code.strip().ljust(32,' ') if line.polished_id.code else ''.ljust(32,' '))
			cad_optima =cad_optima+cade1
			cad_optima =cad_optima+ ('P'.ljust(32,' ') if line.plantilla else ''.ljust(32,' ')) # t a 8
			cad_optima =cad_optima+''.ljust(32,' ') # t a 9
			cad_optima =cad_optima+''.ljust(32,' ') # t a 10
			cad_optima =cad_optima+''.ljust(8,' ') # second spavccer
			cad_optima =cad_optima+'000000.0'
			cad_optima =cad_optima+'000000.0'
			cad_optima =cad_optima+'000000.0'
			cad_optima =cad_optima+'000000.0'
			cad_optima =cad_optima+cmedida.ljust(32,' ')# medidas 
			cad_optima =cad_optima+('(R)'.ljust(32,' ') if line.glass_break else ''.ljust(32,' '))# roto?
			cad_optima =cad_optima+('D'.ljust(32,' ') if desc=='########' else ''.ljust(32,' ')) # descuadre?
			cad_optima =cad_optima+line.product_id.name[32:64].ljust(32,' ') # resto del nombre producto
			arenado = 'Arenado' if line.calc_line_id.arenado_id else ''
			cad_optima =cad_optima+arenado.ljust(32,' ') #

			cad_optima =cad_optima+fecha_prod+'\r\n'
			if data_desc:
				cad_optima =cad_optima+data_desc+'\r\n'
			# NOTE a pedido del Sr. Nicolás Castillo, ya no debe concatenerse ésto (+CPU) a los ítems.
			#cad_optima += ('+CPU' + '\r\n') # TODO definir 3 letras
			n=n+1

		g_lines.mapped('order_id')._refresh_state()
		newlot.optimafile=base64.b64encode(cad_optima)
		if not config_data.optimization_path:
			raise UserError('No se ha configurado la ruta para los archivos de OPTIMA')
		if not config_data.optimization_ext:
			raise UserError('No se ha configurado la ruta para los archivos de OPTIMA')
		newname = newname.rjust(7,'0')
		filename=config_data.optimization_path+newname+"."+config_data.optimization_ext
		f=open(filename,"w+")
		cad_optima = cad_optima.replace('\r','')
		f.write(cad_optima)
		f.close()

		module = __name__.split('addons.')[1].split('.')[0]
		view_lot = 'view_glass_services_lot_form' if self.type_pool == 'services' else 'view_glass_lot_form'

		return {
			'name': newlot.name,
			'type': 'ir.actions.act_window',
			'res_id': newlot.id,
			'res_model': newlot._name,
			'view_id': self.env.ref('%s.%s' % (module, view_lot)).id,
			'view_mode': 'form',
		} 


	def get_descuadre_1(self,op,trim,crystal,position,base1,base2,altura1,altura2):
		data = '060 3\r\n'
		base1,base2,altura1,altura2 = str(base1),str(base2),str(altura1),str(altura2)
		if position == '1':
			if base1 < base2:
				data+='W '+altura2+'\r\n'+'H '+base2+'\r\n'+'H1 '+base1+'\r\n'+'ELAB: OFF 1.50 ROT 90 MY 1'+'\r\n'
			else:
				data+='W '+altura2+'\r\n'+'H '+base1+'\r\n'+'H1 '+base2+'\r\n'+'ELAB: OFF 1.50 ROT 90'+'\r\n'
		elif position == '2':
			if altura2 > altura1:
				data+='W '+base1+'\r\n'+'H '+altura2+'\r\n'+'H1 '+altura1+'\r\n'+'ELAB: OFF 1.50 MY 1'+'\r\n'
			else:
				data+='W '+base1+'\r\n'+'H '+altura1+'\r\n'+'H1 '+altura2+'\r\n'+'ELAB: OFF 1.50'+'\r\n'
		elif position == '3':
			if base2 < base1:
				data+='W '+altura1+'\r\n'+'H '+base1+'\r\n'+'H1 '+base2+'\r\n'+'ELAB: OFF 1.50 ROT 270 MY 1'+'\r\n'
			else:
				data+='W '+altura1+'\r\n'+'H '+altura2+'\r\n'+'H1 '+altura1+'\r\n'+'ELAB: OFF 1.50 ROT 270'+'\r\n'
		elif position == '4':
			if altura2 > altura1:
				data+='W '+base2+'\r\n'+'H '+altura2+'\r\n'+'H1 '+altura1+'\r\n'+'ELAB: OFF 1.50 ROT MX 1 MY 1'+'\r\n'
			else:
				data+='W '+base2+'\r\n'+'H '+altura1+'\r\n'+'H1 '+altura2+'\r\n'+'ELAB: OFF 1.50 MX 1'+'\r\n'

		if '.' in op:
			op = op.split('.')[0]

		crystal = str(crystal).rjust(2, '0')

		return data + '%s_%s 0 0 %s 0' % (str(op), str(crystal), str(trim))
		#return data + str(op) + '_0 ' + str(crystal) + ' 0 0 ' + str(trim) + ' 0'

	def get_descuadre_2(self,op,trim,crystal,positions,b1,b2,h1,h2):
		W  = max(b1, b2) # 1120   974
		W1 = min(b1, b2) # 1074   1120
		H  = max(h1, h2) # 980    980
		H1 = min(h1, h2) # 974    1074
		forma = elab = ''

		if positions == ['1', '2']:
			if b1 > b2 and h1 > h2:
				forma,elab = '069 4','ELAB: OFF 1.50 MY 1'
				H, H1 = H1, H
			elif b1 > b2 and h1 < h2:
				forma,elab = '071 4','ELAB: OFF 1.50 MY 1'
				H, H1 = H1, H
			elif b1 < b2 and h1 > h2:
				forma,elab = '073 4','ELAB: OFF 1.50'
			elif b1 < b2 and h1 < h2:
				forma,elab = '071 4','ELAB: OFF 1.50 MY 1'
				H, H1 = H1, H

		elif positions == ['2', '3']:
			if b1 > b2 and h1 > h2:
				forma,elab = '071 4','ELAB: OFF 1.50'
			elif b1 > b2 and h1 < h2:
				forma,elab = '069 4','ELAB: OFF 1.50'
			elif b1 < b2 and h1 > h2:
				forma,elab = '070 4','ELAB: OFF 1.50'
			elif b1 < b2 and h1 < h2:
				forma,elab = '073 4','ELAB: OFF 1.50 MY 1'
				H, H1 = H1, H # Evaluar ésta vaina

		elif positions == ['3', '4']:
			if b1 > b2 and h1 > h2:
				forma,elab = '070 4','ELAB: OFF 1.50 MX 1'
			elif b1 > b2 and h1 < h2:
				forma,elab = '073 4','ELAB: OFF 1.50 MX 1 MY 1'
				H, H1 = H1, H
				W, W1 = W1, W
			elif b1 < b2 and h1 > h2:
				forma,elab = '071 4','ELAB: OFF 1.50 MX 1'
				W, W1 = W1, W
			elif b1 < b2 and h1 < h2:
				forma,elab = '069 4','ELAB: OFF 1.50 MX 1'
				W, W1 = W1, W

		elif positions == ['1', '4']:
			if b1 > b2 and h1 > h2:
				forma,elab = '073 4','ELAB: OFF 1.50 MX 1'
				W, W1 = W1, W
			elif b1 > b2 and h1 < h2:
				forma,elab = '070 4','ELAB: OFF 1.50 MX 1 MY 1'
				W, W1 = W1, W
				H, H1 = H1, H
			elif b1 < b2 and h1 > h2:
				forma,elab = '069 4','ELAB: OFF 1.50 MX 1 MY 1'
				W, W1 = W1, W
				H, H1 = H1, H
			elif b1 < b2 and h1 < h2:
				forma,elab = '071 4','ELAB: OFF 1.50 MX 1 MY 1'
				W, W1 = W1, W
				H, H1 = H1, H

		#data = '%s\r\nW %d\r\nH %d\r\nW1 %d\r\nH1 %d\r\n%s\r\n%s_0 %s.0 0 0 %s 0' % (forma, W, H, W1, H1, elab, op, crystal, str(trim))
		#data = '\r\n'.join([forma, 'W '+W, 'H '+H, 'W1 '+W1, H1, elab, '%s_%s 0 0 %s 0' % (op, crystal, str(trim))])
		#return data
		if '.' in op:
			op = op.split('.')[0]

		crystal = str(crystal).rjust(2, '0')

		data = '%s\r\nW %d\r\nH %d\r\nW1 %d\r\nH1 %d\r\n%s\r\n%s_%s 0 0 %s 0' % (forma, W, H, W1, H1, elab, op, crystal, str(trim))
		return data
		#return data
		#data = [forma, 'W %d' % W, 'H %d' % H, 'W1 %d' % W1, 'H1 %d' % H1, elab, '%s_0 %s.0 0 0 %s 0' % (op, crystal, str(trim))]
		#return '\r\n'.join(data)

	# @api.model
	# def default_get(self, default_fields):
	# 	res = super(GlassPoolWizard, self).default_get(default_fields)
	# 	return res

class GlassPoolWizardLine(models.TransientModel):
	_name='glass.pool.wizard.line'

	#order_line_id = fields.Many2one('glass.order.line')
	wizard_id = fields.Many2one('glass.pool.wizard')
	product_id = fields.Many2one('product.product',string=u'Producto')
	default_code = fields.Char(related='product_id.default_code',string=u'Código')
	uom_id = fields.Many2one(related='product_id.uom_id',string='Unidad de Medida')
	qty = fields.Float('Cantidad')
	area = fields.Float('M2',digits=(20,4))
	selected = fields.Boolean('Seleccionado')
	area_rest = fields.Float(u'Área Restante M2',digits=(20,4))
	cant_rest = fields.Integer(u'Cantidad restante')
