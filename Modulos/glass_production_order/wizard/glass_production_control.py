# -*- coding: utf-8 -*-
from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
from pyPdf import PdfFileWriter
import PyPDF2, os
from odoo.addons.glass_production_order.models.glass_stage import FORBIDDEN_STAGES as BASE_FORBIDDEN_STAGES

STAGES_WITH_SKETCH_GLASS = {'lavado', 'entalle'}
STAGES_WITH_IMAGE_GLASS = {'corte', 'pulido', 'arenado', 'biselado', 'perforacion',
						'pegado', 'insulado', 'corte_laminado', 'corte_lavado_laminado'}
FORBIDDEN_STAGES = BASE_FORBIDDEN_STAGES | {'optimizado'}
DEFAULT_STAGE_DOMAIN = [('name', 'in', list(STAGES_WITH_IMAGE_GLASS | STAGES_WITH_SKETCH_GLASS))]

class GlassProductionControlWizard(models.TransientModel):
	_name='glass.productioncontrol.wizard'

	stage_id = fields.Many2one('glass.stage', string='Etapa', 
		domain=lambda self: self._context.get('domain_stages', DEFAULT_STAGE_DOMAIN))
	stage_name = fields.Char(related='stage_id.name')
	search_code = fields.Char('Producto')
	crystal_path_pdf = fields.Char('Code tmp')
	production_order = fields.Many2one('glass.order','OP')
	image_glass = fields.Binary("imagen")
	nro_cristal = fields.Char("Nro. Cristal")
	messageline= fields.Char('Mensaje')
	path = fields.Char('Path File')
	# ==== Helpers ====
	show_sketch = fields.Boolean('Mostrar croquis', compute='_compute_show_media', readonly=True)
	show_image_glass = fields.Boolean('Mostrar imagen de cristal', compute='_compute_show_media', readonly=True)

	@api.depends('stage_id')
	def _compute_show_media(self):
		for wiz in self:
			wiz.show_sketch = wiz.stage_id.name in STAGES_WITH_SKETCH_GLASS
			wiz.show_image_glass = wiz.stage_id.name in STAGES_WITH_IMAGE_GLASS

	@api.model
	def get_new_element(self):
		userstage = self.env['glass.order.config'].search([], limit=1).userstage.filtered(lambda x: x.user_id.id == self.env.user.id)

		available_stages = [stg.id for stg in userstage.mapped('stage_ids') if stg.name not in FORBIDDEN_STAGES]
		domain_stages = DEFAULT_STAGE_DOMAIN
		# Si existe una regla pero no tinee etapas definidas, permitir todo:
		if not userstage:
			raise UserError(u'El usuario actual no tiene permisos para acceder a esta funcionalidad')
		elif available_stages:
			domain_stages = [('id', 'in', available_stages)]

		wiz_vals = {
			'stage_id': userstage and userstage[0].stage_id.id or available_stages and available_stages[0] or False
		}

		wizard = self.create(wiz_vals)
		return {
			'name':u'Control de Producción',
			'res_id':wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': {'domain_stages': domain_stages},
		}

	@api.model
	def default_get(self,default_fields):
		res = super(GlassProductionControlWizard,self).default_get(default_fields)
		# userstage = self.env['glass.order.config'].search([],limit=1).userstage
		# if not userstage:
		# 	raise UserError(u'No se encontraron los valores de configuración de producción')	
		# ustage = False
		# for item in userstage:
		# 	if item.user_id.id == self.env.user.id:
		# 		ustage=item
		# if ustage:
		# 	res.update({'stage_id':ustage.stage_id.id})
		# else:
		# 	raise UserError(u'El usuario actual no tiene permisos para acceder a esta funcionalidad')

		path = self.env['main.parameter'].search([],limit=1).download_directory
		path += 'previsualizacion_op.pdf'
		writer = PyPDF2.PdfFileWriter()
		writer.insertBlankPage(width=500, height=500, index=0)
		if not os.path.exists(path):
			raise UserError(u'La ruta %s del pdf no es válida, por favor solicite a su administrador configurar correctamente la ruta.'%path)
		with open(path, "wb") as outputStream: 
			writer.write(outputStream) #write pages to new PDF
		res.update({'path':path})
		return res

	@api.onchange('search_code')
	def onchangecode(self):
		res   = {}
		code  = self.search_code
		stage = self.stage_id
		stage_name = stage.name
		path  = self.path
		if not code: return
		if not stage_name:
			self.messageline = 'No ha seleccionado una etapa'
			self.search_code = ''
			return
		if not path:
			self.messageline = 'No se pudo obtener la ruta de generación de PDF'
			self.search_code = ''
			return
		line = self.env['glass.lot.line'].search([('search_code', '=', code)], limit=1)
		if line:
			self.messageline = self.search_code = ''
			if stage_name in STAGES_WITH_SKETCH_GLASS:
				if stage_name == 'entalle' and not line.calc_line_id.entalle:
					res = {'production_order':False,'nro_cristal':False,'messageline':'El cristal no tiene etapa de entalle'}
					return {'value':res}
				path_pdf = line.order_line_id.image_page_number
				res = self.save_stage(line, stage, path_pdf)
				try:
					with open(path_pdf,'rb') as file:
						content = file.read()
						with open(path,'wb') as file_new:
							file_new.write(content)
				except TypeError as e:
					self.insert_blank_page(path)
				except IOError as e:
					self.insert_blank_page(path)
			elif stage_name in STAGES_WITH_IMAGE_GLASS:
				# La etapa de insulado sólo debe registrarse si es un servicio, no si es un cristal insulado como tal
				res = self.save_stage(line, stage)
				res.update({'image_glass':line.image_glass})
		else:
			msg = u'¡Código no encontrado!' if self.search_code else ''
			self.search_code = ''
			res = {'production_order':False,'nro_cristal':False,'image_glass':False,'messageline':msg}
			if stage_name in ['lavado','entalle']:
				self.insert_blank_page(path)
		return {'value':res}

	@api.model
	def save_stage(self, line, stage, path_pdf=False):
		# TODO CLEAN CODE
		stage_attr = getattr(line, stage.name, None)
		# Sólo se registrarán las etapas que sean también un campo en glass.lot.line
		if stage_attr is None:
			return {}
		
		if stage_attr:
			return self._get_error_empty_values('El cristal ya fue procesado en la etapa: %s' % stage.name.upper())
		
		if stage.name not in line.all_pending_stages.split(','):
			return self._get_error_empty_values('El cristal no tiene requerida la etapa de: %s' % stage.name.upper())

		if stage.name == 'insulado' and not line.is_service:
			return self._get_error_empty_values('La etapa de INSULADO sólo puede ser registrada si es un servicio.')

		if stage.name == 'templado' and not line.is_service:
			return self._get_error_empty_values('La etapa de TEMPLADO sólo puede ser registrada si es un servicio.')

		res = line.register_stage(stage, raise_error=False)
		
		if res.get('status') != 'OK':
			return self._get_error_empty_values(res.get('message', 'Error al registrar etapa de %s' % stage.name.upper()))

		return {
				'production_order':line.order_prod_id.id,
				'nro_cristal':line.nro_cristal,
				'messageline':'',
				'crystal_path_pdf':path_pdf,
				}

	def _get_error_empty_values(self, error_message):
		return {
			'production_order':False,
			'nro_cristal':False,
			'image_glass':False,
			'crystal_path_pdf':False,
			'messageline': error_message or '',
			}

	def view_pdf(self):
		file_pdf = self.stage_name if self.stage_name in ('entalle','lavado') else False
		base_url = self.env['ir.config_parameter'].sudo().get_param('web.base.url')
		
		if self._context.get('show_complete_pdf'):
			path_pdf = self.production_order.croquis_path
		else:
			path_pdf = self.crystal_path_pdf
		if file_pdf and path_pdf and self.path and base_url:
			import random
			path = self.path.replace('previsualizacion_op',file_pdf)
			with open(path_pdf,'rb') as file:
				content = file.read()
				with open(path,'wb') as file_new:
					file_new.write(content)
			url = '%s/import_base_it/static/%s.pdf?t=%d'%(base_url,file_pdf,random.randrange(1, 100000))
			return {
				"type": "ir.actions.act_url",
				"url": url,
				"target": "new",
			}
		else:
			return {"type": "ir.actions.do_nothing",}

	@api.model
	def insert_blank_page(self,path):
		writer = PyPDF2.PdfFileWriter()
		writer.insertBlankPage(width=500, height=500, index=0)
		with open(path, "wb") as outputStream: 
			writer.write(outputStream) #write pages to new PDF