# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round,float_compare

class GlassBreakAptCrystals(models.TransientModel):
	_name = 'glass.break.apt.crystals'

	order_id = fields.Many2one('glass.order',string=u'Órden de Prod.',domain=[('state','in',('process','ended'))])
	detail_lines = fields.One2many('glass.break.apt.crystals.line','wizard_id',string='Detalle')
	picking_type_id = fields.Many2one('stock.picking.type',
		string=u'Tipo de picking',
		domain=lambda self: self._context.get('dom_picking_type',[]))
	location_src_id = fields.Many2one(related='picking_type_id.default_location_src_id',string='Ubicación origen',readonly=True)
	location_dest_id = fields.Many2one(related='picking_type_id.default_location_dest_id',string='Ubicación destino',readonly=True)
	
	traslate_motive_id = fields.Many2one('einvoice.catalog.12',string=u'Motivo de traslado')
	stage_id = fields.Many2one('glass.stage',string=u'Etapa de rotura',domain=[('name','in',('ingresado','lavado','templado','insulado'))])
	# xD
	return_in_process = fields.Boolean('Devolución en proceso')

	@api.multi
	def get_new_element(self):
		wizard = self.create({})
		picking_type_return_apt_ids = self.env['glass.order.config'].search([],limit=1).picking_type_return_apt_ids
		ctx = dict(self._context or {},dom_picking_type=[('id','in',picking_type_return_apt_ids.ids)])
		return {
			'res_id':wizard.id,
			'name':'Devolución APT',
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'target': 'new',
			'context': ctx,
		}

	@api.model
	def default_get(self,default_fields):
		res = super(GlassBreakAptCrystals,self).default_get(default_fields)
		conf = self.env['glass.order.config'].search([],limit=1)
		if not conf.traslate_motive_return_pt:
			raise UserError(u'No existe el motivo de traslado para retorno de APT en la configuración de Producción')
		res.update({
			'traslate_motive_id':conf.traslate_motive_return_pt.id,})
		return res

	def search_crystals(self):
		self.detail_lines.unlink()
		lines = self.order_id.line_ids.mapped('lot_line_id')
		lines = self._verify_lines()
		self.write({'detail_lines':[(0,0,{
			'glass_line_id':l['glass_line_id'],
			'lot_line_id':l['lot_line_id'],
		}) for l in lines]})
		return {"type": "ir.actions.do_nothing",}

	def _verify_lines(self):
		if self.return_in_process:
			query = """
			SELECT 
			gol.id AS glass_line_id,
			gll.id AS lot_line_id
			FROM glass_order_line gol 
			JOIN glass_lot_line gll ON gll.id = gol.lot_line_id
			WHERE 
			--gll.ingresado = true
			gll.location_transfer_id IS NOT NULL
			AND (gll.entregado = false OR gll.entregado IS null)
			AND (gll.is_break = false OR gll.is_break IS null)
			AND (gol.in_packing_list = false OR gol.in_packing_list IS null)
			--AND gol.state = 'instock' 
			AND gol.order_id = %s 
			ORDER BY gll.nro_cristal_fl"""
		else:
			query = """
			SELECT 
			gol.id AS glass_line_id,
			gll.id AS lot_line_id
			FROM glass_order_line gol 
			JOIN glass_lot_line gll ON gll.id = gol.lot_line_id
			WHERE 
			gll.ingresado = true
			AND (gll.entregado = false OR gll.entregado IS null)
			AND (gll.is_break = false OR gll.is_break IS null)
			AND (gol.in_packing_list = false OR gol.in_packing_list IS null)
			AND gol.state = 'instock' 
			AND gol.order_id = %s 
			ORDER BY gll.nro_cristal_fl"""

		self._cr.execute(query,(self.order_id.id,))
		results = self._cr.dictfetchall()
		return results

	def return_crystals(self):
		self.ensure_one()
		selected_lines = self.detail_lines.filtered(lambda i: i.check)
		if not selected_lines:
			raise UserError('Debe seleccionar un(os) cristal(es) para devolver.')
		
		bad_items = selected_lines.filtered(lambda x: not x.motive)
		bad_detail = lambda l: 'OP: %s - Nro: %s'%(l.order_id.name,l.crystal_num)
		if any(bad_items):
			raise UserError(u'Los siguientes cristales no pueden ser rotos/devueltos:\n%s\nMotivo: No ha especificado el motivo de rotura.'%'\n'.join(bad_items.mapped(bad_detail)))

		# verificación de integridad
		allowed_ids = map(lambda r: r['lot_line_id'],self._verify_lines()) 
		bad_items = [i for i in selected_lines if i.lot_line_id.id not in allowed_ids]

		if any(bad_items):
			raise UserError('Los siguientes cristales no pueden ser rotos/devueltos:\n%s\nEs posible que ya hayan sido devueltos, estén en un packing list o ya se hayan entregado al cliente en otra sesión de Odoo.'%'\n'.join(bad_items.mapped(bad_detail)))

		products = selected_lines.mapped('product_id')
		pick_vals = self._prepare_pick_vals()
		pick = self.env['stock.picking'].create(pick_vals)
		move_vals = []
		for product in products:
			filt = selected_lines.filtered(lambda l: l.product_id==product)
			quantity = float_round(sum(filt.mapped('area')),precision_rounding=0.0001)
			glass_ids = filt.mapped('glass_line_id').ids
			move_vals.append((0,0,self._prepare_move_vals(product,quantity,glass_ids)))
		pick.write({'move_lines':move_vals})
		pick.action_confirm()
		pick.action_assign()
		#TODO SI no hay stock disponible se debería forzar la diponibilidad?? por ahora si aunque los saldos y quants se va a ir a la mrd:(
		if pick.state != 'assigned':
			pick.force_assign()
		if pick.state == 'assigned':
			for op in pick.pack_operation_ids:
				op.write({'qty_done':op.product_qty})
			pick.action_done()
			# action = pick.do_new_transfer()
			# if type(action) is dict and action['res_model'] == 'stock.immediate.transfer':
			# 	context = action['context']
			# 	sit = self.env['stock.immediate.transfer'].with_context(context).create({'pick_id':pick.id})	
			# 	res = sit.process()
		
		# FIXME Debería registrarse las etapas si no se consigue transferir el albarán? si el tipo de 
		# de picking es de producción no habría problemas al transferir, pero si no uhmm XDXD
		#ing_stage = self.env['glass.stage'].search([('name','=','ingresado')],limit=1)
		note = u'Rotura de cristal en APT'
		glass_lines = selected_lines.mapped('glass_line_id')
		break_info = dict.fromkeys(glass_lines.ids)
		for line in selected_lines:
			motive = dict(line._fields['motive'].selection).get(line.motive,'No especificado')
			break_info[line.glass_line_id.id] = {'motive':motive,'note':note}
		glass_lines.with_context(apt_returned=True).break_crystal(self.stage_id,break_info)

		action = self.env.ref('stock.action_picking_tree_all').read()[0]
		action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
		action['res_id'] = pick.id
		return action

	def _prepare_pick_vals(self):
		current_date = fields.Date.context_today(self)
		pick_type = self.picking_type_id
		return {
			'partner_id':False,
			'picking_type_id':pick_type.id,
			'date':current_date,
			'fecha_kardex':current_date,
			'location_dest_id':pick_type.default_location_dest_id.id,
			'location_id': pick_type.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': self.traslate_motive_id.id,
		}

	def _prepare_move_vals(self,product,qty,glass_ids):
		current_date = fields.Date.context_today(self)
		pick_type = self.picking_type_id
		return {
		'name': product.name,
		'product_id': product.id,
		'product_uom': product.uom_id.id,
		'date':current_date,
		'date_expected':current_date,
		'picking_type_id': pick_type.id,
		'location_id': pick_type.default_location_src_id.id,
		'location_dest_id': pick_type.default_location_dest_id.id,
		'partner_id': False,
		'move_dest_id': False,
		'state': 'draft',
		'company_id':self.env.user.company_id.id,
		'procurement_id': False,
		'route_ids':pick_type.warehouse_id and [(6,0,pick_type.warehouse_id.route_ids.ids)] or [],
		'warehouse_id': pick_type.warehouse_id and pick_type.warehouse_id.id or False,
		'product_uom_qty':qty,
		'glass_order_line_ids':[(6,0,glass_ids or [])]
		}

class GlassBreakAptCrystalsLine(models.TransientModel):
	_name = 'glass.break.apt.crystals.line'

	wizard_id = fields.Many2one('glass.break.apt.crystals',string='Wizard')
	glass_line_id = fields.Many2one('glass.order.line',string='Order Line')
	check = fields.Boolean(string='Seleccionado')
	lot_line_id = fields.Many2one('glass.lot.line')
	order_id = fields.Many2one(related='glass_line_id.order_id')
	#lot_id  = fields.Many2one(related='lot_line_id.lot_id',string='Lote')
	location_transfer_id = fields.Many2one(related='lot_line_id.location_transfer_id')
	base1   = fields.Integer(related='glass_line_id.base1',string='Base 1')
	base2   = fields.Integer(related='glass_line_id.base2',string='Base 2')
	height1 = fields.Integer(related='glass_line_id.altura1',string='Altura 1')
	height2 = fields.Integer(related='glass_line_id.altura2',string='Altura 2')
	crystal_num =  fields.Char(related='glass_line_id.crystal_number',string='Num. Cristal')
	product_id = fields.Many2one(related='glass_line_id.product_id',string='Producto')
	area = fields.Float(related='glass_line_id.area',string='Area')
	templado  = fields.Boolean(related='lot_line_id.templado',string='Templado')
	ingresado = fields.Boolean(related='lot_line_id.ingresado',string='Ingresado')
	entregado = fields.Boolean(related='lot_line_id.entregado',string='Entregado')
	packing_list = fields.Boolean(related='glass_line_id.in_packing_list',string='Packing List')
	is_break = fields.Boolean(related='lot_line_id.is_break')
	motive = fields.Selection([
		('Vidrio roto','Vidrio roto'), 
		('Error entalle','Error entalle'), 
		('Error medidas','Error medidas'), 
		('Vidrio rayado','Vidrio rayado'), 
		('Planimetria','Planimetria'), 
		('Error ventas','Error ventas'), 
		('Materia prima','Materia prima')],string='Motivo de Rotura')