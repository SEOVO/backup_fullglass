# -*- coding: utf-8 -*-
from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta

class BuildGlassOrderWizard(models.TransientModel):
	_name='build.glass.order.wizard'

	sale_id = fields.Many2one('sale.order',string='Orden de venta',required=True)
	selected_file_id = fields.Many2one('glass.pdf.file',
		string='Archivo a incluir', 
		domain=lambda self: self._context.get('file_domain', []))
	file_name = fields.Char('Nombre del archivo',related="selected_file_id.pdf_name")
	destinity_order = fields.Selection([
		('local','En la ciudad'),
		('external','En otra ciudad')], 
		string=u'Lugar de entrega', default="local")
	send2partner = fields.Boolean('Entregar en ubicacion del cliente',default=False)
	in_obra = fields.Boolean('Entregar en Obra')
	obra_text = fields.Char(u'Descripción de Obra')
	comercial_area=fields.Selection([
		('distribucion', u'Distribución'),
		('obra', 'Obra'),
		('proyecto', 'Proyecto')], 
		string=u'Área Comercial', default="distribucion")

	sketch_required = fields.Boolean('Requiere croquis', compute='_compute_require_sketch', readonly=True)
	

	@api.depends('sale_id')
	def _compute_require_sketch(self):
		for wiz in self:
			wiz.sketch_required = wiz.sale_id.type_sale in ('production',)

	def _get_config_default(self):
		config = self.env['glass.order.config'].search([],limit=1)
		if not config:
			raise UserError(u'No se encontraron los valores de configuración de producción')
		return config.id
	
	config_id = fields.Many2one('glass.order.config',string='Config',default=_get_config_default)

	def create_production_order(self):
		"""Método para crear una Órden de producción, éste debería ser la única forma de crear 
		una órden de producción, ya que valida todo lo necesario"""
		self.ensure_one()
		config = self.config_id
		self.sale_id.validate_sale_pay_terms('generate_op',config)

		order_old =  None  #O P devuelta
		gen_orders = self.sale_id.op_ids.filtered(lambda o: o.state != 'returned') # sale op's
		if gen_orders and not self.sale_id.before_invoice:
			raise UserError(u"El pedido %s ya tiene órdenes de producción asignadas" % self.sale_id.name)
		pendings = self.sale_id.op_returned_ids.filtered(lambda x: not x.corrected)
		if pendings:
			order_old = pendings[0] # Se subsana la primera de las órdenes devueltas 
			# excluir el 'DEV-' (una orden devuelta tiene el prefijo DEV-):
			order_name = order_old.name.replace('DEV-','')
		else:
			order_name = self.sale_id.name
			nextnumber = len(gen_orders)
			if nextnumber > 0:
				order_name = '%s.%d' % (order_name, nextnumber)
		if order_old: order_old.corrected = True

		lines_vals = self._get_glass_line_vals()
		order_vals = self._prepare_glass_order_vals(lines_vals, order_name)
		neworder = self.env['glass.order'].create(order_vals)
		o_lines = neworder.line_ids
		o_lines.mapped('calc_line_id').with_context(force_write=True).write({'glass_order_id': neworder.id,})
		# bloquear entrega manual de albaranes
		pickings = self.sale_id.mapped('picking_ids')
		pickings.write({'sale_picking': True})
		self.selected_file_id.write({
			'is_editable': False,
			'is_used': True,
			'op_id': neworder.id,
		})
		# build pdf page files
		o_lines._build_pdf_page()
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name': neworder.name,
			'res_id': neworder.id,
			'type': 'ir.actions.act_window',
			'res_model': neworder._name,
			'view_id': self.env.ref('%s.view_glass_order_form'%module).id,
			'view_mode': 'form',
			'view_type': 'form',
			}
		
	def _prepare_glass_order_vals(self,glass_line_vals,order_name):
		## make O.P. name
		# param: glass_line_vals = [{values},{values},...]
		# param: order_name = name of order

		if not any(glass_line_vals):
			raise UserError(u"No se han encontrado líneas de calculadora pendientes de Órden de Producción.")

		return {
			'sale_order_id': self.sale_id.id,
			'name': order_name,
			'date_order': datetime.now(),
			'file_name': self.file_name,
			'obra': self.obra_text,
			'destinity_order': self.destinity_order,
			'send2partner': self.send2partner,
			'in_obra': self.in_obra,
			'croquis_path': self.selected_file_id.path_pdf or False,
			'comercial_area': self.comercial_area,
			'reference_order': self.sale_id.reference_order or '',
			'state':'confirmed',
			'line_ids':[(0, 0, values) for values in glass_line_vals]
		}

	#def _get_production_limit(self):
		"""Obtener límites de producción, heredable
			Params: motives: lista de product_ids 
			obtener la con mayor fecha de despacho.
		"""
	#	pass

	@api.model
	def _get_glass_line_vals(self):
		items = []
		for o_line in self.sale_id.order_line:
			if not o_line.calculator_id:
				continue
			to_produce_lines = o_line.calculator_id._get_lines_to_produce()
			for p_line in to_produce_lines.sorted(key=lambda x: x.calculator_id.product_id.id):
				p_line._update_measures_label() # refresh
				for num in p_line.get_crystal_numbers():
					items.append(self._prepare_glass_line_vals(p_line, num))
		return items

	@api.model
	def _prepare_glass_line_vals(self, calc_line, crystal_number):
		"""Destinado a herencia"""
		return {
				'product_id': calc_line.calculator_id.product_id.id, # cristales comunes
				'calc_line_id': calc_line.id,
				'crystal_number': str(crystal_number),
				'area': calc_line.unit_area,
			}
