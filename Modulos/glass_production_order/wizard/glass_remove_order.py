# -*- encoding: utf-8 -*-
from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime
import codecs

class GlassRemoveOrder(models.TransientModel):
	_name = 'glass.remove.order'

	date_remove = fields.Date('Fecha',default=lambda self:fields.Date.context_today(self))
	motive_remove = fields.Text('Motivo')
	order_id = fields.Many2one('glass.order',string=u'Órden de prod.')
	#order_name = fields.Char(string='Órden de Producción')

	def remove_order(self):
		#order = self.env['glass.order'].browse(self._context.get('order_id'))
		order = self.order_id
		op_name = order.name
		if order.state not in ('draft', 'confirmed'):
			raise UserError(u'La O.P. %s debe estar en estado "Generada" o "Emitida" para poder ser devuelta.' % op_name)
		used_lines = order.line_ids.filtered(lambda x: x.is_used or x.lot_line_id)
		if any(used_lines):
			raise UserError(u'No se puede retirar la O.P. %s.\nUno o varios elementos de esta orden de producción ya se encuentran en los lotes de producción.' % op_name)		

		# si no es factura adelantada:
		sale_order = order.sale_order_id
		partner = sale_order.partner_id.name
		seller = sale_order.user_id.partner_id
		if not sale_order.before_invoice:
			#invoices = sale_order.invoice_ids
			#inv_payed = invoices.filtered(lambda x: x.state=='paid')
			#if inv_payed:
			#	msg = '\n'.join(inv_payed.mapped(lambda x: '- '+x.number))
			#	raise UserError(u'La siguientes facturas ya se encuentran en estado pagado:\n%s\nEs necesario anular los pagos asociados para devolver esta O.P.'%msg)
			#invoices.filtered(lambda x: x.state in ('draft','proforma','open')).action_invoice_cancel()
			sale_order.with_context(force_action_cancel=True).action_cancel()
			sale_order.action_draft() 
		
		order.line_ids.mapped('calc_line_id').with_context(force_write=True).write({'glass_order_id':False})
		order.line_ids.unlink()
		pdf_files = self.env['glass.pdf.file'].search([('op_id','=',order.id)])
		pdf_files.write({'is_used':False,'is_editable':True})
		
		label_op = u'Producción' if order.type_sale == 'production' else 'servicio'
		msg = u'El usuario %s ha retirado la Órden de %s: <strong>%s</strong></br><ul>' % (self.env.user.name, label_op, op_name)
		
		if self.motive_remove:
			msg += u'<li><strong>Motivo:</strong>&nbsp;%s</br>' % self.motive_remove.strip()
		if self.date_remove:
			msg += u'<li><strong>Fecha de retiro:</strong>&nbsp;%s</br>' % str(self.date_remove)

		msg += u"""
		<li><strong>Cliente:</strong>&nbsp;%s</li>
		<li><strong>Vendedor:</strong>&nbsp;%s</li>
		</ul>
		""" % (partner, seller.name)

		subject = u'Retiro de O.P. %s, Cliente: %s. Vendedor(a): %s.' % (op_name, partner, seller.name)
		
		sender = self.env['send.email.event'].create({
			'subject': subject,
			'message': msg,
		})
		res = sender.send_emails(motive='op_returned', extra_users=seller) 
		order.write({'state':'returned','name': 'DEV-' + op_name})
		return res



