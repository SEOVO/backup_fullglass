# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class Detail_Crystals_Entered_Wizard(models.TransientModel):
	_name = 'detail.crystals.entered.wizard'
	detail_lines = fields.One2many('detail.crystals.entered.wizard.line','wizard_id')

class Detail_Crystals_Entered_Wizard_Lines(models.TransientModel):
	_name = 'detail.crystals.entered.wizard.line'
	
	check = fields.Boolean(string='Seleccion')
	wizard_id = fields.Many2one('detail.crystals.entered.wizard')
	move_id = fields.Many2one('stock.move')
	glass_line_id = fields.Many2one('glass.order.line')
	lot_line_id = fields.Many2one('glass.lot.line')
	order_id = fields.Many2one(related='glass_line_id.order_id')
	lot_id  = fields.Many2one(related='lot_line_id.lot_id',string='Lote')
	base1   = fields.Integer(related='glass_line_id.base1',string='Base 1')
	base2   = fields.Integer(related='glass_line_id.base2',string='Base 2')
	height1 = fields.Integer(related='glass_line_id.altura1',string='Altura 1')
	height2 = fields.Integer(related='glass_line_id.altura2',string='Altura 2')
	crystal_num =  fields.Char(related='glass_line_id.crystal_number',string='Num. Cristal')
	quantity    = fields.Float(related='move_id.product_qty') 
	picking_id  = fields.Many2one(related='move_id.picking_id') 
	product_id = fields.Many2one(related='glass_line_id.product_id',string='Producto')
	area = fields.Float(related='glass_line_id.area',string='Area')
	templado  = fields.Boolean(related='lot_line_id.templado',string='Templado')
	ingresado = fields.Boolean(related='lot_line_id.ingresado',string='Ingresado')
	entregado = fields.Boolean(related='lot_line_id.entregado',string='Entregado')
	packing_list = fields.Boolean(related='glass_line_id.in_packing_list',string='Packing List')
	req_id = fields.Many2one(related='lot_line_id.lot_id.requisition_id',string='Requisicion')
	is_break = fields.Boolean(related='lot_line_id.is_break')

