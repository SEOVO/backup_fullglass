# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError,ValidationError
from odoo.tools.float_utils import float_round,float_compare

class RequisitionMaterialWizard(models.TransientModel):
	_name = 'requisition.material.wizard'

	mode = fields.Selection([
		('mat_prima','Materia prima'),
		('scraps','Retazos'),
		('return_scraps',u'Devolución de retazos'),
		('mat_prima_services', u'Materia prima para servicios')],
		required=True, default='mat_prima')
	
	requisition_id = fields.Many2one('glass.requisition',string=u'Requisición',required=True)
	line_ids = fields.One2many('requisition.material.wizard.line','wizard_id',string='Detalle')

	def do_process(self):
		self.ensure_one()
		pick_type,tras_motive,rt_loc = self.requisition_id._get_pick_params(self.mode)
		pick_vals = self._prepare_pick_vals(pick_type,tras_motive)
		Scrap = self.env['scraps.entry.wizard']
		
		# Materias primas:
		move_vals = []
		if   self.mode=='mat_prima':
			relation = 'requisition_mp_id'
			move_vals = [(0,0,self._prepare_move_vals(l.product_id,min(l.quantity,l.qty_available),pick_type,relation)) for l in self.line_ids if l.quantity > 0.0]
		
		elif self.mode=='scraps':
			relation = 'requisition_rt_id'
			lines = self.line_ids.filtered(lambda l: l.quantity > 0.0)
			move_vals = [
				(0,0,self._prepare_move_vals(l.product_id,min(l.quantity,l.qty_available),pick_type,relation,l.uom_id)) 
				for l in self.line_ids if l.quantity > 0.0]
		
		elif self.mode=='return_scraps':
			relation = 'requisition_drt_id'
			lines = self.line_ids.filtered(lambda l: l.quantity > 0.0)
			dict_uoms = Scrap._get_uom_plancha(['%d_%d'%(l.width,l.height) for l in lines],make_if_not_exists=True)
			for line in lines:
				width,height = line.width,line.height
				key = '%d_%d'%(width,height)
				line.uom_id = dict_uoms.get(key)

			move_vals = [(0,0,self._prepare_move_vals(l.product_id,l.quantity,pick_type,relation,l.uom_id)) for l in lines]
		if not any(move_vals):
			raise UserError(u'No hay líneas a procesar.')
		pick_vals['move_lines'] = move_vals

		picking = self.env['stock.picking'].create(pick_vals)
		
		picking.action_confirm()
		picking.action_assign()
		if picking.state!='assigned':
			picking.force_assign()
		if picking.state=='assigned':
			if self.mode == 'mat_prima':
				#transferencia normal de planchas en la uom del producto
				for op in picking.pack_operation_ids:
					op.write({'qty_done':op.product_qty})
				picking.action_done()
			elif self.mode in ('scraps','return_scraps'):
				Scrap.sudo().transfer_scraps_picking(picking,force_assign=True)

		return {
			'name':picking.name,
			'res_id':picking.id,
			'type': 'ir.actions.act_window',
			'res_model': picking._name,
			'view_mode': 'form',
		}

	def _prepare_pick_vals(self,pick_type,pick_motive):
		current_date = fields.Date.context_today(self)
		return {
			'partner_id':False,
			'picking_type_id':pick_type.id,
			'date':current_date,
			'origin':self.requisition_id.name,
			'fecha_kardex':current_date,
			'location_dest_id':pick_type.default_location_dest_id.id,
			'location_id': pick_type.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': pick_motive.id,
		}

	def _prepare_move_vals(self,product,qty,pick_type,relation,uom_id=False):
		current_date = fields.Date.context_today(self)
		uom = uom_id or product.uom_id
		return {
			'name': product.name,
			'product_id': product.id,
			'product_uom': uom.id,
			'date':current_date,
			'date_expected':current_date,
			'picking_type_id': pick_type.id,
			'location_id': pick_type.default_location_src_id.id,
			'location_dest_id': pick_type.default_location_dest_id.id,
			'partner_id': False,
			'move_dest_id': False,
			'origin':self.requisition_id.name,
			'state': 'draft',
			'company_id':self.env.user.company_id.id,
			'procurement_id': False,
			'route_ids':pick_type.warehouse_id and [(6,0,pick_type.warehouse_id.route_ids.ids)] or [],
			'warehouse_id': pick_type.warehouse_id and pick_type.warehouse_id.id or False,
			'product_uom_qty':float_round(qty,precision_rounding=1), # las cantidades deberían ser siempre por planchas completas,
			relation:self.requisition_id.id,
		}

class RequisitionMaterialWizardLine(models.TransientModel):
	_name = 'requisition.material.wizard.line'

	wizard_id = fields.Many2one('requisition.material.wizard',string='Wizard')
	product_id = fields.Many2one('product.product',string='Producto',domain=lambda self: self._context.get('dom_scraps_prods',[]))
	quantity = fields.Float('Cantidad',default=0.0,help=u'Cantidad solicitada')
	qty_available = fields.Float('Disponible',default=0.0)
	uom_id = fields.Many2one('product.uom',string=u'Unidad de medida',domain=[('plancha','=',True)])
	width = fields.Integer('Ancho')
	height = fields.Integer('Alto')

	@api.constrains('width','height')
	def _verify_height_width(self):
		for line in self.filtered(lambda l: l.width and l.height):
			if line.width > line.height:
				raise ValidationError(u'El ancho no puede tener una medida mayor al alto.')
	
	@api.model
	def create(self, values):
		t = super(RequisitionMaterialWizardLine,self).create(values)
		if t.wizard_id.mode=='scraps':
			t.width = t.uom_id.ancho
			t.height = t.uom_id.alto
		return t
	