# -*- coding: utf-8 -*-

from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
from odoo.tools.float_utils import float_round

class GlassInProductionWizard(models.TransientModel):
	_name='glass.in.production.wizard'
	
	stock_type_id = fields.Many2one('stock.picking.type',string=u'Operación de almacén',domain=lambda self: self._context.get('dom_picking_type_pt',[])) 
	date_in = fields.Date('Fecha ingreso',default=lambda self: fields.Date.context_today(self))
	start_date = fields.Date(default=datetime.now().date())
	end_date = fields.Date(default=datetime.now().date())
	order_ids = fields.One2many('glass.in.order','mainid','order_id')
	line_ids = fields.Many2many('glass.order.line','glass_in_lineorder_rel','in_id','line_id',string="Lineas")
	order_id = fields.Many2one('glass.order', string='Filtrar OP', domain="[('type_sale', '=', 'production')]")
	search_param = fields.Selection([('glass_order','Orden de Produccion'),('search_code','Lectura de barras')],string='Busqueda por',default='search_code')
	message_erro = fields.Char()
	location_id  = fields.Many2one('custom.glass.location',string=u'Ubicación')
	search_code  = fields.Char(string=u'Codigo de búsqueda')
	count_crystals = fields.Integer(u'Nro de Cristales',compute='_get_count_crystals')
	limit_entry_crystals = fields.Integer(u'Límite de cristales a ingresar', default=0)
	#is_entry_packing_list = fields.Boolean('Ingreso para Packing List',
		#help=u'Define si éste es un ingreso con cristales para enviarse por packing list.')
	# usado solo para obviar la validación de almacén ya que los que se envían por PK 
	#necesitan ingresar al APT AQP, siendo su almacén APT Lima

	def get_new_element(self):
		wizard = self.create({})
		picking_type_pt_ids = self.env['glass.order.config'].search([],limit=1).picking_type_pt_ids
		ctx = dict(self._context or {},dom_picking_type_pt=[('id','in',picking_type_pt_ids.ids)])
		return {
			'res_id':wizard.id,
			'name':'Ingreso a la Produccion',
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'target': 'new',
			'context': ctx,
		}

	@api.depends('line_ids')
	def _get_count_crystals(self):
		for rec in self:
			rec.count_crystals = len(rec.line_ids)

	def get_all_available(self):
		self.ensure_one()
		#start = datetime.strptime(self.start_date)
		if not self.start_date or not self.end_date:
			raise UserError('No ha colocado fechas de Inicio y/o fin.')
		domain = [('date','<=',self.end_date), 
				('date','>=',self.start_date),
				('stage_id.name','=','producido'),
				('done','=',True),
				('lot_line_id.insulado','=',False),
				('lot_line_id.is_service', '=', False)]
		
		limit = self.limit_entry_crystals if self.limit_entry_crystals > 0 else None
		
		stages = self.env['glass.stage.record'].search(domain)
		# TODO FIXME mejorar ésto para no iterar de más.
		lines = stages.mapped('lot_line_id.order_line_id').filtered(lambda x:x.state=='ended')
		
		for i, item in enumerate(lines, 1):
			if item.id not in self.line_ids.ids:
				item.location_tmp = self.location_id.id
				self.write({'line_ids':[(4,item.id)]})
				if limit and i >= limit:
					break
		return {"type": "ir.actions.do_nothing",}

	# @api.multi
	# def refresh_selected_lines(self):
	# 	will_removed = self.line_ids.filtered(lambda x: x.order_id.id not in self.order_ids.mapped('order_id').ids)
	# 	for item in will_removed.ids:
	# 		self.write({'line_ids':[(3,item)]})
	# 	return {"type": "ir.actions.do_nothing",}

	#@api.depends('line_ids')
	@api.onchange('search_code')
	def onchangecode(self):
		limit = self.limit_entry_crystals if self.limit_entry_crystals > 0 else None
		if limit and self.count_crystals >= limit:
			return {}

		if self.search_code:
			existe = self.env['glass.lot.line'].search([('search_code', '=', self.search_code), ('is_service', '=', False)])
			if len(existe)==1:
				line = existe.order_line_id
				if line.state!='ended':
					self.message_erro = u'El cristal %s no se encuentra en estado finalizado'%existe.search_code
					self.search_code=""
					return
				if not existe.producido:
					self.message_erro = u'El cristal %s no se encuentra producido' % existe.search_code
					self.search_code=""
					return
				if existe.insulado:
					self.message_erro = u'Los cristales para insulado no pueden ingresarse por ésta vía'
					self.search_code=""
					return
				this_obj=self.env['glass.in.production.wizard'].browse(self._origin.id) 
				if line not in this_obj.line_ids:
					line.location_tmp = self.location_id.id
					this_obj.write({'line_ids':[(4,line.id)]})
					self.search_code=""
					#return {'value':{'line_ids':this_obj.line_ids.ids,'order_ids':this_obj.order_ids.ids}}
					return {'value':{'line_ids':this_obj.line_ids.ids}}
				else:
					self.message_erro = 'El registro ya se encuentra en la lista'
					self.search_code=""
			else:
				self.message_erro="Registro no encontrado!"
				self.search_code=""
				return
		else:
			return

	def get_crystals_op(self):
		limit = self.limit_entry_crystals if self.limit_entry_crystals > 0 else None
		lines = self.order_id.line_ids.filtered(
			lambda x: x.state=='ended' and not x.lot_line_id.insulado and x.lot_line_id.producido and x.id not in self.line_ids.ids
			)
		values = []
		for i, item in enumerate(lines, 1):
			item.location_tmp = self.location_id.id
			values.append((4,item.id))
			if limit and i >= limit:
				break
		self.write({'line_ids':values})
		return {"type": "ir.actions.do_nothing",}


	# @api.onchange('order_id')
	# def onchange_order_id(self):
	# 	vals = {}
	# 	aorder=[]
	# 	print('la ptm x2 ')
	# 	for existente in self.order_ids:
	# 		vals={
	# 			'selected':existente.selected,
	# 			'order_id':existente.order_id.id,
	# 			'partner_id':existente.partner_id.id,
	# 			'date_production':existente.date_production,
	# 			'total_pzs':existente.total_pzs,
	# 			'total_area':existente.total_area,
	# 			}
	# 		aorder.append((0,0,vals))
	# 	if self.order_id:
	# 		vals={
	# 			'selected':True,
	# 			'order_id':self.order_id.id,
	# 			'partner_id':self.order_id.partner_id.id,
	# 			'date_production':self.order_id.date_production,
	# 			'total_pzs':self.order_id.total_pzs,
	# 			'total_area':self.order_id.total_area,
	# 			}
	# 		aorder.append((0,0,vals))
	# 	return {'value':{'order_ids':aorder}}

	#@api.depends('line_ids')
	# @api.onchange('order_ids')
	# def getlines(self):	
	# 	lines = self.order_ids.filtered(lambda x:x.selected).mapped('order_id.line_ids').filtered(lambda x:x.state=='ended' and not x.lot_line_id.insulado)
	# 	print('esta mrd lo está cagando')
	# 	if len(lines)>0:
	# 		this_obj = self.env['glass.in.production.wizard'].browse(self._origin.id)
	# 		for item in lines:
	# 			if item not in this_obj.line_ids:
	# 				item.location_tmp = self.location_id.id
	# 				this_obj.write({'line_ids':[(4,item.id)]})
	# 		return {'value':{'line_ids':this_obj.line_ids.ids}}

	def makeingreso(self):
		lines = self.line_ids.filtered(
			lambda l: l.lot_line_id and not l.lot_line_id.ingresado and l.lot_line_id.producido and not l.is_service
			) 
		# prevenir re-ingresos de otra sesión
		if not lines:
			raise UserError(u'No hay líneas de cristales a transferir')
		# Verificar que el destino sea el mismo (provisional)
		# Debería haber sólo una ubicación APT destino. los de Lima la están cagando grrr
		
		# Antes se validaba la consistencia de los almacenes, pero 
		# no les gustó y si se huevean me llega al chómpiras csm
		
		# wh_id = lines.mapped('order_id.warehouse_id') 
		# if len(wh_id) > 1:
		# 	wh_id = ', '.join(wh_id.mapped('name'))
		# 	raise UserError(u'Las líneas a transferir contienen más de un almacén fuente: %s\n Sólo puede ingresar los cristales de una sola ubicación APT (Lima o Arequipa por ejemplo)'%wh_id)
		
		# wh_dest = self.stock_type_id.default_location_dest_id.get_warehouse()
		# if wh_id.id != wh_dest.id and not self.is_entry_packing_list:
		# 	raise UserError(u'El almacén de destino: "%s" no coincide con el almacén de entrega (%s) de la órden de producción,\nEs posible que haya seleccionado un tipo de picking incorrecto.\nSi se trata de un crital para Packing list, debe marcar la casilla "Ingreso para Packing List"'%(wh_dest.name,wh_id.name))

		traslate_motive_pt = self.env['glass.order.config'].search([],limit=1).traslate_motive_pt
		if not traslate_motive_pt:
			raise UserError(u'No se encontraron los valores de configuración de producción')
		self.verify_constrains(self.date_in)
		current_date = fields.Date.context_today(self)
		picking = self.env['stock.picking'].create({
			'picking_type_id': self.stock_type_id.id,
			'partner_id': None,
			'date': current_date,
			'fecha_kardex': self.date_in,
			#'origin': order.name,
			'apt_in':True,
			'location_dest_id': self.stock_type_id.default_location_dest_id.id,
			'location_id': self.stock_type_id.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': traslate_motive_pt.id,
		})

		for prod in lines.mapped('product_id'):
			filt = lines.filtered(lambda x: x.product_id.id == prod.id)
			total_area = float_round(sum(filt.mapped('area')),precision_rounding=0.0001)
			self.env['stock.move'].create({
				'name': prod.name or '',
				'product_id': prod.id,
				'product_uom': prod.uom_id.id,
				'date': current_date,
				'date_expected': current_date,
				'location_id': self.stock_type_id.default_location_src_id.id,
				'location_dest_id': self.stock_type_id.default_location_dest_id.id,
				'picking_id': picking.id,
				'partner_id': None,
				'move_dest_id': False,
				'state': 'draft',
				'company_id': self.env.user.company_id.id,
				'picking_type_id': self.stock_type_id.id,
				'procurement_id': False,
				#'origin': order.name,
				'route_ids': self.stock_type_id.warehouse_id and [(6, 0, [x.id for x in self.stock_type_id.warehouse_id.route_ids])] or [],
				'warehouse_id': self.stock_type_id.warehouse_id.id,
				'product_uom_qty': total_area,
				'glass_order_line_ids': [(6,0,filt.ids)]
			})
		
		picking.action_confirm()
		picking.action_assign()
		action = picking.do_new_transfer()
		if type(action) is dict:
			Sit = self.env['stock.immediate.transfer']
			Cdp = self.env['confirm.date.picking']
			if action['res_model'] == Sit._name:
				context = action['context']
				sit = Sit.with_context(context).create({'pick_id':picking.id})	
				sit.process()
			elif action['res_model'] == 'confirm.date.picking':
				context = action['context']
				cdp = Cdp.with_context(context).create({'pick_id':picking.id,'date':picking.fecha_kardex})
				res = cdp.changed_date_pincking()
			
		for line in lines:
			if line.location_tmp:
				line.write({'locations': [(4,line.location_tmp.id)]})
			line.write({
				'state' : 'instock',
				'location_tmp':False,
				})
			line.lot_line_id.with_context(force_register=True).register_stage('ingresado')
		return {
				'name':picking.name,
				'res_id':picking.id,
				'view_mode':'form',
				'res_model':'stock.picking',
				'type':'ir.actions.act_window',
				}

	@api.multi
	def verify_constrains(self,date):
		# validando restricciones de terminos de pago (desactivado temporalmente NO BORRAR):
		# with_pay_terms = self.line_ids.filtered(lambda x: x.order_id.sale_order_id.payment_term_id)
		# if len(with_pay_terms) > 0:
		# 	conf=self.env['config.payment.term'].search([('operation','=','enter_apt')])
		# 	if len(conf) == 1: # solo puede estar en una conf
		# 		msg =''
		# 		for item in with_pay_terms:
		# 			sale = item.order_id.sale_order_id
		# 			if sale.payment_term_id.id in conf[0].payment_term_ids.ids:
		# 				invoice = sale.invoice_ids[0]
		# 				payed = invoice.amount_total - invoice.residual
		# 				percentage = (payed/invoice.amount_total) * 100
		# 				if percentage < conf[0].minimal:
		# 					msg += '-> '+item.crystal_number+' '+item.product_id.name+'\n'
		# 					raise exceptions.Warning('Las facturas de los siguientes cristales no fueron pagadas al '+str(conf[0].minimal)+' %.:\n'+msg)
		# 	else:
		# 		raise UserError('No ha configurado las condiciones para el Plazo de pago al Ingresar a APT')

		# currency_obj = self.env['res.currency'].search([('name','=','USD')])
		# if len(currency_obj)>0:
		# 	currency_obj = currency_obj[0]
		# else:
		# 	raise UserError( 'Error!\nNo existe la moneda USD \nEs necesario crear la moneda USD para un correcto proceso.' )

		# tipo_cambio = self.env['res.currency.rate'].search([('name','=',str(date)),('currency_id','=',currency_obj.id)])

		# if len(tipo_cambio)>0:
		# 	tipo_cambio = tipo_cambio[0]
		# else:
		# 	raise UserError( u'Error!\nNo existe el tipo de cambio para la fecha: '+ str(date) + u'\n Debe actualizar sus tipos de cambio para realizar esta operación')


		# restricción de requisición temporalmente deshabilitada
		# bad_lines = self.line_ids.mapped('lot_line_id').filtered(lambda x: not x.requisicion)
		# if len(bad_lines) > 0:
		# 	msg = ''
		# 	for item in bad_lines:
		# 		msg += u'-> Lote: %s -> Cristal Nro: %s\n.'%(item.lot_id.name,item.nro_cristal)
		# 	raise UserError(u'Los siguientes Cristales no tienen orden de Requisición:\n'+msg+'Recuerde: Los lotes de los cristales a ingresar deben tener Orden de requisición')
		pass


class GlassInOrder(models.TransientModel):
	_name = "glass.in.order"

	selected = fields.Boolean('Seleccionada')
	order_id = fields.Many2one('glass.order','Orden')
	partner_id = fields.Many2one('res.partner','Cliente')
	date_production = fields.Date(u'Fecha de Producción')
	total_pzs = fields.Float("Cantidad")
	total_area = fields.Float(u'M2')
	
	mainid=fields.Many2one('glass.in.production.wizard','mainid')
