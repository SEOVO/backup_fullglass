# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

ALLOW_STAGES = (
	'insulado', 'pegado', 'arenado', 'biselado', 'producido', 'entalle', 
	'perforacion', 'pulido', 'corte_laminado', 'corte_lavado_laminado')


# OLD MODEL
# A remover, no les pareció... :(
class ServicesRegisterWizard(models.Model):
	_name = 'mtf.services.register.wizard'
	_description = u'Control de etapas en servicios'


	stage_id = fields.Many2one('glass.stage', 
		domain=[('name', 'in', ALLOW_STAGES)], string='Etapa/Servicio', help=u'Etapa de servicio a registrar')

	line_ids = fields.Many2many('glass.order.line', 
		relation='glass_service_in_wiz_rel', column1='wizard_id', column2='glass_line_id', string="Lineas")
	#line_ids = fields.One2many('mtf.services.register.wizard.line', 'wizard_id', string=u'Líneas')
	order_id = fields.Many2one('glass.order', string='Filtrar O.S.')
	search_param = fields.Selection([
		('glass_order', u'Orden de Servicio'),
		('search_code', u'Lectura de barras')], string=u'Búsqueda por', default='search_code')
	message_erro = fields.Char('Error msg')
	message_success = fields.Char('Success msg')
	#location_id  = fields.Many2one('custom.glass.location',string=u'Ubicación')
	search_code  = fields.Char(string=u'Código de búsqueda')
	count_crystals = fields.Integer(u'Nro de Cristales', compute='_get_count_crystals')
	

	def get_new_element(self):
		wizard = self.create({})
		# picking_type_pt_ids = self.env['glass.order.config'].search([],limit=1).picking_type_pt_ids
		# ctx = dict(self._context or {},dom_picking_type_pt=[('id','in',picking_type_pt_ids.ids)])
		return {
			'res_id': wizard.id,
			'name':'Registro de servicios',
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'target': 'new',
			'context': dict(self._context or {}),
		}

	@api.depends('line_ids')
	def _get_count_crystals(self):
		for rec in self:
			rec.count_crystals = len(rec.line_ids)

	#@api.depends('line_ids')
	@api.onchange('search_code')
	def onchangecode(self):
		if self.search_code:
			existe = self.env['glass.lot.line'].search([('search_code', '=', self.search_code)], limit=1)
			if existe:

				# TODO verificar si debe validar ésto
				line = existe.order_line_id


				this_obj = self.env['mtf.services.register.wizard'].browse(self._origin.id) 
				
				if line not in self.line_ids.ids:
					#line.location_tmp = self.location_id.id
					this_obj.write({
						'line_ids' : [(4, line.id)],
						'search_code': '',
						})
					#self.search_code = ""
					self.update({
						'message_success': '',
						'search_code': '',
						'message_erro': ''
						})
					#return {'value':{'line_ids':this_obj.line_ids.ids,'order_ids':this_obj.order_ids.ids}}
					return {'value': {'line_ids': this_obj.line_ids.ids}}
				else:
					self.update({
						'message_erro': 'El registro ya se encuentra en la lista',
						'message_success': '',
						'search_code': '',
					})
			else:
				self.update({
					'message_erro': "Registro no encontrado!",
					'message_success': '',
					'search_code': "",
				})
				return {}
		else:
			return {}

	def get_crystals_op(self):
		#existing = self.line_ids.mapped('glass_line_id').ids
		#lines = self.order_id.line_ids.filtered(lambda x: x.is_service and not x.lot_line_id.producido and x.id not in existing)
		existing = self.line_ids.ids
		lines = self.order_id.line_ids.filtered(lambda x: (not x.lot_line_id.producido or not x.is_service) and x.id not in existing)
		self.write({'line_ids': [(4, l.id) for l in lines]})
		return {"type": "ir.actions.do_nothing",}

	def register_stage(self):
		self.message_success = ''
		self._cr.commit()

		if not self.line_ids:
			raise UserError('Debe agregar cristales para registrar los servicios culminados')
		
		check_orders = []
		PROD_STG = ('arenado', 'biselado', 'perforacion')

		for line in self.line_ids:
			#line = line_item.glass_line_id

			# NOTE el pulido y entalle se registra por "Control de la producción"
			if not line.is_service and self.stage_id.name not in PROD_STG:
				raise UserError(u'Los cristales de Ordenes de producción sólo pueden registrar por ésta via las etapas de: \n%s' % '\n- '.join(PROD_STG))

			force_register = False
			
			if self.stage_id.name == 'producido':

				order = line.order_id
				# Mejorar Validación
				if order.id not in check_orders and order.mtf_req_line_ids:
					if not any(mtf_order.state == 'done' for mtf_order in order.mtf_requirement_ids):
						raise UserError(u'La Orden de Servicio %s tiene materiales pendientes de requisición.' % order.name)
					else:
						check_orders.append(order.id)

				#line.lot_line_id.with_context(force_register=True).register_stage(self.stage_id.name)
				all_stages = line.lot_line_id.stage_ids
				post_stages = ['producido', 'ingresado', 'entregado']
				error_msg = '\n'.join([rs.stage_id.name.upper() for rs in all_stages if not rs.done and rs.stage_id.name not in post_stages])
				
				if error_msg:
					raise UserError(u'No es posible dar como producido el cristal de servicio O.S.: %s Nro: %s ya que no pasó por las siguientes etapas solicitadas en ficha maestra: \n%s' % (line.order_id.name, line.crystal_number, error_msg))

				line.state = 'ended'
				# el producido es una etapa que todos deberían tenr, al margen de lo que diga en la ficha
				force_register = True

			line.lot_line_id.with_context(force_register=force_register).register_stage(self.stage_id.name)
		
		self.message_success = u'La etapa de %s se registró con éxito.' % self.stage_id.name.upper()
		return {"type": "ir.actions.do_nothing",}
		

# class ServicesRegisterWizardLine(models.TransientModel):
# 	_name = "mtf.services.register.wizard.line"

# 	wizard_id = fields.Many2one('mtf.services.register.wizard', readonly=True)
# 	glass_line_id = fields.Many2one('glass.order.line', string=u'Línea de servicio', readonly=True)
# 	pending_stages_string = fields.Char(related='glass_line_id.lot_line_id.pending_stages_string', 
# 		readonly=True)
# 	measures = fields.Char(related='glass_line_id.measures', readonly=True)
# 	order_id = fields.Many2one(related='glass_line_id.order_id', readonly=True, string='Ord. de servicio')
# 	partner_id = fields.Many2one(related='glass_line_id.partner_id', string='Cliente', readonly=True)
# 	product_id = fields.Many2one(related='glass_line_id.product_id', string='Producto', readonly=True)
# 	crystal_number = fields.Char(related='glass_line_id.crystal_number', string='Producto', readonly=True)


	#selected = fields.Boolean('Seleccionada')
	#order_id = fields.Many2one('glass.order', string='Orden')
	#partner_id = fields.Many2one('res.partner', string='Cliente')
	#date_production = fields.Date(u'Fecha de Producción')
	#total_pzs = fields.Float("Cantidad")
	#total_area = fields.Float(u'M2')
