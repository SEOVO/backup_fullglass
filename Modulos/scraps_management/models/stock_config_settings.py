# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class StockConfigSettings(models.TransientModel):
	_inherit = 'stock.config.settings'

	uom_plancha_categ_id = fields.Many2one('product.uom.categ',string=u'Cageroría de U. de med. planchas')

	@api.multi
	def set_uom_plancha_categ_id_defaults(self):
		return self.env['ir.values'].sudo().set_default(
			'stock.config.settings', 'uom_plancha_categ_id', self.uom_plancha_categ_id.id)