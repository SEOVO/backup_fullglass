# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from datetime import datetime

class detalle_simple_fisico_total_d_wizard_poliglass(models.TransientModel):
	_name = 'detalle.simple.fisico.total.d.wizard.poliglass'

	fiscalyear_id = fields.Many2one('account.fiscalyear', u'Año fiscal', required=True)
	location_id = fields.Many2one('stock.location',string=u'Ubicación',domain=lambda self: self._context.get('dom_scrap_locations',[]))

	@api.multi
	def get_element(self):
		"""heredable"""
		module = __name__.split('addons.')[1].split('.')[0]
		wizard = self.create({})
		return {
			'name':'Saldos y movimentos de retazos y planchas',
			'res_id': wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_id': self.env.ref('%s.view_detalle_simple_fisico_total_d_wizard_form_poliglass'%module).id,
			'view_mode': 'form',
			'target': 'new',
		}

	@api.model
	def default_get(self, fields):
		res = super(detalle_simple_fisico_total_d_wizard_poliglass,self).default_get(fields)
		n = str(datetime.now().year)
		af = self.env['account.fiscalyear'].search([('name','=',n)])
		res['fiscalyear_id'] = af[0].id if len(af) else False
		return res

	def do_rebuild(self):
		year = self.fiscalyear_id.name
		query = """
			DROP VIEW IF EXISTS detalle_simple_fisico_total_d_poliglass;
			CREATE VIEW detalle_simple_fisico_total_d_poliglass AS (

			SELECT row_number() OVER () AS id,* from (
			SELECT 
			ubicacion as almacen,
			product_id as producto,
			pt.categ_id as categoria,
			unidad as unidad,
			round(sum(stock_disponible) * pu.factor,4) AS saldo,
			round(sum(saldo_fisico) * pu.factor,4) AS saldo_fisico,
			round(sum(por_ingresar) * pu.factor,4) AS por_ingresar,
			round(sum(transito) * pu.factor,4) AS transito,
			round(sum(salida_espera) * pu.factor,4) AS salida_espera,
			round(sum(reservas) * pu.factor,4) AS reservas,
			round(sum(previsto) * pu.factor,4) AS saldo_virtual,

			replace(replace(array_agg(id_stock_disponible)::text,'{','['),'}',']') as id_stock_disponible,
			replace(replace(array_agg(id_saldo_fisico)::text,'{','['),'}',']') as id_saldo_fisico,
			replace(replace(array_agg(id_por_ingresar)::text,'{','['),'}',']') as id_por_ingresar,
			replace(replace(array_agg(id_transito)::text,'{','['),'}',']') as id_transito,
			replace(replace(array_agg(id_salida_espera)::text,'{','['),'}',']') as id_salida_espera,
			replace(replace(array_agg(id_reservas)::text,'{','['),'}',']') as id_reservas,
			replace(replace(array_agg(id_previsto)::text,'{','['),'}',']') as id_previsto

			FROM vst_kardex_onlyfisico_total_poliglass vst_kf
			INNER JOIN product_template pt ON pt.id = product_tmpl_id
			JOIN product_uom pu ON pu.id = vst_kf.unidad
			WHERE vst_kf.date >= '%s-01-01'
			AND vst_kf.date <= '%s-12-31'
			AND ubicacion = %d 
			AND pu.plancha = true
			GROUP BY ubicacion, product_id, unidad, pt.categ_id, pu.factor
			ORDER BY ubicacion, product_id, unidad, pt.categ_id
			) Todo
			);
			"""%(year,year,self.location_id.id)
		self.env.cr.execute(query)
		view_id = self.env.ref('scraps_management.view_kardex_fisico_d_poliglass',False)
		return {
			'name': 'Saldos en planchas',
			'type'     : 'ir.actions.act_window',
			'res_model': 'detalle.simple.fisico.total.d.poliglass',
			'view_id'  : view_id.id,
			'view_type': 'form',
			'view_mode': 'tree',
			'views'    : [(view_id.id, 'tree')],
		}

class detalle_simple_fisico_total_d_poliglass(models.Model):
	_name = 'detalle.simple.fisico.total.d.poliglass'

	producto = fields.Many2one('product.product','Producto')
	categoria = fields.Many2one('product.category',u'Categoría')
	almacen = fields.Many2one('stock.location','Almacen')
	saldo = fields.Float('Stock Disponible',digits=(15,3))
	saldo_fisico = fields.Float('Stock Fisico',digits=(15,3))
	por_ingresar = fields.Float('Por Ingresar',digits=(15,3))
	transito = fields.Float('Ingresos Transito',digits=(15,3))
	salida_espera = fields.Float('Salida Espera',digits=(15,3))
	reservas = fields.Float('Reservas',digits=(15,3))
	saldo_virtual = fields.Float('Previsto',digits=(15,3))

	id_stock_disponible = fields.Text('ids')
	id_saldo_fisico = fields.Text('ids')
	id_por_ingresar = fields.Text('ids')
	id_transito = fields.Text('ids')
	id_salida_espera = fields.Text('ids')
	id_reservas = fields.Text('ids')
	id_previsto = fields.Text('ids')

	@api.multi
	def get_stock_disponible(self):
		t = eval(self.id_stock_disponible.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	@api.multi
	def get_saldo_fisico(self):
		t = eval(self.id_saldo_fisico.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	@api.multi
	def get_por_ingresar(self):
		t = eval(self.id_por_ingresar.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	@api.multi
	def get_transito(self):
		t = eval(self.id_transito.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	@api.multi
	def get_salida_espera(self):
		t = eval(self.id_salida_espera.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	@api.multi
	def get_reservas(self):
		t = eval(self.id_reservas.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	@api.multi
	def get_saldo_virtual(self):
		t = eval(self.id_previsto.replace('None','0').replace('NULL','0'))
		elem = []
		for i in t:
			if i!= 0:
				data = {
					'move_id': i,
				}
				tmp = self.env['detalle.saldo.fisico'].create(data)
				elem.append(tmp.id)

		return {
			'domain' : [('id','in',elem)],
			'type': 'ir.actions.act_window',
			'res_model': 'detalle.saldo.fisico',
			'view_mode': 'tree',
			'view_type': 'form',
			'views': [(False, 'tree')],
			'target': 'new',
		}

	unidad = fields.Many2one('product.uom','Unidad')

	_order = 'producto,unidad,categoria,almacen'
	_auto = False
