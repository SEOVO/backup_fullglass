# -*- coding: utf-8 -*-
from datetime import datetime,timedelta
from odoo import models, fields, api, _
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round,float_compare
from decimal import Decimal, ROUND_HALF_UP

class ScrapsEntryWizard(models.TransientModel):
	_name = 'scraps.entry.wizard'
	traslate_motive = fields.Many2one('einvoice.catalog.12', string='Motivo de Traslado')
	picking_type = fields.Many2one('stock.picking.type', 
		string='Tipo de Picking',
		domain=lambda self: self._context.get('dom_picking_type',[('code','in',('internal','incoming'))]))
	partner_id = fields.Many2one('res.partner',string=u'Partner')
	line_ids = fields.One2many('scraps.entry.wizard.line','main_id')

	@api.multi
	def get_element(self):
		"""heredable"""
		module = __name__.split('addons.')[1].split('.')[0]
		wizard = self.create({})
		return {
			'name':'Ingresar Retazos',
			'res_id': wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': 'scraps.entry.wizard',
			'view_id': self.env.ref('%s.scraps_entry_wizard_view_form' % module).id,
			'view_mode': 'form',
			'target': 'new',
		}
		
	def process_scraps(self):
		lines = self.line_ids.filtered(lambda l: l.quantity > 0.0)
		dict_uoms = self._get_uom_plancha(['%d_%d'%(l.width,l.height) for l in lines],make_if_not_exists=True)

		for line in lines:
			width,height = line.width,line.height
			key = '%d_%d'%(width,height)
			line.uom_id = dict_uoms.get(key)

		move_vals = [(0,0,self._prepare_move_vals(l.product_id,l.quantity,l.uom_id)) for l in lines]
		if not any(move_vals):
			raise UserError(u'No hay líneas a procesar.')

		pick_vals = self._prepare_pick_vals()
		pick_vals['move_lines'] = move_vals

		picking = self.env['stock.picking'].create(pick_vals)
		return self.transfer_scraps_picking(picking)

	def _prepare_pick_vals(self):
		current_date = fields.Date.context_today(self)
		pick_type = self.picking_type
		pick_motive = self.traslate_motive
		return {
			'partner_id':self.partner_id and self.partner_id.id or False,
			'picking_type_id':pick_type.id,
			'date':current_date,
			'fecha_kardex':current_date,
			'location_dest_id':pick_type.default_location_dest_id.id,
			'location_id': pick_type.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': pick_motive.id,
		}

	def _prepare_move_vals(self,product,qty,uom_id):
		current_date = fields.Date.context_today(self)
		pick_type = self.picking_type
		return {
		'name': product.name,
		'product_id': product.id,
		'product_uom': uom_id.id,
		'date':current_date,
		'date_expected':current_date,
		'picking_type_id': pick_type.id,
		'location_id': pick_type.default_location_src_id.id,
		'location_dest_id': pick_type.default_location_dest_id.id,
		'partner_id': self.partner_id and self.partner_id.id or False,
		'move_dest_id': False,
		'state': 'draft',
		'company_id':self.env.user.company_id.id,
		'procurement_id': False,
		'route_ids':pick_type.warehouse_id and [(6,0,pick_type.warehouse_id.route_ids.ids)] or [],
		'warehouse_id': pick_type.warehouse_id and pick_type.warehouse_id.id or False,
		'product_uom_qty':float_round(qty,precision_rounding=1), # las cantidades deberían ser 
													#siempre por planchas completas, sin permitir decimales
		}

	@api.model
	def transfer_scraps_picking(self,picking,force_assign=False,stock_raise=True):
		"""transferencia de productos de tipo plancha en diferentes unidades de medida (retazos)"""
		default_action =  {
			'name':picking.name,
			'res_id':picking.id,
			'type': 'ir.actions.act_window',
			'res_model': picking._name,
			'view_mode': 'form',
		}
		if picking.state=='draft':
			picking.action_confirm()
		if picking.state != 'assigned':
			picking.action_assign()
		
		if picking.state!='assigned' and force_assign:
			picking.force_assign()
		
		if picking.state != 'assigned':
			if stock_raise:
				raise UserError(u'No se pudo realizar la reservación de los producto probablemente a falta de stock.')
			else:
				return default_action

		if picking.state=='assigned':
			# El Odoo de mierda no está convirtiendo adecuadamente cuando se transfiere en diferentes unidades de medida
			# respecto a la medida de producto, es necesario colocar manualmente las cantidades a realizar y 
			# hacer un reajuste de redondeo:
			# IMPORTANTE: se tiene en cuenta que la transferencia va a ser total y no va a generar entregas parciales

			move_qtys = {m.id: m.product_uom_qty for m in picking.move_lines} # cantidades para subsanar si el Odoo la caga
			
			format_float = lambda f: float(Decimal(str(f)).quantize(Decimal('0.0000'), rounding=ROUND_HALF_UP))
			
			dict_op_qty_done = {}
			for op in picking.pack_operation_ids:
				rounding = 0.0001
				moves = op.linked_move_operation_ids.mapped('move_id')
				qty_moves = sum(moves.mapped(lambda m: m.product_uom._compute_quantity(m.product_uom_qty, op.product_uom_id)))
				qty_moves = float_round(qty_moves,precision_rounding=rounding)
				compare = float_compare(qty_moves, op.product_qty, precision_rounding=rounding)
				if compare==-1:
					qty_done = qty_moves
				elif compare==0:
					qty_done = qty_moves
				elif compare==1:
					qty_done = op.product_qty
				dict_op_qty_done[op.id] = qty_done

			error = True
			while error:
				try:
					for op in picking.pack_operation_ids:
						op.qty_done = dict_op_qty_done[op.id]
					action = picking.do_new_transfer()
					error = False
				except UserError as e:
					e_msg = e[0]
					if e_msg[:36] == 'Los redondeos de su unidad de medida':
						uom_name = e_msg.split(' ')[7]
						bad_op = picking.pack_operation_ids.filtered(lambda op: op.product_uom_id.name==uom_name)
						dict_op_qty_done[bad_op.id] = format_float(dict_op_qty_done[bad_op.id] - 0.0001)
					else:
						raise UserError(e_msg)

			SBC = self.env['stock.backorder.confirmation']
			if type(action) is dict and action['res_model'] == SBC._name:
				context = dict(action['context'], mail_notrack=True, tracking_disable=True)
				sbc = SBC.with_context(context).create({'pick_id':picking.id})
				sbc.process_cancel_backorder() # ignorar las diferencia por cantidades pequeñas prod. de la diferencia decimal
				pick = self.env['stock.picking'].search([('backorder_id', '=', picking.id)])
				if pick.pack_operation_ids:
					self._cr.execute("DELETE FROM stock_pack_operation WHERE id IN %s;", (tuple(pick.pack_operation_ids.ids),))
				pick.unlink()# eliminar el albarán que genera por diferencia decimal
				# reajustar las cantidades que cagó el Odoo:
				for move in picking.move_lines:
					if move.id in move_qtys:
						sql = "UPDATE stock_move SET product_uom_qty = %f WHERE id = %d;" % (move_qtys[move.id], move.id)
					else:
						sql = "DELETE FROM stock_move  WHERE id = %d;" % move.id
					self._cr.execute(sql)
				for op in picking.pack_operation_ids:
					self._cr.execute("UPDATE stock_pack_operation SET product_qty = %f WHERE id = %d;" % (op.qty_done, op.id))
			return action is dict and action or default_action

	@api.model
	def _get_uom_plancha(self,dimensions,make_if_not_exists=False):
		"""Obtener una unidad de medida de tipo plancha
		param:  dimensions: itereable con elemento de tipo: [600_700,550_800,...]
		param: make_if_not_exists: crear la unidad si no existe (ovbio no? xD) 
		Returns: diccionario de tipo : {ancho_alto : uom_id,...}
		params
		"""
		Uom = self.env['product.uom'].sudo()
		dict_uoms = {}
		settings = self.env['ir.values'].sudo().get_defaults_dict('stock.config.settings')
		categ_uom = settings.get('uom_plancha_categ_id')
		if not categ_uom:
			raise UserError(u'No se ha encontrado la categoría de planchas por defecto.')

		for key in dimensions:
			values = key.split('_')
			width,height = float(values[0]),float(values[1])
			if key not in dict_uoms.keys():
				uom = Uom.search([('plancha','=',True),('ancho','=',width),('alto','=',height)],order='id',limit=1)
				if not uom and make_if_not_exists:
					uom_values = Uom._get_values_plancha_type(width, height, 0.0001)
					uom_values.update({'category_id': categ_uom})
					uom = Uom.with_context(prevent_check_plancha=True).create(uom_values)
				if not uom:
					raise UserError('Unidad de medida no existente para las dimensiones: ancho: %.2f y alto: %.2f.'%(width,height))
				dict_uoms[key] = uom.id
		return dict_uoms

class ScrapsEntryWizardLine(models.TransientModel):
	_name = 'scraps.entry.wizard.line'
	main_id = fields.Many2one('scraps.entry.wizard')
	product_id = fields.Many2one('product.product', string='Producto',domain=[('uom_id.plancha','=',True)])
	uom_id = fields.Many2one(string='Unidad de Medida')
	quantity   = fields.Integer('Cantidad')
	width  = fields.Integer('Ancho')
	height = fields.Integer('Alto')

	@api.constrains('width','height')
	def _verify_measures(self):
		for rec in self:
			if rec.width <= 0 or rec.height <= 0:
				raise UserError(u'Los ancho y altos ingresados deben ser mayores a cero.')
			if rec.width > rec.height:
				raise UserError(u'El ancho no puede ser mayor al alto.\nLínea: Ancho: %d Alto: %d'%(rec.width,rec.height))
