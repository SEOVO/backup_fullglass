
{
    'name': 'Administración de retazos - Fullglass',
    'version': '10',
    'license': 'AGPL-3',
    'author': 'ITGRUPO-POLIGLASS',
    'category': 'Production',
    'summary': """
    Manejo y visualización de saldos de planchas y retazos
    """,
    'depends': ['stock','unidad_medida_it'],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'wizard/scraps_entry_wizard_view.xml',
        'wizard/scraps_import_view_wizard.xml',
        'wizard/saldo_scraps.xml',
        'views/stock_config_settings.xml',
    ],
    'installable': True,
}
