# -*- coding: utf-8 -*-
{
	'name': "Logística de Insulados - Fullglass",

	'summary': """
		Gestión de la logística de cristales insulados
		""",

	'description': """
		- Encargado de gestionar el envío de cristales base para su Insulación.
		- Encargado de adicionar la etapa de marcar la etapa de insulado a los cristales solicitados con estas características. 
	""",

	'author': "ITGRUPO-POLIGLASS",
	'website': "http://www.itgrupo.net",

	'category': 'Sale-Production',
	'version': '0.1',

	# any module necessary for this one to work correctly
	'depends': ['master_template_fullglass','packing_list_fullglass','print_product_labels'],

	# always loaded
	'data': [
		'security/groups.xml',
		'security/ir.model.access.csv',
		'wizard/finish_insulados_wizard.xml',
		#'wizard/traslate_insulados_wizard.xml',
		'views/glass_order_config.xml',
		'views/packing_list.xml',
		'views/packing_list_config.xml',
		'report/insulado_labels.xml',
	],
	'demo': [],
	'installable': True
}