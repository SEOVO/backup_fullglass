# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError


class IlFinishInsuladosWizard(models.TransientModel):
	_name = 'il.finish.insulados.wizard'

	_description = u'Culminación de proceso para insulados'

	glass_order_ids = fields.Many2many('glass.order',string=u'Órdenes de producción')
	picking_type_prod_id = fields.Many2one('stock.picking.type',string=u'Tipo de Operación Ing a producción')
	picking_type_ing_apt_id = fields.Many2one('stock.picking.type',string=u'Tipo de Operación Ing a APT')
	
	req_traslate_motive = fields.Many2one('einvoice.catalog.12',u'Motivo de traslado',help=u'Motivo de traslado para ambas operaciones')
	line_ids = fields.One2many('il.finish.insulados.wizard.line','wizard_id')
	#show_all_available = fields.Boolean('Ver todos los disponibles')

	def get_new_element(self):
		conf = self.env['glass.order.config'].search([],limit=1)
		picking_type_prod_id = conf.pick_type_out_ins_id
		picking_type_ing_apt_id = conf.pick_type_in_ins_id

		if not (picking_type_prod_id and picking_type_ing_apt_id):
			raise UserError(u'No se han encontrado los tipos de picking para ésta operación en sus parámetros de configuración.')

		wizard = self.create({
			'picking_type_prod_id': picking_type_prod_id.id,
			'picking_type_ing_apt_id': picking_type_ing_apt_id.id,
			})
		return {
			'name':u'Traslado e ingreso de insulados',
			'res_id': wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': wizard._name,
			'view_mode': 'form',
			'view_type': 'form',
			#'flags': {'initial_mode': 'edit'},
			'target': 'inline',
		}

	def get_ins_crystals(self):
		"""Obtener solo órdenes con requirimiento completo"""
		self.ensure_one()
		glass_order_ids = self.glass_order_ids.filtered(lambda o: 'ended' in o.mtf_requirement_ids.mapped('state'))
		if not glass_order_ids:
			raise UserError(u"Ninguna de las Op's seleccionadas ha finalizado su Orden de requisición de Ficha maestra")
		self.line_ids.unlink()
		## get complete process:

		query = """
		SELECT 
		COALESCE(array_agg(T.lot_line_ids),'{}') AS lot_line_ids
		FROM 
			(SELECT  
			DISTINCT(gll.id) AS lot_line_ids
			FROM glass_lot_line gll
			JOIN glass_order_line gol ON gol.id = gll.order_line_id
			WHERE gll.from_insulado = true
			AND gll.templado = true 
			AND is_break = false 
			AND active = true 
			AND gll.il_in_transfer = true 
			AND (gll.insulado = false OR gll.insulado IS null) 
			AND (gol.in_packing_list = false OR gol.in_packing_list IS null)
			AND (gll.producido = false OR gll.producido IS null)
			AND COALESCE(gll.is_service, false) = false 
			UNION 
			SELECT 
			DISTINCT(gll.id) AS lot_line_ids
			FROM glass_lot_line gll
			JOIN glass_order_line gol ON gol.id = gll.order_line_id
			JOIN glass_lot gl ON gl.id = gll.lot_id
			JOIN glass_requisition gr ON gr.id = gl.requisition_id
			WHERE gll.from_insulado = true
			AND gll.templado = true 
			AND is_break = false 
			AND active = true 
			-- para los laminados q están en lima considero su ord de req. no es lo más fiable, mejorar
			AND gr.location = 'prod_lima'
			AND gr.state IN ('confirm','done') 
			AND (gll.il_in_transfer = false OR gll.il_in_transfer IS null)
			AND (gll.insulado = false OR gll.insulado IS null) 
			AND (gol.in_packing_list = false OR gol.in_packing_list IS null)
			AND COALESCE(gll.is_service, false) = false 
			AND (gll.producido = false OR gll.producido IS null))T """
		self._cr.execute(query)
		res = self._cr.fetchone()[0]
		# only with mtf_requirement_ids ended..
		ext_domain = [('id','in',res)]
		calcs = self.glass_order_ids.mapped('line_ids.calc_line_id.parent_id')
		available_items = []
		
		completes_dict = calcs._get_insulados_dict_crystals(extra_domain=ext_domain,only_completes=True)
		for k,v in completes_dict.items():
			calc = calcs.filtered(lambda l: l.id == k)
			items = v.items()
			for k2,v2 in items:
				available_items.append((0,0,{
				'calc_line_id':calc.id,
				'crystal_num':k2,
				'base_crystals':[(6,0,v2)],
				}))
		self.write({'line_ids':available_items})
		return {"type": "ir.actions.do_nothing",}

	def process_transfer(self):
		lines = self.line_ids
		if not lines:
			raise UserError('No hay cristales insulados disponibles para ingresar a Producto Terminado')

		base_lines = lines.mapped('base_crystals')

		#Primer picking
		# FIXME por ahora me baso en la sede de la requisión para saber si un cristal 
		# tiene procedencia Lima y no debe incluirse en el primer albarán (En proceso->producción)
		# no es lo más fiable...
		pick_ids = []
		first_transfer_lines = base_lines.filtered(lambda l: l.lot_id.requisition_id.location=='prod_aqp')
		if first_transfer_lines:
			products = first_transfer_lines.mapped('product_id')
			pick_type = self.picking_type_prod_id
			pick_vals = self._prepare_pick_vals(pick_type)
			move_vals = []
			for product in products:
				filt = first_transfer_lines.filtered(lambda l: l.product_id==product)
				quantity = sum(filt.mapped('area'))
				glass_ids = filt.mapped('order_line_id').ids
				move_vals.append((0,0,self._prepare_move_vals(pick_type,product,quantity,glass_ids)))
			
			pick_vals['move_lines'] = move_vals
			pick = self.env['stock.picking'].create(pick_vals)
			pick.action_confirm()
			pick.action_assign()
			if pick.state != 'assigned':
				pick.force_assign()
			if pick.state == 'assigned':
				for op in pick.pack_operation_ids:
					op.write({'qty_done':op.product_qty})
				pick.action_done()
			else:
				raise UserError(u'La reserva de productos no pudo completarse')

			pick_ids.append(pick.id)

		# SEGUNDO ALBARÁN -> PROD -> APT LIMA
		products = lines.mapped('product_id')
		pick_type = self.picking_type_ing_apt_id
		pick_vals = self._prepare_pick_vals(pick_type)
		move_vals = []
		for product in products:
			filt = lines.filtered(lambda l: l.product_id==product)
			quantity = sum(filt.mapped('area'))
			glass_ids = filt.mapped('base_crystals.order_line_id').ids
			move_vals.append((0,0,self._prepare_move_vals(pick_type,product,quantity,glass_ids)))
		
		pick_vals['move_lines'] = move_vals
		pick2 = self.env['stock.picking'].create(pick_vals)
		pick2.action_confirm()
		pick2.action_assign()
		if pick2.state != 'assigned':
			pick2.force_assign()
		if pick2.state == 'assigned':
			for op in pick2.pack_operation_ids:
				op.write({'qty_done':op.product_qty})
			pick2.action_done()
			pick_ids.append(pick2.id)
		else:
			raise UserError(u'La reserva de productos no pudo completarse')

		# FIXME Debebería registrarse las etapas si no se consigue transferir el albarán? si el tipo de 
		# de picking es de producción no habría problemas al transferir, pero si no uhmm XDXD
		base_lines.with_context(force_register=True).register_stage('insulado')
		base_lines.with_context(force_register=True).register_stage('producido')
		base_lines.with_context(force_register=True).register_stage('ingresado')
		base_lines.write({'il_in_transfer':False})
		base_lines.mapped('order_line_id').write({'state':'instock'})

		action = self.env.ref('stock.action_picking_tree_all').read()[0]
		if len(pick_ids) > 1:
			action['domain'] = [('id', 'in', pick_ids)]
		else:
			action['views']  = [(self.env.ref('stock.view_picking_form').id, 'form')]
			action['res_id'] = pick_ids[0]
		action['target'] = 'main'
		return action
	
	def print_labels(self):
		glass_lines = self.line_ids.mapped('base_crystals.order_line_id')
		if not glass_lines:
			raise UserError(u'No hay cristales disponibles para la generación de etiquetas.')
		return glass_lines.print_end_insulado_labels()

	@api.model
	def _prepare_pick_vals(self,pick_type):
		current_date = fields.Date.context_today(self)
		return {
			# TODO que partner le ponemos??
			# al parecer siempres serán transf. internas, si son incoming, asignar un partner 
			'partner_id':False,
			'picking_type_id':pick_type.id,
			'date':current_date,
			'fecha_kardex':current_date,
			'location_dest_id':pick_type.default_location_dest_id.id,
			'location_id': pick_type.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': self.req_traslate_motive.id,
		}
	
	@api.model
	def _prepare_move_vals(self,pick_type,product,qty,glass_ids):
		current_date = fields.Date.context_today(self)
		return {
		'name': product.name,
		'product_id': product.id,
		'product_uom': product.uom_id.id,
		'date':current_date,
		'date_expected':current_date,
		'picking_type_id': pick_type.id,
		'location_id': pick_type.default_location_src_id.id,
		'location_dest_id': pick_type.default_location_dest_id.id,
		'partner_id': False,
		'move_dest_id': False,
		'state': 'draft',
		'company_id':self.env.user.company_id.id,
		'procurement_id': False,
		'route_ids':pick_type.warehouse_id and [(6,0,pick_type.warehouse_id.route_ids.ids)] or [],
		'warehouse_id': pick_type.warehouse_id and pick_type.warehouse_id.id or False,
		'product_uom_qty':qty,
		'glass_order_line_ids':[(6,0,glass_ids or [])]
		}

class IlFinishInsuladosWizardLine(models.TransientModel):
	_name = 'il.finish.insulados.wizard.line'
	_order = 'crystal_num_fl'

	wizard_id = fields.Many2one('il.finish.insulados.wizard', string=u'Wizard')
	calc_line_id = fields.Many2one('glass.sale.calculator.line', string=u'Línea de Cristal')
	crystal_num = fields.Char('Nro de Cristal')
	crystal_num_fl = fields.Float('Nro de Cristal Aux', compute='_get_crystal_num_fl', store=True)
	product_id = fields.Many2one(related='calc_line_id.calculator_id.product_id', string="Producto")
	uom_id = fields.Many2one(related='product_id.uom_id', string="Unidad")
	quantity = fields.Integer(default=1, string='Cantidad')
	base1 = fields.Integer(related='calc_line_id.base1')
	base2 = fields.Integer(related='calc_line_id.base2')
	height1 = fields.Integer(related='calc_line_id.height1')
	height2 = fields.Integer(related='calc_line_id.height2')
	area = fields.Float(related='calc_line_id.unit_area')
	base_crystals = fields.Many2many('glass.lot.line', string=u'Cristales producidos que lo conforman')

	@api.depends('crystal_num')
	def _get_crystal_num_fl(self):
		for line in self:
			line.crystal_num_fl = float(line.crystal_num)

	def view_crystals(self):
		return {
			'name':'Cristales Componentes',
			'type': 'ir.actions.act_window',
			'res_model': 'glass.lot.line',
			'view_id': self.env.ref('glass_production_order.glass_lot_line_view_tree').id,
			'view_mode': 'tree',
			'view_type': 'form',
			'target': 'new',
			'domain':[('id','in',self.base_crystals.ids)]
			}
		
		

