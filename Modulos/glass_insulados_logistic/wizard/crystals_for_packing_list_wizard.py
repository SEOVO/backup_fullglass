# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError


class CrystalsForPackinglistWizard(models.TransientModel):
	_inherit = 'crystals.for.packinglist.wizard'

	def add_items(self,items):
		if self._context.get('add_insulados'):
			# si hay intersecciónde calculadorar no permitir...
			remaining = (self.detail_lines - items).mapped('calc_line_id.parent_id')
			calc_items = items.mapped('calc_line_id.parent_id')
			if (remaining & calc_items):
				raise UserError(u'Debe seleccionar todos los cristales componentes de un insulado final!')
		return super(CrystalsForPackinglistWizard,self).add_items(items)

class CrystalsForPackinglistWizardLine(models.TransientModel):
	_inherit = 'crystals.for.packinglist.wizard.line'

	calc_line_id = fields.Many2one(related='order_line_id.calc_line_id')