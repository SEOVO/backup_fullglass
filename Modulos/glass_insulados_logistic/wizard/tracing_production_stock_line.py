# -*- coding: utf-8 -*-
from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError

class Tracing_Production_Stock_Line_Lot(models.TransientModel):
	_inherit = 'tracing.production.stock.line.lot'

	il_in_transfer = fields.Boolean(related='lot_line_id.il_in_transfer')
	