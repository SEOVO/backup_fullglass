# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round
from itertools import chain

class PackingList(models.Model):
	_inherit = 'packing.list'

	def _get_to_done_lines(self):
		lines = super(PackingList,self)._get_to_done_lines()
		return lines.filtered(lambda l: not l.from_insulado) # excluir los que son para insulados ya que se van en otro picking

	def get_insulados_op(self):
		self.ensure_one()
		if self.type_pk!='to_transfer':
			raise UserError(u'Sólo puede agregar cristales insulados a un packing list de tranferencia a otra ubicación')
		if self.order_filter:
			sql_ext = ''
			added  = self.selected_line_ids.mapped('order_line_id').ids
			if added:
				added_str = ','.join(map(str,added))
				sql_ext = ' AND gol.id NOT IN (%s)'%added_str
				
			query = """
				SELECT 
				COALESCE(array_agg(gll.id),'{}')
				FROM glass_lot_line gll
				JOIN glass_order_line gol ON gol.id = gll.order_line_id 
				JOIN glass_lot gl ON gl.id = gll.lot_id
				JOIN glass_requisition gr ON gr.id = gl.requisition_id
				WHERE gll.from_insulado = true
				AND gll.templado = true 
				AND is_break = false
				AND active = true
				AND gr.location = 'prod_aqp' 
				AND gr.state IN ('confirm','done') 
				AND (gll.il_in_transfer = false OR gll.il_in_transfer IS null)
				AND (gll.insulado = false OR gll.insulado IS null) 
				AND (gol.in_packing_list = false OR gol.in_packing_list is null)
				AND (gll.producido = false OR gll.producido IS null)
				AND COALESCE(gol.is_service, false) = false 
				AND gll.order_prod_id = %d 
				%s ;"""%(self.order_filter.id,sql_ext)

			self._cr.execute(query)
			ext_domain = [('id','in',self._cr.fetchone()[0])]
			available_items = []
			GlassOrderLine = self.env['glass.order.line'] 
			calcs = self.order_filter.line_ids.mapped('calc_line_id.parent_id')
			invoice = self.order_filter.invoice_ids.filtered(lambda i: i.state!='cancel')
			invoice_id = invoice and invoice[0].id or False
			
			completes_dict = calcs._get_insulados_dict_crystals(ext_domain,only_completes=False)
			values = list(chain(*[i.values() for i in completes_dict.values()]))
			lot_line_ids = list(chain(*[j for j in values]))
			
			for item in lot_line_ids:
				g_line = GlassOrderLine.search_read([('lot_line_id','=',item)],['id'])
				available_items.append((0,0,{
					'invoice_id': invoice_id,
					'order_line_id': g_line and g_line[0]['id'] or False,
				}))

			wizard = self.env['crystals.for.packinglist.wizard'].create({
				'packing_id':self.id,
				'detail_lines': available_items
			})

			name = self.order_filter.name
			self.order_filter = False
			ctx = dict(self._context or {},add_insulados=True)
			return {
				'name':'Cristales para insulado de OP: '+name,
				'res_id':wizard.id,
				'type': 'ir.actions.act_window',
				'res_model': wizard._name,
				'view_id': self.env.ref('packing_list_fullglass.crystals_for_packinglist_wizard_form').id,
				'view_mode': 'form',
				'target': 'new',
				'context':ctx
				}
		else:
			raise UserError('No ha seleccionado ninguna OP')

	def action_process_pk_to_location(self):
		res = super(PackingList,self).action_process_pk_to_location()
		ins_lines = self.selected_line_ids.filtered('from_insulado')
		if not ins_lines:
			return res
		
		conf = self.env['packing.list.config'].search([],limit=1)
		type_pick_ins = conf.pick_type_out_ins_id
		traslate_motive = conf.traslate_motive_ins_id
		if not (traslate_motive and type_pick_ins):
			raise UserError(u'No se ha encontrado el tipo de picking y/o motivo de traslado para insulados')
		
		#pick_type = self.picking_type_id
		pick_vals = self._prepare_pick_vals(type_pick_ins)
		move_vals = []
		products = ins_lines.mapped('product_id')

		for product in products:
			filt = ins_lines.filtered(lambda l: l.product_id==product)
			g_lines = filt.mapped('order_line_id')
			qty = float_round(sum(g_lines.mapped('area')),precision_rounding=0.0001)
			move_vals.append((0,0,self._prepare_move_vals(type_pick_ins,product,qty,g_lines.ids)))
			g_lines.write({'in_packing_list':False})
			g_lines.mapped('lot_line_id').write({
				'il_in_transfer':True,
				'location_transfer_id':type_pick_ins.default_location_dest_id.id,
				})
			filt.write({'state':'done'})

		pick_vals['move_lines'] = move_vals
		picking = self.env['stock.picking'].create(pick_vals)

		#Sit = self.env['stock.immediate.transfer']
		picking.action_confirm()
		picking.action_assign()
		if picking.state!='assigned':
			picking.force_assign() # :'v
		for op in picking.pack_operation_ids:
			op.write({'qty_done':op.product_qty})
		picking.action_done()

		self.picking_ids |= picking
		self.state = 'done'

		action = self.env.ref('stock.action_picking_tree_all').read()[0]
		pick_ids = [picking.id]
		if res is dict and res.get('res_id'):
			pick_ids.append(res['res_id'])
		if len(pick_ids) > 1:
			action['domain'] = [('id', 'in', pick_ids)]
		else :
			action['views'] = [(self.env.ref('stock.view_picking_form').id, 'form')]
			action['res_id'] = picking.id
		return action
	

class GlassOrderLinePackingList(models.Model):
	_inherit = 'glass.order.line.packing.list'

	from_insulado = fields.Boolean(related='lot_line_id.from_insulado')	
	calc_line_id  = fields.Many2one(related='order_line_id.calc_line_id')


	# @api.multi
	# def unlink(self):

	# 	to_remove = self.env['glass.order.line.packing.list']
	# 	self = self.exists()
	# 	for line in self.filtered('from_insulado'):
	# 		# FIXME se están eliminando todas las líneas asociadas al parente id, 
	# 		to_remove |= line.packing_id.selected_line_ids.filtered(lambda l: l.calc_line_id.parent_id==line.calc_line_id.parent_id)
	# 	self |= to_remove
	# 	return super(GlassOrderLinePackingList,self).unlink()
	