# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class PackingListConfig(models.Model):
	_inherit = 'packing.list.config'

	pick_type_out_ins_id = fields.Many2one('stock.picking.type',string=u'Tipo de picking Prod. Tmp -> Prod. Insulados',help=u'Tipo de picking para salida de cristales para insulado a planta de insulación')
	
	traslate_motive_ins_id = fields.Many2one('einvoice.catalog.12',u'Motivo de traslado insulados')
