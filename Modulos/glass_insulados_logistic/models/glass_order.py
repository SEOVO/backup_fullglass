# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
import base64
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.units import inch, cm
from reportlab.pdfgen.canvas import Canvas
from reportlab.platypus import Table, TableStyle, Paragraph
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet,ParagraphStyle
from reportlab.lib.enums import TA_CENTER,TA_RIGHT
from reportlab.graphics.barcode import code128
from datetime import datetime
from itertools import groupby,chain
from collections import OrderedDict

class GlassOrder(models.Model):
	_inherit = 'glass.order'

	def print_ended_insulado_labels(self):
		return self.mapped('line_ids').print_end_insulado_labels()

class GlassOrderLine(models.Model):
	_inherit = 'glass.order.line'

	def print_end_insulado_labels(self):
		if not self.env.user.has_group('print_product_labels.group_print_product_labels'):
			raise UserError(u'Ud. No tiene autorización para realizar ésta acción.')
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
		pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))
		path = self.env['main.parameter'].search([])[0].dir_create_file
		now = fields.Datetime.context_timestamp(self,datetime.now())
		file_name = 'Etiq. insulados %s.pdf'%str(now)[:19].replace(':','_')
		path+=file_name
		separator = 10
		pos = 95
		pos_left = 8
		width_page,height_page = 7.62 * cm , 5.08 * cm # 216 144
		c = Canvas(path, pagesize=(width_page,height_page)) 
		p  = ParagraphStyle('p1',alignment=TA_CENTER,fontSize=9,fontName="Calibri")
		
		key_name = lambda x: x.order_id.name
		lines = self.filtered(lambda l: l.lot_line_id and l.calc_line_id.from_insulado and not l.is_service).sorted(key=key_name)
		if not lines:
			raise UserError(u'No hay líneas aptas para generación de etiquetas de insulado.\n')
		
		CalcLine = self.env['glass.sale.calculator.line']
		LotLine = self.env['glass.lot.line'].with_context(final_insulado_code=True)
		grouped = groupby(lines,key=key_name)
		for key,group in grouped:
			group = list(group)
			calcs = CalcLine.browse(list({l.calc_line_id.parent_id.id for l in group}))
			calc_info = calcs._get_insulados_dict_crystals(only_completes=True)
			total_items = len(list(chain(*[cl.get_crystal_numbers() for cl in calcs])))
			
			for calc in calcs:
				info = OrderedDict(sorted(calc_info[calc.id].items(),key=lambda x: int(x[0])))
				# info->{'3': [26027,26008], '5': [26029,26010], '4': [26028,26009], '6': [26030,26011]}
				for k,v in info.items():
					lot_lines = LotLine.browse(v)
					c.setFont("Calibri", 10)
					c.drawString(pos_left,pos,'O/P')
					c.setFont("Calibri-Bold", 9)
					c.drawString(pos_left+20,pos,key) # op name
					c.setFont("Calibri-Bold", 11)
					measures = lot_lines[0].measures.replace('X',' X ')
					c.drawRightString(width_page-8,pos,measures)
					pos -= separator
					c.drawRightString(width_page-8,pos,'INSULADO')
					c.setFont("Calibri", 10)
					c.drawString(pos_left,pos,'Cristal Nro.')
					
					c.drawString(pos_left+50,pos,'%s/%d'%(k,total_items))
					c.setFont("Calibri", 7)
					pos -= separator
					c.drawString(pos_left,pos,calc.calculator_id.product_id.name or '')
					c.setFont("Calibri-Bold", 10)
					pos -= separator
					c.drawString(pos_left,pos,(calc.calculator_id.order_id.partner_id.name or '')[:40])
					c.setFont("Calibri", 10)
					pos -= separator
					street = lot_lines[0].order_prod_id.obra or calc.calculator_id.order_id.partner_id.street or ''
					c.drawString(pos_left,pos,street[:40])
					pos -= separator+6
					width_rec = 180

					query = """
					SELECT 
					COALESCE(STRING_AGG(DISTINCT(gfo.name),','),'')
					FROM glass_lot_line gll
					JOIN glass_furnace_line_out gflo ON gflo.lot_line_id = gll.id
					JOIN glass_furnace_out gfo ON gfo.id = gflo.main_id
					WHERE gfo.state = 'done'
					AND (gll.is_break = false OR gll.is_break IS NULL)
					AND gll.id IN %s ;"""
					self._cr.execute(query,(tuple(v),))

					d1 = self._cr.fetchone()[0]
					d2 = calc.polished_id.code or ''
					d3 = 'E' if calc.entalle else ''
					d4 = 'PL' if calc.template else ''
					d5 = 'EM' if calc.packed else ''

					aux = (width_page/2)-(width_rec/2)

					data = [
					[Paragraph(d1,p),
					'',
					Paragraph(d2,p),
					Paragraph(d3,p),
					Paragraph(d4,p),
					Paragraph(d5,p)],
					]

					t=Table(data,colWidths=6*(30,),rowHeights=(12,))
					t.setStyle(TableStyle([
					('GRID',(0,0),(-1,-1),1,colors.black),
					('VALIGN',(0,0),(-1,-1),'MIDDLE'),
					('SPAN',(0,0),(1,0)),
					]))
					w_table,h_table=t.wrap(0,0)
					t.wrapOn(c,120,500)
					t.drawOn(c,(width_page/2)-(w_table/2),pos)
					pos -= separator
					
					code_line = next(iter(lot_lines.filtered('insulado_search_code')),False)
					if code_line:
						search_code = code_line.insulado_search_code
					else:
						args = {'order_name': key,'crystal_number': k, 'total_ins_items': total_items}
						search_code = LotLine.make_search_code(**args)
						lot_lines.write({'insulado_search_code':search_code})

					c.drawCentredString((width_page/2),pos,search_code)
					story=[]
					st = code128.Code128(search_code,barHeight=.32*inch,barWidth=1.2)
					story.append(st)
					st.drawOn(c,10,4)
					
					pos = 95
					c.showPage()
		c.setTitle(file_name)
		c.save()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),
				'content_type':'application/pdf'
			})
		return export.export_file(clear=True,path=path)