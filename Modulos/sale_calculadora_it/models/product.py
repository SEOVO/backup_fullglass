# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from odoo import fields, models,api, _
from odoo.exceptions import UserError

class ProductCategory(models.Model):
	_inherit = 'product.category'

	min_sale = fields.Float(u'Mínimo Vendible')
	uom_min_sale = fields.Many2one('product.uom', u'Unidad')

class ProductTemplate(models.Model):
	_inherit='product.template'
	
	# NOTE éstos campos son muy sensibles y serán restringidos sólo a un grupo de usuario
	# se podría permitr mayor flexibilidad, pero habría que dar un control adecuado , para lo cual 
	# no se tiene tiempo disponible en éste momento

	allow_metric_sale = fields.Boolean(u'Permite Venta metro lineal', 
		track_visibility='onchange', 
		help=u'Define el cobro por el perímetro del cristal.')

	# Esto sólo aplica para calculadoras de servicio, debería marcarse para entalle, biselado y perforaciones
	allow_pz_sale = fields.Boolean(u'Servicio por cantidad de aplicaciones', default=False,
		track_visibility='onchange', 
		help=u"""Define el cobro por la cantidad de aplicaciones del servicio en el cristal, 
				aplica para entalles, biselados, perforaciones""")

	# TODO FIXME agregar resticcción para validar que cuando "is_production_service" sea true, el tipo de producto sea "product"
	# para que genere su respectivo albarán de entrega

	# TODO REMOVE quitar éste campo cuando se valide que todo va bien (quitar las referencias en xml y related's)
	is_production_service = fields.Boolean(u'Es un servicio de producción', 
		default=False, help='Define si es un servicio que genera OP y un picking de servicio.')
	# NOTE considerando que los weyes de fullglass vas a querer entregar los servicios realizados desde un picking
	# Los productos aunque sean servicios, deberán ser de tipo almacenable.

	_sql_constraints = [(
			'once_sale_method_calc', 
			'CHECK ((COALESCE(allow_metric_sale, false) AND COALESCE(allow_pz_sale, false)) = false)',
			u'¡No puede establecer una venta por metro lineal y una venta por cant. de aplicaciones a la vez!'
		),
		# (
		# 	'check_allow_pz_sale_consistence', 
		# 	"""CHECK(((COALESCE(allow_pz_sale, false) AND COALESCE(is_production_service, false)) = true) 
		# 	OR 
		# 	(COALESCE(allow_pz_sale, false) = false))""",
		# 	u'El servicio por cant. de aplicaciones sólo se permite en productos de tipo servicio de producción.'
		# ),
	]

# class ProductProduct(models.Model):
# 	_inherit='product.product'
# 	allow_metric_sale = fields.Boolean(related='product_tmpl_id.allow_metric_sale')