# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError
from decimal import Decimal
import re,os,base64
import PIL.ImageDraw as ImageDraw
import PIL.Image as Image
from PIL import ImageFont
from odoo.tools.float_utils import float_round, float_compare


def split_by_comma(strint_to_split):
	if not strint_to_split:
		return []
	if not re.match(r'^[1-4]+(,[1-4]+)*$', strint_to_split):
		return False
	return strint_to_split.split(',')


# Pulido
class PulidoProforma(models.Model):
	_name = 'sale.pulido.proforma'
	_description = 'Tipo de pulido'

	name = fields.Char('Tipo Pulido', required=True)
	code = fields.Char(u'Código')

# Biselados
class GlassTypeBiselado(models.Model):
	_name = 'glass.type.biselado'
	_description = 'Tipo de biselado'

	name = fields.Char('Nombre', index=True, copy=False, required=True)
	description = fields.Text(u'Descripción')

	_sql_constraints = [('uniq_name', 'UNIQUE (name)', 'El nombre de los tipos de biselado debe ser único')]

# Arenados
class GlassTypeArenado(models.Model):
	_name = 'glass.type.arenado'
	_description = 'Tipo de arenado'

	name = fields.Char('Nombre', index=True, copy=False, required=True)
	description = fields.Text(u'Descripción')

	_sql_constraints = [('uniq_name', 'UNIQUE (name)', 'El nombre de los tipos de arenado debe ser único')]


class GlassSaleCalculator(models.Model):
	_name = 'glass.sale.calculator'

	order_line_id = fields.Many2one('sale.order.line', 
		ondelete='cascade', required=True, readonly=True, copy=False)
	order_id = fields.Many2one(related='order_line_id.order_id', store=True)
	product_id = fields.Many2one(string='Producto',related='order_line_id.product_id', store=True)
	sale_uom_id = fields.Many2one(related='order_line_id.product_uom')
	type_calculator = fields.Selection([
		('common_product', 'Producto'), 
		('service', 'Servicio')], 
		string='Tipo de calculadora', required=True, readonly=True, default='common_product')
	currency_id = fields.Many2one(related='order_id.currency_id', readonly=True)
	total_services_amount = fields.Monetary('Monto total servicios', compute='_compute_totals', readonly=True)
	foreign_calculator_id = fields.Many2one('glass.sale.calculator', 
		string='Calculadora externa', 
		domain="[('order_line_id', '!=', order_line_id), ('order_id', '=', order_id), ('type_calculator', '=', type_calculator), ('additional_service_calc', '=', False)]")

	additional_service_calc = fields.Boolean('Calculadora adicional', default=False, 
		help='Define si ésta calculadora será generada a partir de otra calculadora')

	allow_metric_sale = fields.Boolean(related='product_id.allow_metric_sale', readonly=True)
	total_area = fields.Float(string=u'Área total',digits=(10,4),compute='_compute_totals',store=True)
	total_sold_area = fields.Float(string='Área cobrada total',digits=(10,4),compute='_compute_totals',store=True)
	total_perimeter = fields.Float(string='Perímetro total',digits=(10,4),compute='_compute_totals',store=True)
	total_pieces = fields.Integer(string='Número de Piezas/Cristales',compute='_compute_totals',store=True)
	qty_invoiced = fields.Float('Cantidad Facturada',related='order_line_id.product_uom_qty')
	qty_invoiced_rest = fields.Float('Saldo por atender',digits=(10,4),compute='_compute_qty_invoiced_rest')
	line_ids = fields.One2many('glass.sale.calculator.line', 'calculator_id')


	@api.depends('product_id', 'total_pieces')
	def name_get(self):
		result = []
		for record in self:
			name = '%s (%d Piezas)' % (record.product_id.name, record.total_pieces)
			result.append((record.id, name))
		return result


	def write(self,values):
		if 'type_calculator' in values:
			self.line_ids.unlink() # si cambia de tipo de calculadora, borrar líneas anteriores
		return super(GlassSaleCalculator, self).write(values)

	### Computed methods ###
	@api.depends('qty_invoiced', 'total_sold_area')
	def _compute_qty_invoiced_rest(self):
		for rec in self:
			rec.qty_invoiced_rest = rec.qty_invoiced - rec.total_sold_area

	#@api.depends('line_ids.area', 'line_ids.perimeter', 'line_ids.sold_area', 'line_ids.quantity')
	@api.depends('line_ids')
	def _compute_totals(self):
		# A la antigua nomás...
		for rec in self:
			totals = [0.0, 0.0, 0.0, 0, 0.0]
			for l in rec.line_ids:
				totals[0] += l.area
				totals[1] += l.sold_area
				totals[2] += l.perimeter
				totals[3] += l.quantity
				totals[4] += l.services_amount

			rec.total_area = totals[0]
			rec.total_sold_area = totals[1]
			rec.total_perimeter = totals[2]
			rec.total_pieces = totals[3]
			rec.total_services_amount = totals[4]

	#@api.depends('product_id', 'product_id.allow_metric_sale')
	def _get_quantity_to_sale(self):
		# Obtener la cantidad de producto a vender
		# Paara cristales templados, se basa casi siempre en el área de cristal.
		# Para productos como siliconas y otros, se basa en el prímetro lineal solicitado
		# Para servicios por aplicación, se toma el nro de aplicaciones, e.g. biselados, entalles
		qty = 0.0
		for l in self.line_ids:
			qty += l._get_sale_quantity()
		return qty

	def save_calculator(self):
		self.ensure_one()
		self.order_line_id.write(self.get_order_line_write_vals())
		# recargar para actualizar visualmente
		action = self.env.ref('sale.action_quotations').read()[0]
		action['views'] = [(self.env.ref('sale.view_order_form').id, 'form')]
		action['res_id'] = self.order_id.id
		return action

	def get_order_line_write_vals(self):
		vals = {}

		is_invoiced = self.order_id.invoice_ids.filtered(lambda i: i.state != 'cancel')
		qty_to_sale = self._get_quantity_to_sale()
		
		# Validar cantidad a ventder vs cantidad facturada
		if is_invoiced or self.order_id.state != 'draft' or self.order_id.before_invoice:
			if float_compare(qty_to_sale, self.qty_invoiced, 4) == 1:
				"""temporal XDXD bug de ODOO: IMPORTANTE: quitar cuando hayan procesado 
					los cristales anteriores a esta fecha XDXDXD (sólo permitido para os usuarios q les arroja error desconocido)"""
				date_lol = fields.Datetime.from_string('2020-01-18 02:00:00')
				date_create = fields.Datetime.from_string(self.order_id.create_date)
				if date_create < date_lol and self._context.get('uid') in (1,13,31,34):
					return # :(
				else:
					uom_n = self.sale_uom_id.name
					msg = """La cantidad solicitada de producto (%.4f %s) 
							supera a la cantidad facturada (%.4f %s)""" % (qty_to_sale, uom_n, self.qty_invoiced, uom_n)
					raise ValidationError(msg)

		if not self.order_id.before_invoice:
			vals['product_uom_qty'] = qty_to_sale
		return vals


	# Clone items from other calculator:
	def clone_items_from_other_calc(self):
		if not self.foreign_calculator_id:
			raise UserError('Debe indicar una calculadora a partir de la cual clonar las líneas de cristales/servicios')
		
		lines_vals = []
		self.line_ids.unlink()
		for calc_line in self.foreign_calculator_id.line_ids:
			lines_vals.append((0, 0, self._prepare_clone_calc_items_vals(calc_line)))
		self.write({
			'line_ids': lines_vals,
			'foreign_calculator_id': False,
			'additional_service_calc': False,
			})
		return {"type": "ir.actions.do_nothing",}

	def _prepare_clone_calc_items_vals(self, calc_line):
		# Hook method
		return {
			'base1': calc_line.base1,
			'base2': calc_line.base2,
			'height1': calc_line.height1,
			'height2': calc_line.height2,
			'quantity': calc_line.quantity,
			'crystal_num': calc_line.crystal_num,
			'descuadre': calc_line.descuadre,
			}

class GlassSaleCalculatorLine(models.Model):
	_name = 'glass.sale.calculator.line'

	calculator_id = fields.Many2one('glass.sale.calculator', 
		ondelete='cascade', readonly=True)
	type_calculator = fields.Selection(related='calculator_id.type_calculator')
	base1 = fields.Integer('Base 1 (mm)', required=True)
	base2 = fields.Integer('Base 2 (mm)', required=True)
	height1 = fields.Integer('Altura 1 (mm)', required=True)
	height2 = fields.Integer('Altura 2 (mm)', required=True)
	quantity = fields.Integer('Cantidad', required=True, default=0)
	crystal_num = fields.Char('Nro. de Cristales', required=True)
	descuadre = fields.Char('Descuadre', size=7)
	descuadre_bool = fields.Boolean('Descuadre') # aux field
	
	# NOTE EL campo descuadre se viene usando desde versiones anteriores para 
	# la determinación de los lados donde un servicio es aplicado, por motivos de compatibilidad se seguirá haciendo de esa forma,
	# tneer en cuenta par auna futura refactorización y empezar a usar 'service_sides' en su lugar.
	service_sides = fields.Char(string=u'Lados aplicación', size=7, 
		help=u'Lados donde se aplicará el servicio.')
	# Services
	perforacion = fields.Integer('Perforaciones') # Nro de perforaciones
	arenado_id = fields.Many2one('glass.type.arenado', string='Arenado')
	biselado_id = fields.Many2one('glass.type.biselado', string='Biselado')
	#biselado_id = fields.Many2one('glass.biselado.service.assign', string='Biselado')
	biselado_sides = fields.Char('Lados Biselado', size=7)
	entalle = fields.Integer('Entalle') # Nro de entalles
	polished_id = fields.Many2one('sale.pulido.proforma', string=u'Pulido')
	currency_id = fields.Many2one(related='calculator_id.currency_id', readonly=True)
	#services_amount_repr = fields.Text()
	services_amount = fields.Monetary('Monto Servicios', currency_field='currency_id', readonly=True)
	page_number = fields.Integer(u'Nro. Pág.')
	template = fields.Boolean('Plantilla')
	packed = fields.Boolean('Embalado')
	image = fields.Binary('Imagen')
	measures = fields.Char('Medidas', compute='_compute_crystal_measures', readonly=True, store=True)
	perimeter = fields.Float(u"Perímetro", digits=(12, 4), compute='_compute_crystal_measures', store=True)
	area = fields.Float(u'Área', digits=(10, 4), 
		compute='_compute_crystal_measures', readonly=True, 
		help=u'Área de los cristales calculada en M2.',store=True)
	unit_area = fields.Float(u'Área Unitaria', digits=(10, 4), 
		compute='_compute_crystal_measures', readonly=True, 
		help=u'Área unitaria de los cristales calculada en M2.',store=True)
	sold_area = fields.Float(u'Área cobrada', digits=(10,4), 
		compute='_compute_crystal_measures', readonly=True, 
		help=u'Área de los cristales calculada en la unidad de medida de la línea de pedido de venta.', store=True)

	_sql_constraints = [
		('check_positive_base1','CHECK (base1>0)',u'La Base 1 debe ser mayor a cero.'),
		('check_positive_base2','CHECK (base2>0)',u'La Base 2 debe ser mayor a cero.'),
		('check_positive_height1','CHECK (height1>0)',u'La Altura 1 debe ser mayor a cero.'),
		('check_positive_height2','CHECK (height2>0)',u'La Altura 2 debe ser mayor a cero.'),
		('check_positive_quantity','CHECK (quantity>0)',u'La Cantidad debe ser mayor a cero.'),
		('check_not_negative_entalle','CHECK (entalle>=0)',u'El número de entalles debe ser positivo.'),
		('check_not_negative_perforacion','CHECK (perforacion>=0)',u'El número de perforaciones debe ser positivo.'),
	]

	def _get_linked_services(self):
		# Determina los servicios vinculados al cristal templado,
		# Si el cristal tiene servicios vinculados no concluídos, 
		# no se dará automáticamente por producido a la salida de horno.
		# Enc aso no tenga servicios vinculados y tenga otras etapas inconclusas , se fuerza el producido

		# Obtener los sevicios vinculados a cristales de producción (tmeplados y insulados comunes)
		perm_services_linked = self.calculator_id.type_calculator in ('common_product',)
		if not perm_services_linked:
			return []
		
		linked_services = []
		
		if self.perforacion > 0:
			linked_services.append('perforacion')
		if self.entalle > 0:
			linked_services.append('entalle')
		if self.arenado_id:
			linked_services.append('arenado')
		if self.biselado_id:
			linked_services.append('biselado')
		if self.polished_id:
			linked_services.append('pulido')
		# TODO agg los serviciso adicionales de plantilla y embalado (tmr)
		return linked_services


	#@api.depends('base1', 'base2', 'height1', 'height2', 'quantity', 'service_sides')
	@api.depends('base1', 'base2', 'height1', 'height2', 'quantity', 'descuadre')
	def _compute_crystal_measures(self):
	#def _compute_area(self):
		# Compute area, perimeter, measures label, unit area
		for rec in self:
			# 1) compute area
			l1 = float(max([rec.base2, rec.base1]))
			l2 = float(max([rec.height1, rec.height2]))
			unit_area = float_round((l1 * l2 / 1000000.0), precision_rounding=0.0001)
			total_area = unit_area * rec.quantity
			if rec.quantity:
				assert float_compare(total_area/rec.quantity, unit_area, 4) == 0, 'Wrong value for Unit Area.'
			
			# 2) compute sold area:
			# Debería ser el mínima vendible por cada cristal
			sale_uom  = rec.calculator_id.sale_uom_id
			prod_uom  = rec.calculator_id.product_id.categ_id.uom_min_sale
			min_sale  = rec.calculator_id.product_id.categ_id.min_sale
			
			value = prod_uom._compute_quantity(unit_area, sale_uom)
			if min_sale > value:
				unit_area = prod_uom._compute_quantity(min_sale, sale_uom)

			# 3) compute perimeter
			perimeter = 0.0
			#if rec.type_calculator == 'service' and rec.service_sides:
			if rec.type_calculator == 'service' and rec.descuadre:
				"""Para la huevada de los servicios se decidió 
				re-usar el campo de descuadres para especificar los lados donde se aplicará el servicio."""
				dcd = rec.validate_descuadre()
				perimeter = rec.get_perimeter_from_sides(dcd)
				if perimeter is None:
					raise UserError(u'El valor de lados asignado a los descuadres es incorrecto.')
			else:
				perimeter = float(sum([rec.base1, rec.base2, rec.height1, rec.height2]) * rec.quantity) / 1000.0
			
			rec.unit_area = unit_area
			rec.area = total_area
			rec.sold_area = float_round(unit_area * rec.quantity, precision_rounding=0.0001)
			rec.perimeter = float_round(perimeter, precision_rounding=0.0001)
			rec.measures = rec._get_measures_label() # 4) compute measures label


	def get_perimeter_from_sides(self, sides_list):
		dcd = list(set(sides_list))
		"""Para la huevada de los servicios se decidió 
		re-usar el campo de descuadres para especificar los lados donde se aplicará el servicio."""
		dic_dcd = {
			'1': 'height1',
			'2': 'base2',
			'3': 'height2',
			'4': 'base1',
		}
		dcd = [getattr(self, dic_dcd.get(i, ''), None) for i in dcd]
		if None in dcd:
			return None
			#raise UserError(u'El valor de lados asignado es incorrecto.')
		perimeter = (sum(dcd) * self.quantity) / 1000.0
		return perimeter

	# Mejorar ésta vaina, la prisa...
	def get_perimeter_biselado(self):
		sides = split_by_comma(self.biselado_sides)
		perimeter = self.get_perimeter_from_sides(sides) if sides else 0.0
		if perimeter is None:
			raise UserError(u'El valor de lados asignado a los lados de biselado es incorrecto.')
		return perimeter

	# Refactor
	#@api.depends('base1', 'base2', 'height1', 'height2')
	def _update_measures_label(self):
		for rec in self:
			rec.measures = rec._get_measures_label()

	@api.model
	def _get_measures_label(self):
		# esure one, por motivos de performance, obviar
		b1, b2, h1, h2, label = str(self.base1), str(self.base2), str(self.height1), str(self.height2), ''
		if b1 == b2: label += b1
		else: label += b1 + '/' + b2
		label += 'X'
		if h1 == h2: label += h1
		else: label += h1 + '/' + h2
		return label

	@api.constrains('page_number')
	def _verify_page_number(self):
		for rec in self:
			if rec.page_number <= 0 and rec.type_calculator != 'service':
				raise ValidationError(u'El Nro de Página debe ser mayor a cero.')

	@api.constrains('crystal_num', 'quantity')
	def _check_crystal_num(self):
		for rec in self:
			validate = self.validate_crystal_number(rec.crystal_num,rec.quantity)
			if type(validate) in (str,unicode):
				raise ValidationError(validate)


	@api.constrains('descuadre', 'biselado_sides', 'base1', 'base2', 'height1', 'height2')
	def _check_descuadre_and_measures(self):
		"""Check measures & descuadre"""
		for rec in self:
			if rec.descuadre:
				validate = rec.validate_descuadre()
				if type(validate) in (str, unicode):
					raise ValidationError(validate)

			if rec.biselado_sides:
				sides = split_by_comma(rec.biselado_sides)
				if not sides or self.get_perimeter_from_sides(sides) is None:
					raise ValidationError(u'Error en la asignación de lados de biselado')

			if not rec.descuadre and (rec.base1 != rec.base2 or rec.height1 != rec.height2) and rec.type_calculator != 'service':
				raise ValidationError(u'Existen dimensiones desiguales en las medidas %s y no se ha asignado descuadres, es posible que una(s) dimensiones no estén correctamente establecidas.' % rec.measures)

	def validate_descuadre(self):
		self.ensure_one()
		string = self.descuadre
		if not string:
			return []
		# New
		dcd = split_by_comma(string)
		if not dcd:
			return u'El formato del descuadre es incorrecto, el patrón debe ser similar a 1,2 (con un máximo de 4 descuadres)\nValor ingresado: ' + string
		
		if self.type_calculator == 'service': # se reutiliza, para mentener compatibilidad...
			return dcd
		
		duplicates = list(set([i for i in dcd if dcd.count(i)>1]))
		if any(duplicates):
			return u'Los siguientes valores se encuentran duplicados en el descuadre:\n%s\nValor ingresado: %s'%(' '.join(duplicates),string)

		h1 = self.height1
		h2 = self.height2
		b1 = self.base1
		b2 = self.base2
		data_line = u'¡No existe descuadre en el lado %d!\nDetalle de la línea:\nNro: %s\nAltura 1: %d\nAltura 2: %d\nBase 1: %d\nBase 2: %d'
		if '1' in dcd and b2 == b1:
			return data_line % (1, self.crystal_num or '', h1, h2, b1, b2)
		if '3' in dcd and b2 == b1:
			return data_line % (3, self.crystal_num or '', h1, h2, b1, b2)
		if '2' in dcd and h2 == h1:
			return data_line % (2, self.crystal_num or '', h1, h2, b1, b2)
		if '4' in dcd and h2 == h1:
			return data_line % (4, self.crystal_num or '', h1, h2, b1, b2)
		return dcd

	@api.model
	def validate_crystal_number(self, string, qty):
		regex1 = r'\b[0-9]{1,}-[0-9]{1,}\b' # validate format 3-5 23-56 ...
		#regex2 = r'^[0-9](,[0-9])*$' 		
		regex2 = r'^[0-9]+(,[0-9]+)*$'		# validate format 1,2,3,4,5...^[0-9]+(,[0-9]+)*$
		regex3 = r'^[0-9]+$'				# validate only numbers
		if qty == 0:
			return u'La cantidad ingresada debe ser mayor a cero.'
		not_zero = lambda x: all([int(j) for j in x])
		if re.match(regex1, string, re.I):
			acadnro = string.split('-')
			if not not_zero(acadnro):
				return u"No es posible asignar cero a un número de cristal.\nValor ingresado: "+string
			if len(acadnro)>2:
				return u"Sólo puede ingresar un rango en el número de cristales (Ejemplo: 2-5)\nValor ingresado: "+string
			if int(acadnro[0])>int(acadnro[1]):
				return u"El rango de cristales debe comenzar con el número menor (Ejemplo: 2-5)\nValor Ingresado: "+string	
			init,end = int(acadnro[0]),int(acadnro[1])
			if (end-init+1) != qty:
				return u"El número de cristales debe corresponder a la cantidad indicada (%d):\nValor ingresado: %s"%(qty,string)
			return list(range(init,end+1))
		elif re.match(regex2, string, re.I) and ',' in string:
			acadnro = string.split(',')
			if not not_zero(acadnro):
				return u"No es posible asignar cero a un número de cristal.\nValor ingresado: "+string
			if len(acadnro) != qty:
				return u"El número de cristales debe corresponder a la cantidad indicada (%d)\nValor Ingresado: %s"%(qty,string)
			
			duplicates = list(set([i for i in acadnro if acadnro.count(i) > 1]))
			if any(duplicates):
				return u'Los siguientes valores se encuentran duplicados en el número de cristales:\n%s\nValor ingresado: %s'%(' '.join(duplicates),string)
			return [int(n) for n in acadnro]
		elif re.match(regex3, string, re.I):
			if int(string) == 0:
				return u"No es posible asignar cero a un número de cristal.\nValor ingresado: "+string
			if qty>1:
				return u"El número de cristales debe corresponder a la cantidad indicada: "+str(qty)
			return [int(string)]
		else:
			return u'Error en formato de número de cristales!\nEl formato de número de cristal ingresado debe ser un número único, o en su defecto similar a 1-4 (rango de cristales) o una secuencia de números, similar a 1,2,3...\nValor Ingresado:'+string

	@api.onchange('crystal_num','quantity')
	def _onchange_crystal_num_quantity(self):
		res = {}
		if self.crystal_num and self.quantity:
			validate = self.validate_crystal_number(self.crystal_num,self.quantity)
			if type(validate) in (str,unicode):
				res['warning']={'title':u'Error en validación de número de cristales','message':validate}
		return res

	@api.onchange('descuadre')
	def _onchange_descuadre(self):
		res = {}
		self.update({'descuadre_bool':bool(self.descuadre)})
		if self.base1 and self.base2 and self.height1 and self.height2 and self.descuadre:
			validate = self.validate_descuadre()
			if type(validate) in (str,unicode):
				res['warning']={'title':u'Error en validación de descuadre','message':validate}
		return res

	@api.onchange('biselado_sides', 'biselado_id')
	def _onchange_biselado_fields(self):
		res = {}
		if not self.biselado_id:
			self.biselado_sides = False
			return res

		if self.biselado_sides:
			sides = split_by_comma(self.biselado_sides)
			if not sides or self.get_perimeter_from_sides(sides) is None:
				res['warning']={
					'title': u'Error en validación de lados de biselado', 
					'message': 'Los lados del biselado deben ser similares a "1,2,3,4"'
					}
		return res

	@api.onchange('base1', 'height1')
	def _onchange_base_height(self):
		if self.base2==0:
			self.base2 = self.base1
		if self.height2==0:
			self.height2 = self.height1

	@api.model
	def create(self,values):
		b1 = values['base1']
		b2 = values['base2']
		h1 = values['height1']
		h2 = values['height2']
		dcd = values.get('descuadre',[])
		values['descuadre_bool'] = bool(values.get('descuadre', False))
		t = super(GlassSaleCalculatorLine, self).create(values)
		t.image = self.draw_crystal(b1, b2, h1, h2, dcd, is_service=t.type_calculator == 'services')
		return t

	@api.multi
	def write(self, values):
		image_depends = ('base1', 'base2', 'height1', 'height2', 'descuadre')
		for rec in self:
			if any(i for i in image_depends if i in values):
				b1 = values.get('base1',rec.base1)
				b2 = values.get('base2',rec.base2)
				h1 = values.get('height1',rec.height1)
				h2 = values.get('height2',rec.height2)
				dcd = values.get('descuadre',(rec.descuadre or None))
				is_service = values.get('type_calculator', rec.type_calculator) == 'services'
				if 'image' not in values:
					values['image'] = self.draw_crystal(b1, b2, h1, h2, dcd, is_service=is_service)
				values['descuadre_bool'] = bool(dcd)
		return super(GlassSaleCalculatorLine, self).write(values)

	def show_crystal_image(self):
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name':'Imagen de Cristal',
			'res_id':self.id,
			'type': 'ir.actions.act_window',
			'res_model': self._name,
			'view_id': self.env.ref('%s.calculator_line_image_only_form'%module).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			}

	def back_view(self):
		module = __name__.split('addons.')[1].split('.')[0]
		module = self._context.get('module_name',module)
		values = self.calculator_id.order_line_id.with_context(module_name=module)._prepare_calculator_vals()
		return {
			'name':values['act_name'],
			'res_id': self.calculator_id.id,
			'type': 'ir.actions.act_window',
			'res_model': self.calculator_id._name,
			'view_id': self.env.ref(values['view']).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			}

	def get_crystal_numbers(self):
		self.ensure_one()
		res = self.validate_crystal_number(self.crystal_num, self.quantity)
		if type(res) != list:
			raise UserError(res)
		return sorted(res)

	def _get_sale_quantity(self):
		return self.perimeter if self.calculator_id.product_id.allow_metric_sale else self.sold_area
		
	# @Params : base1:int base2:int height2:int height2:int dcd:descuadre -> text 
	# @return : encoded file to base64
	@api.model
	def draw_crystal(self, b1, b2, h1, h2, dcd=None, bg_color=None, is_service=False):
		size = [b1, b2, h1, h2]
		if not all(size):
			return False
		
		if is_service:
			dcd = None
			bg_color = bg_color or (0, 158, 207)
		else:
			bg_color = bg_color or (255, 154, 82) # standard prod color

		e_max = max(size)
		size = [x*500/e_max for x in size]
		
		w=850 # img width 
		h=650 # img height 
		image = Image.new("RGB",(w,h),color=(255,255,255))
		draw = ImageDraw.Draw(image)
		left= 170 
		top = 250 
		
		dcd = [int(x) for x in dcd.split(',')] if dcd else []

		p0 = (0.+left ,h/2+top)
		p1 = (0.+left, h/2-size[2]+top)
		p2 = (size[1]+left, h/2-size[3]+top)
		
		p3 = (size[0]+left, h/2-0.+top)
		p3_x = size[0]+left
		p3_y = h/2-0.+top

		if 1 in dcd:
			if b2>b1:
				p0 = ( p1[0]+(size[1]-size[0]),p0[1])
				p3_x = size[1]+left
			else:
				p1=(p1[0]+(size[0]-size[1]),p1[1])
				p2=(p3[0],p2[1])
		if 4 in dcd:
			if h2<h1:
				tmp=p1
				p1=(p0[0],p2[1])
				p3=(p3[0],p1[1]-(size[2]-size[3]))
				p3_y = p1[1]+(size[3]-(size[2]-size[3]))
			else:
				p2 = (size[1]+left, h/2-size[2]+top)
				p3_y = h/2-(size[2]-size[3])+top
		p3 = (p3_x,p3_y)
		points = (p0,p1,p2,p3,p0,)

		path = os.path.join(os.path.dirname(os.path.abspath(__file__)))+'/arial.ttf'
		font = ImageFont.truetype(path,36)
		
		maxheight = size[3] if p0[1]<p3[1] else size[2]
		maxwidth  = size[1] if size[0]<size[1] else size[0]
		moreleft  = p0[0] if p0[0]<p1[0] else p1[0]
		moreright = p3[0] if p2[0]<p3[0] else  p2[0]
		moretop   = p2[1] if p1[1]>p2[1] else p1[1]
		moreboot  = p3[1] if p0[1]<p3[1] else p0[1]

		draw.text((moreleft-155,(moreboot-(maxheight/2))), "LADO 1",font=font,fill=(255, 43, 0,128))
		draw.text((moreleft-155,((moreboot-(maxheight/2))+30)), str(h1)+" mm",font=font,fill=(51, 123, 164))
		
		draw.text((moreleft+((maxwidth/2)-120),moretop-80), "LADO 2",font=font,fill=(255, 43, 0,128))
		draw.text((moreleft+((maxwidth/2)-120),moretop-50), str(b2)+" mm",font=font,fill=(51, 123, 164))

		draw.text((moreright+20,(moreboot-(maxheight/2))), "LADO 3",font=font,fill=(255, 43, 0,128))
		draw.text((moreright+20,(moreboot-(maxheight/2))+30), str(h2)+" mm",font=font,fill=(51, 123, 164))

		draw.text((moreleft+((maxwidth/2)-120),moreboot+10), "LADO 4",font=font,fill=(255, 43, 0,128))
		draw.text((moreleft+((maxwidth/2)-120),moreboot+40), str(b1)+" mm",font=font,fill=(51, 123, 164))
		draw.polygon((points),fill=bg_color)

		for d in dcd:
			draw.line((points[d-1][0],points[d-1][1],points[d][0],points[d][1]),fill=(0,61,4),width=15)

		path = self.env['main.parameter'].search([])[0].dir_create_file+'glass.png'
		image.save(path)
		with open(path,'rb') as file:
			encoded_file = base64.b64encode(file.read())
		if os.path.exists(path):
			os.remove(path)
		return encoded_file


# Servicios asociados a la línea de calculadora
# TODO REMOVE
class GlassCalculatorService(models.Model):
	_name = 'glass.calculator.service'
	_description = 'Servicio de Calculadora'

	name = fields.Char('Nombre', required=True, copy=False)
	#product_id = fields.Many2one('product.product', required=True)

	_sql_constraints = [('uniq_name', 'UNIQUE (name)', u'EL nombre del servicio de calculadora debe ser único')]

	