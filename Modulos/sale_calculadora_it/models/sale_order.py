# -*- coding: utf-8 -*-
from odoo import fields, models,api, _
from odoo.exceptions import UserError

class SaleOrder(models.Model):
	_inherit='sale.order'

	before_invoice = fields.Boolean('Factura Adelantada',default=False)

	def unlink(self):
		for order in self:
			if order.invoice_count > 0:
				raise UserError(u"No se puede eliminar un pedido de venta si ya fue facturado")
			# for l in order.order_line:
			# 	if l.id_type:
			# 		raise UserError(u"No se puede eliminar un pedido de venta si tiene calculadora")
		return super(SaleOrder, self).unlink()

class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	calculator_id = fields.Many2one('glass.sale.calculator', string='Calculadora', readonly=True, copy=False)

	def show_calculator(self):
		self.ensure_one()
		calculator = self.env['glass.sale.calculator']
		values = self._prepare_calculator_vals() 
		if self.calculator_id:
			calculator = self.calculator_id
		else:
			calculator = calculator.create(values['vals'])
			self.calculator_id = calculator.id
		ctx = dict(self._context or {}, 
			default_product_id=self.product_id.id, 
			default_order_line_id=self.id,
			default_type_calculator=values['vals']['type_calculator'])
		return {
			'name':values['act_name'],
			'res_id': calculator.id,
			'type': 'ir.actions.act_window',
			'res_model': calculator._name,
			'view_id': self.env.ref(values['view']).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': ctx,
		}

	def _prepare_calculator_vals(self):
		self.ensure_one()
		product = self._context.get('defined_product',self.product_id)
		module_name = self._context.get('module_name',(__name__.split('addons.')[1].split('.')[0]))
		
		# TODO pendiente de definir
		# tomar en cuenta si usan guía de remisión para entregar ésto
		# pero también si dicha guía debe seguir la secuencia de la guía de albaranes
		# q huevada
		# if product.is_production_service:
		if product.type == 'service':
			return {
				'vals': {'order_line_id': self.id, 'type_calculator': 'service'},
				'view': '%s.glass_calculator_services_form' % module_name,
				'act_name':'Calculadora de Servicios',
			}
		else:
			return {
				'vals': {'order_line_id': self.id, 'type_calculator': 'common_product'},
				'view': '%s.glass_calculator_form_view' % module_name,
				'act_name':'Calculadora de cristales',
			}

	@api.multi
	def write(self,values):
		"""Forzar a eliminar la línea de pedido para mantener consistenci con calculadora."""
		for line in self:
			if line.calculator_id and 'product_id' in values:
				product = self.env['product.product'].browse(values['product_id'])
				raise UserError(u'Por motivos de consistencia de datos no es posible modificar el producto en una línea de pedido de venta/cotización asociada a una calculadora de cristales, deberá eliminar y volver a crear dicha línea.\nLínea con producto: %s' % product.name)

		#if 'product_id' in values:
			#prod = self.env['product.product'].browse(values['product_id'])
			#self.calculator_id.type_calculator = self.with_context(defined_product=prod)._prepare_calculator_vals()['vals']['type_calculator']
		return super(SaleOrderLine, self).write(values)


