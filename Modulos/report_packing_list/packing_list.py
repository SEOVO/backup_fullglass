# -*- coding: utf-8 -*-
import base64,StringIO,decimal
from odoo import api, fields, models, exceptions
from datetime import datetime,timedelta
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.lib.utils import ImageReader
from reportlab.pdfgen import canvas

class PackingList(models.Model):
	_inherit = 'packing.list'

	def print_report(self):
		self.ensure_one()
		packing = self
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
		pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))
		path = self.env['main.parameter'].search([])[0].dir_create_file
		file_name = 'Packing-List %s.pdf'%self.name.replace('/','-')
		path+=file_name
		c = NumberedCanvas(path, pagesize= A4)
		width ,height  = A4  # 595 , 842
		pos_left = 20
		size_widths = [300,60,60]
		total_width_size = sum(size_widths)
		pos = height - 115
		separator = 12
		self.header(c,width,height,pos_left,size_widths)
		for item in packing.grouped_lines:
			c.setFont("Calibri-Bold", 9)
			pos = self.get_pos(c,width,height,pos,separator,pos_left,size_widths)
			aux = pos_left
			lines = packing.selected_line_ids.filtered(lambda x: x.product_id.id == item.product_id.id)
			c.drawString(aux,pos,item.product_id.default_code+' '+item.product_id.name)
			aux += size_widths[0]
			c.drawString(aux,pos,str(item.area) + ' M2')
			aux += size_widths[1]
			c.drawString(aux,pos,str(len(lines))+' Piezas')
			aux += size_widths[2]
			c.drawString(aux,pos,str(item.weight)+' Kg.')
			container = [lines[i:i+4] for i in range(0,len(lines),4)]
			pos-=separator
			c.setFont("Calibri", 9)
			for group in container:
				aux2 = pos_left + 20
				for line in group:
					c.drawString(aux2,pos,line.order_id.name+'-'+str(line.crystal_number)+'('+line.measures+')')
					aux2 += 130
				pos-=separator
		
		pos = self.get_pos(c,width,height,pos,separator,pos_left,size_widths)
		c.setFont("Calibri-Bold", 9)
		c.drawString(50,pos,'Total Piezas: '+str(len(packing.selected_line_ids)))
		c.drawString(235,pos,'Total M2: '+str(sum(packing.grouped_lines.mapped('area'))))
		c.drawString(420,pos,'Total Peso: '+str(sum(packing.grouped_lines.mapped('weight')))+' Kg.')
		c.roundRect(pos_left,pos-12,width-40,25,5)
		c.showPage()
		c.save(555,745)

		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),
				'content_type':'application/pdf'
			})
		return export.export_file(clear=True,path=path)

	# header para el segundo reporte
	def header(self,c,wReal,hReal,pos_left,size_widths=None):
		try:
			company = self.env['res.company'].search([])[0]
		except IndexError:
			raise exceptions.Warning(u"No ha confugurado su Compañía.\n Configure los datos de su compañía para poder mostrarlos en este reporte.")
		if company.logo:
			file = base64.b64decode(company.logo)
			c.drawImage(ImageReader(StringIO.StringIO(file)),pos_left,792,width=80,height=42,mask=None)
		else:
			c.setFont("Calibri", 12)
			c.drawString(pos_left,800,company.name or '')
		# Datos de Conpañía para la cabecera
		c.setFont("Calibri", 6)
		c.drawString(pos_left,784,company.street or '')
		c.drawString(pos_left,778,u'Telf. : '+company.phone if company.phone else '')
		c.drawString(pos_left+70,778,u'Fax: '+company.fax if company.fax else '')
		c.drawString(pos_left,770,company.website or '')
		c.drawString(pos_left+100,770,company.email or '')
		ruc = company.partner_id.nro_documento or ''
		c.setFont("Calibri", 8)
		c.drawString(pos_left,762,'RUC: '+ruc)		
		c.roundRect(380,760,200,50,5)
		c.setFont("Calibri-Bold",13)
		c.drawString( 440,785,'PACKING LIST')
		c.setFont("Calibri",12)
		c.drawString( 440,765,self.name)
		warehouse = self.location.location_code.get_warehouse()
		if warehouse:
			c.setFont("Calibri",9)
			c.drawString(380,745,warehouse.name)
		c.line(pos_left,738,580,738)
		c.setFont("Calibri-Bold",9)
		c.drawString(pos_left,730,self.location.name)
		c.drawString(pos_left+100,730,str(len(self.selected_line_ids)) + ' Piezas')
		area   = sum(self.grouped_lines.mapped('area'))
		weight = sum(self.grouped_lines.mapped('weight'))
		c.drawString(pos_left+150,730,str(area) + ' M2')
		c.drawString(pos_left+200,730,str(weight) + ' Kg.')
	
	def get_pos(self,c,wReal,hReal,pos,valor,pos_left,size_widths):
		if pos <40:
			c.showPage()
			self.header(c,wReal,hReal,pos_left,size_widths)
			c.setFont("Calibri-Bold", 8)
			return hReal-127
		else:
			return pos-valor

class NumberedCanvas(canvas.Canvas):
	def __init__(self, *args, **kwargs):
		canvas.Canvas.__init__(self, *args, **kwargs)
		self._saved_page_states = []

	def showPage(self):
		self._saved_page_states.append(dict(self.__dict__))
		self._startPage()

	def save(self,x,y): # x,y posición de la paginación(width,height)
		"""add page info to each page (page x of y)"""
		count_pages = len(self._saved_page_states)
		for state in self._saved_page_states:
			self.__dict__.update(state)
			self.draw_page_number(count_pages,x,y)
			canvas.Canvas.showPage(self)
		canvas.Canvas.save(self)

	def draw_page_number(self,page_count,x,y):
		self.setFont("Calibri", 9)
		self.drawRightString(x,y,
			"Página %d de %d" % (self._pageNumber, page_count))