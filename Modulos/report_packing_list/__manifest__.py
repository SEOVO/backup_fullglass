# -*- encoding: utf-8 -*-
{
	'name': u'Reporte de Packing List',
	'category': 'reports',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['packing_list_fullglass','export_file_manager_it'],
	'version': '1.0.0',
	'description':"""
	Módulo que habilita el el action menu para emitir el reporte de orden de Packing List
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'report_packing_list_view.xml',
		],
	'installable': True
}
