# -*- encoding: utf-8 -*-
{
    'name': 'Reporte de Pendientes - Fullglass',
    'category': 'report',
    'author': 'ITGRUPO-POLIGLASS',
    'depends': ['export_file_manager_it','glass_production_order','sales_team'],
    'version': '1.0',
    'description':"""
        Modulo para emitir el reporte de Pendientes en Fullglass
    """,
    'auto_install': False,
    'demo': [],
    'data':    [
        'wizard/report_of_pendings_wizard_view.xml',
        ],
    'installable': True
}