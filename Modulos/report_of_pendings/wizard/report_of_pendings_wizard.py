# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime,timedelta
import base64,string 

class ReportPendingWizard(models.TransientModel):
	_name='report.pending.wizard'

	def _get_start_date(self):
		now = datetime.now().date()
		start_date = now - timedelta(days=30)
		return start_date

	start_date = fields.Date(string='Fecha Inicio',default=_get_start_date)
	end_date = fields.Date(string='Fecha Fin',default=fields.Date.today)
	search_param = fields.Selection([
		('glass_order', u'Órdenes de Producción'),
		('service_order', u'Órdenes de servicios')],
		string=u'Tipo de Órdenes',default='glass_order')
	glass_order_ids = fields.Many2many('glass.order',
		relation='glass_order_rep_pendings_rel', column1='wizard_id', column2='glass_order_id',
		string=u'Órdenes de Producción', 
		domain="[('state','!=','returned'), ('type_sale', '=', 'production')]")
	service_order_ids = fields.Many2many('glass.order',
		relation='service_order_rep_pendings_rel', column1='wizard_id', column2='service_order_id',
		string=u'Órdenes de Servicios', 
		domain="[('state','!=','returned'), ('type_sale', '=', 'services')]")
	product_id = fields.Many2one('product.product', string='Producto')
	customer_id = fields.Many2one('res.partner', string='Cliente', domain=[('customer','=',True)])
	show_in_dates_range = fields.Boolean('Filtrar por rango de fechas')
	#all_items = fields.Boolean('Todos')

	@api.onchange('search_param')
	def _onchange_search_param(self):
		domain = []
		if self.search_param == 'glass_order':
			domain = [('type', '!=', 'service')]
		elif self.search_param == 'service_order':
			domain = [('type', '=', 'service')]
		return {'domain': {'product_id': domain}}


	def do_rebuild(self):
		from xlsxwriter.workbook import Workbook
		path = self.env['main.parameter'].search([])[0].dir_create_file
		file_name = 'Reporte de pendientes.xlsx'
		path+=file_name
		workbook = Workbook(path)
		worksheet = workbook.add_worksheet("Pendientes")	
		bold = workbook.add_format({'bold': True})
		normal = workbook.add_format()
		normal.set_border(style=1)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=1)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(11)
		boldbord.set_bg_color('#bfbfbf')
		decinal4 = workbook.add_format({'num_format':'0.0000'})
		decinal4.set_border(style=1)	
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberdos.set_border(style=1)
		bord = workbook.add_format()
		bord.set_border(style=1)
		#bord.set_text_wrap()
		title = workbook.add_format({'bold': True})
		title.set_align('center')
		title.set_align('vcenter')
		title.set_text_wrap()
		title.set_font_size(20)
		worksheet.set_row(0, 30)
		boldborda = workbook.add_format({'bold': True})
		boldborda.set_border(style=2)
		boldborda.set_align('center')
		boldborda.set_align('vcenter')
		boldborda.set_text_wrap()
		boldborda.set_font_size(9)
		boldborda.set_bg_color('#ffff40')
		fdatetime = workbook.add_format({'num_format': 'dd/mm/yyyy hh:mm','border':1})
		fdate = workbook.add_format({'num_format': 'dd/mm/yyyy','border':1})

		x= 9 # caminador de iteraciones para cada linea			
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		com = self.env['res.company']._company_default_get(self._name)
		if com.logo:
			from StringIO import StringIO
			image_data = {'image_data':StringIO(base64.b64decode(com.logo)),'x_scale': 0.4, 'y_scale': 0.4}
			worksheet.insert_image('A1','fullglass.jpg',image_data)

		worksheet.merge_range(1,6,0,12, u"REPORTE DE PENDIENTES",title)
		
		if self.start_date and self.end_date and self.show_in_dates_range:
			worksheet.merge_range(2,7,2,9, u"FECHA DEL: %s AL %s" % (self.start_date,self.end_date),bold)
		
		elif self.search_param == 'glass_order' and self.glass_order_ids:
			worksheet.write(3,7, u"ÓRDENES DE PRODUCCIÓN:", bold)
			worksheet.write(3,8, ','.join(self.glass_order_ids.mapped('name')),bold)

		elif self.search_param == 'service_order' and self.service_order_ids:
			worksheet.write(3,7, u"ÓRDENES DE SERVICIOS:", bold)
			worksheet.write(3,8, ','.join(self.service_order_ids.mapped('name')),bold)

		if self.product_id:
			worksheet.write(4,7, u"PRODUCTO:", bold)
			worksheet.write(4,8, self.product_id.name,bold)

		if self.customer_id:
			worksheet.write(5,7, u"CLIENTE:", bold)
			worksheet.write(5,8, self.customer_id.name,bold)
		
		ext_sql = ''
		
		#if self.show_in_dates_range:
		if self.show_in_dates_range:
			ext_sql += " AND go.date_order::date >= '%s' AND go.date_order::date <= '%s' " % (self.start_date, self.end_date)
			
		if self.search_param == 'glass_order':
			ext_sql += " AND go.type_sale = 'production' "
			if self.glass_order_ids and not self.show_in_dates_range:
				ext_sql += " AND go.id IN (%s) " % ','.join(map(str, self.glass_order_ids.ids))

		if self.search_param == 'service_order':
			ext_sql += " AND go.type_sale = 'services' "
			if self.service_order_ids and not self.show_in_dates_range:
				ext_sql += " AND go.id IN (%s) " % ','.join(map(str, self.service_order_ids.ids))

		if self.product_id:
			ext_sql += " AND pp.id = %d " % self.product_id.id

		if self.customer_id:
			ext_sql += " AND rp.id = %d " % self.customer_id.id

		# if self.glass_order_ids and self.search_param == 'glass_order':
		# 	ext_sql += """ AND go.id IN (%s) """%','.join(map(str,self.glass_order_ids.ids))
		# else:
		# 	if self.start_date and self.end_date:
		# 		ext_sql += """ AND go.date_order::date >= '%s' AND go.date_order::date <= '%s' """%(self.start_date,self.end_date)
		# 	if self.product_id and self.search_param == 'product' and not self.all_items:
		# 		ext_sql += """ AND pp.id = %d"""%self.product_id.id

		# 	if self.customer_id and not self.all_items:
		# 		ext_sql += """ AND rp.id = %d"""%self.customer_id.id

		d = fields.Date
		dt = fields.Datetime
		tam_col = []

		if self.search_param == 'glass_order':

			worksheet.merge_range(7,1,8,1, u"ÓRDEN DE PROD.",boldbord)
			worksheet.merge_range(7,2,8,2, u"LOTES",boldbord)
			worksheet.merge_range(7,3,8,3, u"FECHA EMISIÓN",boldbord)
			worksheet.merge_range(7,4,8,4, u"FECHA PRODUCCIÓN",boldbord)
			worksheet.merge_range(7,5,8,5, u"FECHA DE ENTREGA",boldbord)
			worksheet.merge_range(7,6,8,6, u"NOMBRE CLIENTE",boldbord)
			worksheet.merge_range(7,7,8,7, u"OBRA",boldbord)
			worksheet.merge_range(7,8,8,8, u"COD PRESENTACIÓN",boldbord)
			worksheet.merge_range(7,9,8,9, u"UNIDAD",boldbord)
			worksheet.merge_range(7,10,8,10, u"DSCRIPCIÓN DE PRODUCTO",boldbord)
			worksheet.merge_range(7,11,8,11, u"TOTAL M2 SOLICITADOS",boldbord)
			worksheet.merge_range(7,12,8,12, u"TOTAL VIDRIOS SOLICITADOS",boldbord)		
			
			# worksheet.merge_range(7,13,7,14, u"PEND. DE OPTIMIZADO",boldbord)
			# worksheet.merge_range(7,15,7,16, u"PEND. DE CORTE",boldbord)
			# worksheet.merge_range(7,17,7,18, u"PEND. DE LAVADO",boldbord)
			# worksheet.merge_range(7,19,7,20, u"PEND. DE HORNO",boldbord)
			# worksheet.merge_range(7,21,7,22, u"PEND. DE TEMPLADO",boldbord)
			# worksheet.merge_range(7,23,7,24, u"PEND. DE INSULADO",boldbord)
			# worksheet.merge_range(7,25,7,26, u"PEND. DE PRODUCIDO",boldbord)
			# # SERVICIOS VINCULADOS
			# worksheet.merge_range(7,27,7,28, u"PEND. DE PULIDO",boldbord)
			# worksheet.merge_range(7,29,7,30, u"PEND. DE ARENADO",boldbord)
			# worksheet.merge_range(7,31,7,32, u"PEND. DE BISELADO",boldbord)
			# worksheet.merge_range(7,33,7,34, u"PEND. DE ENTALLE",boldbord)
			# worksheet.merge_range(7,35,7,36, u"PEND. DE PERFORACIÓN",boldbord)
			
			# Nuevo Órden solicitado por N. Castillo:
			worksheet.merge_range(7,13,7,14, u"PEND. DE OPTIMIZADO",boldbord)
			worksheet.merge_range(7,15,7,16, u"PEND. DE CORTE",boldbord)
			worksheet.merge_range(7,17,7,18, u"PEND. DE PULIDO",boldbord)
			worksheet.merge_range(7,19,7,20, u"PEND. DE BISELADO",boldbord)
			worksheet.merge_range(7,21,7,22, u"PEND. DE ENTALLE",boldbord)
			worksheet.merge_range(7,23,7,24, u"PEND. DE LAVADO",boldbord)
			worksheet.merge_range(7,25,7,26, u"PEND. DE HORNO",boldbord)
			worksheet.merge_range(7,27,7,28, u"PEND. DE TEMPLADO",boldbord)
			worksheet.merge_range(7,29,7,30, u"PEND. DE ARENADO",boldbord)
			worksheet.merge_range(7,31,7,32, u"PEND. DE PERFORACIÓN",boldbord)
			worksheet.merge_range(7,33,7,34, u"PEND. DE INSULADO",boldbord)
			worksheet.merge_range(7,35,7,36, u"PEND. DE PRODUCIDO",boldbord)

			worksheet.set_row(7, 25)

			for i in range(13,37,2):
				worksheet.write(8,i, u"Área (M2)",boldbord)
				worksheet.write(8,i+1, u"Nro de cristales",boldbord)

			query = """
			SELECT T.*, 
			T.p_items_opt + T.p_items_corte + T.p_items_lava + 
			T.p_items_horno + T.p_items_temp + T.p_items_insu + 
			T.p_items_produ + T.p_items_pulido + T.p_items_aren + 
			T.p_items_bisel + T.p_items_enta + T.p_items_perf
			AS total_pnd
			FROM(
			SELECT
			go.name AS order_name,
			(go.date_order-interval '5' hour) AS date_order,
			go.date_production::DATE AS date_prod,
			go.date_delivery AS date_deli,
			rp.name AS customer,
			go.obra,
			patv.name AS cod_pres,
			pu.name AS uom,
			pt.name AS product,
			--group
			REPLACE(REPLACE(array_agg(DISTINCT(COALESCE(gl.name,'')))::TEXT,'{',''),'}','') AS lots,
			SUM(gol.area) AS area_total,
			COUNT(gol.*) AS items_total,
			--optimizado
			SUM(CASE WHEN gll.optimizado THEN 0.0 ELSE gol.area END) AS p_area_opt,
			COUNT(*) FILTER(WHERE COALESCE(gll.optimizado,false)!=true) AS p_items_opt,
			--corte
			SUM(CASE WHEN gll.corte THEN 0.0 ELSE gol.area END) AS p_area_corte,
			COUNT(*) FILTER(WHERE COALESCE(gll.corte,false)!=true) AS p_items_corte,
			--lavado
			SUM(CASE WHEN gll.lavado THEN 0.0 ELSE gol.area END) AS p_area_lava,
			COUNT(*) FILTER(WHERE COALESCE(gll.lavado,false)!=true) AS p_items_lava,
			--horno
			SUM(CASE WHEN gll.horno THEN 0.0 ELSE gol.area END) AS p_area_horno,
			COUNT(*) FILTER(WHERE COALESCE(gll.horno,false)!=true) AS p_items_horno,
			--templado
			SUM(CASE WHEN gll.templado THEN 0.0 ELSE gol.area END) AS p_area_temp,
			COUNT(*) FILTER(WHERE COALESCE(gll.templado,false)!=true) AS p_items_temp,
			--insulado
			SUM(CASE WHEN gscl.from_insulado=true AND COALESCE(gll.insulado,false)!=true THEN gol.area ELSE 0.0 END) AS p_area_insu,
			COUNT(*) FILTER(WHERE gscl.from_insulado=true AND COALESCE(gll.insulado,false)!=true) AS p_items_insu,
			--producido
			SUM(CASE WHEN gll.producido THEN 0.0 ELSE gol.area END) AS p_area_produ,
			COUNT(*) FILTER(WHERE COALESCE(gll.producido,false)!=true) AS p_items_produ,
			--SERVICIOS
			--pulido
			SUM(CASE WHEN gscl.polished_id IS NOT NULL AND COALESCE(gll.pulido,false)!=true THEN gol.area ELSE 0.0 END) AS p_area_pulido,
			COUNT(*) FILTER(WHERE gscl.polished_id IS NOT NULL AND COALESCE(gll.pulido,false)!=true) AS p_items_pulido,
			--arenado
			SUM(CASE WHEN gscl.arenado_id IS NOT NULL AND COALESCE(gll.arenado,false)!=true THEN gol.area ELSE 0.0 END) AS p_area_aren,
			COUNT(*) FILTER(WHERE gscl.arenado_id IS NOT NULL AND COALESCE(gll.arenado,false)!=true) AS p_items_aren,
			--biselado
			SUM(CASE WHEN gscl.biselado_id IS NOT NULL AND COALESCE(gll.biselado,false)!=true THEN gol.area ELSE 0.0 END) AS p_area_bisel,
			COUNT(*) FILTER(WHERE gscl.biselado_id IS NOT NULL AND COALESCE(gll.biselado,false)!=true) AS p_items_bisel,
			--entalle
			SUM(CASE WHEN gscl.entalle > 0 AND COALESCE(gll.entalle,false)!=true THEN gol.area ELSE 0.0 END) AS p_area_enta,
			COUNT(*) FILTER(WHERE gscl.entalle > 0 AND COALESCE(gll.entalle,false)!=true) AS p_items_enta,
			--perforacion
			SUM(CASE WHEN gscl.perforacion > 0 AND COALESCE(gll.perforacion,false)!=true THEN gol.area ELSE 0.0 END) AS p_area_perf,
			COUNT(*) FILTER(WHERE gscl.perforacion > 0 AND COALESCE(gll.perforacion,false)!=true) AS p_items_perf
			FROM glass_order_line gol
			JOIN glass_order go ON go.id = gol.order_id
			JOIN glass_sale_calculator_line gscl ON gscl.id = gol.calc_line_id
			JOIN res_partner rp ON rp.id = go.partner_id
			JOIN product_product pp ON pp.id = gol.product_id
			JOIN product_template pt ON pt.id = pp.product_tmpl_id
			JOIN product_uom pu ON pu.id = pt.uom_id
			JOIN res_users ru ON ru.id = go.seller_id
			LEFT JOIN glass_lot_line gll ON (gll.order_line_id = gol.id AND COALESCE(gll.active, false) = true)
			LEFT JOIN glass_lot gl ON gl.id = gll.lot_id
			LEFT JOIN product_selecionable psel ON psel.product_id = pt.id AND psel.atributo_id = 4 -- tmr
			LEFT JOIN product_atributo_valores patv ON psel.valor_id = patv.id AND psel.atributo_id = patv.atributo_id --tmr
			WHERE 
			go.state != 'returned'
			AND gol.state != 'cancelled' 
			--AND COALESCE(gll.active, false) = true
			%s 
			GROUP BY 1,2,3,4,5,6,7,8,9
			ORDER BY go.name, pt.name)T
			""" % (ext_sql)
			self._cr.execute(query)
			results = self._cr.dictfetchall()

			for line in results:
				if not line['total_pnd'] > 0:
					continue
				worksheet.write(x,1,line['order_name'],bord)
				worksheet.write(x,2,line['lots'].replace('"',''),normal)
				worksheet.write_datetime(x,3,dt.from_string(line['date_order']),fdatetime)
				worksheet.write_datetime(x,4,dt.from_string(line['date_prod']),fdate) 
				worksheet.write_datetime(x,5,d.from_string(line['date_deli']),fdate)
				worksheet.write(x,6,line['customer'] or '-',normal)
				worksheet.write(x,7,line['obra'] or '-',bord)
				worksheet.write(x,8,line['cod_pres'] or '-',bord)
				worksheet.write(x,9,line['uom'] or '-',bord)
				worksheet.write(x,10,line['product'] or '-',bord)
				worksheet.write(x,11,line['area_total'],decinal4)
				worksheet.write(x,12,line['items_total'],bord)
				# stages:
				worksheet.write(x,13,line['p_area_opt'],decinal4)
				worksheet.write(x,14,line['p_items_opt'],bord)
				worksheet.write(x,15,line['p_area_corte'],decinal4)
				worksheet.write(x,16,line['p_items_corte'],bord)
				worksheet.write(x,17,line['p_area_pulido'],decinal4)
				worksheet.write(x,18,line['p_items_pulido'],bord)
				worksheet.write(x,19,line['p_area_bisel'],decinal4)
				worksheet.write(x,20,line['p_items_bisel'],bord)
				worksheet.write(x,21,line['p_area_enta'],decinal4)
				worksheet.write(x,22,line['p_items_enta'],bord)
				worksheet.write(x,23,line['p_area_lava'],decinal4)
				worksheet.write(x,24,line['p_items_lava'],bord)
				worksheet.write(x,25,line['p_area_horno'],decinal4)
				worksheet.write(x,26,line['p_items_horno'],bord)
				worksheet.write(x,27,line['p_area_temp'],decinal4)
				worksheet.write(x,28,line['p_items_temp'],bord)
				worksheet.write(x,29,line['p_area_aren'],decinal4)
				worksheet.write(x,30,line['p_items_aren'],bord)
				worksheet.write(x,31,line['p_area_perf'],decinal4)
				worksheet.write(x,32,line['p_items_perf'],bord)
				worksheet.write(x,33,line['p_area_insu'],decinal4)
				worksheet.write(x,34,line['p_items_insu'],bord)
				worksheet.write(x,35,line['p_area_produ'],decinal4)
				worksheet.write(x,36,line['p_items_produ'],bord)
				x+=1
			tam_col = [3,9,10,16,13,10,35,20,14,8,40, 12, 12] + 24 * [10]


		elif self.search_param == 'service_order':
			worksheet.merge_range(7,1,8,1, u"ÓRDEN DE PROD.",boldbord)
			worksheet.merge_range(7,2,8,2, u"LOTES",boldbord)
			worksheet.merge_range(7,3,8,3, u"FECHA EMISIÓN",boldbord)
			worksheet.merge_range(7,4,8,4, u"FECHA PRODUCCIÓN",boldbord)
			worksheet.merge_range(7,5,8,5, u"FECHA DE ENTREGA",boldbord)
			worksheet.merge_range(7,6,8,6, u"NOMBRE CLIENTE",boldbord)
			worksheet.merge_range(7,7,8,7, u"OBRA",boldbord)
			worksheet.merge_range(7,8,8,8, u"COD PRESENTACIÓN",boldbord)
			worksheet.merge_range(7,9,8,9, u"UNIDAD",boldbord)
			worksheet.merge_range(7,10,8,10, u"DSCRIPCIÓN DE PRODUCTO",boldbord)
			worksheet.merge_range(7,11,8,11, u"TOTAL M2 SOLICITADOS",boldbord)
			worksheet.merge_range(7,12,8,12, u"TOTAL VIDRIOS SOLICITADOS",boldbord)		
			worksheet.merge_range(7,13,7,14, u"PEND. DE OPTIMIZADO",boldbord)
			worksheet.merge_range(7,15,7,16, u"PEND. DE CORTE",boldbord)
			worksheet.merge_range(7,17,7,18, u"PEND. DE LAVADO",boldbord)
			worksheet.merge_range(7,19,7,20, u"PEND. DE HORNO",boldbord)
			#worksheet.merge_range(7,21,7,22, u"PEND. DE TEMPLADO",boldbord)
			worksheet.merge_range(7,21,7,22, u"PEND. DE SERV. INSULADO",boldbord)
			worksheet.merge_range(7,23,7,24, u"PEND. DE SERV. PULIDO",boldbord)
			worksheet.merge_range(7,25,7,26, u"PEND. DE SERV. ARENADO",boldbord)
			worksheet.merge_range(7,27,7,28, u"PEND. DE SERV. CORTE LAM.",boldbord)
			worksheet.merge_range(7,29,7,30, u"PEND. DE SERV. CORTE LAV. LAM.",boldbord)
			worksheet.merge_range(7,31,7,32, u"PEND. DE SERV. PEGADO",boldbord)
			worksheet.merge_range(7,33,7,34, u"PEND. DE SERV. BISELADO",boldbord)
			worksheet.merge_range(7,35,7,36, u"PEND. DE SERV. ENTALLE",boldbord)
			worksheet.merge_range(7,37,7,38, u"PEND. DE SERV. PERFORACIÓN",boldbord)
			worksheet.merge_range(7,39,7,40, u"PEND. DE PRODUCIDO",boldbord)
			worksheet.set_row(7, 25)

			for i in range(13,41,2):
				worksheet.write(8,i, u"Área (M2)",boldbord)
				worksheet.write(8,i+1, u"Nro de cristales",boldbord)

			query = """
			SELECT T.*, 
			T.p_items_opt + T.p_items_corte + T.p_items_lava + 
			T.p_items_horno + T.p_items_insu + 
			T.p_items_produ + T.p_items_pulido + T.p_items_aren + 
			T.p_items_bisel + T.p_items_enta + T.p_items_perf
			AS total_pnd
			FROM(
			SELECT
			go.name AS order_name,
			(go.date_order-interval '5' hour) AS date_order,
			go.date_production::DATE AS date_prod,
			go.date_delivery AS date_deli,
			rp.name AS customer,
			go.obra,
			patv.name AS cod_pres,
			pu.name AS uom,
			pt.name AS product,
			--group
			REPLACE(REPLACE(array_agg(DISTINCT(COALESCE(gl.name,'')))::TEXT,'{',''),'}','') AS lots,
			SUM(gol.area) AS area_total,
			COUNT(gol.*) AS items_total,
			--optimizado (todos los servicios deben pasar por optimizado)
			SUM(CASE WHEN gll.optimizado THEN 0.0 ELSE gol.area END) AS p_area_opt,
			COUNT(*) FILTER(WHERE COALESCE(gll.optimizado,false)!=true) AS p_items_opt,
			--corte
			SUM(CASE WHEN 'corte' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_corte,
			COUNT(*) FILTER(WHERE 'corte' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_corte,
			--lavado
			SUM(CASE WHEN 'lavado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_lava,
			COUNT(*) FILTER(WHERE 'lavado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_lava,
			--horno
			SUM(CASE WHEN 'horno' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_horno,
			COUNT(*) FILTER(WHERE 'horno' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_horno,
			--insulado
			SUM(CASE WHEN 'insulado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_insu,
			COUNT(*) FILTER(WHERE 'insulado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_insu,
			--pulido
			SUM(CASE WHEN 'pulido' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_pulido,
			COUNT(*) FILTER(WHERE 'pulido' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_pulido,
			--arenado
			SUM(CASE WHEN 'arenado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_aren,
			COUNT(*) FILTER(WHERE 'arenado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_aren,
			--corte_laminado
			SUM(CASE WHEN 'corte_laminado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_c_lam,
			COUNT(*) FILTER(WHERE 'corte_laminado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_c_lam,
			--corte_lavado_laminado
			SUM(CASE WHEN 'corte_lavado_laminado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_c_lav_lam,
			COUNT(*) FILTER(WHERE 'corte_lavado_laminado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_c_lav_lam,
			--pegado
			SUM(CASE WHEN 'pegado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_peg,
			COUNT(*) FILTER(WHERE 'pegado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_peg,
			--biselado
			SUM(CASE WHEN 'biselado' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_bisel,
			COUNT(*) FILTER(WHERE 'biselado' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_bisel,
			--entalle
			SUM(CASE WHEN 'entalle' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_enta,
			COUNT(*) FILTER(WHERE 'entalle' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_enta,
			--perforacion
			SUM(CASE WHEN 'perforacion' = ANY(string_to_array(gll.all_pending_stages, ',')) THEN gol.area ELSE 0.0 END) AS p_area_perf,
			COUNT(*) FILTER(WHERE 'perforacion' = ANY(string_to_array(gll.all_pending_stages, ','))) AS p_items_perf,
			--producido
			SUM(CASE WHEN gll.producido THEN 0.0 ELSE gol.area END) AS p_area_produ,
			COUNT(*) FILTER(WHERE COALESCE(gll.producido,false)!=true) AS p_items_produ
				
			FROM glass_order_line gol
			JOIN glass_order go ON go.id = gol.order_id
			JOIN glass_sale_calculator_line gscl ON gscl.id = gol.calc_line_id
			LEFT JOIN glass_lot_line gll ON (gll.order_line_id = gol.id AND gll.active)
			LEFT JOIN glass_lot gl ON gl.id = gll.lot_id
			JOIN res_partner rp ON rp.id = go.partner_id
			JOIN product_product pp ON pp.id = gol.product_id
			JOIN product_template pt ON pt.id = pp.product_tmpl_id
			JOIN product_uom pu ON pu.id = pt.uom_id
			JOIN res_users ru ON ru.id = go.seller_id
			LEFT JOIN product_selecionable psel ON psel.product_id = pt.id AND psel.atributo_id = 4 -- tmr
			LEFT JOIN product_atributo_valores patv ON psel.valor_id = patv.id AND psel.atributo_id = patv.atributo_id --tmr
			LEFT JOIN glass_stage_record gsr ON (gsr.lot_line_id = gll.id AND COALESCE(gsr.done, false) = false)
			WHERE 
			go.state != 'returned'
			AND gol.state != 'cancelled'
			AND COALESCE(gll.active, false) = true 
			%s 
			GROUP BY 1,2,3,4,5,6,7,8,9
			ORDER BY go.name, pt.name)T
			""" % (ext_sql)
			self._cr.execute(query)
			results = self._cr.dictfetchall()

			for line in results:
				if not line['total_pnd'] > 0:
					continue
				worksheet.write(x,1,line['order_name'],bord)
				worksheet.write(x,2,line['lots'].replace('"',''),normal)
				worksheet.write_datetime(x,3,dt.from_string(line['date_order']),fdatetime)
				worksheet.write_datetime(x,4,dt.from_string(line['date_prod']),fdate) 
				worksheet.write_datetime(x,5,d.from_string(line['date_deli']),fdate)
				worksheet.write(x,6,line['customer'] or '-',normal)
				worksheet.write(x,7,line['obra'] or '-',bord)
				worksheet.write(x,8,line['cod_pres'] or '-',bord)
				worksheet.write(x,9,line['uom'] or '-',bord)
				worksheet.write(x,10,line['product'] or '-',bord)
				worksheet.write(x,11,line['area_total'],decinal4)
				worksheet.write(x,12,line['items_total'],bord)
				worksheet.write(x,13,line['p_area_opt'],decinal4)
				worksheet.write(x,14,line['p_items_opt'],bord)
				worksheet.write(x,15,line['p_area_corte'],decinal4)
				worksheet.write(x,16,line['p_items_corte'],bord)
				worksheet.write(x,17,line['p_area_lava'],decinal4)
				worksheet.write(x,18,line['p_items_lava'],bord)
				worksheet.write(x,19,line['p_area_horno'],decinal4)
				worksheet.write(x,20,line['p_items_horno'],bord)
				#worksheet.write(x,21,line['p_area_temp'],decinal4)
				#worksheet.write(x,22,line['p_items_temp'],bord)
				# SERVICIOS
				worksheet.write(x,21,line['p_area_insu'],decinal4)
				worksheet.write(x,22,line['p_items_insu'],bord)
				worksheet.write(x,23,line['p_area_pulido'],decinal4)
				worksheet.write(x,24,line['p_items_pulido'],bord)
				worksheet.write(x,25,line['p_area_aren'],decinal4)
				worksheet.write(x,26,line['p_items_aren'],bord)
				worksheet.write(x,27,line['p_area_c_lam'],decinal4)
				worksheet.write(x,28,line['p_items_c_lam'],bord)
				worksheet.write(x,29,line['p_area_c_lav_lam'],decinal4)
				worksheet.write(x,30,line['p_items_c_lav_lam'],bord)
				worksheet.write(x,31,line['p_area_peg'],decinal4)
				worksheet.write(x,32,line['p_items_peg'],bord)
				worksheet.write(x,33,line['p_area_bisel'],decinal4)
				worksheet.write(x,34,line['p_items_bisel'],bord)
				worksheet.write(x,35,line['p_area_enta'],decinal4)
				worksheet.write(x,36,line['p_items_enta'],bord)
				worksheet.write(x,37,line['p_area_perf'],decinal4)
				worksheet.write(x,38,line['p_items_perf'],bord)
				worksheet.write(x,39,line['p_area_produ'],decinal4)
				worksheet.write(x,40,line['p_items_produ'],bord)
				x+=1
			tam_col = [3,9,10,16,13,10,35,20,14,8,40, 12, 12] + 28 * [10]

		alpha,prev,acum = list(string.ascii_uppercase),'',0
		for i,item in enumerate(tam_col):
			worksheet.set_column(prev+alpha[i%26]+':'+prev+alpha[i%26],item)
			if i % 26 == 25:
				prev = alpha[acum]
				acum+=1
		workbook.close()

		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path)