# -*- coding: utf-8 -*-
from odoo import api, fields, models, exceptions

class StockMove(models.Model):
	_inherit = 'stock.move'
	import_glass_ids = fields.One2many('import.glass.line','move_id')
	show_det_imp_button = fields.Boolean('Ver Detalles Imp.',compute='_get_show_det_imp_button')

	@api.depends('import_glass_ids')
	def _get_show_det_imp_button(self):
		for rec in self:
			if any(rec.import_glass_ids):
				rec.show_det_imp_button = True
			else:
				rec.show_det_imp_button = False
	@api.multi
	def get_import_lines(self):
		wizard = self.env['import.glass.wizard'].create({})
		for line in self.import_glass_ids:
			self.env['import.glass.wizard.line'].create({
				'wizard_id':wizard.id,
				'line_id':line.id,
			})

		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.import_glass_wizard_view_form' % module)
		
		return {
			'name':'Detalle Cristales Importados',
			'res_id':wizard.id,
			'type': 'ir.actions.act_window',
			'res_model': 'import.glass.wizard',
			'view_id': view.id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			}
		
class ImportGlassLine(models.Model):
	_name = 'import.glass.line'
	move_id    = fields.Many2one('stock.move')
	partner_id = fields.Many2one('res.partner')
	product_id = fields.Many2one('product.product')
	invoice    = fields.Char('account.invoice')
	order      = fields.Char('OP')
	base1      = fields.Integer('base1')
	base2      = fields.Integer('base2')
	altura1    = fields.Integer('altura1')
	altura2    = fields.Integer('altura2')
	entregado  = fields.Boolean('Entregado')
	area       = fields.Float('Area')

class ImportGlassWizard(models.TransientModel):
	_name = 'import.glass.wizard'
	line_ids = fields.One2many('import.glass.wizard.line','wizard_id')

	@api.multi
	def process(self):
		selected = self.line_ids.filtered(lambda x: x.selected)
		if not any(selected):
			raise exceptions.Warning(u'No ha seleccionado ningún cristal')
		used = selected.filtered(lambda x: x.entregado)
		msg = ''
		for i in used:
			msg += '-> '+ i.product_id.name + ' - ' + order+'\n'
		if msg != '':
			raise exceptions.Warning('Los siguientes cristales ya fueron entregados:\n'+msg)
		selected.mapped('line_id').write({'entregado':True})

class ImportGlassWizardLine(models.TransientModel):
	_name = 'import.glass.wizard.line'
	wizard_id  = fields.Many2one('import.glass.wizard')
	selected   = fields.Boolean('Seleccionado')
	line_id    = fields.Many2one('import.glass.line')
	partner_id = fields.Many2one(related='line_id.partner_id')
	product_id = fields.Many2one(related='line_id.product_id')
	invoice    = fields.Char(related='line_id.invoice')
	order      = fields.Char(related='line_id.order')
	base1      = fields.Integer(related='line_id.base1')
	base2      = fields.Integer(related='line_id.base2')
	altura1    = fields.Integer(related='line_id.altura1')
	altura2    = fields.Integer(related='line_id.altura2')
	entregado  = fields.Boolean(related='line_id.entregado')
	area       = fields.Float(related='line_id.area')