
{
    'name': 'Initial Inventory APT',
    'version': '10',
    'license': 'AGPL-3',
    'author': 'ITGRUPO-POLIGLASS',
    'website': 'http://www.noviat.com',
    'category': 'Production',
    'summary': """
    Importador de cristales en Inventario Inicial en Fullglass 
    """,
    'depends': ['stock','glass_production_order'],
    'data': [
        'wizard/scraps_import_view_wizard.xml',
        'views/stock_move.xml',
    ],
    'demo': [
        #'demo/account_move.xml',
    ],
    'installable': True,
}
