# -*- encoding: utf-8 -*-
import base64,codecs,pprint
from openerp import models, fields, api, exceptions
from datetime import datetime,timedelta
from time import time
import string

class ReportCrystaBreaksWizard(models.TransientModel):
	_name='report.crystal.breaks.wizard'

	start_date = fields.Date(string='Fecha Inicio',default=datetime.now().date())
	end_date   = fields.Date(string='Fecha Fin',default=datetime.now().date())
	search_param = fields.Selection([('glass_order','Orden de Produccion'),('product','Producto')],string='Filtro')
	glass_order_id = fields.Many2one('glass.order','Nro de Orden de Produccion',domain=[('state','in',('process','ended','delivered'))])
	product_id = fields.Many2one('product.product','Nombre de producto')
	show_all   = fields.Boolean(string='Mostrar todos')
	 
	def do_rebuild(self):
		from xlsxwriter.workbook import Workbook
		path = self.env['main.parameter'].search([])[0].dir_create_file
		file_name = 'Reporte de Cristales Rotos.xlsx'
		path+=file_name
		workbook = Workbook(path)
		worksheet = workbook.add_worksheet("Cristales Rotos")
		#Print Format
		worksheet.set_landscape() #Horizontal
		worksheet.set_paper(9) #A-4
		worksheet.set_margins(left=0.75, right=0.75, top=1, bottom=1)
		worksheet.fit_to_pages(1, 0)  # Ajustar por Columna	
		center = workbook.add_format()
		center.set_align('center')
		center.set_border(style=1)
		bold = workbook.add_format({'bold':True})
		bold.set_font_size(12)
		normal = workbook.add_format()
		normal.set_border(style=1)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		numbertres = workbook.add_format({'num_format':'0.000'})
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		bord.set_text_wrap()
		numberdos.set_border(style=1)
		numbertres.set_border(style=1)	
		title = workbook.add_format({'bold': True})
		title.set_align('center')
		title.set_align('vcenter')
		title.set_text_wrap()
		title.set_font_size(20)
		worksheet.set_row(0,30)
		boldborda = workbook.add_format({'bold': True})
		boldborda.set_border(style=2)
		boldborda.set_align('center')
		boldborda.set_align('vcenter')
		boldborda.set_text_wrap()
		boldborda.set_font_size(9)
		boldborda.set_bg_color('#ffff40')

		x= 9 # caminador de iteraciones para cada fila			
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		# display values for selection fields:
		search= dict(self._fields['search_param'].selection).get(self.search_param)
		label,param = '',''
		if search == 'Producto' and not self.show_all:
			label = "PRODUCTO:"
			param = self.product_id.name
		elif search == 'Orden de Produccion' and not self.show_all:
			label = "OP:"
			param = self.glass_order_id.name
		worksheet.merge_range(1,1,0,7, u"REPORTE DE ROTURA DE CRISTALES",title)
		worksheet.write(2,1, u"FECHA DEL:",bold)
		worksheet.write(3,1, u"BUSQUEDA:",bold)
		worksheet.write(4,1, label,bold)
		worksheet.write(2,2, str(self.start_date),bold)
		worksheet.write(2,3, u"AL",bold)
		worksheet.write(2,4, str(self.end_date),bold)
		worksheet.write(3,2, search if not self.show_all else 'TODOS',bold)
		worksheet.merge_range(4,2,4,3,param,bold)
		worksheet.merge_range(7,1,8,1, u"ORDEN DE PRODUCCION",boldbord)
		worksheet.merge_range(7,2,8,2, u"NRO CRISTAL",boldbord)
		worksheet.merge_range(7,3,8,3, u"LOTE",boldbord)
		worksheet.merge_range(7,4,8,4, u"BASE 1",boldbord)
		worksheet.merge_range(7,5,8,5, u"BASE 2",boldbord)
		worksheet.merge_range(7,6,8,6, u"ALTURA 1",boldbord)
		worksheet.merge_range(7,7,8,7, u"ALTURA 2",boldbord)
		worksheet.merge_range(7,8,8,8, u"ENTALLE",boldbord)
		worksheet.merge_range(7,9,8,9, u"FECHA DE ROTURA",boldbord)
		worksheet.merge_range(7,10,8,10, u"HORA DE ROTURA",boldbord)
		worksheet.merge_range(7,11,8,11, u"NRO DOCUMENTO",boldbord)
		worksheet.merge_range(7,12,8,12, u"NOMBRE CLIENTE",boldbord)
		worksheet.merge_range(7,13,8,13, u"OBRA",boldbord)
		worksheet.merge_range(7,14,8,14, u"FECHA DE ENTREGA",boldbord)
		worksheet.merge_range(7,15,8,15, u"OBSERVACIÓN",boldbord)
		worksheet.merge_range(7,16,8,16, u"OCURRENCIA",boldbord)
		worksheet.merge_range(7,17,8,17, u"AREA",boldbord)
		worksheet.merge_range(7,18,8,18, u"PESO",boldbord)
		worksheet.merge_range(7,19,8,19, u"MOTIVO DE ROTURA",boldbord)
		worksheet.merge_range(7,20,8,20, u"USUARIO ENCARGADO",boldbord)
		worksheet.merge_range(7,21,8,21, u"COSTO",boldbord)
		worksheet.merge_range(7,22,8,22, u"CÓDIGO PROD.",boldbord)
		worksheet.merge_range(7,23,8,23, u"PRESENTACIÓN",boldbord)
		worksheet.merge_range(7,24,8,24, u"DESC. PRODUCTO",boldbord)
		worksheet.merge_range(7,25,8,25, u"ETAPA DE ROTURA",boldbord)
		
		domain = [('stage_id.name','=','rotura'),('date','>=',self.start_date),('date','<=',self.end_date)]
		
		if self.product_id and self.search_param == 'product' and not self.show_all:
			domain.append(('lot_line_id.product_id','=',self.product_id.id))

		elif self.glass_order_id and self.search_param == 'glass_order' and not self.show_all:
			domain.append(('lot_line_id.order_prod_id','=',self.glass_order_id.id))
		
		b_stages = self.env['glass.stage.record'].search(domain)

		dt = fields.Datetime
		
		for b_stage in b_stages:
			#stage = line.stage_ids.filtered(lambda x: x.stage=='roto')[0]
			line = b_stage.lot_line_id
			tz_date = dt.context_timestamp(self,dt.from_string(b_stage.date))

			worksheet.write(x,1,line.order_prod_id.name,center)
			worksheet.write(x,2,line.nro_cristal,center)
			worksheet.write(x,3,line.lot_id.name,center)
			worksheet.write(x,4,line.base1,center)
			worksheet.write(x,5,line.base2,center)
			worksheet.write(x,6,line.altura1,center)
			worksheet.write(x,7,line.altura2,center)
			worksheet.write(x,8,line.calc_line_id.entalle or '',center)
			worksheet.write(x,9,str(tz_date)[:10].replace('-','/'),center)
			worksheet.write(x,10,str(tz_date)[11:19],center)
			worksheet.write(x,11,line.order_prod_id.partner_id.nro_documento or '',center)
			worksheet.write(x,12,line.order_prod_id.partner_id.name,normal)
			worksheet.write(x,13,line.order_prod_id.obra or '',normal)
			worksheet.write(x,14,line.order_prod_id.date_delivery,center)
			worksheet.write(x,15,b_stage.break_note or '',normal)
			worksheet.write(x,16,1,center)
			worksheet.write(x,17,line.area,numbertres)
			worksheet.write(x,18,line.product_id.weight * line.area if line.product_id.weight else 0,numbertres)
			worksheet.write(x,19,b_stage.break_motive or '',center)
			worksheet.write(x,20,b_stage.user_id.name,normal)
			worksheet.write(x,21,line.product_id.standard_price * line.area if line.product_id.standard_price else 0,numberdos)
			worksheet.write(x,22,line.product_id.default_code,center)
			
			attr = line.product_id.product_tmpl_id.atributo_ids.filtered(lambda x: x.atributo_id.id==4)
			if any(attr):
				attr = attr[0].valor_id.name
			else:
				attr = ''
			worksheet.write(x,23,attr,center)
			worksheet.write(x,24,line.product_id.name,normal)
			worksheet.write(x,25,b_stage.break_stage_id.name,center)
			x=x+1
		
		tam_col=[3,10,10,10,10,10,10,10,8,12,12,12,40,17,10,17,7,10,10,12,17,10,15,12,25,12]
		alpha = list(string.ascii_uppercase)
		for i,item in enumerate(tam_col):
			worksheet.set_column(alpha[i]+':'+alpha[i],item)
		workbook.close()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path)

	