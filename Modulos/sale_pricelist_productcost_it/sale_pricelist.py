# -*- coding: utf-8 -*-
from odoo import models,fields
class Pricelist(models.Model):
	_inherit = "product.pricelist"

	categ_client = fields.Selection([
		('a', 'A'),
		('b', 'B'),
		('c', 'C'),
		('d', 'D'),
		('e', 'E'),
		], string=u'Categoría de Cliente')