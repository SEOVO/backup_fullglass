# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime,timedelta
import base64,string 

class ReporteSeguimientoProduccionWizard(models.TransientModel):
	_name='report.production.tracing.wizard'

	def _str2date(self,string):
			return fields.Datetime.from_string(string)

	def _get_start_date(self):
		today = self._str2date(fields.Date.context_today(self))
		return today - timedelta(days=30)

	start_date = fields.Date(string='Fecha Inicio',default=_get_start_date)
	end_date = fields.Date(string='Fecha Fin',default=lambda self: fields.Date.context_today(self))
	# search_param = fields.Selection([
	# 	('glass_order',u'Órdenes de Producción'),
	# 	('product',u'Producto')],default='glass_order',string=u'Búsqueda por')
	search_param = fields.Selection([
		('glass_order', u'Órdenes de Producción'),
		('service_order', u'Órdenes de servicios')],
		string=u'Tipo de Órdenes',default='glass_order')

	#glass_order_ids = fields.Many2many('glass.order',string=u'Órdenes de Producción',domain=[('state','!=','returned')])
	glass_order_ids = fields.Many2many('glass.order',
		relation='glass_order_rep_global_tack_rel', column1='wizard_id', column2='glass_order_id',
		string=u'Órdenes de Producción', 
		domain="[('state','!=','returned'), ('type_sale', '=', 'production')]")
	service_order_ids = fields.Many2many('glass.order',
		relation='service_order_rep_global_tack_rel', column1='wizard_id', column2='service_order_id',
		string=u'Órdenes de Servicios', 
		domain="[('state','!=','returned'), ('type_sale', '=', 'services')]")
	product_id = fields.Many2one('product.product',string=u'Producto')
	customer_id = fields.Many2one('res.partner',string=u'Cliente',domain=[('customer','=',True)])
	# TODO to remove
	filters = fields.Selection([
		('all','Todos'),
		('pending','En proceso'),
		('in_stock','Producidos y en APT'),
		#('to_inter','Por ingresar'),
		('delivered','Entregados'),
		('expired','Vencidos')],default='all',string='Estado')
	#show_breaks = fields.Boolean('Mostrar Rotos') #??
	show_for_dates = fields.Boolean('Mostrar en rango de fechas')


	@api.onchange('search_param')
	def _onchange_search_param(self):
		domain = []
		if self.search_param == 'glass_order':
			domain = [('type', '!=', 'service')]
		elif self.search_param == 'service_order':
			domain = [('type', '=', 'service')]
		return {'domain': {'product_id': domain}}

	
	def do_rebuild(self):
		from xlsxwriter.workbook import Workbook
		path = self.env['main.parameter'].search([])[0].dir_create_file
		file_name = u'Reporte Seguimiento Producción.xlsx'
		path+=file_name
		workbook = Workbook(path)
		worksheet = workbook.add_worksheet(u"Seguimiento de Producción")
		bold = workbook.add_format({'bold': True})
		normal = workbook.add_format()
		normal.set_border(style=1)
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=1)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(11)
		boldbord.set_bg_color('#bfbfbf')
		decinal4 = workbook.add_format({'num_format':'0.0000'})
		decinal4.set_border(style=1)
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberdos.set_border(style=1)
		bord = workbook.add_format({'font_size':10})
		bord.set_border(style=1)
		#bord.set_text_wrap()
		title = workbook.add_format({'bold': True})
		title.set_align('center')
		title.set_align('vcenter')
		title.set_text_wrap()
		title.set_font_size(20)
		worksheet.set_row(0, 30)
		boldborda = workbook.add_format({'bold': True})
		boldborda.set_border(style=2)
		boldborda.set_align('center')
		boldborda.set_align('vcenter')
		boldborda.set_text_wrap()
		boldborda.set_font_size(9)
		boldborda.set_bg_color('#ffff40')
		fdatetime = workbook.add_format({'num_format': 'dd/mm/yyyy hh:mm','border':1})
		fdate = workbook.add_format({'num_format': 'dd/mm/yyyy','border':1})
		ftime = workbook.add_format({'num_format': 'hh:mm:ss','border':1})

		x= 9
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		# param = search = ''
		# if not self.show_for_dates and self.search_param:
		# 	search= dict(self._fields['search_param'].selection).get(self.search_param)
		# 	label = "PRODUCTO:" if search=='Producto' else 'OP:'
		# 	param = ', '.join(self.glass_order_ids.mapped('name')) if self.glass_order_ids else self.product_id.name
		# else:
		# 	label = 'Rango de fechas'
		
		# state = dict(self._fields['filters'].selection).get(self.filters)
		
		com = self.env['res.company']._company_default_get(self._name)
		if com.logo:
			from StringIO import StringIO
			image_data = {'image_data':StringIO(base64.b64decode(com.logo)),'x_scale': 0.4, 'y_scale': 0.4}
			worksheet.insert_image('A1','fullglass.jpg',image_data)

		worksheet.merge_range(1,4,0,10, u"REPORTE DE SEGUIMIENTO GLOBAL DE LA PRODUCCIÓN",title)
		# if self.start_date and self.end_date:
		# 	worksheet.merge_range(2,4,2,7, u"FECHA DEL: %s AL %s"%(self.start_date,self.end_date),bold)
		# worksheet.write(3,4, u"BÚSQUEDA:",bold)
		# worksheet.write(4,4, label,bold)
		# worksheet.write(5,4, u"ESTADO:",bold)
		# worksheet.write(3,5, search,bold)
		# worksheet.merge_range(4,5,4,6,param,bold)
		# worksheet.write(5,5, state,bold)
		if self.start_date and self.end_date and self.show_for_dates:
			worksheet.merge_range(2,7,2,9, u"FECHA DEL: %s AL %s" % (self.start_date,self.end_date),bold)
		
		elif self.search_param == 'glass_order' and self.glass_order_ids:
			worksheet.write(3,4, u"ÓRDENES DE PRODUCCIÓN:", bold)
			worksheet.write(3,5, ','.join(self.glass_order_ids.mapped('name')),bold)

		elif self.search_param == 'service_order' and self.service_order_ids:
			worksheet.write(3,4, u"ÓRDENES DE SERVICIOS:", bold)
			worksheet.write(3,5, ','.join(self.service_order_ids.mapped('name')),bold)

		if self.product_id:
			worksheet.write(4,4, u"PRODUCTO:", bold)
			worksheet.write(4,5, self.product_id.name,bold)

		if self.customer_id:
			worksheet.write(5,4, u"CLIENTE:", bold)
			worksheet.write(5,5, self.customer_id.name,bold)

		ext_sql = ''
		# if self.glass_order_ids and self.search_param == 'glass_order':
		# 	ext_sql += """ AND go.id IN (%s) """%','.join(map(str,self.glass_order_ids.ids))
		# else:
		# 	if self.start_date and self.end_date:
		# 		ext_sql += """ AND go.date_order::date >= '%s' AND go.date_order::date <= '%s' """%(self.start_date,self.end_date)
		# 	if self.product_id and self.search_param == 'product' and not self.show_for_dates:
		# 		ext_sql += """ AND pp.id = %d"""%self.product_id.id

		# 	if self.customer_id and not self.show_for_dates:
		# 		ext_sql += """ AND rp.id = %d"""%self.customer_id.id

		if self.show_for_dates:
			ext_sql += " AND go.date_order::date >= '%s' AND go.date_order::date <= '%s' " % (self.start_date, self.end_date)
			
		if self.search_param == 'glass_order':
			ext_sql += " AND go.type_sale = 'production' "
			if self.glass_order_ids and not self.show_for_dates:
				ext_sql += " AND go.id IN (%s) " % ','.join(map(str, self.glass_order_ids.ids))

		if self.search_param == 'service_order':
			ext_sql += " AND go.type_sale = 'services' "
			if self.service_order_ids and not self.show_for_dates:
				ext_sql += " AND go.id IN (%s) " % ','.join(map(str, self.service_order_ids.ids))

		if self.product_id:
			ext_sql += " AND pp.id = %d " % self.product_id.id

		if self.customer_id:
			ext_sql += " AND rp.id = %d " % self.customer_id.id

			# GG
			# if self.filters == 'pending':
			# 	ext_sql += """ AND go.state IN ('confirmed','process') """
			# elif self.filters == 'in_stock':
			# 	ext_sql += """ AND go.state = 'ended' """
			# elif self.filters == 'delivered':
			# 	ext_sql += """ AND go.state = 'delivered' """
			# elif self.filters == 'expired':
			# 	now = datetime.now() + timedelta(seconds=1)
			# 	ext_sql += """ AND go.date_production <= '%s' AND go.state IN ('draft','confirmed','process') """%str(now)[:19]
		d = fields.Date
		dt = fields.Datetime
		tam_col = []

		worksheet.merge_range(7,0,8,0, u"ÓRDEN DE PROD.",boldbord)
		worksheet.merge_range(7,1,8,1, u"LOTES",boldbord)
		worksheet.merge_range(7,2,8,2, u"FECHA DE EMISIÓN",boldbord)
		worksheet.merge_range(7,3,8,3, u"HORA DE EMISIÓN",boldbord)
		worksheet.merge_range(7,4,8,4, u"FECHA DE PROD.",boldbord)
		worksheet.merge_range(7,5,8,5, u"FECHA DE ENTREGA",boldbord)
		worksheet.merge_range(7,6,8,6, u"FECHA DE DESPACHO",boldbord) # 
		worksheet.merge_range(7,7,8,7, u"NOMBRE CLIENTE",boldbord)
		worksheet.merge_range(7,8,8,8, u"OBRA",boldbord)
		worksheet.merge_range(7,9,8,9, u"COD PRESENTACIÓN",boldbord)
		worksheet.merge_range(7,10,8,10, u"DESCRIPCIÓN DE PRODUCTO",boldbord)
		worksheet.merge_range(7,11,8,11, u"DOCUMENTO",boldbord)
		worksheet.merge_range(7,12,8,12, u"TIPO DOC.",boldbord)
		worksheet.merge_range(7,13,8,13, u"RUC PARTNER",boldbord)
		worksheet.merge_range(7,14,8,14, u"VENDEDOR",boldbord)
		worksheet.merge_range(7,15,8,15, u"PTO. LLEGADA",boldbord)
		worksheet.merge_range(7,16,8,16, u"PROVINCIA",boldbord)
		worksheet.merge_range(7,17,8,17, u"NRO PENDIENTES",boldbord)
		worksheet.merge_range(7,18,8,18, u"AREA PENDIENTE(M2)",boldbord)
		worksheet.merge_range(7,19,8,19, u"ESTADO OP",boldbord)
		worksheet.merge_range(7,20,8,20, u"FECHA DESPACHO REAL",boldbord)
		worksheet.merge_range(7,21,8,21, u"DIAS DESPACHO",boldbord)
		worksheet.merge_range(7,22,8,22, u"AÑO",boldbord)
		worksheet.merge_range(7,23,8,23, u"MES",boldbord)
		worksheet.merge_range(7,24,8,24, u"SEMANA",boldbord)
		worksheet.merge_range(7,25,8,25, u"TOTAL M2 SOLICITADOS",boldbord)
		worksheet.merge_range(7,26,8,26, u"TOTAL VIDRIOS SOLICITADOS",boldbord)		
		worksheet.merge_range(7,27,8,27, u"TOTAL ÁREA VIDRIOS CON ENTALLE",boldbord)
		worksheet.merge_range(7,28,8,28, u"TOTAL VIDRIOS CON ENTALLE",boldbord)

		worksheet.merge_range(7,29,7,30, u"OPTIMIZADO",boldbord)
		worksheet.merge_range(7,31,7,32, u"CORTE",boldbord)
		worksheet.merge_range(7,33,7,34, u"LAVADO",boldbord)
		worksheet.merge_range(7,35,7,36, u"TEMPLADO",boldbord)
		worksheet.merge_range(7,37,7,38, u"INSULADO",boldbord)
		worksheet.merge_range(7,39,7,40, u"SERV. PULIDO",boldbord)
		worksheet.merge_range(7,41,7,42, u"SERV. ARENADO",boldbord)
		worksheet.merge_range(7,43,7,44, u"SERV. BISELADO",boldbord)
		worksheet.merge_range(7,45,7,46, u"SERV. ENTALLE",boldbord)
		worksheet.merge_range(7,47,7,48, u"SERV. PERFORACIÓN",boldbord)
		worksheet.merge_range(7,49,7,50, u"SERV. CORTE LAMINADO",boldbord)
		worksheet.merge_range(7,51,7,52, u"SERV. CORTE LAV. LAMINADO",boldbord)
		worksheet.merge_range(7,53,7,54, u"SERV. CORTE PEGADO",boldbord)
		worksheet.merge_range(7,55,7,56, u"PRODUCIDO",boldbord)
		worksheet.merge_range(7,57,7,58, u"INGRESADO",boldbord)
		worksheet.merge_range(7,59,7,60, u"ENTREGADO",boldbord)
		worksheet.set_row(7, 25)
		#worksheet.merge_range(7,55,7,56, u"PRODUCIDOPEND. DE ",boldbord)

		#worksheet.merge_range(7,56,7,57, u"ESTADO",boldbord)
		for i in range(29,61,2):
			worksheet.write(8,i, u"M2 cristales",boldbord)
			worksheet.write(8,i+1, u"Número de cristales",boldbord)


		query = """
		SELECT 
		go.name AS order_name,
		--(go.date_order-interval '5' hour) AS date_order,
		SPLIT_PART((go.date_order-interval '5' hour)::TEXT,' ',1) AS date_order,
		SPLIT_PART((go.date_order-interval '5' hour)::TEXT,' ',2) AS hour_order,
		go.date_production::DATE AS date_prod,
		go.date_delivery AS date_deli,
		go.date_send,
		rp.name AS customer,
		go.obra,
		patv.name AS cod_pres,
		rp.nro_documento AS customer_doc,
		rp2.name AS seller,
		go.state AS order_state,
		go.delivery_street AS pto_entr,
		go.delivery_province AS province,
		--group
		pt.name AS product,
		REPLACE(REPLACE(array_agg(DISTINCT(COALESCE(gl.name,'')))::TEXT,'{',''),'}','') AS lots,
		(SELECT 
			MIN(fecha_kardex) 
			FROM stock_picking sp 
			WHERE sp.origin = so.name AND sp.state = 'done') AS fec_kardex,
		(SELECT 
			string_agg(ai.number,',') || '_' ||string_agg(eic.name,',')
		 	FROM account_invoice ai 
		 	LEFT JOIN einvoice_catalog_01 eic ON eic.id = ai.it_type_document
		 	WHERE ai.origin = so.name AND ai.state NOT IN ('cancel','draft')) AS invoices,

		SUM(gol.area) AS area_total,
		COUNT(gol.*) AS items_total,
		--entalle solicitado
		SUM(CASE WHEN gscl.entalle>0 THEN gol.area ELSE 0.0 END) AS area_enta_req,
		COUNT(gol.*) FILTER(WHERE gscl.entalle>0) AS items_enta_req,
		--optimizado
		SUM(CASE WHEN gll.optimizado THEN gll.area ELSE 0.0 END) AS area_opt,
		COUNT(*) FILTER(WHERE optimizado=true) AS items_opt,
		--corte
		SUM(CASE WHEN gll.corte THEN gll.area ELSE 0.0 END) AS area_corte,
		COUNT(gll.*) FILTER(WHERE gll.corte=true) AS items_corte,
		--lavado
		SUM(CASE WHEN gll.lavado THEN gll.area ELSE 0.0 END) AS area_lava,
		COUNT(gll.*) FILTER(WHERE gll.lavado=true) AS items_lava,
		--templado
		SUM(CASE WHEN gll.templado THEN gll.area ELSE 0.0 END) AS area_temp,
		COUNT(gll.*) FILTER(WHERE gll.templado=true) AS items_temp,
		--insulado
		SUM(CASE WHEN gll.insulado THEN gll.area ELSE 0.0 END) AS area_insu,
		COUNT(gll.*) FILTER(WHERE gll.insulado=true) AS items_insu,
		--pulido
		SUM(CASE WHEN gll.pulido THEN gll.area ELSE 0.0 END) AS area_pulido,
		COUNT(gll.*) FILTER(WHERE gll.pulido=true) AS items_pulido,
		--arenado
		SUM(CASE WHEN gll.arenado THEN gll.area ELSE 0.0 END) AS area_aren,
		COUNT(gll.*) FILTER(WHERE gll.arenado=true) AS items_aren,
		--biselado
		SUM(CASE WHEN gll.biselado THEN gll.area ELSE 0.0 END) AS area_bisel,
		COUNT(gll.*) FILTER(WHERE gll.biselado=true) AS items_bisel,
		--entalle
		SUM(CASE WHEN gll.entalle THEN gll.area ELSE 0.0 END) AS area_enta,
		COUNT(gll.*) FILTER(WHERE gll.entalle=true) AS items_enta,
		--perforac.
		SUM(CASE WHEN gll.perforacion THEN gll.area ELSE 0.0 END) AS area_perf,
		COUNT(gll.*) FILTER(WHERE gll.perforacion=true) AS items_perf,
		--corte_laminado
		SUM(CASE WHEN gll.corte_laminado THEN gll.area ELSE 0.0 END) AS area_c_lam,
		COUNT(gll.*) FILTER(WHERE gll.corte_laminado=true) AS items_c_lam,
		--corte_lavado_laminado
		SUM(CASE WHEN gll.corte_lavado_laminado THEN gll.area ELSE 0.0 END) AS area_c_lav_lam,
		COUNT(gll.*) FILTER(WHERE gll.corte_lavado_laminado=true) AS items_c_lav_lam,
		--pegado
		SUM(CASE WHEN gll.pegado THEN gll.area ELSE 0.0 END) AS area_peg,
		COUNT(gll.*) FILTER(WHERE gll.pegado=true) AS items_peg,
		--producido
		SUM(CASE WHEN gll.producido THEN gll.area ELSE 0.0 END) AS area_produ,
		COUNT(gll.*) FILTER(WHERE gll.producido=true) AS items_produ,
		--ingresado
		SUM(CASE WHEN gll.ingresado THEN gll.area ELSE 0.0 END) AS area_ingre,
		COUNT(gll.*) FILTER(WHERE gll.ingresado=true) AS items_ingre,
		--entregado
		SUM(CASE WHEN gll.entregado THEN gll.area ELSE 0.0 END) AS area_entre,
		COUNT(gll.*) FILTER(WHERE gll.entregado=true) AS items_entre
		
		FROM glass_order_line gol
		JOIN glass_order go ON go.id = gol.order_id
		JOIN glass_sale_calculator_line gscl ON gscl.id = gol.calc_line_id
		JOIN res_partner rp ON rp.id = go.partner_id
		JOIN product_product pp ON pp.id = gol.product_id
		JOIN product_template pt ON pt.id = pp.product_tmpl_id
		JOIN res_users ru ON ru.id = go.seller_id
		JOIN res_partner rp2 ON rp2.id = ru.partner_id
		JOIN sale_order so ON so.id = go.sale_order_id
		LEFT JOIN glass_lot_line gll ON (gll.order_line_id = gol.id AND COALESCE(gll.active) = true)
		LEFT JOIN glass_lot gl ON gl.id = gll.lot_id
		LEFT JOIN product_selecionable psel ON psel.product_id = pt.id AND psel.atributo_id = 4 -- tmr
		LEFT JOIN product_atributo_valores patv ON psel.valor_id = patv.id AND psel.atributo_id = patv.atributo_id --tmr
		WHERE 
		go.state != 'returned'
		AND gol.state != 'cancelled' 
		%s 
		GROUP BY 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,so.name
		ORDER BY go.name,pt.name """ % ext_sql

		self._cr.execute(query)
		results = self._cr.dictfetchall()
		dict_state_op = dict(self.env['glass.order']._fields['state'].selection)
		now = datetime.now().date()
		for line in results:
			worksheet.write(x,0,line['order_name'],bord)
			worksheet.write(x,1,line['lots'].replace('"',''),bord)
			worksheet.write_datetime(x,2,dt.from_string(line['date_order']),fdate)
			worksheet.write_datetime(x,3,datetime.strptime(line['hour_order'][:8],'%H:%M:%S'),ftime)
			worksheet.write_datetime(x,4,dt.from_string(line['date_prod']),fdate)
			
			date_deli = d.from_string(line['date_deli'])
			date_send = d.from_string(line['date_send'])
			
			worksheet.write_datetime(x,5,date_deli,fdate)
			worksheet.write_datetime(x,6,date_send,fdate)
			worksheet.write(x,7,line['customer'] or '-',normal)
			worksheet.write(x,8,line['obra'] or '-',bord)
			worksheet.write(x,9,line['cod_pres'] or '-',bord)
			worksheet.write(x,10,line['product'] or '-',bord)

			inv_string = eic_string = ''
			if line['invoices']:
				invoices = line['invoices'].split('_')
				try:
					inv_string = invoices[0]
					eic_string = invoices[1]
				except IndexError:
					pass
			worksheet.write(x,11,inv_string or '-',bord)
			worksheet.write(x,12,eic_string or '-',bord)
			worksheet.write(x,13,line['customer_doc'] or '-',bord)
			worksheet.write(x,14,line['seller'] or '-',normal)
			worksheet.write(x,15,line['pto_entr'] or '-',normal)
			worksheet.write(x,16,line['province'] or '-',bord)
			
			tot_area   = line['area_total']
			tot_items  = line['items_total']
			prod_area  = line['area_produ']
			prod_items = line['items_produ']
			
			pending_area = tot_area - prod_area
			pending_items = tot_items - prod_items
			
			worksheet.write(x,17,pending_items,bord)
			worksheet.write(x,18,pending_area,decinal4)
			worksheet.write(x,19,dict_state_op.get(line['order_state'],''),bord)
			
			fec_kardex,days_kardex = '','-'
			if line['fec_kardex']:
				fec_kardex = d.from_string(line['fec_kardex'])
				days_kardex = (fec_kardex - date_send).days
			
			worksheet.write(x,20,fec_kardex or '-',fdate)
			worksheet.write(x,21,days_kardex or '-',bord)
			worksheet.write(x,22,now.year or '-',bord)
			worksheet.write(x,23,now.month or '-',bord)
			worksheet.write(x,24,now.isocalendar()[1] or '-',bord)
			worksheet.write(x,25,tot_area,decinal4)
			worksheet.write(x,26,tot_items,bord )
			worksheet.write(x,27,line['area_enta_req'],decinal4)
			worksheet.write(x,28,line['items_enta_req'],bord)
			
			# Conteo de área e ítems
			worksheet.write(x,29,line['area_opt'],decinal4)
			worksheet.write(x,30,line['items_opt'],bord)
			worksheet.write(x,31,line['area_corte'],decinal4)
			worksheet.write(x,32,line['items_corte'],bord)
			worksheet.write(x,33,line['area_lava'],decinal4)
			worksheet.write(x,34,line['items_lava'],bord)
			worksheet.write(x,35,line['area_temp'],decinal4)
			worksheet.write(x,36,line['items_temp'],bord)
			worksheet.write(x,37,line['area_insu'],decinal4)
			worksheet.write(x,38,line['items_insu'],bord)
			worksheet.write(x,39,line['area_pulido'],decinal4)
			worksheet.write(x,40,line['items_pulido'],bord)
			worksheet.write(x,41,line['area_aren'],decinal4)
			worksheet.write(x,42,line['items_aren'],bord)
			worksheet.write(x,43,line['area_bisel'],decinal4)
			worksheet.write(x,44,line['items_bisel'],bord)
			worksheet.write(x,45,line['area_enta'],bord)
			worksheet.write(x,46,line['items_enta'],bord)
			worksheet.write(x,47,line['area_perf'],decinal4)
			worksheet.write(x,48,line['items_perf'],bord)
			worksheet.write(x,49,line['area_c_lam'],decinal4)
			worksheet.write(x,50,line['items_c_lam'],bord)
			worksheet.write(x,51,line['area_c_lav_lam'],decinal4)
			worksheet.write(x,52,line['items_c_lav_lam'],bord)
			worksheet.write(x,53,line['area_peg'],decinal4)
			worksheet.write(x,54,line['items_peg'],bord)
			worksheet.write(x,55,line['area_produ'],decinal4)
			worksheet.write(x,56,line['items_produ'],bord)
			worksheet.write(x,57,line['area_ingre'],bord)
			worksheet.write(x,58,line['items_ingre'],bord)
			worksheet.write(x,59,line['area_entre'],decinal4)
			worksheet.write(x,60,line['items_entre'],bord)
			#worksheet.write(x,55,line['area_compr'],bord)
			#worksheet.write(x,56,line['items_compr'],bord)
			x+=1
			tam_col = [10,10,10,10,10,10,10,25,20,9,35,13,7,13,20,30,10,9,9,10,10,8,5,5,5]+41*[10]
		
		alpha,prev,acum = list(string.ascii_uppercase),'',0
		for i,item in enumerate(tam_col):
			worksheet.set_column(prev+alpha[i%26]+':'+prev+alpha[i%26],item)
			if i % 26 == 25:
				prev = alpha[acum]
				acum+=1
		workbook.close()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path)