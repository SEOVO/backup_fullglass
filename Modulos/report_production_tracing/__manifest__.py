# -*- encoding: utf-8 -*-
{
    'name': 'Reporte de seguimiento global de la Producción',
    'category': 'report',
    'author': 'ITGRUPO-POLIGLASS',
    'depends': ['import_base_it','master_template_fullglass','export_file_manager_it'],
    'version': '1.0',
    'description':"""
        Módulo para emitir el reporte Seguimiento General de la produccion en Fullglass
    """,
    'auto_install': False,
    'demo': [],
    'data':    [
        'wizard/report_production_tracing_wizard_view.xml',
        ],
    'installable': True
}