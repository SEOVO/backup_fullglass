# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class ResCurrency(models.Model):
	_inherit = 'res.currency'

	@api.model
	def _get_conversion_rate(self, from_currency, to_currency):
		res = super(ResCurrency,self)._get_conversion_rate(from_currency,to_currency)
		if self._context.get('use_special_extype'):
			"""Usar tipo de cambio comercial #NOTE si viene de un 
			pedido de venta debería asignar fecha en el ctx"""
			date = self._context.get('date') 
			query = """
			SELECT COALESCE(tipo_venta,0.0) from tipo_cambio_comercial
			WHERE name <= '%s'
			order by name desc limit 1
			"""%date # NOTE Uso sólo para conversión de dólares a soles
			self._cr.execute(query)
			type_sale = self._cr.fetchone()[0]
			if type_sale:
				return type_sale
		return res