# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError

class tipo_cambio_comercial(models.Model):
	_name = 'tipo.cambio.comercial'

	name = fields.Date('Fecha',required=True)
	tipo_venta = fields.Float('Tipo de Venta',digits=(12,3),required=True)

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	@api.one
	def cambiar_lista_precio(self):
		for i in self.order_line:
			i.product_id_change()
			i._onchange_discount()

	@api.onchange('pricelist_id')
	def _onchange_pricelist_id(self):
		self.cambiar_lista_precio()
	

class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'

	# NO TUVE OPCIÓN !!!!!!!!
	# perdonen a este pecador por hacer esto xdxdxd
	@api.multi
	def _get_display_price(self, product):
		# TO DO: move me in master/saas-16 on sale.order
		if self.order_id.pricelist_id.discount_policy == 'with_discount':
			return product.with_context(pricelist=self.order_id.pricelist_id.id).price
		product_context = dict(self.env.context, partner_id=self.order_id.partner_id.id, date=self.order_id.date_order, uom=self.product_uom.id,use_special_extype=True)

		final_price, rule_id = self.order_id.pricelist_id.with_context(product_context).get_product_price_rule(self.product_id, self.product_uom_qty or 1.0, self.order_id.partner_id)
		base_price, currency_id = self.with_context(product_context)._get_real_price_currency(product, rule_id, self.product_uom_qty, self.product_uom, self.order_id.pricelist_id.id)
		if currency_id != self.order_id.pricelist_id.currency_id.id:
			base_price = self.env['res.currency'].browse(currency_id).with_context(product_context).compute(base_price, self.order_id.pricelist_id.currency_id)
		# negative discounts (= surcharge) are included in the display price
		return max(base_price, final_price)


	def _get_real_price_currency(self, product, rule_id, qty, uom, pricelist_id):
		"""Retrieve the price before applying the pricelist
			:param obj product: object of current product record
			:parem float qty: total quentity of product
			:param tuple price_and_rule: tuple(price, suitable_rule) coming from pricelist computation
			:param obj uom: unit of measure of current order line
			:param integer pricelist_id: pricelist id of sale order"""
		PricelistItem = self.env['product.pricelist.item']
		field_name = 'lst_price'
		currency_id = None
		product_currency = None
		if rule_id:
			pricelist_item = PricelistItem.browse(rule_id)
			if pricelist_item.pricelist_id.discount_policy == 'without_discount':
				while pricelist_item.base == 'pricelist' and pricelist_item.base_pricelist_id and pricelist_item.base_pricelist_id.discount_policy == 'without_discount':
					price, rule_id = pricelist_item.base_pricelist_id.with_context(uom=uom.id).get_product_price_rule(product, qty, self.order_id.partner_id)
					pricelist_item = PricelistItem.browse(rule_id)

			if pricelist_item.base == 'standard_price':
				field_name = 'standard_price'
			if pricelist_item.base == 'pricelist' and pricelist_item.base_pricelist_id:
				field_name = 'price'
				product = product.with_context(pricelist=pricelist_item.base_pricelist_id.id)
				product_currency = pricelist_item.base_pricelist_id.currency_id
			currency_id = pricelist_item.pricelist_id.currency_id

		product_currency = product_currency or(product.company_id and product.company_id.currency_id) or self.env.user.company_id.currency_id
		if not currency_id:
			currency_id = product_currency
			cur_factor = 1.0
		else:
			if currency_id.id == product_currency.id:
				cur_factor = 1.0
			else:
				cur_factor = currency_id.with_context(use_special_extype=True)._get_conversion_rate(product_currency, currency_id)

		product_uom = self.env.context.get('uom') or product.uom_id.id
		if uom and uom.id != product_uom:
			# the unit price is in a different uom
			uom_factor = uom._compute_price(1.0, product.uom_id)
		else:
			uom_factor = 1.0
		return product[field_name] * uom_factor * cur_factor, currency_id.id
