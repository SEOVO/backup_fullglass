# -*- encoding: utf-8 -*-
{
	'name': 'Impresion de guia de remision',
	'category': 'account',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['print_guia_remision_it','glass_production_order','kardex_it','purchase_carrier','export_file_manager_it'],
	'version': '1.0',
	'description':"""
	Imprime guia en remision en caso de que la orden de entrega sea para un cliente
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
	'views/stock_picking_view.xml',
	'wizard/print_pick_guides_wizard.xml',
	],
	'installable': True
}
