# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import base64,decimal
import sys
from odoo.exceptions import UserError
import pprint
from odoo.exceptions import ValidationError
class stock_picking(models.Model):
	_inherit = 'stock.picking'
	send_agency = fields.Boolean(string='Envío a Agencia')
	first_route = fields.Char('Primer Tramo')
	second_route = fields.Char('Segundo Tramo')
	
	@api.onchange('send_agency')
	def on_change_send_agency(self):
		if self.partner_id and self.send_agency:
			street_partner = self.partner_id.street or ''
			province = '/'+self.partner_id.province_id.name if self.partner_id.province_id else ''
			district = '/'+self.partner_id.district_id.name if self.partner_id.district_id else ''
			self.second_route = street_partner+district+province

	def get_print_wizard(self):
		module = __name__.split('addons.')[1].split('.')[0]
		wizard = self.env['print.picking.guides.wizard'].create({'picking_id': self.id})
		ctx = dict(self._context or {}, 
			active_model='stock.picking', 
			active_id=self.id,
			called_method='print_guide')

		return {
			'name':'Elegir Sede',
			'type': 'ir.actions.act_window',
			'res_id': wizard.id,
			'res_model': wizard._name,
			'view_id': self.env.ref('%s.print_picking_guides_wizard_view_form' % module).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': ctx,
			}
		
	def print_guide(self,path,start_point=False):		
		if self.state != 'done':
			raise UserError(u'La guía de remisión sólo puen imprimirse cuando el albarán si haya marcado como realizado.')
		if self.picking_type_id.code != 'internal':
			partner_id = self.partner_id.parent_id or self.partner_id
		else:
			#loc_partner = self.location_dest_id.location_id.get_warehouse().partner_id
			#partner_id = loc_partner or self.env['res.company']._company_default_get(self.name).partner_id
			partner_id = self.env['res.company']._company_default_get(self.name).partner_id
		
		document = partner_id.nro_documento or partner_id.parent_id.nro_documento
		if not (partner_id.street and document):
			raise UserError(u'El partner %s no tiene dirección y/o número de documento'%partner_id.name)

		if not self.numberg:
			raise UserError(_(u'Debe ingresar el número de guía'))

		if not self.punto_partida and start_point:
			self.punto_partida = start_point

		name_file = '%s.txt'%self.numberg.replace('/','-')
		
		conf = self.env['glass.order.config'].search([],limit=1)
		if type(path) not in (str,unicode):
			path = conf.path_remission_guides
		path+=name_file
		
		data = ''

		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		txt = chr(27) + chr(15) + chr(27) + chr(48)
		data+=txt
		#file.write(txt)
		data+=(6 * "\n")
		# change

		def replace_chr(string):
			return string.replace('\xe1',chr(160)).replace('\xe9',chr(130)).replace('\xed',chr(161)).replace('\xf3',chr(162)).replace('\xfa',chr(163)).replace('\xc1',chr(160)).replace('\xc9',chr(144)).replace('\xcd',chr(161)).replace('\xd3',chr(162)).replace('\xda',chr(163)).replace('\xd1',chr(164)).replace('\xf1',chr(165))

		def get_line(data,widths=None,style='columns',space=2):
			position,line,styles = 'left','',['consecutive','columns'] # por defecto en left
			if style not in styles:
				return ''
			for i,item in enumerate(data):
				if type(item) is dict:
					value = str(item.get('value','undefined'))
					position = item.get('position','left')
				else:
					value = str(item).encode('iso-8859-1','ignore')
				if style == 'consecutive':
					line += value + space * ' '
					continue
				# Quitamos espacios en blanco y si si el item es mayor al ancho se recorta a la longitud del ancho dejando 2 espacios:
				value = value.strip()[:widths[i]-1] 
				count = widths[i] - len(value)
				if position == 'right':
					line += count * ' ' + value
					continue
				if position == 'center':
					count = int(count / 2)
					line += count * ' ' + value + count * ' '
					continue
				line += value + count * ' '
			return replace_chr(line)+'\n'
		date_guide = fields.Date.context_today(self)
		data+=get_line(('',date_guide,self.invoice_id.number or ''),(14,55,30))
		data+=get_line(('',partner_id.name,'',document),(14,42,13,30))
		# end change
		phone = partner_id.phone if partner_id.phone else ''

		#partner_province = [partner_id.district_id.name,partner_id.province_id.name,partner_id.state_id.name]
		#partner_province = ' - '.join([item for item in partner_province if item])
		partner_province = [partner_id.district_id.name,partner_id.province_id.name]
		partner_province = ' - '.join([item for item in partner_province if item])

		street_partner = partner_id.street+' '+partner_province

		data+=get_line(('',street_partner[:55],phone),(14,55,30))

		#data+=get_line(('',partner_province,'',self.numberg),(14,55,43,18))
		data+=get_line(('','','',self.numberg),(14,55,43,18)) # dicen q no lo usarán por lo dejo por siaca xD
		data+=(1 * "\n")

		nombre = self.transporter_id.name or 'EL MISMO'
		ruc = self.transporter_id.nro_documento or self.ruc or ''
		trans_street = self.transporter_id.street or ''
		trans_phone = self.transporter_id.phone or self.transporter_id.mobile or ''
		
		data+=get_line(('',nombre,'',ruc),(14,46,6,20))
		data+=get_line(('',trans_street,'',trans_phone),(10,50,6,20))
		data+=(1*"\n")
		
		partida = self.punto_partida or self.picking_type_id.warehouse_id.partner_id.street or ''
		
		data+=get_line(('',partida),(24,73))

		wh_street =  self.location_dest_id.location_id.get_warehouse().partner_id.street
		end_point = self.punto_llegada or ((wh_street or partner_id.street or '') + partner_province)

		data+=get_line(('',end_point),(24,73))

		left = 0
		if self.einvoice_12.code != '01':
			left+=18
		if self.send_agency:
			data+=get_line(('',self.first_route or ''),(24,73))
			data+=get_line(('',self.second_route or '','X'),(24,73+left,3))
			data+=(4 * "\n")
		else:
			data+=(6 * "\n")

		acum_weight = 0
		total_weight = 0
		aux = 0
		headers_widths = (1,3,18,10,5,90,10) # anchos para datos de producto

		for cont,move in enumerate(self.move_lines,1):
			detail_lines = move.glass_order_line_ids
			container = []
			description = move.product_id.name
			acum_weight = 0.0
			if detail_lines:
				for detail in detail_lines:
					try:
						op_name = str(int(detail.order_id.name))
					except ValueError:
						op_name = detail.order_id.name
					show_data = '%s-%s(%s)'%(op_name,detail.crystal_number,detail.measures)
					acum_weight += detail.peso
					container.append(show_data)
				description+=' (%d Pzs)'%(len(detail_lines))
			else:
				acum_weight = move.product_uom_qty * move.product_id.weight
			
			srt_weight = '{:,.2f}'.format(decimal.Decimal ("%0.2f"%acum_weight))
			data+=get_line(('',cont,move.product_id.default_code or '',move.product_uom_qty,move.product_uom.name,description,{'value':srt_weight,'position':'right'}),headers_widths)
			if container:
				container = [container[i:i+5] for i in range(0,len(container), 5)]
				for sub_array in container:
					line = ''.join([x.ljust(25,' ') for x in sub_array])
					data+=get_line(('',line),(4,125))
					aux += 1
			total_weight += acum_weight
			aux += 1
		data+=((32-aux) * '\n')
		extra_info = u'Conductor: %s Marca: %s Placa: %s Licencia: %s'%(self.driver_delivery or '',self.marca or '',self.placa or '',self.licencia or '')
		data+=get_line(('',extra_info,'','Total Peso:',{'value':round(total_weight,4),'position':'right'}),(4,108,1,12,12))
		data+=(10*'\n')
		with open(path,'w') as file:
			file.write(data)
