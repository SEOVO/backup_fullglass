# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
import os

class PrintPickingGuidesWizard(models.TransientModel):
	_name = 'print.picking.guides.wizard'

	picking_id = fields.Many2one('stock.picking', string='Picking') # Old field TDO clean
	print_origin = fields.Selection([
		('pque_industrial', u'Parque Industrial'),
		('indep', u'Independencia'),
		('lima_1', u'Lima')], default='pque_industrial', string='Sede')

	#called_method = fields.Char(u'Método a ejecutar')

	def execute_method(self):
		# todos los métodos pasados deberían admitir un parámetro para el path
		path, start_point = self.get_path_and_start_point()
		ctx = self._context

		called_method = ctx.get('called_method')
		active_model = ctx.get('active_model') # TODO mejorar
		active_id = ctx.get('active_id')

		assert called_method and active_model and active_id, 'Wrong context'

		obj = self.env[active_model].browse(active_id)

		called_method = getattr(obj, called_method, None)
		if called_method and callable(called_method):
			return called_method(path, start_point)
		else:
			raise UserError(u'El método a ejecutar no ha sido establecido')


	def get_path_and_start_point(self):
		path, start_point = '', False
		config = self.env['glass.order.config'].search([],limit=1)
		
		if self.print_origin == 'pque_industrial':
			path = config.path_remission_guides_pque
			start_point = config.street_picking_pque
		elif self.print_origin == 'indep':
			path = config.path_remission_guides_ind
			start_point = config.street_picking_ind
		elif self.print_origin == 'lima_1':
			path = config.path_remission_guides_lim
			start_point = config.street_picking_lim
		if not path:
			raise UserError(u'No se ha encontrado la ruta de generación de archivo.')
		if not os.path.exists(path):
			raise UserError(u'La ruta %s no es una ruta de sistema válida, solicite a su admistrador su correción.' % path)

		return path, start_point
