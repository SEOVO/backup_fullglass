
{
    'name': 'Stage Crystals Importer',
    'version': '10',
    'license': 'AGPL-3',
    'author': 'ITGRUPO-POLIGLASS',
    'website': 'http://www.noviat.com',
    'category': 'Production',
    'summary': """
    Importador etapas de cristales en Fullglass 
    """,
    'depends': ['stock','glass_production_order'],
    'data': [
        'wizard/stage_importer_view.xml',
    ],
    'demo': [
        #'demo/account_move.xml',
    ],
    'installable': True,
}
