# -*- coding: utf-8 -*-
try:
	import cStringIO as StringIO
except ImportError:
	import StringIO
import base64
import csv
from datetime import datetime,timedelta
from sys import exc_info
from traceback import format_exception

from odoo import models, fields, api, _
from odoo.exceptions import Warning,UserError

import logging
_logger = logging.getLogger(__name__)

class StagesImport(models.TransientModel):
	_name = 'stages.import'
	sii_data = fields.Binary(string='File', required=True)
	sii_fname = fields.Char(string='Filename')
	lines = fields.Binary(
		compute='_compute_lines', string='Input Lines', required=True)
	dialect = fields.Binary(
		compute='_compute_dialect', string='Dialect', required=True)
	csv_separator = fields.Selection(
		[(',', 'Coma (,)'), (';', 'Punto y coma (;)')],
		string='CSV Separator',default=';')
	decimal_separator = fields.Selection(
		[('.', 'Punto (.)'), (',', 'Coma (,)')],
		string='Decimal Separator',
		default='.')
	codepage = fields.Char(
		string='Code Page',
		default=lambda self: self._default_codepage(),
		help="Code Page of the system that has generated the csv file."
			 "\nE.g. Windows-1252, utf-8")
	note = fields.Text('Log')

	@api.multi
	def get_element(self):
		module = __name__.split('addons.')[1].split('.')[0]
		view = self.env.ref('%s.stages_import_view_form' % module)
		return {
			'name':'Importacion de Etapas',
			'type': 'ir.actions.act_window',
			'res_model': 'stages.import',
			'view_id': view.id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
		}
		
	def _input_fields(self):
		"""
		Extend this dictionary if you want to add support for
		fields requiring pre-processing before being added to
		the pricelist line values dict.
		"""
		return {}

	@api.multi
	def stages_import(self):
		self._err_log = '' # no usado 
		self._get_orm_fields() # no usado
		lines, header = self._remove_leading_lines(self.lines)
		header_fields = csv.reader(
			StringIO.StringIO(header), dialect=self.dialect).next()
		
		header_fields = self.traslate_header_fields(header_fields)
		self._header_fields = self._process_header(header_fields)
		reader = csv.DictReader(
			StringIO.StringIO(lines), fieldnames=self._header_fields,
			dialect=self.dialect)

		item_lines = []
		error_line = False
		for line in reader:
			line = self._process_line_vals(line)
			exist = list(filter(lambda x: x['search_code'] == line['search_code'] and x['stage'] == line['stage'],item_lines))
			if len(exist) == 1:
				raise Warning('Existen lineas con codigos y etapas duplicadas:\n'+exist[0]['search_code']+' | '+str(exist[0]['stage']))
			item_lines.append(line)
		for item in item_lines:
			print('item', item)
			crystal = self.env['glass.lot.line'].search([('search_code','=',item['search_code'])])
			if not any(line):
				raise Warning('El cristal '+item['search_code'] + 'no existe en la base de datos')
			self.save_stage(item['stage'],crystal)

	@api.one
	def save_stage(self,stage,line):
		if line[stage]:
			raise Warning('El cristal '+str(line.search_code) +'ya fue procesado en la etapa :'+stage.upper())
		self.env['glass.stage.record'].create({
			'user_id':self.env.uid,
			'date':(datetime.now()-timedelta(hours=5)).date(),
			'time':(datetime.now()-timedelta(hours=5)).time(),
			'stage':stage,
			'lot_line_id':line.id,
		})
		line.write({stage:True})
		if stage == 'templado':
			line.is_used = True
			line.order_line_id.state = 'ended'

#### other methods
	def _process_line_vals(self, line):
		if line['stage'] not in ('corte','pulido','lavado','entalle','horno','templado'):
			raise Warning(u'El valor del Campo Etapa esta vacío o tiene un valor inválido')
		return line
	
	def traslate_header_fields(self,header_fields):
		dic_values = {
			'codigo':'search_code',
			'etapa':'stage',
		}

		for i,header in enumerate(header_fields):
			if header in dic_values:
				header_fields[i] = dic_values[header]
		return header_fields


	def _get_orm_fields(self):
		sii_mod = self.env['product.pricelist.item']
		orm_fields = sii_mod.fields_get()
		blacklist = models.MAGIC_COLUMNS + [sii_mod.CONCURRENCY_CHECK_FIELD]
		self._orm_fields = {
			f: orm_fields[f] for f in orm_fields
			if f not in blacklist
			and not orm_fields[f].get('depends')}


	def _process_header(self, header_fields):

		self._field_methods = self._input_fields()
		self._skip_fields = []

		# header fields after blank column are considered as comments
		column_cnt = 0

		for cnt in range(len(header_fields)):
			if header_fields[cnt] == '':
				column_cnt = cnt
				break
			elif cnt == len(header_fields) - 1:
				column_cnt = cnt + 1
				break
		header_fields = header_fields[:column_cnt]

		# check for duplicate header fields
		header_fields2 = []
		for hf in header_fields:
			if hf in header_fields2:
				raise Warning(_(
					"Duplicate header field '%s' found !"
					"\nPlease correct the input file.")
					% hf)
			else:
				header_fields2.append(hf)

		for i, hf in enumerate(header_fields):

			if hf in self._field_methods:
				continue

			if hf not in self._orm_fields \
					and hf not in [self._orm_fields[f]['string'].lower()
								   for f in self._orm_fields]:
				_logger.error(
					_("%s, undefined field '%s' found "
					  "while importing move lines"),
					self._name, hf)
				self._skip_fields.append(hf)
				continue

			field_def = self._orm_fields.get(hf)
			if not field_def:
				for f in self._orm_fields:
					if self._orm_fields[f]['string'].lower() == hf:
						orm_field = f
						field_def = self._orm_fields.get(f)
						break
			else:
				orm_field = hf
			field_type = field_def['type']

			if field_type in ['char', 'text']:
				self._field_methods[hf] = {
					'method': self._handle_orm_char,
					'orm_field': orm_field,
					}
			elif field_type == 'integer':
				self._field_methods[hf] = {
					'method': self._handle_orm_integer,
					'orm_field': orm_field,
					}
			elif field_type == 'float':
				self._field_methods[hf] = {
					'method': self._handle_orm_float,
					'orm_field': orm_field,
					}
			elif field_type == 'many2one':
				self._field_methods[hf] = {
					'method': self._handle_orm_many2one,
					'orm_field': orm_field,
					}
			else:
				_logger.error(
					_("%s, the import of ORM fields of type '%s' "
					  "is not supported"),
					self._name, hf, field_type)
				self._skip_fields.append(hf)

		return header_fields

	def _log_line_error(self, line, msg):
		data = self.csv_separator.join(
			[line[hf] for hf in self._header_fields])
		self._err_log += _(
			"Error when processing line '%s'") % data + ':\n' + msg + '\n\n'

	def _handle_orm_char(self, field, line, pricelist, sii_vals,
						 orm_field=False):
		orm_field = orm_field or field
		if not sii_vals.get(orm_field):
			sii_vals[orm_field] = line[field]

	def _handle_orm_integer(self, field, line, pricelist, sii_vals,
							orm_field=False):
		orm_field = orm_field or field
		if not sii_vals.get(orm_field):
			val = str2int(
				line[field], self.decimal_separator)
			if val is False:
				msg = _(
					"Incorrect value '%s' "
					"for field '%s' of type Integer !"
					) % (line[field], field)
				self._log_line_error(line, msg)
			else:
				sii_vals[orm_field] = val

	def _handle_orm_float(self, field, line, pricelist, sii_vals,
						  orm_field=False):
		orm_field = orm_field or field
		if not sii_vals.get(orm_field):
			sii_vals[orm_field] = str2float(
				line[field], self.decimal_separator)

			val = str2float(
				line[field], self.decimal_separator)
			if val is False:
				msg = _(
					"Incorrect value '%s' "
					"for field '%s' of type Numeric !"
					) % (line[field], field)
				self._log_line_error(line, msg)
			else:
				sii_vals[orm_field] = val

	def _handle_orm_many2one(self, field, line, pricelist, sii_vals,
							 orm_field=False):
		orm_field = orm_field or field
		if not sii_vals.get(orm_field):
			val = str2int(
				line[field], self.decimal_separator)
			if val is False:
				msg = _(
					"Incorrect value '%s' "
					"for field '%s' of type Many2One !"
					"\nYou should specify the database key "
					"or contact your IT department "
					"to add support for this field."
					) % (line[field], field)
				self._log_line_error(line, msg)
			else:
				sii_vals[orm_field] = val

	@api.model
	def _default_codepage(self):
		return 'Windows-1252'

	@api.one
	@api.depends('sii_data')
	def _compute_lines(self):
		if self.sii_data:
			self.lines = base64.decodestring(self.sii_data)

	@api.one
	@api.depends('lines', 'csv_separator')
	def _compute_dialect(self):
		if self.lines:
			try:
				self.dialect = csv.Sniffer().sniff(
					self.lines[:128], delimiters=';,')
			except:
				# csv.Sniffer is not always reliable
				# in the detection of the delimiter
				self.dialect = csv.Sniffer().sniff(
					'"header 1";"header 2";\r\n')
				if ',' in self.lines[128]:
					self.dialect.delimiter = ','
				elif ';' in self.lines[128]:
					self.dialect.delimiter = ';'
		if self.csv_separator:
			self.dialect.delimiter = str(self.csv_separator)

	@api.onchange('sii_data')
	def _onchange_sii_data(self):
		if self.lines:
			self.csv_separator = self.dialect.delimiter
			if self.csv_separator == ';':
				self.decimal_separator = ','

	@api.onchange('csv_separator')
	def _onchange_csv_separator(self):
		if self.csv_separator and self.sii_data:
			self.dialect.delimiter = self.csv_separator

	def _remove_leading_lines(self, lines):
		""" remove leading blank or comment lines """
		input = StringIO.StringIO(lines)
		header = False
		while not header:
			ln = input.next()
			if not ln or ln and ln[0] in [self.csv_separator, '#']:
				continue
			else:
				header = ln.lower()
		if not header:
			raise Warning(
				_("No header line found in the input file !"))
		output = input.read()
		return output, header

def str2float(amount, decimal_separator):
	if not amount:
		return 0.0
	try:
		if decimal_separator == '.':
			return float(amount.replace(',', ''))
		else:
			return float(amount.replace('.', '').replace(',', '.'))
	except:
		return False


def str2int(amount, decimal_separator):
	if not amount:
		return 0
	try:
		if decimal_separator == '.':
			return int(amount.replace(',', ''))
		else:
			return int(amount.replace('.', '').replace(',', '.'))
	except:
		return False
