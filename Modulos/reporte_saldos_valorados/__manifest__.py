# -*- encoding: utf-8 -*-
{
    'name': 'Reporte Saldos Valorados',
    'version': '1.0',
    'author': 'ITGRUPO-POLIGLASS',
    'website': '',
    'category': 'reports',
    'depends': ['import_base_it','stock_account','product','stock','mrp','purchase_requisition','kardex_campos_it','account_move_advanceadd_it','sale_stock','account_parametros_it'],
    'description': """Functionality Fullglass""",
    'demo': [],
    'data': [
        'views/saldo_valorado_wizard_view.xml',
    ],
    'auto_install': False,
    'installable': True
}
