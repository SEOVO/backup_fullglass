# -*- coding: utf-8 -*-
from odoo import api, fields, models, exceptions
import string,base64
class SaldoValoradoWizard(models.TransientModel):
	_name = 'saldo.valorado.wizard'
	year_id  = fields.Many2one('account.fiscalyear','Anio')

	@api.multi
	def make_report(self):
		products = self.env['product.product'].search([]).ids
		locations = self.env['stock.location'].search([('usage','in',('internal','inventory','transit','procurement','production'))]).ids
		
		products = '{'+','.join(map(str,products))+'}'
		locations = '{'+','.join(map(str,locations))+'}'
		import io
		from xlsxwriter.workbook import Workbook
		from datetime import datetime
		
		start_date = self.year_id.name+'0101'
		end_date = self.year_id.name+str(datetime.now().date()).replace('-','')[4:]

		output = io.BytesIO()
		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		workbook = Workbook(direccion +'reporte_saldos_valorados.xlsx')
		worksheet = workbook.add_worksheet("Kardex")
		bold = workbook.add_format({'bold': True})
		bold.set_font_size(8)
		normal = workbook.add_format()
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(8)
		boldbord.set_bg_color('#DCE6F1')
		especial1 = workbook.add_format({'bold': True})
		especial1.set_align('center')
		especial1.set_align('vcenter')
		especial1.set_text_wrap()
		especial1.set_font_size(15)
		numbertres = workbook.add_format({'num_format':'0.000'})
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberseis = workbook.add_format({'num_format':'0.000000'})
		numberseis.set_font_size(8)
		numberocho = workbook.add_format({'num_format':'0.00000000'})
		numberocho.set_font_size(8)
		bord = workbook.add_format()
		bord.set_border(style=1)
		bord.set_font_size(8)
		numberdos.set_border(style=1)
		numberdos.set_font_size(8)
		numbertres.set_border(style=1)
		numberseis.set_border(style=1)
		numberocho.set_border(style=1)
		numberdosbold = workbook.add_format({'num_format':'0.00','bold':True})
		numberdosbold.set_font_size(8)
		x= 7
		tam_letra = 1.2
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')

		worksheet.merge_range(1,1,1,6, "REPORTE DE SALDOS VALORADOS", especial1)
		worksheet.write(2,0,'FECHA INICIO:',bold)
		worksheet.write(3,0,'FECHA FIN:',bold)

		worksheet.write(2,1,start_date[:4]+'/'+start_date[4:6]+'/'+start_date[6:8])
		worksheet.write(3,1,end_date[:4]+'/'+end_date[4:6]+'/'+end_date[6:8])
		worksheet.merge_range(5,1,6,1, u"PRODUCTO",boldbord)
		worksheet.merge_range(5,2,6,2, u"UNIDAD",boldbord)
		worksheet.merge_range(5,3,6,3, u"ALMACÉN",boldbord)
		worksheet.merge_range(5,4,6,4, u"INGRESO FÍSICO",boldbord)
		worksheet.merge_range(5,5,6,5, u"INGRESO VALORADO",boldbord)
		worksheet.merge_range(5,6,6,6, u"SALIDA FÍSICA",boldbord)
		worksheet.merge_range(5,7,6,7, u"SALIDA VALORADA",boldbord)
		worksheet.merge_range(5,8,6,8, u"SALDO FÍSICO",boldbord)
		worksheet.merge_range(5,9,6,9, u"SALDO VALORADO",boldbord)
		self.env.cr.execute("""
			select
			get_kardex_v.name_template as "Producto",
			get_kardex_v.unidad as "Unidad",
			get_kardex_v.almacen as "Almacen",
			sum(get_kardex_v.ingreso) as "Ingreso Fisico",
			sum(round(get_kardex_v.debit,6)) as "Ingreso Valorado.",
			sum(get_kardex_v.salida) as "Salida Fisico",
			sum(round(get_kardex_v.credit,6)) as "Salida Valorada",
			sum(get_kardex_v.ingreso-get_kardex_v.salida) as "Saldo Fisico",
			sum(round(get_kardex_v.debit,6)-round(get_kardex_v.credit,6)) as "Saldo valorado"

			from get_kardex_v("""+ start_date + "," + end_date + ",'" + products + """'::INT[], '""" + locations + """'::INT[])
			left join stock_move on get_kardex_v.stock_moveid = stock_move.id
			left join product_product on product_product.id = stock_move.product_id
			left join product_template on product_template.id = product_product.product_tmpl_id
			left join stock_picking on stock_move.picking_id = stock_picking.id
			
			group by get_kardex_v.name_template,get_kardex_v.unidad,get_kardex_v.almacen
			order by get_kardex_v.name_template
		""")

		for line in self.env.cr.fetchall():
			worksheet.write(x,1,line[0] if line[0] else 0 ,bord)
			worksheet.write(x,2,line[1] if line[1] else 0 ,bord)
			worksheet.write(x,3,line[2] if line[2] else 0 ,bord)
			worksheet.write(x,4,line[3] if line[3] else 0 ,numberdos)
			worksheet.write(x,5,line[4] if line[4] else 0 ,numberdos)
			worksheet.write(x,6,line[5] if line[5] else 0 ,numberdos)
			worksheet.write(x,7,line[6] if line[6] else 0 ,numberdos)
			worksheet.write(x,8,line[7] if line[7] else 0 ,numberdos)
			worksheet.write(x,9,line[8] if line[8] else 0 ,numberdos)
			x+=1

		tam_col = [10,35,12,24,12,12,12,12,12,12,11]
		alpha = list(string.ascii_uppercase)
		for i,item in enumerate(tam_col):
			worksheet.set_column(alpha[i]+':'+alpha[i],item)
		workbook.close()

		f = open(direccion + 'reporte_saldos_valorados.xlsx', 'rb')
		vals = {
			'output_name': 'reporte_saldos_valorados.xlsx',
			'output_file': base64.encodestring(''.join(f.readlines())),
		}
		mod_obj = self.env['ir.model.data']
		act_obj = self.env['ir.actions.act_window']
		sfs_id = self.env['export.file.save'].create(vals)
		result = {}
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}