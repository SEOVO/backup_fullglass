# -*- encoding: utf-8 -*-
{
	'name': 'Reparador de Etapas Incompletas',
	'category': 'repair',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['glass_production_order'],
	'version': '1.0',
	'description':"""
		Módulo para subsanar las etapas no registradas en Fullglass
	""",
	'auto_install': False,
	'demo': [],
	'data': [
		'security/groups.xml',
		'security/ir.model.access.csv',
		'wizard/repair_glass_stage_wizard.xml',
		],
	'installable': True
}