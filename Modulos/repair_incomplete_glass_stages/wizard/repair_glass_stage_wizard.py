# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

REP_STAGES = ('corte', 'pulido', 'entalle', 'lavado', 'arenado', 'biselado', 'perforacion', 'producido') 
# etapas registradas por pistoleo 
# (sólo repararemos éstas debido a qué son las únicas que podrían no registrarse debido a fallas de hardware)
# TODO FIXME Retirar el producido una vez que hayan subsanado su data.

class RepairGlassStageWizard(models.TransientModel):
	_name='repair.glass.stage.wizard'

	stage_id = fields.Many2one('glass.stage',string='Etapa',domain=[('name','in',REP_STAGES)])
	lot_line_ids = fields.Many2many('glass.lot.line',string='Cristales')

	def get_incomplete_crystals(self):
		"""Asumimos las etapas incompletas como las etapas solicitadas y no registradas
		en cristales marcados como producidos
		"""
		#self.ensure_one()
		# TODO FIXME par ael caso de los entalles, recurriremos directamente a la línea de calculadora
		# para disminuir el procesamiento, mejorar ésto.
		
		stage = self.stage_id.name
		#domain = [('is_service', '=', False), ('producido', '=', True), (stage, '=', False)]


		self._cr.execute("""
			SELECT 
			ARRAY_AGG(id)
			FROM 
			glass_lot_line
			WHERE 
			COALESCE(is_service, false) = false
			AND COALESCE(templado, false) = true
			AND COALESCE(active, false) = true
			AND '%s' = ANY(string_to_array(all_pending_stages, ','));""" % stage)

		r = self._cr.fetchone()
		
		# if stage == 'entalle':
		# 	domain.append(('calc_line_id.entalle', '>', 0))
		# 	lines = self.env['glass.lot.line'].search(domain).ids
		# else:
		# 	lines = self.env['glass.lot.line'].search(domain)
		# 	lines = [l.id for l in lines if stage in l._get_requested_stages()]
		
		self.lot_line_ids = [(6, 0, r and r[0] or [])]
		return {"type": "ir.actions.do_nothing",}

	def repair_stage(self):
		#self.ensure_one()
		if not self.lot_line_ids:
			raise UserError(u'No hay cristales para subsanar')

		for line in self.lot_line_ids:
			if not line[self.stage_id.name]:
				if self.stage_id.name != 'producido': # TEMPORAL , reestablecer cuando hayan subsanado su data
					line.register_stage(self.stage_id)
				# Si no tiene pendientes y no está producido, dar como tal:
				if line.templado and (not line.all_pending_stages or line.all_pending_stages == 'producido') \
				and not line.producido and not line.from_insulado:
					line.with_context(force_register=True).register_stage('producido')

		#self.lot_line_ids.register_stage(self.stage_id)
		self.lot_line_ids = [(5,)]
		return {"type": "ir.actions.do_nothing",}


	