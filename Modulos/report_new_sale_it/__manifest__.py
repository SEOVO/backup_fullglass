# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'New Reporte Orden de Venta',
    'category': 'deliveryslip',
    'author': ' ITGRUPO-POLIGLASS',
    'version': '1.1',
    'description': """
    New Reporte Orden de Venta

    """,
    'depends': ['sale','sale_order_custom_report_it','purchase','reports_glass_order','glass_production_order'],
    'auto_install': False,
    'demo': [],
    'data': [
        'security/groups.xml',
        'security/ir.model.access.csv',
        'views/pdf_report_view.xml',
        'views/views.xml',
        'views/header.xml',
        'views/report_purchase_it.xml',
        'views/reporte_op.xml',

             ],
    'installable': True
}
