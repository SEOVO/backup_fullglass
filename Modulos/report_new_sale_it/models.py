from odoo import models, fields, api,tools
from num2words import num2words
from datetime import datetime, timedelta

class Oficina(models.Model):
    _name = 'oficina.fullglass'
    name     = fields.Char(required=True,string="Nombre")
    street   = fields.Char(string="Direccion")
    phone    = fields.Char(string="Telefono")
    email    = fields.Char()

class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
    desc_crystals = fields.Char(compute="get_cal")

    def get_cal(self):
        for record in self:
            calc_lines = record.calculator_id.line_ids
            if calc_lines:
                # desc_crystals = ' '.join(['%d(%s)' % (10, '1200x1200') for xd in range(250)]) # test
                record.desc_crystals = ' '.join(calc_lines.mapped(lambda l: '%d(%s)' % (l.quantity, l.measures)))
            else:
                record.desc_crystals = None

class SaleOrder(models.Model):
    _inherit = 'sale.order'
    total_sub = fields.Char(compute="get_tt")
    total_discount = fields.Char(compute="get_tt")
    num_label      = fields.Char(compute="get_tt")
    currency_it = fields.Char(compute="get_tt")

    @api.model
    def _default_ofi(self):
        of = self.env['oficina.fullglass'].search([], limit=1)
        return of.id
    oficina = fields.Many2one('oficina.fullglass',default=_default_ofi)




    def get_tt(self):
        for record in self:
            currency = self.env['account_currency_name_it.currency_name'].search(
                [('currency_id', '=', self.pricelist_id.currency_id.id)], limit=1)
            record.currency_it = str(currency.name_plural)+" "+str(currency.currency_id.symbol)
            total_discount = total_sub = 0.0
            for i, line in enumerate(self.order_line, 1):
                price_sub = line.price_subtotal / (1 - line.discount / 100.0)
                total_sub += price_sub
                total_discount += (line.price_subtotal - price_sub)
            record.total_discount = total_discount
            record.total_sub = total_sub

            integ = str(self.amount_total).split('.')
            try:
                number_label = num2words(int(integ[0]), lang='es')
            except NotImplementedError:
                number_label = num2words(int(integ[0]), lang='en')
            record.num_label = u'%s CON %s/100 %s' % (number_label.upper(), integ[1].ljust(2, '0'), currency.name_plural)

class Compras(models.Model):
    _inherit = 'purchase.order'
    currency_it = fields.Char(compute="get_tt")
    def get_tt(self):
        for record in self:
            currency = self.env['account_currency_name_it.currency_name'].search(
                [('currency_id', '=', self.currency_id.id)], limit=1)
            record.currency_it = str(currency.name_plural)+" "+str(currency.currency_id.symbol)

    @api.model
    def _default_ofi(self):
        of = self.env['oficina.fullglass'].search([], limit=1)
        return of.id

    oficina = fields.Many2one('oficina.fullglass', default=_default_ofi)

    @api.model
    def get_user_it(self):
        now = fields.Datetime.context_timestamp(self, datetime.now())
        self.user_actual = str(self.env.user.name)

    user_actual = fields.Char(compute="get_user_it")



class OP(models.Model):
    _inherit = 'glass.order'

    @api.model
    def _default_ofi(self):
        of = self.env['oficina.fullglass'].search([], limit=1)
        return of.id

    oficina = fields.Many2one('oficina.fullglass', default=_default_ofi)


    @api.model
    def get_company_it(self):
        com = self.env['res.company']._company_default_get(self._name)
        self.company_id = com.id
            # return of.id
    company_id = fields.Many2one('res.company',compute="get_company_it")

    @api.model
    def get_date_menos_cinco(self):
        self.date_order_menos_cinco = str(datetime.strptime(self.date_order,"%Y-%m-%d %H:%M:%S")-timedelta(hours=5))
        # return of.id
    date_order_menos_cinco =  fields.Char(compute="get_date_menos_cinco")

    @api.model
    def get_user_it(self):
        now = fields.Datetime.context_timestamp(self, datetime.now())
        self.user_actual = str(self.env.user.name)+" "+str(now)[:19]
    user_actual = fields.Char(compute="get_user_it")




