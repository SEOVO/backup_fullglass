# -*- coding: utf-8 -*-
from odoo.tools.misc import DEFAULT_SERVER_DATETIME_FORMAT
import time
import odoo.addons.decimal_precision as dp
from openerp.osv import osv
import base64
from odoo import models, fields, api
import codecs,string
values = {}

class make_kardex(models.TransientModel):
	_inherit = "make.kardex"

	@api.multi
	def do_csvtoexcel(self):
		cad = ""
		s_prod = [-1,-1,-1]
		s_loca = [-1,-1,-1]
		if self.alllocations == True:
			locat_ids = self.env['stock.location'].search( [('usage','in',('internal','inventory','transit','procurement','production'))] )
			lst_locations = locat_ids.ids
		else:
			lst_locations = self.location_ids.ids
		lst_products  = self.products_ids.ids
		productos='{'
		almacenes='{'
		date_ini=self.fini
		date_fin=self.ffin
		if self.allproducts:
			lst_products = self.env['product.product'].with_context(active_test=False).search([]).ids
		else:
			lst_products = self.products_ids.ids
		if len(lst_products) == 0:
			raise osv.except_osv('Alerta','No existen productos seleccionados')

		for producto in lst_products:
			productos=productos+str(producto)+','
			s_prod.append(producto)
		productos=productos[:-1]+'}'
		for location in lst_locations:
			almacenes=almacenes+str(location)+','
			s_loca.append(location)
		almacenes=almacenes[:-1]+'}'
		# raise osv.except_osv('Alertafis',[almacenes,productos])

		if self.env.context['tipo']=='valorado':

			import io
			from xlsxwriter.workbook import Workbook
			output = io.BytesIO()
			path = self.env['main.parameter'].search([])[0].dir_create_file
			file_name = 'Kardex Valorado.xlsx'
			path+=file_name
			workbook = Workbook(path)
			worksheet = workbook.add_worksheet("Kardex")
			bold = workbook.add_format({'bold': True})
			bold.set_font_size(8)
			normal = workbook.add_format()
			boldbord = workbook.add_format({'bold': True})
			boldbord.set_border(style=2)
			boldbord.set_align('center')
			boldbord.set_align('vcenter')
			boldbord.set_text_wrap()
			boldbord.set_font_size(8)
			boldbord.set_bg_color('#DCE6F1')

			especial1 = workbook.add_format({'bold': True})
			especial1.set_align('center')
			especial1.set_align('vcenter')
			especial1.set_text_wrap()
			especial1.set_font_size(15)

			numbertres = workbook.add_format({'num_format':'0.000'})
			numberdos = workbook.add_format({'num_format':'0.00'})
			numberseis = workbook.add_format({'num_format':'0.000000'})
			numberseis.set_font_size(8)
			numberocho = workbook.add_format({'num_format':'0.00000000'})
			numberocho.set_font_size(8)
			bord = workbook.add_format()
			bord.set_border(style=1)
			bord.set_font_size(8)
			numberdos.set_border(style=1)
			numberdos.set_font_size(8)
			numbertres.set_border(style=1)
			numberseis.set_border(style=1)
			numberocho.set_border(style=1)
			numberdosbold = workbook.add_format({'num_format':'0.00','bold':True})
			numberdosbold.set_font_size(8)
			x= 10
			tam_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			tam_letra = 1.2
			import sys
			reload(sys)
			sys.setdefaultencoding('iso-8859-1')

			worksheet.merge_range(1,5,1,10, "KARDEX VALORADO", especial1)
			worksheet.write(2,0,'FECHA INICIO:',bold)
			worksheet.write(3,0,'FECHA FIN:',bold)

			worksheet.write(2,1,self.fini)
			worksheet.write(3,1,self.ffin)
			import datetime

			worksheet.merge_range(8,0,9,0, u"Fecha Alm.",boldbord)
			worksheet.merge_range(8,1,9,1, u"Fecha",boldbord)
			worksheet.merge_range(8,2,9,2, u"Tipo",boldbord)
			worksheet.merge_range(8,3,9,3, u"Serie",boldbord)
			worksheet.merge_range(8,4,9,4, u"Número",boldbord)
			worksheet.merge_range(8,5,9,5, u"Guía de remisión",boldbord)

			worksheet.merge_range(8,6,9,6, u"Doc. Almacen",boldbord)
			worksheet.merge_range(8,7,9,7, u"RUC",boldbord)
			worksheet.merge_range(8,8,9,8, u"Empresa",boldbord)

			worksheet.merge_range(8,9,9,9, u"T. OP.",boldbord)

			worksheet.merge_range(8,10,9,10, u"Código",boldbord)
			worksheet.merge_range(8,11,9,11, u"Producto",boldbord)
			worksheet.merge_range(8,12,9,12, u"Unidad",boldbord)
			worksheet.merge_range(8,13,9,13, u"Unidad Medida Retazo",boldbord)
			worksheet.merge_range(8,14,8,15, u"Ingreso",boldbord)
			worksheet.write(9,14, "Cantidad",boldbord)
			worksheet.write(9,15, "Costo",boldbord)
			worksheet.merge_range(8,16,8,17, u"Salida",boldbord)
			worksheet.write(9,16, "Cantidad",boldbord)
			worksheet.write(9,17, "Costo",boldbord)
			worksheet.merge_range(8,18,8,19, u"Saldo",boldbord)
			worksheet.write(9,18, "Cantidad",boldbord)
			worksheet.write(9,19, "Costo",boldbord)

			worksheet.merge_range(8,20,9,20, u"Costo Adquisición",boldbord)
			worksheet.merge_range(8,21,9,21, "Costo Promedio",boldbord)

			worksheet.merge_range(8,22,9,22, "Ubicacion Origen",boldbord)
			worksheet.merge_range(8,23,9,23, "Ubicacion Destino",boldbord)
			worksheet.merge_range(8,24,9,24, "Almacen",boldbord)


			self.env.cr.execute("""
				 select

				get_kardex_v.fecha_albaran as "Fecha Alb.",
				get_kardex_v.fecha as "Fecha",
				get_kardex_v.type_doc as "T. Doc.",
				get_kardex_v.serial as "Serie",
				get_kardex_v.nro as "Nro. Documento",
				get_kardex_v.stock_doc as "Nro. Documento",
				get_kardex_v.doc_partner as "Nro Doc. Partner",
				get_kardex_v.name as "Proveedor",
				get_kardex_v.operation_type as "Tipo de operacion",
				get_kardex_v.name_template as "Producto",
				get_kardex_v.unidad as "Unidad",
				get_kardex_v.ingreso as "Ingreso Fisico",
				round(get_kardex_v.debit,6) as "Ingreso Valorado.",
				get_kardex_v.salida as "Salida Fisico",
				round(get_kardex_v.credit,6) as "Salida Valorada",
				get_kardex_v.saldof as "Saldo Fisico",
				round(get_kardex_v.saldov,6) as "Saldo valorado",
				round(get_kardex_v.cadquiere,6) as "Costo adquisicion",
				round(get_kardex_v.cprom,6) as "Costo promedio",
					get_kardex_v.origen as "Origen",
					get_kardex_v.destino as "Destino",
				get_kardex_v.almacen AS "Almacen",
				coalesce(product_product.default_code,product_template.default_code) as "Codigo",
				stock_picking.numberg as "Guia de Remision",
				stock_move.id as move_id
				from get_kardex_v("""+ str(date_ini).replace('-','') + "," + str(date_fin).replace('-','') + ",'" + productos + """'::INT[], '""" + almacenes + """'::INT[])
				left join stock_move on get_kardex_v.stock_moveid = stock_move.id
				left join product_product on product_product.id = stock_move.product_id
				left join product_template on product_template.id = product_product.product_tmpl_id
				left join stock_picking on stock_move.picking_id = stock_picking.id
				
				order by get_kardex_v.correlativovisual
			""")

			ingreso1= 0
			ingreso2= 0
			salida1= 0
			salida2= 0
			for line in self.env.cr.fetchall():
				scrap = self.env['glass.scrap.record'].search([('move_id','=',line[24])])
				worksheet.write(x,0,line[0] if line[0] else '' ,bord )
				worksheet.write(x,1,line[1] if line[1] else '' ,bord )
				worksheet.write(x,2,line[2] if line[2] else '' ,bord )
				worksheet.write(x,3,line[3] if line[3] else '' ,bord )
				worksheet.write(x,4,line[4] if line[4] else '' ,bord )
				worksheet.write(x,5,line[23] if line[23] else '' ,bord )
				worksheet.write(x,6,line[5] if line[5] else '' ,bord )
				worksheet.write(x,7,line[6] if line[6] else '' ,bord )
				worksheet.write(x,8,line[7] if line[7] else '' ,bord )
				worksheet.write(x,9,line[8] if line[8] else '' ,bord )
				worksheet.write(x,10,line[22] if line[22] else '' ,bord )

				worksheet.write(x,11,line[9] if line[9] else 0 ,numberdos )
				worksheet.write(x,12,line[10] if line[10] else 0 ,numberdos )
				worksheet.write(x,13,scrap.scrap_move_id.name or '',numberdos )
				worksheet.write(x,14,line[11] if line[11] else 0 ,bord )
				worksheet.write(x,15,line[12] if line[12] else 0 ,numberdos )
				worksheet.write(x,16,line[13] if line[13] else 0 ,numberdos )
				worksheet.write(x,17,line[14] if line[14] else 0 ,numberdos )
				worksheet.write(x,18,line[15] if line[15] else 0 ,numberseis )
				worksheet.write(x,19,line[16] if line[16] else 0 ,numberocho )

				worksheet.write(x,20,line[17] if line[17] else '' ,bord )
				worksheet.write(x,21,line[18] if line[18] else '' ,bord )
				worksheet.write(x,22,line[19] if line[19] else '' ,bord )
				worksheet.write(x,23,line[20] if line[20] else '' ,bord )
				worksheet.write(x,24,line[21] if line[21] else '' ,bord )

				ingreso1 += line[11] if line[11] else 0
				ingreso2 +=line[12] if line[12] else 0
				salida1 +=line[13] if line[13] else 0
				salida2 += line[14] if line[14] else 0

				x+=1

			tam_col = [11,11,5,5,7,7,5]+[11 for i in range(18)]
			alpha = list(string.ascii_uppercase)
			for i,item in enumerate(tam_col):
				worksheet.set_column(alpha[i]+':'+alpha[i],item)
			workbook.close()
			file = open(path, 'rb').read()
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file),	
			})
			return export.export_file(clear=True,path=path)
		else:
			import io
			from xlsxwriter.workbook import Workbook
			output = io.BytesIO()
			path = self.env['main.parameter'].search([])[0].dir_create_file
			file_name = u'Kardex Físico.xlsx'
			path+=file_name
			workbook = Workbook(path)
			worksheet = workbook.add_worksheet("Kardex")
			bold = workbook.add_format({'bold': True})
			bold.set_font_size(8)
			normal = workbook.add_format()
			boldbord = workbook.add_format({'bold': True})
			boldbord.set_border(style=2)
			boldbord.set_align('center')
			boldbord.set_align('vcenter')
			boldbord.set_text_wrap()
			boldbord.set_font_size(8)
			boldbord.set_bg_color('#DCE6F1')

			especial1 = workbook.add_format({'bold': True})
			especial1.set_align('center')
			especial1.set_align('vcenter')
			especial1.set_text_wrap()
			especial1.set_font_size(15)

			numbertres = workbook.add_format({'num_format':'0.000'})
			numberdos = workbook.add_format({'num_format':'0.00'})
			numberseis = workbook.add_format({'num_format':'0.000000'})
			numberseis.set_font_size(8)
			numberocho = workbook.add_format({'num_format':'0.00000000'})
			numberocho.set_font_size(8)
			bord = workbook.add_format()
			bord.set_border(style=1)
			bord.set_font_size(8)
			numberdos.set_border(style=1)
			numberdos.set_font_size(8)
			numbertres.set_border(style=1)
			numberseis.set_border(style=1)
			numberocho.set_border(style=1)
			numberdosbold = workbook.add_format({'num_format':'0.00','bold':True})
			numberdosbold.set_font_size(8)
			x= 10
			tam_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
			tam_letra = 1.2
			import sys
			reload(sys)
			sys.setdefaultencoding('iso-8859-1')

			worksheet.merge_range(1,5,1,10, "KARDEX FISICO", especial1)
			worksheet.write(2,0,'FECHA INICIO:',bold)
			worksheet.write(3,0,'FECHA FIN:',bold)

			worksheet.write(2,1,self.fini)
			worksheet.write(3,1,self.ffin)
			import datetime

			worksheet.merge_range(8,0,9,0, u"Ubicacion Origen",boldbord)
			worksheet.merge_range(8,1,9,1, u"Ubicacion Destino",boldbord)
			worksheet.merge_range(8,2,9,2, u"Almacen",boldbord)
			worksheet.merge_range(8,3,9,3, u"Tipo de Operación",boldbord)
			worksheet.merge_range(8,4,9,4, u"Categoria",boldbord)

			worksheet.merge_range(8,5,9,5, u"Producto",boldbord)
			worksheet.merge_range(8,6,9,6, u"Codigo P.",boldbord)
			worksheet.merge_range(8,7,9,7, u"Unidad",boldbord)
			worksheet.merge_range(8,8,9,8, u"Unidad Medida Retazo",boldbord)

			worksheet.merge_range(8,9,9,9, u"Fecha",boldbord)

			worksheet.merge_range(8,10,9,10, u"Doc. Almacen",boldbord)

			worksheet.write(8,11, "Ingreso",boldbord)
			worksheet.write(9,11, "Cantidad",boldbord)
			worksheet.write(8,12, "Salida",boldbord)
			worksheet.write(9,12, "Cantidad",boldbord)
			worksheet.write(8,13, "Saldo",boldbord)
			worksheet.write(9,13, "Cantidad",boldbord)
			self.env.cr.execute("""

				select
origen.complete_name AS "Ubicación Origen",
destino.complete_name AS "Ubicación Destino",
almacen.complete_name AS "Almacén",
vstf.motivo_guia AS "Tipo de operación",
pc.name as "Categoria",
product_name.name as "Producto",
pp.default_code as "Codigo P.",
pu.name as "unidad",
vstf.fecha as "Fecha",
sp.name as "Doc. Almacén",
vstf.entrada as "Entrada",
vstf.salida as "Salida",
sm.id
from
(
	select vst_kardex_fisico.date::date as fecha,vst_kardex_fisico.location_id as origen, vst_kardex_fisico.location_dest_id as destino, vst_kardex_fisico.location_dest_id as almacen, vst_kardex_fisico.product_qty as entrada, 0 as salida,vst_kardex_fisico.id  as stock_move,vst_kardex_fisico.guia as motivo_guia,vst_kardex_fisico.product_id,vst_kardex_fisico.estado from vst_kardex_fisico
join stock_move sm on sm.id = vst_kardex_fisico.id
join stock_picking sp on sm.picking_id = sp.id
join stock_location l_o on l_o.id = vst_kardex_fisico.location_id
join stock_location l_d on l_d.id = vst_kardex_fisico.location_dest_id
where ( (l_o.usage = 'internal' and l_o.usage = 'internal' and coalesce(sp.en_ruta,false) = false )  or ( l_o.usage != 'internal' or l_o.usage != 'internal' ) )
	union all
	select vst_kardex_fisico.date::date as fecha,vst_kardex_fisico.location_id as origen, vst_kardex_fisico.location_dest_id as destino, vst_kardex_fisico.location_id as almacen, 0 as entrada, vst_kardex_fisico.product_qty as salida,vst_kardex_fisico.id  as stock_move ,vst_kardex_fisico.guia as motivo_guia ,vst_kardex_fisico.product_id ,vst_kardex_fisico.estado from vst_kardex_fisico
) as vstf
inner join stock_location origen on origen.id = vstf.origen
inner join stock_location destino on destino.id = vstf.destino
inner join stock_location almacen on almacen.id = vstf.almacen
inner join product_product pp on pp.id = vstf.product_id
INNER JOIN ( SELECT pp.id,
               pt.name::text || COALESCE((' ('::text || string_agg(pav.name::text, ', '::text)) || ')'::text, ''::text) AS name
              FROM product_product pp
         JOIN product_template pt ON pt.id = pp.product_tmpl_id
    LEFT JOIN product_attribute_value_product_product_rel pavpp ON pavpp.product_product_id = pp.id
   LEFT JOIN product_attribute_value pav ON pav.id = pavpp.product_attribute_value_id
  GROUP BY pp.id, pt.name) product_name ON product_name.id = pp.id

inner join product_template pt on pt.id = pp.product_tmpl_id
inner join product_category pc on pc.id = pt.categ_id
inner join product_uom pu on pu.id = (CASE WHEN pt.unidad_kardex IS NOT NULL THEN pt.unidad_kardex else  pt.uom_id end )
inner join stock_move sm on sm.id = vstf.stock_move
inner join stock_picking sp on sp.id = sm.picking_id
left join purchase_order po on po.id = sp.po_id
where vstf.fecha >='""" +str(date_ini)+ """' and vstf.fecha <='""" +str(date_fin)+ """'
and vstf.product_id in """ +str(tuple(s_prod))+ """
and vstf.almacen in """ +str(tuple(s_loca))+ """
and vstf.estado = 'done'
and almacen.usage = 'internal'
order by
almacen.id,pp.id,vstf.fecha,vstf.entrada desc
			""")

			ingreso1= 0
			ingreso2= 0
			salida1= 0
			salida2= 0

			saldo = 0
			almacen = None
			producto = None
			for line in self.env.cr.fetchall():
				if almacen == None:
					almacen = (line[2] if line[2] else '')
					producto = (line[5] if line[5] else '')
					saldo = line[10] - line[11]
				elif almacen != (line[2] if line[2] else '') or producto != (line[5] if line[5] else ''):
					almacen = (line[2] if line[2] else '')
					producto = (line[5] if line[5] else '')
					saldo = line[10] - line[11]
				else:
					saldo = saldo + line[10] - line[11]

				scrap = self.env['glass.scrap.record'].search([('move_id','=',line[12])])

				worksheet.write(x,0,line[0] if line[0] else '' ,bord )
				worksheet.write(x,1,line[1] if line[1] else '' ,bord )
				worksheet.write(x,2,line[2] if line[2] else '' ,bord )
				worksheet.write(x,3,line[3] if line[3] else '' ,bord )
				worksheet.write(x,4,line[4] if line[4] else '' ,bord )
				worksheet.write(x,5,line[5] if line[5] else '' ,bord )
				worksheet.write(x,6,line[6] if line[6] else '' ,bord )
				worksheet.write(x,7,line[7] if line[7] else '' ,bord )
				worksheet.write(x,8,scrap.scrap_move_id.name or '',bord )
				worksheet.write(x,9,line[8] if line[8] else '' ,bord )
				worksheet.write(x,10,line[9] if line[9] else '' ,bord )
				worksheet.write(x,11,line[10] if line[10] else 0 ,numberdos )
				worksheet.write(x,12,line[11] if line[11] else 0 ,numberdos )
				worksheet.write(x,13,saldo ,numberdos )
				x = x +1

			tam_col = [11,11,5,5,7,5]+[11 for i in range(18)]
			alpha = list(string.ascii_uppercase)
			for i,item in enumerate(tam_col):
				worksheet.set_column(alpha[i]+':'+alpha[i],item)
			workbook.close()
			file = open(path, 'rb').read()
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file),	
			})
			return export.export_file(clear=True,path=path)