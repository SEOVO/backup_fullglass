# -*- encoding: utf-8 -*-
{
    'name': 'Kardex Fullglass',
    'version': '1.0',
    'author': 'ITGRUPO-POLIGLASS',
    'website': '',
    'category': 'account',
    'depends': ['kardex_it','export_file_manager_it'],
    'description': """KARDEX FULLGLASS""",
    'demo': [],
    'data': [],
    'auto_install': False,
    'installable': True
}
