# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class GlassListMainWizard(models.TransientModel):
	_inherit = 'glass.list.main.wizard'

	def print_labels(self):
		self.ensure_one()
		"""Impresión de etiquetas de servicios, esta vaina sólo es provisiconal hasta terminar el
		módulo de órdenes de servicios."""
		if self.search_param != 'glass_order' or not self.order_id:
			raise UserError(u'El tipo de búsqueda debe ser por Órden de producción')
		if not self.order_id:
			return
		self.build_list()
		lines = self.line_ids.filtered(lambda l: not l.glass_break and l.lot_line_id).mapped('order_line')
		if len(lines) != len(self.order_id.line_ids):
			raise UserError(u'Para poder imprimir etiquetas, todos los elementos de la órden de producción deben tener un lote de producción')
		return lines.print_labels()

