# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
import codecs,base64
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.units import inch, cm
from reportlab.pdfgen.canvas import Canvas
from reportlab.graphics.barcode import code128
from datetime import datetime

class GlassOrder(models.Model):
	_inherit = 'glass.order'

	def print_crystal_labels(self):
		return self.mapped('line_ids').print_labels()

class GlassOrderLine(models.Model):
	_inherit='glass.order.line'

	def print_labels(self):
		module = __name__.split('addons.')[1].split('.')[0]
		if not self.env.user.has_group('%s.group_print_product_labels'%module):
			raise UserError(u'Ud. No tiene autorización para realizar ésta acción.')
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
		pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))
		path = self.env['main.parameter'].search([])[0].dir_create_file
		now = fields.Datetime.context_timestamp(self,datetime.now())
		file_name = 'Etiquetas %s.pdf'%str(now)[:19].replace(':','_')
		path+=file_name
		separator = 10
		pos = 95
		pos_left = 8
		width_page,height_page = 7.62 * cm , 5.08 * cm # 216 144
		c = Canvas(path, pagesize=(width_page,height_page)) 
		#lines = self.filtered(lambda line: not line.glass_break)
		query = """
		SELECT gol.id, gfo.name
		FROM glass_order_line gol 
			JOIN glass_lot_line gll ON (gll.order_line_id = gol.id AND gll.id = gol.lot_line_id)
			JOIN glass_furnace_line_out gflo ON gflo.lot_line_id = gll.id
			JOIN glass_furnace_out gfo ON gfo.id = gflo.main_id
		WHERE gfo.state = 'done'
		AND (gll.is_break = false OR gll.is_break is NULL)
		AND gol.id IN %s """
		self._cr.execute(query,(tuple(self.ids),))
		furnace_info = dict(self._cr.fetchall())

		for line in self:
			c.setFont("Calibri", 10)
			c.drawString(pos_left,pos,'O/P' if line.order_id.type_sale == 'production' else 'O/S')
			c.setFont("Calibri-Bold", 9)
			c.drawString(pos_left+20,pos,line.order_id.name)
			c.setFont("Calibri-Bold", 11)
			measures = line.measures.replace('X',' X ')
			c.drawRightString(width_page-8,pos,measures)
			c.setFont("Calibri", 10)
			pos -= separator
			c.drawString(pos_left,pos,'Cristal Nro.')
			total_cr = len(line.order_id.line_ids)
			c.drawString(pos_left+50,pos,'%s/%d'%(line.crystal_number,total_cr))
			c.setFont("Calibri", 7)
			pos -= separator
			c.drawString(pos_left,pos,line.product_id.name or '')
			c.setFont("Calibri-Bold", 10)
			pos -= separator
			c.drawString(pos_left,pos,(line.order_id.partner_id.name or '')[:40])
			c.setFont("Calibri", 10)
			pos -= separator
			street = line.order_id.obra or line.order_id.partner_id.street or ''
			c.drawString(pos_left,pos,street[:40])
			pos -= separator+6
			width_rec = 180
			d1 = furnace_info.get(line.id,'')
			d2 = line.calc_line_id.polished_id.code or ''
			d3 = 'E' if line.calc_line_id.entalle else ''
			d4 = 'PL' if line.calc_line_id.template else ''
			d5 = 'EM' if line.calc_line_id.packed else ''
			d6 = '' # pendiente
			data = [d1,d2,d3,d4,d5,d6]
			c.setFont("Calibri", 9)
			inter = int(width_rec/len(data))
			aux = (width_page/2)-(width_rec/2)
			for item in data:
				c.drawCentredString(aux+inter/2,pos+3,item)
				c.rect(aux, pos, inter, 12, stroke=1, fill=0)
				aux+=inter
			pos -= separator
			if line.search_code:
				string = line.search_code.replace('.','')
				c.drawCentredString((width_page/2),pos,string)
				story=[]
				st = code128.Code128(string,barHeight=.32*inch,barWidth=1.2)
				story.append(st)
				st.drawOn(c,10,4)
			pos = 95
			c.showPage()
		c.setTitle(file_name)
		c.save()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),
				'content_type':'application/pdf'
			})
		return export.export_file(clear=True,path=path)
