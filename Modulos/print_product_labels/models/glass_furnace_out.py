# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class GlassFurnaceOut(models.Model):
	_inherit = 'glass.furnace.out'

	def furnace_out(self):
		res = super(GlassFurnaceOut,self).furnace_out()
		action = self.print_labels() # imprimir etiquetas al momento de validar las salidas de horno
		action.update(res)
		return action

	def print_labels(self):
		if self.filtered(lambda r: r.state != 'done'):
			raise UserError(u'Sólo se permite el etiquetado en lotes de horno finalizado.')
		lines = self.mapped('line_ids').filtered(lambda x: not x.etiqueta and not x.lot_line_id.is_break)
		if not lines:
			raise UserError(u'No hay cristales pendientes de etiquetado')
		glass_order_lines = lines.mapped('lot_line_id.order_line_id')
		action = glass_order_lines.print_labels()
		lines.write({'etiqueta':True})
		return action