# -*- encoding: utf-8 -*-
{
	'name': 'Etiquetado de Productos',
	'category': 'customization',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['glass_production_order','export_file_manager_it'],
	'version': '1.0.0',
	'description':"""
	
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'security/groups.xml',
		'report/report_views.xml',
		'views/glass_list_main_wizard.xml',
		'views/glass_order.xml',
		],
	'installable': True
}
