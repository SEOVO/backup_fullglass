# -*- coding: utf-8 -*-

from odoo import models, fields, api
from odoo.exceptions import UserError,ValidationError
from odoo.tools.float_utils import float_compare, float_round, float_is_zero

ROUNDING_M2 = 0.0001 # redondeo por defecto para unidades de medida de plancha

class ProductUom(models.Model):
	_inherit = 'product.uom'

	plancha = fields.Boolean('Plancha')
	ancho = fields.Float('Ancho (mm)')
	alto = fields.Float('Alto (mm)')

	# @api.onchange('plancha', 'ancho', 'alto')
	# def _onchange_paa(self):
	# 	if self.plancha and self.ancho and self.alto:
	# 		values = self._get_values_plancha_type(self.ancho, self.alto, self.rounding)
	# 		self.update(values)

	def recalculate_uom_plancha_factor(self):
		"""ajustar el factor en las unidades de medida de plancha"""
		for uom in self.filtered('plancha'):
			uom.with_context(prevent_check_plancha=True).write(self._get_values_plancha_type(uom.ancho, uom.alto, uom.rounding))

	@api.constrains('plancha', 'ancho', 'alto')
	def _verify_width_height(self):
		for uom in self.filtered('plancha'):
			if not (uom.ancho > 0 and uom.alto > 0):
				raise ValidationError(u'Las unidades marcadas como "plancha" deben tener un alto y ancho mayores a cero')
			if uom.ancho > uom.alto:
				raise ValidationError(u'En las unidades de "plancha" el alto debe ser mayor o igual al ancho.')

	@api.model
	def create(self, values):
		width  = values.get('ancho',0.0)
		height = values.get('alto',0.0)
		if width or height:
			values['plancha'] = True
			# Odoo bug
			#values.update(self._get_values_plancha_type(width, height, values.get('rounding', ROUNDING_M2)))
		uom = super(ProductUom, self).create(values)
		if not self._context.get('prevent_check_plancha'):
			uom.recalculate_uom_plancha_factor()
		return uom

	def write(self, values):
		if not self._context.get('prevent_check_plancha'):
			for uom in self:
				width  = values.get('ancho', uom.ancho or 0.0)
				height = values.get('alto', uom.alto or 0.0)
				if width or height:
					values['plancha'] = True
					values.update(self._get_values_plancha_type(width, height, values.get('rounding', uom.rounding)))
				uom.with_context(prevent_check_plancha=True).write(values)
		return super(ProductUom, self).write(values)

	@api.model
	def _get_values_plancha_type(self, width, height, rounding):
		width, height = float(width), float(height)
		rounding = min(rounding, ROUNDING_M2)
		if float_is_zero(width * height, precision_rounding=rounding):
			raise UserError(u'EL ancho y alto deben ser mayores a cero.')
		factor = 1.0 / ((width * height) / 1000000) # factor para conversión a M2
		compare = float_compare(factor, 1.0, precision_rounding=rounding)
		if compare == -1:
			uom_type = 'bigger'
		elif compare == 0:
			uom_type = 'reference'
		elif compare == 1:
			uom_type = 'smaller'
		return {
			'ancho': width,
			'alto': height,
			'name': ('%dx%dmm' % (int(width), int(height))).replace('1000x1000mm','M2'),
			'uom_type': uom_type,
			'factor': factor,
			'rounding': rounding,
		}