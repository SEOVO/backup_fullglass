# -*- coding: utf-8 -*-

from openerp import models, fields, api

class productos_exportacion(models.Model):
	_name = 'productos.exportacion'
	_auto = False

	@api.model_cr
	def init(self):
		self.env.cr.execute("""
      drop view if exists vst_final_atributos cascade; 
			CREATE VIEW "public"."vst_final_atributos" AS  SELECT pp.id,
    pt.name,
    (((product_attribute.name)::text || '-'::text) || (ppv.name)::text) AS namec,
    pp.default_code
   FROM ((((product_product pp
     JOIN product_template pt ON ((pt.id = pp.product_tmpl_id)))
     LEFT JOIN product_attribute_value_product_product_rel ppvpp ON ((ppvpp.product_product_id = pp.id)))
     LEFT JOIN product_attribute_value ppv ON ((ppv.id = ppvpp.product_attribute_value_id)))
     LEFT JOIN product_attribute ON ((ppv.attribute_id = product_attribute.id)))
  ORDER BY pp.id, ppv.id;

      drop view if exists vst_final_nombre_producto; 

CREATE VIEW "public"."vst_final_nombre_producto" AS  SELECT vst_final_atributos.id,
    ((((vst_final_atributos.name)::text || ' '::text) || string_agg(COALESCE(vst_final_atributos.namec, (''::character varying)::text), ','::text)) || ' '::text) AS nombrecompleto,
    vst_final_atributos.default_code
   FROM vst_final_atributos
  GROUP BY vst_final_atributos.id, vst_final_atributos.name, vst_final_atributos.default_code
  ORDER BY vst_final_atributos.id;


""")
	