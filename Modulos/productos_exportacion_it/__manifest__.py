# -*- encoding: utf-8 -*-
{
	'name': 'Exportar Productos',
	'category': 'account',
	'author': 'ITGRUPO-COMPATIBLE-BO',
	'depends': ['import_base_it'],
	'ITGRUPO_VERSION':2,
	'version': '1.0',
	'description':"""
	""",
	'auto_install': False,
	'demo': [],
	'data':	['wizard/account_purchase_register_wizard_view.xml'],
	'installable': True
}
