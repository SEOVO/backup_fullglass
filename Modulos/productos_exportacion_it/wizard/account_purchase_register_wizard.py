# -*- encoding: utf-8 -*-
from openerp.osv import osv
import base64
import codecs
from openerp import models, fields, api  , exceptions , _
from datetime import datetime, date, time, timedelta
from openerp.osv import osv
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.colors import magenta, red , black , blue, gray, Color, HexColor
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table
from reportlab.lib.units import  cm,mm
from reportlab.lib.utils import simpleSplit
from cgi import escape


from reportlab.lib import colors
from reportlab.lib.pagesizes import A4, inch, landscape
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle, Paragraph
from reportlab.lib.styles import getSampleStyleSheet

import time
from reportlab.lib.enums import TA_JUSTIFY,TA_CENTER
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.lib.units import inch


class exportacion_productos_wizard(osv.TransientModel):
	_name='exportacion.productos.wizard'


	@api.multi
	def do_rebuild(self):
		       	

		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		docname = 'productos.csv'
		#CSV
		sql_query = """	COPY (SELECT * FROM vst_final_nombre_producto )TO '"""+direccion+docname+"""'   WITH DELIMITER ',' CSV HEADER			
						"""
		
		self.env.cr.execute(sql_query)
		#Caracteres Especiales
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		f = open(direccion + docname, 'rb')			
		vals = {
			'output_name': docname,
			'output_file': base64.encodestring(''.join(f.readlines())),		
		}
		sfs_id = self.env['export.file.save'].create(vals)
		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

