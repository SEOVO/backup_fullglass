# -*- encoding: utf-8 -*-
import base64,string
from odoo import models, fields, api
from odoo.exceptions import UserError
from datetime import datetime,timedelta
from dateutil.relativedelta import relativedelta
from xlsxwriter.workbook import Workbook
from StringIO import StringIO
from itertools import groupby

class ReportRequisitionsOrdersWizard(models.TransientModel):
	_name='report.requisition.orders.wizard'

	search_param = fields.Selection([
		('requisition','Órdenes de Requisición'),
		('all_raw_materials','Todas las materias primas'),
		],string=u'Parámetro de búsqueda')
	location = fields.Selection([
		('all','Todas'),
		('prod_aqp','Arequipa'),
		('prod_lima','Lima')],string=u'Sede',default='all')
	start_date = fields.Date(string='Fecha Inicio',default=datetime.now().date().replace(day=1))
	end_date = fields.Date(string='Fecha Fin',default=datetime.now().date() + relativedelta(day=31))
	only_finished = fields.Boolean(u'Sólo Req. finalizadas')

	def do_rebuild(self):
		import sys
		reload(sys) 
		sys.setdefaultencoding('iso-8859-1')

		path = self.env['main.parameter'].search([])[0].dir_create_file
		now = fields.Datetime.context_timestamp(self,datetime.now())
		alpha = list(string.ascii_uppercase)
		file_name = u'Reporte de órdenes de requisición %s.xlsx'%str(now)[:19].replace(':','_')
		com = self.env['res.company']._company_default_get(self._name)
		path+=file_name
		workbook = Workbook(path)
		worksheet = workbook.add_worksheet("Hoja 1")
		worksheet.fit_to_pages(1, 0)  # Ajustar por Columna	
		bold = workbook.add_format({'bold': True})
		normal = workbook.add_format({'border':1})
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=1)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(11)
		boldbord.set_bg_color('#bfbfbf')
		numberfour = workbook.add_format({'num_format':'0.0000'})
		numberdos = workbook.add_format({'num_format':'0.00'})
		bord = workbook.add_format()
		bord.set_border(style=1)
		bord.set_text_wrap()
		numberdos.set_border(style=1)
		numberfour.set_border(style=1)
		title = workbook.add_format({'bold': True})
		title.set_align('center')
		title.set_align('vcenter')
		title.set_text_wrap()
		title.set_font_size(20)
		decimal4_bord_12 = workbook.add_format({'num_format':'0.0000','bold':True,'font_size':12})
		decimal4_bord_12.set_bottom(5)
		decimal4_bord_12.set_top(2)
		worksheet.set_row(0,30)

		fdatetime = workbook.add_format({'num_format': 'dd/mm/yyyy hh:mm','border':1})
		fdate = workbook.add_format({'num_format': 'dd/mm/yyyy','border':1})
		ftime = workbook.add_format({'num_format': 'hh:mm:ss','border':1})
		
		location = dict(self._fields['location'].selection).get(self.location)

		domain = [('date_order','>=',self.start_date),('date_order','<=',self.end_date)]
		if self.only_finished:
			domain.append(('state','=','done'))
		else:
			domain.append(('state','!=','cancel'))
		
		if self.location != 'all':
			domain.append(('location','=',self.location))
		
		requisitions = self.env['glass.requisition'].search(domain)

		if self.search_param == 'all_raw_materials':
			totals= [0,0,0,0,0,0]
			x = 7
			worksheet.merge_range(1,3,0,10, u"REPORTE DE ÓRDENES DE REQUISICIÓN (Materias Primas)",title)
			if com.logo:
				image_data = {'image_data':StringIO(base64.b64decode(com.logo)),'x_scale': 0.4, 'y_scale': 0.4}
				worksheet.insert_image('A1','fullglass.jpg',image_data)
			
			worksheet.write(2,3, u"FECHA INICIO:",bold)
			worksheet.write(2,4, self.start_date,bold)
			worksheet.write(3,3 , u"FECHA FIN",bold)
			worksheet.write(3,4, self.end_date,bold)
			worksheet.write(4,3 , u"SEDE",bold)
			worksheet.write(4,4,location,bold)
			
			worksheet.write(x,1, u"CÓDIGO",boldbord)
			worksheet.write(x,2, u"PRODUCTO",boldbord)
			worksheet.write(x,3, u"MEDIDA",boldbord)
			worksheet.write(x,4, u"UNIDAD MED. KARDEX",boldbord)
			worksheet.write(x,5, u"MATERIA PRIMA",boldbord)
			worksheet.write(x,6, u"M2 LOTE",boldbord)
			worksheet.write(x,7, u"M2 DEV.",boldbord)
			worksheet.write(x,8, u"CONSUMO",boldbord)
			worksheet.write(x,9, u"DESPERDICIO",boldbord)
			worksheet.write(x,10, u"% DESPERDICIO",boldbord)
			worksheet.write(x,11, u"MATERIA PRIMA VALORADO",boldbord)
			worksheet.write(x,12, u"M2 LOTE VALORADO",boldbord)
			worksheet.write(x,13, u"M2 DEV. VALORADO",boldbord)
			worksheet.write(x,14, u"CONSUMO VALORADO",boldbord)
			worksheet.write(x,15, u"DESPERDICIO VALORADO",boldbord)	
			x+=1
			
			dict_data = {}
			for r in requisitions:
				lines = r.move_mp_ids | r.move_srv_raw_ids
				lines2 = r.move_rt_ids
				lines3 = r.move_drt_ids

				moves = lines | lines2 | lines3
				prods = moves.mapped('product_id')
				req_area = r.required_area
				used     = r.used_area

				# TODO FIXME Mejorar ésta vaina, muy ineficiente

				for prod in prods:
					fil1 = lines.filtered(lambda x: x.product_id.id == prod.id)
					fil2 = lines2.filtered(lambda x: x.product_id.id == prod.id)
					fil3 = lines3.filtered(lambda x: x.product_id.id == prod.id)

					lines = (lines - fil1)
					lines2 = (lines2 - fil2)
					lines3 = (lines3 - fil3)

					area1 = sum([m.product_uom_qty / m.product_id.uom_id.factor for m in fil1])
					area2 = sum([m.product_uom_qty / m.product_uom.factor for m in fil2])
					area3 = sum([m.product_uom_qty / m.product_uom.factor for m in fil3])
					
					mp_req_prod = area1 + area2
					used_prod = mp_req_prod - area3
					pror = float(used_prod) / used * req_area # prorrateado
					
					if prod.id in dict_data.keys():
						dict_data[prod.id]['mp'] += mp_req_prod
						dict_data[prod.id]['m2lote'] += pror
						dict_data[prod.id]['m2dev'] += area3
						dict_data[prod.id]['used'] += used_prod
						dict_data[prod.id]['merma'] += (used_prod-pror)
					else:
						dict_data[prod.id] = {
							'name': prod.name,
							'code': prod.default_code or '',
							'uom': prod.uom_id.name or '',
							'uom_k':prod.unidad_kardex.name or '',
							'mp': mp_req_prod,
							'm2lote': pror,
							'm2dev': area3,
							'used': used_prod,
							'merma': used_prod-pror,
							'stand_price': prod.product_tmpl_id.standard_price or 0.0,
						}

			for key,value in dict_data.items():
				#filt = filter(lambda x: x['product_id'] == prod,data)
				worksheet.write(x,1,value['code'],bord)
				worksheet.write(x,2,value['name'],bord)
				worksheet.write(x,3,value['uom'],bord)
				worksheet.write(x,4,value['uom_k'],bord)

				raw_mat = value['mp']
				worksheet.write(x,5,raw_mat,numberfour)
				totals[0]+=raw_mat
				
				m2_lot = value['m2lote']
				worksheet.write(x,6,m2_lot,numberfour)
				totals[1]+=m2_lot
				
				m2_dev = value['m2dev']
				worksheet.write(x,7,m2_dev,numberfour)
				totals[2]+=m2_dev
				
				m2_used = value['used']
				worksheet.write(x,8,m2_used,numberfour)
				totals[3]+=m2_used
				
				m2_desp = value['merma']
				worksheet.write(x,9,m2_desp,numberfour)
				totals[4]+=m2_desp
				
				try:
					percent_desp = float(m2_desp)/m2_used * 100
				except ZeroDivisionError:
					percent_desp = 0.0
				worksheet.write(x,10,percent_desp,numberdos)
				stand_price = value['stand_price']
				
				worksheet.write(x,11,raw_mat * stand_price,numberfour)
				worksheet.write(x,12,m2_lot * stand_price,numberfour)
				worksheet.write(x,13,m2_dev * stand_price,numberfour)
				worksheet.write(x,14,m2_used * stand_price,numberfour)
				worksheet.write(x,15,m2_desp * stand_price,numberfour)
				x+=1
			
			worksheet.merge_range(x,1,x,4, "TOTAL GENERAL", bold)
			worksheet.write(x,5,totals[0],decimal4_bord_12)
			worksheet.write(x,6,totals[1],decimal4_bord_12)
			worksheet.write(x,7,totals[2],decimal4_bord_12)
			worksheet.write(x,8,totals[3],decimal4_bord_12)
			worksheet.write(x,9,totals[4],decimal4_bord_12)
			
			tam_col = [3,14,45,13,13] + 11 * [12]
			for i,item in enumerate(tam_col):
				worksheet.set_column(alpha[i]+':'+alpha[i],item)

		elif self.search_param == 'requisition':
			x = 7
			worksheet.merge_range(1,3,0,8, u"REPORTE DE ÓRDENES DE REQUISICIÓN",title)
			if com.logo:
				image_data = {'image_data':StringIO(base64.b64decode(com.logo)),'x_scale': 0.4, 'y_scale': 0.4}
				worksheet.insert_image('A1','fullglass.jpg',image_data)

			worksheet.write(2,3, u"FECHA INICIO:",bold)
			worksheet.write(2,4, self.start_date,bold)
			worksheet.write(3,3 , u"FECHA FIN",bold)
			worksheet.write(3,4, self.end_date,bold)
			worksheet.write(4,3 , u"SEDE",bold)
			worksheet.write(4,4,location,bold)
			
			worksheet.write(x,1, u"TIPO DE OPERACIÓN",boldbord)
			worksheet.write(x,2, u"NRO. ALMACÉN",boldbord)
			worksheet.write(x,3, u"NRO. REQUISICIÓN",boldbord)
			worksheet.write(x,4, u"ÍTEM",boldbord)
			worksheet.write(x,5, u"FECHA REQUISICIÓN",boldbord)
			worksheet.write(x,6, u"ESTADO",boldbord)
			worksheet.write(x,7, u"SEDE",boldbord)
			worksheet.write(x,8, u"NRO MESA",boldbord)
			worksheet.write(x,9, u"LOTES",boldbord)
			worksheet.write(x,10, u"PRODUCTO",boldbord)
			worksheet.write(x,11, u"UN. MED. PRODUCTO",boldbord)
			worksheet.write(x,12, u"UN. MED. KARDEX",boldbord)
			worksheet.write(x,13, u"UN. MED. RETAZO",boldbord)
			worksheet.write(x,14, u"CANT. PLANCHAS",boldbord)
			worksheet.write(x,15, u"CANTIDAD PEDIDA (M2)",boldbord)
			worksheet.write(x,16, u"DEVOLUCIÓN RETAZOS (M2)",boldbord)
			worksheet.write(x,17, u"CANTIDAD UTILIZADA (M2)",boldbord)
			x+=1
			
			dict_states = dict(self.env['glass.requisition']._fields['state'].selection)
			dict_locations = dict(self.env['glass.requisition']._fields['location'].selection)

			required = returned = used = 0.0
			for req in requisitions:
				
				date_order = fields.Date.from_string(req.date_order)
				state_order = dict_states.get(req.state,'')
				loc_order = dict_locations.get(req.location,'')
				lot_string = ', '.join(req.glass_lot_ids.mapped('name'))
				n_item = 1
				for move_mp in (req.move_mp_ids | req.move_srv_raw_ids):
					product1 = move_mp.product_id
					qty = move_mp.product_uom_qty
					worksheet.write(x,1,move_mp.picking_id.einvoice_12.name or '',bord)
					worksheet.write(x,2,move_mp.picking_id.name,bord)
					worksheet.write(x,3,req.name,bord)
					worksheet.write(x,4,str(n_item),bord)
					worksheet.write(x,5,date_order,fdate)
					worksheet.write(x,6,state_order,bold)
					worksheet.write(x,7,loc_order,bold)
					worksheet.write(x,8,req.table_number,bold)
					worksheet.write(x,9,lot_string,normal)
					worksheet.write(x,10,product1.name,bord)
					worksheet.write(x,11,product1.uom_id.name,bord)
					worksheet.write(x,12,product1.unidad_kardex.name or '',bord)
					worksheet.write(x,13,'',bord)
					area = qty / product1.uom_id.factor
					worksheet.write(x,14,qty,bord)
					worksheet.write(x,15,area,numberfour)
					worksheet.write(x,16,0.0,numberfour)
					worksheet.write(x,17,'',bord)
					x+=1
					required += area
					n_item+=1
					#pieces += qty

				for move_rt in req.move_rt_ids:
					product = move_rt.product_id
					qty = move_rt.product_uom_qty
					worksheet.write(x,1,move_rt.picking_id.einvoice_12.name,bord)
					worksheet.write(x,2,move_rt.picking_id.name,bord)
					worksheet.write(x,3,req.name,bord)
					worksheet.write(x,4,str(n_item),bord)
					worksheet.write(x,5,date_order,fdate)
					worksheet.write(x,6,state_order,bold)
					worksheet.write(x,7,loc_order,bold)
					worksheet.write(x,8,req.table_number,bold)
					worksheet.write(x,9,lot_string,normal)
					worksheet.write(x,10,product.name,bord)
					worksheet.write(x,11,product.uom_id.name,bord)
					worksheet.write(x,12,product.unidad_kardex.name or '',bord)
					worksheet.write(x,13,move_rt.product_uom.name,bord)
					area = qty / move_rt.product_uom.factor
					worksheet.write(x,14,qty,bord)
					worksheet.write(x,15,area,numberfour)
					worksheet.write(x,16,0.0,numberfour)
					worksheet.write(x,17,'',bord)
					x+=1
					required += area
					n_item+=1
					#pieces += qty

				for move_drt in req.move_drt_ids:
					product = move_drt.product_id
					qty = move_drt.product_uom_qty
					worksheet.write(x,1,move_drt.picking_id.einvoice_12.name,bord)
					worksheet.write(x,2,move_drt.picking_id.name,bord)
					worksheet.write(x,3,req.name,bord)
					worksheet.write(x,4,str(n_item),bord)
					worksheet.write(x,5,date_order,fdate)
					worksheet.write(x,6,state_order,bold)
					worksheet.write(x,7,loc_order,bold)
					worksheet.write(x,8,req.table_number,bold)
					worksheet.write(x,9,lot_string,normal)
					worksheet.write(x,10,product.name,bord)
					worksheet.write(x,11,product.uom_id.name,bord)
					worksheet.write(x,12,product.unidad_kardex.name or '',bord)
					worksheet.write(x,13,move_drt.product_uom.name,bord)
					area = qty / move_drt.product_uom.factor
					worksheet.write(x,14,-qty,bord)
					worksheet.write(x,15,0.0,numberfour)
					worksheet.write(x,16,-area,numberfour)
					worksheet.write(x,17,'',bord)
					x+=1
					returned += area
					n_item+=1
					#pieces -= qty

			worksheet.merge_range(x,12,x,14, "Totales", bold)
			#worksheet.write(x,6,'TOTAL '+req.table_number,bold)
			#worksheet.write(x,11,pieces,numberfour)
			worksheet.write(x,15,required,decimal4_bord_12)
			worksheet.write(x,16,-returned,decimal4_bord_12)
			worksheet.write(x,17,required-returned,decimal4_bord_12)
			#x=x+2

			tam_col = [3,31,15,12,5,12,12,14,10,15,47,13,13,13] + 4*[14]
			for i,item in enumerate(tam_col):
				worksheet.set_column(alpha[i]+':'+alpha[i],item)
			
		workbook.close()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path)

