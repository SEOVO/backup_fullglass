# -*- encoding: utf-8 -*-
{
    'name': u'Reporte de Órdenes de Requisición',
    'category': 'report',
    'author': 'ITGRUPO-POLIGLASS',
    'depends': ['glass_production_order','export_file_manager_it'],
    'version': '1.0',
    'description':"""
        Módulo para emitir el reporte de Ordenes de Requisición en Fullglass
    """,
    'auto_install': False,
    'demo': [],
    'data':[
        'wizard/report_of_requisitions_orders_view.xml',
        ],
    'installable': True
}