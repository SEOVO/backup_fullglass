# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError
	
class CollectionReportUserLine(models.Model):
	_name = 'collection.report.config.line'
	_order = 'name'

	name = fields.Char('Nombre', default='/', required=True, copy=False)
	user_id = fields.Many2one('res.users', string=u'Usuario', required=True, default=lambda self: self.env.uid)
	journal_ids = fields.Many2many('account.journal', string=u'Diarios', copy=True)
	invoice_serie_ids = fields.Many2many('it.invoice.serie', string='Series', copy=True)
	journal_initial_id = fields.Many2one('account.journal', string='Diario de Saldos Iniciales')
	
	@api.constrains('name', 'user_id')
	def _verify_uniq_line_by_user(self):
		for line in self:
			if self.search_count([('user_id', '=', line.user_id.id), ('name', '!=', '/'), ('name', '=', line.name)]) > 1:
				raise UserError(u'Ya existe una configuración con el nombre "%s" para el usuario %s.' % (line.name, line.user_id.name))

	@api.model
	def create(self, values):
		values['name'] = (values.get('name') or '').strip()
		return super(CollectionReportUserLine, self).create(values)

	def write(self, values):
		if 'name' in values:
			values['name'] = values['name'].strip()
		return super(CollectionReportUserLine, self).write(values)


	@api.depends('name', 'user_id')
	def name_get(self):
		result = []
		for record in self:
			name = '%s - %s' % (record.name, record.user_id.name)
			result.append((record.id, name))
		return result
