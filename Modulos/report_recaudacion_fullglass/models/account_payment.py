# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from datetime import datetime


class AccountPayment(models.Model):
	_inherit = 'account.payment'

	cashbox_date = fields.Date('Fecha de Caja', default=datetime.today())

	@api.model
	def get_payment_vals(self):
		res = super(AccountPayment, self).get_payment_vals()
		res['cashbox_date'] = self.cashbox_date
		return res