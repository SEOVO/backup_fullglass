# -*- encoding: utf-8 -*-
import base64,io,string,decimal,os
from odoo import models, fields, api
from xlsxwriter.workbook import Workbook
from datetime import datetime,timedelta
from odoo.exceptions import UserError
from itertools import chain

class CollectionReportWizard(models.TransientModel):
	_name='collection.report.wizard'

	def _get_conf_line(self):
		line = self.env['collection.report.config.line'].search([('user_id', '=', self.env.uid)], limit=1)
		if not line:
			raise UserError(u'No se ha encontrado una configuración para el usuario %s.' % self.env.user.name)
		return line.id

	date_report = fields.Date(default=datetime.now().date())
	conf_line_id = fields.Many2one('collection.report.config.line', 
		string='Configuración', domain="[('user_id', '=', uid)]", default=_get_conf_line)
	journal_ids = fields.Many2many(related='conf_line_id.journal_ids', string=u'Diarios', readonly=True)
	invoice_serie_ids = fields.Many2many(related='conf_line_id.invoice_serie_ids', string=u'Series de factura', readonly=True)

	def build_report_excel(self):
		conf_line = self.conf_line_id
		journals = self.journal_ids
		inv_series = self.invoice_serie_ids.ids
		journal_si = conf_line.journal_initial_id

		if not journals:
			raise UserError(u'No ha establecido los diarios para éste reporte en sus parámetros de configuración')
		if not inv_series:
			raise UserError(u'No ha establecido las secuencias para éste reporte en sus parámetros de configuración')
		if not journal_si:
			raise UserError(u'No ha establecido el diario de Saldo Iniciales en sus parámetros de configuración')

		accounts = journals.mapped('default_debit_account_id').ids
		if not accounts:
			raise UserError(u'No ha establecido cuentas para los diarios configurados')
				
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		path = self.env['main.parameter'].search([],limit=1).dir_create_file
		file_name = u'Reporte de recaudación %s.xlsx'%(self.date_report)
		path+=file_name
		com = self.env['res.company']._company_default_get(self._name)
		workbook = Workbook(path)
		worksheet = workbook.add_worksheet("Reporte")
		
		bold = workbook.add_format({'bold': True,'font_size':11})
		bold14 = workbook.add_format({'bold': True,'font_size':14,'align':'center'})
		bold_g = workbook.add_format({'bold': True,'font_size':11})
		bold_g.set_bg_color('#95e79f')
		bold_r = workbook.add_format({'bold': True,'font_size':11})
		bold_r.set_bg_color('#ffb3b3')
		normal = workbook.add_format({'text_wrap':True,'font_size':11})
		normal2 = workbook.add_format({'font_size':11})

		title = workbook.add_format({'bold': True})
		title.set_align('center')
		title.set_align('vcenter')
		title.set_text_wrap()
		title.set_font_size(20)

		title_in = workbook.add_format({'bold':True,'font_size':11,'border':1}) # ingresos
		title_in.set_text_wrap()
		title_in.set_align('center')
		title_in.set_align('vcenter')
		title_in.set_bg_color('#def0c8')
		
		title_out = workbook.add_format({'bold':True,'font_size':11,'border':1}) # ingresos
		title_out.set_text_wrap()
		title_out.set_align('center')
		title_out.set_align('vcenter')
		title_out.set_bg_color('#f4ddbe')

		title_tot = workbook.add_format({'bold':True,'font_size':11,'border':1}) # ingresos
		title_tot.set_align('center')
		title_tot.set_align('vcenter')
		title_tot.set_bg_color('#95b3d7')

		title2 = workbook.add_format({'bold':True,'font_size':11}) # plomito
		title2.set_bg_color('#a6a6a6')

		title3 = workbook.add_format({'font_size':11,'text_wrap':True,'border':1})
		title3.set_align('center')
		title3.set_align('vcenter')
		title3.set_bg_color('#d9d9d9')

		decimal2 = workbook.add_format({'num_format':'0.00'})
		decimal2_bold = workbook.add_format({'num_format':'0.00','bold':True,'bg_color':'#a6a6a6'})
		decimal2_bold2 = workbook.add_format({'num_format':'0.00','bold':True,'bg_color':'#d9d9d9'})
		decimal2_bold_16 = workbook.add_format({'num_format':'0.00','bold':True,'font_size':16})
		decimal2_bord_16 = workbook.add_format({'num_format':'0.00','bold':True,'font_size':16})
		decimal2_bord_16.set_bottom(5)
		decimal2_bord_16.set_top(2)		

		usd2 = workbook.add_format({'num_format':'"$"#,##0.00'})
		usd2_bold = workbook.add_format({'num_format':'"$"#,##0.00','bold':True,'bg_color':'#a6a6a6'})
		usd2_bold2 = workbook.add_format({'num_format':'"$"#,##0.00','bold':True,'bg_color':'#d9d9d9'})
		usd2_bold_16 = workbook.add_format({'num_format':'"$"#,##0.00','bold':True,'font_size':16})
		usd2_bord_16 = workbook.add_format({'num_format':'"$"#,##0.00','bold':True,'font_size':16})
		usd2_bord_16.set_bottom(5)
		usd2_bord_16.set_top(2)

		pen2 = workbook.add_format({'num_format':'"S/."#,##0.00'})
		pen2_negative = workbook.add_format({'num_format':'"S/."#,##0.00', 'bg_color': '#f8dcdc'})
		pen2_bold = workbook.add_format({'num_format':'"S/."#,##0.00','bold':True,'bg_color':'#a6a6a6'})
		pen2_bold2 = workbook.add_format({'num_format':'"S/."#,##0.00','bold':True,'bg_color':'#d9d9d9'})
		pen2_bold_16 = workbook.add_format({'num_format':'"S/."#,##0.00','bold':True,'font_size':16})
		pen2_bord_16 = workbook.add_format({'num_format':'"S/."#,##0.00','bold':True,'font_size':16})
		pen2_bord_16.set_bottom(5)
		pen2_bord_16.set_top(2)

		x = 9
		# getting data:
		params = 3*(self.date_report,)+(tuple(journals.ids,),tuple(inv_series,),self.date_report,tuple(journals.ids,),journal_si.id,)
		query2 = """
		SELECT * FROM ((
			SELECT
			aj.name AS journal_name,
			COALESCE(emp.name, 'NO DEFINIDO') AS pay_method,
			ai.date_invoice AS date_document,
			eic.name AS type_document,
			ai.number AS num_document,
			rp.name AS partner,
			CASE WHEN rc.name = 'USD' THEN ap.amount ELSE 0.0 END AS amount_usd,
			CASE WHEN rc.name = 'USD' THEN ap.change_type ELSE 0.0 END AS tc,
			CASE WHEN rc.name = 'PEN' THEN ap.amount END AS amount_pen,
			CASE WHEN ai.date_invoice < %s THEN 'cobranza' ELSE 'venta_dia' END AS type_in,
			ap.nro_caja AS cashbox_op,
			ap.cashbox_date
			FROM
			account_payment ap
			JOIN einvoice_catalog_01 eic ON eic.id = ap.it_type_document
			JOIN account_invoice ai ON ai.number = ap.nro_comprobante AND ai.it_type_document = eic.id
			LEFT JOIN account_journal aj ON aj.id = ap.journal_id
			LEFT JOIN einvoice_means_payment emp ON emp.id = ap.means_payment_id
			LEFT JOIN res_partner rp ON rp.id = ap.partner_id
			LEFT JOIN res_currency rc ON rc.id = ap.currency_id
			WHERE
			ap.state != 'draft'
			AND ap.cashbox_date = %s
			AND ai.date_invoice <= %s
			AND ap.payment_type = 'inbound'
			AND ap.journal_id IN %s
			AND ai.serie_id IN %s
			)
		UNION ALL
		(
			SELECT
			aj.name AS journal_name,
			COALESCE(emp.name, 'NO DEFINIDO') AS pay_method,
			ai.date_invoice AS date_document,
			eic.name AS type_document,
			ai.number AS num_document,
			rp.name AS partner,
			CASE WHEN rc.name = 'USD' THEN ap.amount ELSE 0.0 END AS amount_usd,
			CASE WHEN rc.name = 'USD' THEN ap.change_type ELSE 0.0 END AS tc,
			CASE WHEN rc.name = 'PEN' THEN ap.amount END AS amount_pen,
			'cobranza' AS type_in,
			ap.nro_caja AS cashbox_op,
			ap.cashbox_date
			FROM
			account_payment ap
			JOIN einvoice_catalog_01 eic ON eic.id = ap.it_type_document
			JOIN account_invoice ai ON ai.number = ap.nro_comprobante AND ai.it_type_document = eic.id
			LEFT JOIN account_journal aj ON aj.id = ap.journal_id
			LEFT JOIN einvoice_means_payment emp ON emp.id = ap.means_payment_id
			LEFT JOIN res_partner rp ON rp.id = ap.partner_id
			LEFT JOIN res_currency rc ON rc.id = ap.currency_id
			JOIN account_invoice_line ail ON ail.invoice_id = ai.id
			WHERE
			ap.state != 'draft'
			AND ap.cashbox_date = %s
			AND ap.payment_type = 'inbound'
			AND ap.journal_id IN %s
			AND ai.journal_id = %s
			AND ail.name = 'SALDOS INICIALES'
		))T
		ORDER BY num_document"""
		
		self._cr.execute(query2, params)
		income_res = self._cr.dictfetchall()
				
		if com.logo:
			from StringIO import StringIO
			image_data = {'image_data':StringIO(base64.b64decode(com.logo)),'x_scale': 0.4, 'y_scale': 0.4}
			worksheet.insert_image('B1','fullglass.jpg',image_data)

		worksheet.merge_range(0,3,0,5,com.name, title)
		worksheet.merge_range(2,3,2,5,u"PLANILLA DE RECAUDACIÓN", bold14)
		
		worksheet.write(4,1, u"FECHA EMISIÓN:", bold)
		worksheet.merge_range(4,2,4,4,self.date_report, normal)
		worksheet.write(5,1, "DIARIOS:", bold)
		worksheet.merge_range(5,2,5,4,', '.join(conf_line.journal_ids.mapped('name')),normal)
		worksheet.write(6,1,'USUARIO:', bold)
		worksheet.merge_range(6,2,6,4,self.env.user.name,normal)

		def push_amount(dict_obj, journal, method, amount):
			if journal not in dict_obj:
				dict_obj[journal] = {}

			if method not in dict_obj[journal]:
				dict_obj[journal][method] = 0.0
			
			dict_obj[journal][method] += amount
			return dict_obj

		# totals 
		income_usd_total = income_pen_total = 0.0
		expense_usd_total = expense_pen_total = 0.0

		summary_dict = {} # dict para el pinche cuadrito

		# === INGRESOS ===
		worksheet.merge_range(x,1,x,10,u"INGRESOS",title_in)
		worksheet.set_row(x,30)
		x+=1
		worksheet.merge_range(x,1,x,10,u"VENTAS DEL DÍA",title2)
		x+=1
		worksheet.write(x,1, u"FECHA DE DOCUMENTO",title3)
		worksheet.write(x,2, u"TIPO DE DOCUMENTO",title3)
		worksheet.write(x,3, u"NRO DE DOCUMENTO",title3)
		worksheet.write(x,4, u"DIARIO DE PAGO",title3)
		worksheet.write(x,5, u"MEDIO DE PAGO",title3)
		worksheet.write(x,6, u"PARTNER",title3)
		worksheet.write(x,7, u"NRO DE OP. CAJA",title3)
		worksheet.write(x,8, u"VALOR DÓLARES",title3)
		worksheet.write(x,9, u"T.C.",title3)
		worksheet.write(x,10, u"VALOR SOLES",title3)
		worksheet.set_row(x,43)
		x+=1
		sales_today = list(filter(lambda x: x['type_in']=='venta_dia',income_res))
		for item in sales_today:
			amount_usd = item['amount_usd'] or 0.0
			amount_pen = item['amount_pen'] or 0.0
			tc = item['tc'] or 0.0
			journal_name = item['journal_name']
			pay_method = item['pay_method']

			summary_dict = push_amount(summary_dict, journal_name, pay_method, amount_usd * tc if amount_usd > 0.0 else amount_pen)
			
			worksheet.write(x,1,item['date_document'] or '',normal)
			worksheet.write(x,2,item['type_document'] or '',normal2)
			worksheet.write(x,3,item['num_document'] or '',normal)
			worksheet.write(x,4,journal_name,normal)
			worksheet.write(x,5,pay_method,normal)
			worksheet.write(x,6,item['partner'] or '',normal2)
			worksheet.write(x,7,item['cashbox_op'] or '',normal)
			worksheet.write(x,8,amount_usd,usd2)
			worksheet.write(x,9,tc or '',decimal2)
			worksheet.write(x,10,amount_pen,pen2)
			income_usd_total += amount_usd
			income_pen_total += amount_pen
			x+=1

		worksheet.merge_range(x,1,x,10,u"INGRESOS POR COBRANZAS",title2)
		x+=1
		worksheet.write(x,1, u"FECHA DE DOCUMENTO",title3)
		worksheet.write(x,2, u"TIPO DE DOCUMENTO",title3)
		worksheet.write(x,3, u"NRO DE DOCUMENTO",title3)
		worksheet.write(x,4, u"DIARIO DE PAGO",title3)
		worksheet.write(x,5, u"MEDIO DE PAGO",title3)
		worksheet.write(x,6, u"PARTNER",title3)
		worksheet.write(x,7, u"NRO DE OP. CAJA",title3)
		worksheet.write(x,8, u"VALOR DÓLARES",title3)
		worksheet.write(x,9, u"TC",title3)
		worksheet.write(x,10, u"VALOR SOLES",title3)
		worksheet.set_row(x,43)
		x+=1
		for item in list(filter(lambda x: x['type_in']=='cobranza',income_res)):
			amount_usd = item['amount_usd'] or 0.0
			amount_pen = item['amount_pen'] or 0.0
			tc = item['tc'] or 0.0
			journal_name = item['journal_name']
			pay_method = item['pay_method']

			summary_dict = push_amount(summary_dict, journal_name, pay_method, amount_usd * tc if amount_usd > 0.0 else amount_pen)

			worksheet.write(x,1,item['date_document'] or '',normal)
			worksheet.write(x,2,item['type_document'] or '',normal2)
			worksheet.write(x,3,item['num_document'] or '',normal)
			worksheet.write(x,4,journal_name,normal)
			worksheet.write(x,5,pay_method,normal)
			worksheet.write(x,6,item['partner'] or '',normal2)
			worksheet.write(x,7,item['cashbox_op'] or '',normal)
			worksheet.write(x,8,amount_usd,usd2)
			worksheet.write(x,9,tc or '',decimal2)
			worksheet.write(x,10,amount_pen,pen2)
			
			income_usd_total += amount_usd
			income_pen_total += amount_pen
			x+=1

		x+=2
		# parte final
		total_usd = income_usd_total
		total_pen = income_pen_total
		
		worksheet.merge_range(x,1,x,7,'SALDO DEL DIA', title_tot)
		worksheet.write(x,8,total_usd,usd2_bord_16)
		worksheet.write(x,10,total_pen,pen2_bord_16)
		#x += 3

		tam_col = [3,13,18,18,22,20,45,21,18,8,18]
		alpha = list(string.ascii_uppercase)
		for i,item in enumerate(tam_col):
			worksheet.set_column(alpha[i]+':'+alpha[i],item)

		# CUADRO COMPARATIVO
		x = 2
		worksheet = workbook.add_worksheet("Resumen")
		worksheet.merge_range(x,1,x,7,'CUADRO RESUMEN POR DIARIO Y MEDIO DE PAGO',bold14)
		worksheet.set_row(x, 30)
		x += 2

		y = 2
		aux_x = x
		pay_methods   = set(chain(*[pm.keys() for pm in summary_dict.values()]))
		totals = []
		for key, value in summary_dict.items():
			worksheet.write(aux_x, y, key, title_in)
			aux_x+=1
			jr_acum = 0.0
			sub_totals = []
			for pay_m in pay_methods:
				amount = value.get(pay_m, 0.0)
				if y == 2:
					worksheet.write(aux_x, 1, pay_m, title_out)
				worksheet.write(aux_x, y, amount, pen2 if amount >= 0.0 else pen2_negative) # XD
				sub_totals.append(amount)
				aux_x+=1
			totals.append(sub_totals)
			aux_x = x
			y+=1
		
		#x = x + len(pay_methods) + 1
		
		totals_x = map(lambda x: sum(x), totals)
		totals_y = map(lambda y: sum(y), zip(*totals))
		
		worksheet.write(x, y, 'ACUMULADO', title_tot)
		worksheet.set_row(x, 30)
		#y = 2
		x+=1
		for tot in totals_y:
			worksheet.write(x, y, tot, pen2_bold)
			worksheet.set_row(x, 30)
			x+=1

		worksheet.write(x, y, sum(totals_y), pen2_bord_16) # total general puede ser de x o y
		worksheet.set_row(x, 30)

		y = 2
		worksheet.write(x, 1, 'TOTALES', title_tot)
		for tot in totals_x:
			worksheet.write(x, y, tot, pen2_bold)
			y+=1


		# Set cols widths
		tam_col = [3] + 10 * [20]
		alpha = list(string.ascii_uppercase)
		for i,item in enumerate(tam_col):
			worksheet.set_column(alpha[i]+':'+alpha[i],item)
		
		workbook.close()

		with open(path, 'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),
			})
		return export.export_file(clear=True, path=path)