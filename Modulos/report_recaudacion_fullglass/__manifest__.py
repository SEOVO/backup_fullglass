# -*- encoding: utf-8 -*-
{
	'name': 'Reporte de Recaudación - Fullglass',
	'category': 'account',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['export_file_manager_it', 'account_payment_it', 'account_invoice_series_it'],
	'version': '1.0',
	'description':"""
		Reporte de recaudación para Fullglass
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'security/groups.xml',
		'security/ir.model.access.csv',
		'wizard/collection_report_wizard.xml',
		'views/collection_report_config.xml',
		'views/account_payment.xml',
		],
	'installable': True
}
