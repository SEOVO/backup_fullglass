# -*- encoding: utf-8 -*-
import base64,codecs,pprint,math,string
from cgi import escape
import decimal
from odoo.exceptions import UserError
from odoo import api, fields, models, _, tools
from datetime import datetime

class ReporteWizard(models.TransientModel):
	_name = 'reporte.wizard'

	start_date = fields.Date(default=lambda self: fields.Date.context_today(self),string="Fecha Inicio : ") 
	end_date = fields.Date(default=lambda self: fields.Date.context_today(self),string="Fecha Fin : ")
	stage_id = fields.Many2one('glass.stage',string='Etapa',domain=[('name','in',('optimizado','lavado','corte','pulido','templado','producido','entalle','ingresado','entregado'))])
	show_all = fields.Boolean('Mostrar todos')

	def do_rebuild(self):
		ext_sql = " and gs.name = '%s'" % self.stage_id.name if not self.show_all and self.stage_id else ''
		if self.start_date and self.end_date:
			ext_sql+=" and gsr.date::date <= '%s' and  gsr.date::date >= '%s'"%(self.end_date,self.start_date)
		query = """
		SELECT  
		gor.name AS op,
		glt.nro_cristal,
		glot.name AS lote,
		gfo.name AS lote_horno,
		glt.base1,
		glt.base2,
		glt.altura1,
		glt.altura2,
		scpl.entalle,
		--gsr.date as date,
		(gsr.date-interval '5' hour) AS date,
		rp.nro_documento,
		rp.name AS partner,
		gor.obra,
		gor.date_delivery,
		null AS observacion,
		CASE WHEN (glt.producido is null or glt.producido=false) THEN 'En Proceso' ELSE 'Producido' END AS estado,
		1 AS cant,
		glt.area,
		ROUND(pt.weight * glt.area,4) AS peso,
		rp2.name AS user,
		pt.id AS id_prod,
		pt.default_code,
		patv.name AS presentacion,
		pt.name AS producto,
		gs.name AS stage,
		glt.corte AS corte,
		glt.pulido AS pulido,
		glt.lavado AS lavado,
		glt.entalle AS entalle2,
		glt.templado AS templado
		FROM glass_stage_record gsr
		JOIN glass_stage gs ON gs.id = gsr.stage_id
		JOIN glass_lot_line glt ON gsr.lot_line_id = glt.id
		JOIN glass_order_line gol ON glt.order_line_id = gol.id
		JOIN glass_order gor ON gol.order_id = gor.id
		JOIN glass_lot glot ON glt.lot_id = glot.id
		JOIN sale_order so ON gor.sale_order_id = so.id
		JOIN res_partner rp ON so.partner_id = rp.id
		JOIN product_product pp ON glt.product_id = pp.id
		JOIN product_template pt ON pp.product_tmpl_id = pt.id
		JOIN product_selecionable psel ON psel.product_id = pt.id AND psel.atributo_id = 4 -- Atributo Espesor
		JOIN product_atributo_valores patv ON psel.valor_id = patv.id AND psel.atributo_id = patv.atributo_id
		JOIN glass_sale_calculator_line scpl ON scpl.id = glt.calc_line_id
		JOIN res_users rus ON rus.id = gsr.user_id
		JOIN res_partner rp2 ON rp2.id = rus.partner_id
		LEFT JOIN glass_furnace_line_out gflo ON gflo.lot_line_id = glt.id
		LEFT JOIN glass_furnace_out gfo ON gfo.id = gflo.main_id
		WHERE gsr.done = true 
		AND glt.active = true
		%s 
		ORDER BY gsr.stage, op, nro_cristal """ % ext_sql

		from xlsxwriter.workbook import Workbook
		path = self.env['main.parameter'].search([])[0].dir_create_file
		file_name = 'Reporte de Procesos.xlsx'
		path+=file_name
		workbook = Workbook(path)
		## PRIMERA HOJA
		worksheet = workbook.add_worksheet("DETALLE")
		#Print Format
		worksheet.set_landscape() #Horizontal
		worksheet.set_paper(9) #A-4
		worksheet.set_margins(left=0.75, right=0.75, top=1, bottom=1)
		worksheet.fit_to_pages(1, 0)  # Ajustar por Columna 

		bold = workbook.add_format({'bold': False})
		bold.set_align('center')
		bold.set_align('vcenter')

		normal = workbook.add_format()
		boldbord = workbook.add_format({'bold': True})
		boldbord.set_border(style=2)
		boldbord.set_align('center')
		boldbord.set_align('vcenter')
		boldbord.set_text_wrap()
		boldbord.set_font_size(9)
		boldbord.set_bg_color('#DCE6F1')
		bold2 = workbook.add_format({'bold': False})
		bold2.set_align('center')
		bold2.set_align('vcenter')
		bold2.set_bg_color('#E9EFF6')
		bold7 = workbook.add_format({'num_format':'0.0000'})
		bold7.set_bg_color('#E9EFF6')
		bold8 = workbook.add_format({'num_format':'0.0000'})
		bold4 = workbook.add_format({'bold': False})
		bold4.set_align('left')
		bold4.set_align('vcenter')
		bold4.set_bg_color('#E9EFF6')
		bold3 = workbook.add_format({'bold': False})
		bold3.set_align('left')
		bold3.set_align('vcenter')
		bold5 = workbook.add_format({'bold': True})
		bold5.set_border(style=2)
		bold5.set_align('center')
		bold5.set_align('vcenter')
		bold6 = workbook.add_format({'bold': True})
		bold6.set_border(style=2)
		bold6.set_align('right')
		bold6.set_align('vcenter')
		# bottom
		bold9 = workbook.add_format({'bold': True})
		bold9.set_border(style=2)
		bold9.set_align('center')
		numbertres = workbook.add_format({'num_format':'0.000'})
		numberdos = workbook.add_format({'num_format':'0.00'})
		numberdosbord = workbook.add_format({'num_format':'0.00','bold': True})
		numberdosbord.set_border(style=1)
		bord = workbook.add_format()
		bord.set_border(style=1)
		bord.set_text_wrap()
		bord.set_align('center')
		bord.set_align('vcenter')
		numberdos.set_border(style=1)
		numbertres.set_border(style=1)  
		title = workbook.add_format({'bold': True})
		title.set_align('center')
		title.set_align('vcenter')
		title.set_text_wrap()
		title.set_font_size(20)
		worksheet.set_row(0, 30)
		saldo_inicial = 0
		saldo_tmp = 0 ## saldo acumulativo
				 
		tam_col = [0,0,0,0]
		tam_letra = 1.2
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')    

		worksheet.merge_range(0,0,0,12, u"REPORTE DE PROCESOS", title)
		
		worksheet.write(2,0, u"Orden de Producción", boldbord)
		worksheet.write(2,1, u"Número de Cristal", boldbord)
		worksheet.write(2,2, u"Lote", boldbord)
		worksheet.write(2,3, u"Lote Horno", boldbord)
		worksheet.write(2,4, u"Base 1", boldbord)
		worksheet.write(2,5, u"Base 2", boldbord)
		worksheet.write(2,6, u"Altura 1", boldbord)
		worksheet.write(2,7, u"Altura 2", boldbord)
		worksheet.write(2,8, u"Entalle", boldbord)
		worksheet.write(2,9, u"Fecha", boldbord)
		worksheet.write(2,10, u"Hora", boldbord)
		worksheet.write(2,11, u"Nro Documento", boldbord)
		worksheet.write(2,12, u"Cliente", boldbord)
		worksheet.write(2,13, u"Obra", boldbord)
		worksheet.write(2,14, u"Fecha de entrega", boldbord)
		worksheet.write(2,15, u"Observación", boldbord)
		worksheet.write(2,16, u"Estado", boldbord)
		worksheet.write(2,17, u"Ocurrencia", boldbord)
		worksheet.write(2,18, u"Area(M2)", boldbord)
		worksheet.write(2,19, u"Peso", boldbord)
		worksheet.write(2,20, u"Usuario Responsable", boldbord)
		worksheet.write(2,21, u"Costo", boldbord)
		worksheet.write(2,22, u"Código", boldbord)
		worksheet.write(2,23, u"Presentación", boldbord)
		worksheet.write(2,24, u"Producto", boldbord)
		worksheet.write(2,25, u"Etapa", boldbord)
		x=3
		aux = x
		sum_cantidad = 0
		sum_area = 0 
		self._cr.execute(query)
		for line in self._cr.dictfetchall():
			style1 = bold
			style2 = bold8
			if x % 2 == 0:
				style1 = bold2
				style2 = bold7
			worksheet.write(x,0,line['op'],style1) # op
			worksheet.write(x,1,line['nro_cristal'],style1) # nro cristal
			worksheet.write(x,2,line['lote'],style1) # lote
			worksheet.write(x,3,line['lote_horno'],style1) # lote de horno
			worksheet.write(x,4,line['base1'],style1) # base1
			worksheet.write(x,5,line['base2'],style1) # base2
			worksheet.write(x,6,line['altura1'],style1) # alt 1
			worksheet.write(x,7,line['altura2'],style1) # alt 2
			worksheet.write(x,8,line['entalle'],style1) # entalle
			str_date = line['date'][:10]
			str_time = line['date'][11:19]
			worksheet.write(x,9,str_date,style1) # fecha
			worksheet.write(x,10,str_time,style1) # hora
			worksheet.write(x,11,line['nro_documento'],style1) # nro doc
			worksheet.write(x,12,line['partner'],style1) # clinte
			worksheet.write(x,13,line['obra'],style1) # obra
			worksheet.write(x,14,line['date_delivery'],style1) # fec entrega
			worksheet.write(x,15,line['observacion'],style2) # observ
			worksheet.write(x,16,line['estado'],style1) # estado
			worksheet.write(x,17,line['cant'],style1) # ocurrencia
			worksheet.write(x,18,line['area'],style2) # area
			worksheet.write(x,19,line['peso'],style2) # peso
			worksheet.write(x,20,line['user'],style1) # usuario
			
			#pt = self.env['product.template'].browse(int(line['id_prod']))
			#worksheet.write(x,20,pt.standard_price * line['costo'] if pt.standard_price else 0,style2) # costo
			worksheet.write(x,21,0.0,style2) # costo???

			worksheet.write(x,22,line['default_code'],style1) # codigo
			worksheet.write(x,23,line['presentacion'],style1) # presentacion
			worksheet.write(x,24,line['producto'],style1) # nom prd
			worksheet.write(x,25,line['stage'],style1) # stage
			sum_area += float(line['area'])
			x+=1

		worksheet.write(x,14,"Total",boldbord) # TOTAL
		worksheet.write(x,15,str(x-aux),bold5) # sum cantidad
		worksheet.write(x,16,str(sum_area),bold6) # sum area

		tam_col = [10,9,10,10,9,9,9,9,9,10,10,13,35,17,10,10,12,9,10,10,17,10,15,10,38,10]
		alpha,prev,acum = list(string.ascii_uppercase),'',0
		for i,item in enumerate(tam_col):
			worksheet.set_column(prev+alpha[i%26]+':'+prev+alpha[i%26],item)
			if i == 25:
				prev = alpha[acum]
				acum+=1

		## SEGUNDA HOJA		
		worksheet = workbook.add_worksheet("RESUMEN")
		#Print Format
		worksheet.set_landscape() #Horizontal
		worksheet.set_paper(9) #A-4
		worksheet.set_margins(left=0.75, right=0.75, top=1, bottom=1)
		worksheet.fit_to_pages(1, 0)  # Ajustar por Columna
		worksheet.merge_range(0,0,0,2, u"REPORTE DE PROCESOS", title)
		worksheet.write(4,1, u" Producto", boldbord)
		worksheet.merge_range(2,2,2,13, u"PROCESOS", boldbord)
		
		worksheet.merge_range(3,2,3,3, u"CORTE", boldbord)
		worksheet.merge_range(3,4,3,5, u"PULIDO", boldbord)
		worksheet.merge_range(3,6,3,7, u"ENTALLE", boldbord)
		worksheet.merge_range(3,8,3,9, u"LAVADO", boldbord)
		worksheet.merge_range(3,10,3,11, u"TEMPLADO", boldbord)
		worksheet.merge_range(3,12,3,13, u"PRODUCIDO", boldbord)
		worksheet.merge_range(2,14,3,15, u"TOTALES", boldbord)

		for i in range(2,14,2):
			worksheet.write(4,i, u" Cant.", boldbord)
			worksheet.write(4,i+1, u"M2", boldbord)

		worksheet.write(4,14, u" Cantidad", boldbord)
		worksheet.write(4,15, u" Area", boldbord)
		x = 5
		sum_cantidad = sum_area = 0

		query = """
		SELECT 
		pt.name AS producto,
		--corte
		SUM(CASE WHEN gs.name = 'corte' THEN gll.area ELSE 0.0 END) AS area_corte,
		COUNT(gll.*) FILTER(WHERE gs.name = 'corte') AS items_corte,
		--pulido
		SUM(CASE WHEN gs.name = 'pulido' THEN gll.area ELSE 0.0 END) AS area_pulido,
		COUNT(gll.*) FILTER(WHERE gs.name = 'pulido') AS items_pulido,
		--entalle
		SUM(CASE WHEN gs.name = 'entalle' THEN gll.area ELSE 0.0 END) AS area_enta,
		COUNT(gll.*) FILTER(WHERE gs.name = 'entalle') AS items_enta,
		--lavado
		SUM(CASE WHEN gs.name = 'lavado' THEN gll.area ELSE 0.0 END) AS area_lava,
		COUNT(gll.*) FILTER(WHERE gs.name = 'lavado') AS items_lava,
		--templado
		SUM(CASE WHEN gs.name = 'templado' THEN gll.area ELSE 0.0 END) AS area_temp,
		COUNT(gll.*) FILTER(WHERE gs.name = 'templado') AS items_temp,
		--producido
		SUM(CASE WHEN gs.name = 'producido' THEN gll.area ELSE 0.0 END) AS area_produ,
		COUNT(gll.*) FILTER(WHERE gs.name = 'producido') AS items_produ
		FROM 
		glass_stage_record gsr
		JOIN glass_stage gs ON gs.id = gsr.stage_id
		JOIN glass_lot_line gll ON gsr.lot_line_id = gll.id
		JOIN product_product pp ON gll.product_id = pp.id
		JOIN product_template pt ON pp.product_tmpl_id = pt.id
		WHERE 
		gsr.done = true 
		AND gll.active = true
		%s
		GROUP BY pt.id, pt.name
		ORDER BY pt.name """ % ext_sql

		self._cr.execute(query)

		for item in self._cr.dictfetchall():
			style1, style2 = bold, bold8
			if x % 2 == 0:
				style1, style2 = bold2, bold7

			producto = item['producto']
			items_corte = item['items_corte']
			area_corte = item['area_corte']
			items_pulido = item['items_pulido']
			area_pulido = item['area_pulido']
			items_enta = item['items_enta']
			area_enta = item['area_enta']
			items_lava = item['items_lava']
			area_lava = item['area_lava']
			items_temp = item['items_temp']
			area_temp = item['area_temp']
			items_produ = item['items_produ']
			area_produ = item['area_produ']
			
			worksheet.write(x, 1, producto, style1) # producto
			worksheet.write(x, 2, items_corte, style1)
			worksheet.write(x, 3, area_corte, style2)
			worksheet.write(x, 4, items_pulido, style1)
			worksheet.write(x, 5, area_pulido, style2)
			worksheet.write(x, 6, items_enta, style1)
			worksheet.write(x, 7, area_enta, style2)
			worksheet.write(x, 8, items_lava, style1)
			worksheet.write(x, 9, area_lava, style2)
			worksheet.write(x, 10, items_temp, style1)
			worksheet.write(x, 11, area_temp, style2)
			worksheet.write(x, 12, items_produ, style1)
			worksheet.write(x, 13, area_produ, style2)
			
			total = items_corte + items_pulido + items_enta + items_lava + items_temp + items_produ
			total_area =  area_corte + area_pulido + area_enta + area_lava + area_temp + area_produ

			worksheet.write(x,14, total,style2)
			worksheet.write(x,15, total_area,style2)
			sum_cantidad += total
			sum_area += total_area
			x+=1

		worksheet.write(x,1,"Total",boldbord) # TOTAL
		worksheet.write(x,14,str(sum_cantidad),bold9) # sum cantidad
		worksheet.write(x,15,str(sum_area),bold6) # sum area

		tam_col = [6, 78] + 12 * [9] + [13, 13]
		for i,item in enumerate(tam_col):
			worksheet.set_column(alpha[i]+':'+alpha[i],item)
		workbook.close()

		with open(path, 'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),
				'content_type':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
			})
		return export.export_file(clear=True,path=path)