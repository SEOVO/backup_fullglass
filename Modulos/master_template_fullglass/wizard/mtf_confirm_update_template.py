# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class MtfConfirmUpdateTemplate(models.TransientModel):
	_name = 'mtf.confirm.update.template'

	template_id = fields.Many2one('mtf.template',required=True)
	current_sale_price = fields.Float('Actual Precio de venta',default=0.0)
	new_sale_price = fields.Float('Nuevo Precio de venta',default=0.0)
	product_id = fields.Many2one('product.product', string="Producto" , domain=[('type', 'in', ['product', 'service'])])
	mode = fields.Selection([
		('update_template_values', 'Actualizar valores de ficha'),
		('clone_template', 'Duplicar ficha maestra'),
	], string='Modo', default='update_template_values', readonly=True)

	def confirm(self):
		if self.mode == 'update_template_values':
			self.template_id.write(self._context.get('vals', {}))
		return True
	# Para la duplicación de ficahs reutilizaremos éste wizard, 
	# me da flojera hacer otro para algo tal simple

	def action_clone_template(self):
		if self.mode == 'clone_template' and self.product_id:
			if self.env['mtf.template'].search_count([('product_id', '=', self.product_id.id)]) > 0:
				raise UserError(u'Ya existe una ficha maestra para el producto "%s".' % self.product_id.name)
			default_vals = {
				'product_id': self.product_id.id,
			}
			tmpl = self.template_id.copy(default=default_vals)
		
		return {
			'name': tmpl.product_id.name,
			'type': 'ir.actions.act_window',
			'res_model': 'mtf.template',
			'res_id': tmpl.id,
			'view_mode': 'form',
			'view_id': self.env.ref('master_template_fullglass.master_template_view_form', False).id,
		}
