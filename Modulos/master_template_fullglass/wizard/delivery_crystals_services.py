# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class GlassDeliveryPickingCrystals(models.TransientModel):
	_inherit = 'glass.delivery.picking.crystals'

	def get_crystals_list(self):
		self.line_ids.unlink()
		if self.picking_id: # si es para servicios.
			return super(GlassDeliveryPickingCrystals, self).get_crystals_list()

		domain = self._context.get('extra_domain', [])
		domain += [('is_service', '=', True)]
		
		if self.glass_order_ids:
			domain.append(('order_prod_id', 'in', self.glass_order_ids.ids))
		if self.show_only_allowed:
			domain += [('producido', '=',True), ('entregado','=',False), ('in_packing_list','=',False)]

		#values = self.picking_id.move_lines.get_results(extra_domain=domain,limit=self.max_items if self.only_max_items else False)
		limit = self.max_items if self.only_max_items else False

		GlassLotLine = self.env['glass.lot.line']
		stages = ('producido', 'ingresado', 'entregado', 'in_packing_list')
		values = []

		for order in self.glass_order_ids:
			for calculator in order.line_ids.mapped('calc_line_id.calculator_id'):
				dict_vals = calculator._get_dict_crystals(extra_domain=domain, only_exists=True, limit=limit)
				for key, value in dict_vals.items():
					for k, v in value.items():
						if not any(v): continue
						# NOTE v = [lot_line.ids]
						lot_lines = GlassLotLine.browse(v) # en insulados puede ser mas de un cristal
						vals = {
							# NOTE en insulados todos deberían ser de la misma OP
							# no debería afectar sacar la data del primar item, ya q en isulados las áreas 
							# y dimensiones debería corresponderse...
							'move_id': False,
							'product_id': calculator.product_id.id,# aplica para templados e insulados
							'order_id': lot_lines[0].order_prod_id.id,
							'base1': lot_lines[0].base1,
							'base2': lot_lines[0].base2,
							'height1': lot_lines[0].altura1,
							'height2': lot_lines[0].altura2,
							'measures': lot_lines[0].measures,
							'area': lot_lines[0].area,
							'crystal_num': str(k),
							'order_name': lot_lines[0].order_prod_id.name,
							'crystal_num_fl': float(k),
							'lot_line_ids': [(6, 0, lot_lines.ids)],
						}
						# NOTE para el caso de insulados: sólo si ambos cristales cumplen la etapa 
						# se considera la etapa del cristal final como concluída:
						for st in stages:
							vals[st] = bool(all(l[st] for l in lot_lines))
						values.append((0,0,vals))
						if limit > 0: limit-=1

		values = sorted(values, key=lambda l: (l[2]['order_name'], l[2]['crystal_num_fl']), reverse=False)
		self.write({'line_ids': values})
		return {"type": "ir.actions.do_nothing",}



	def delivery(self):
		if self.picking_id:
			return super(GlassDeliveryPickingCrystals, self).delivery()



		lines = self.line_ids.filtered(lambda x: x.check)
		
		if len(lines) > 0:
			breaks       = lines.mapped('lot_line_ids').filtered(lambda x: x.is_break)
			sended       = lines.filtered(lambda x: x.entregado)
			#not_entried  = lines.filtered(lambda x: not x.ingresado)
			#packing_list = lines.filtered(lambda x: x.in_packing_list)
			msg = ''
			for item in breaks:
				msg+='%s - %s Motivo: Roto\n'%(item.order_id.name,item.crystal_num)
			for item in sended:
				msg+='%s - %s Motivo: Ya Entregado\n'%(item.order_id.name,item.crystal_num)
			#for item in not_entried:
			#	msg+='%s - %s Motivo: No ingresado\n'%(item.order_id.name,item.crystal_num)
			#for item in packing_list:
			#	msg+='%s - %s Motivo: En Packing List\n'%(item.order_id.name,item.crystal_num)
			if msg != '':
				raise UserError(u'No es posible procesar los siguientes cristales/servicios:\n'+msg)
		else:
			#lines = self.line_ids.filtered(lambda x: not x.entregado and x.ingresado and not x.in_packing_list)
			lines = self.line_ids.filtered(lambda x: not x.entregado and not x.in_packing_list)
		
		if not lines:
			raise UserError(u'No hay líneas que cumplan los requisitos para procesarse (no entregadas).')

		lines = lines[:self.max_items] #NOTE no remover hata que se regularice el límite en insulado


		line_vals = []
		rg_vals = {
			#'name'
			'sale_order_id': self.sale_order_id.id,
			'serie_guia_id': self.serie_guia_id.id,
			#'numberg'
		}

		glass_lines = self.env['glass.order.line']
		post_stages = ['ingresado', 'entregado']
		check_orders = []

		for line in lines:
			g_line = line.lot_line_ids.mapped('order_line_id') # para servicios, sólo debería existir 1 ítem
			g_line.ensure_one()

			if not g_line.is_service:
				raise UserError(u'El cristal/servicio: "O.S. %s Nro: %s" no es de tipo servicio' % (g_line.order_id.name, g_line.crystal_number))

			# Verificar que hayan pasado por las etapas según ficha maestra
			all_stages = g_line.lot_line_id.stage_ids
			error_msg = '\n'.join([rs.stage_id.name.upper() for rs in all_stages if not rs.done and rs.stage_id.name not in post_stages])
			
			if error_msg:
				raise UserError(u'No es posible dar como producido el cristal de servicio O.S.: %s Nro: %s ya que no pasó por las siguientes etapas solicitadas en ficha maestra: \n%s' % (g_line.order_id.name, g_line.crystal_number, error_msg))

			# Verificar que se hayan completado las Ord. de requisición:
			order = g_line.order_id
			# Mejorar Validación
			if order.id not in check_orders and order.mtf_req_line_ids:
				if not any(mtf_order.state == 'ended' for mtf_order in order.mtf_requirement_ids):
					raise UserError(u'La Órden de Servicio "%s" tiene materiales pendientes de requisición.' % order.name)
				else:
					check_orders.append(order.id)

			line_vals.append((0, 0, {
				'glass_line_id': g_line.id,
				}))

			line.lot_line_ids.with_context(force_register=True).register_stage('ingresado')
			line.lot_line_ids.with_context(force_register=True).register_stage('entregado')
			glass_lines |= g_line
		
		glass_lines.write({'state': 'send2partner'})
		rg_vals['line_ids'] = line_vals
		rg_obj = self.env['glass.service.remission_guide'].create(rg_vals)
		rg_obj.action_set_number_remission_guide()

		return {
			'name': u'Guía de remisión ' + (rg_obj.name or ''),
			'type': 'ir.actions.act_window',
			'res_id': rg_obj.id,
			'res_model': 'glass.service.remission_guide',
			'view_id': self.env.ref('glass_production_order.glass_service_remission_guide_view_form').id,
			'view_mode': 'form',
		}

		#key = 


		# Create remission guide:





		#picking = self.picking_id.with_context(glass_force_assign=True,delivery_transfer=True)
		#if picking.state=='draft':
		#	picking.action_confirm()
		#if picking.state=='confirmed':
		#	picking.action_assign()
		
		#Sit = self.env['stock.immediate.transfer']
		#Sbc = self.env['stock.backorder.confirmation']
		#if picking.state != 'assigned':
		#	picking.force_assign()
		
		#ops_to_transfer = self.env['stock.pack.operation']
		#if picking.state == 'assigned':
		#	moves = lines.mapped('move_id')
		#	rounding = 0.0001
		#	for move in moves:
		#		pack_operation = move.linked_move_operation_ids.mapped('operation_id') # FIXME debería haber sólo una operación relacionada
		#		filt = lines.filtered(lambda l: l.move_id==move)
		#		quantity = float_round(sum(filt.mapped('area')),precision_rounding=rounding)
				#pack_operation = self._get_operation(picking,move.product_id)
		#		compare = float_compare(quantity,pack_operation.product_qty,precision_rounding=rounding)
		#		if compare==1:
					# dado que todo está disponible, la cantidad en stock_move no debería superar a la del producto
		#			quantity = pack_operation.product_qty 

				# esta vaina es para subsanar las diferencias en calculadora, retirar cuando las 
				# ordenes generadas antes del 18 de enero se hayan entregado, ya no tiene sentido mantener esta verificación...
		#		calc = move.procurement_id.sale_line_id.calculator_id
		#		a_cobrada = calc.total_sold_area
		#		a_invoiced = calc.qty_invoiced
		#		diff = float(Decimal(str(a_cobrada-a_invoiced)).quantize(Decimal('0.0001'), rounding=ROUND_HALF_UP))
		#		if diff > 0.0:
		#			quantity = quantity - diff

		#		pack_operation.write({'qty_done':quantity})
		#		loc_id = move.location_id.id
		#		order_lines_ids = self.env['glass.order.line']
		#		for line in filt:
		#			lot_lines = line.lot_line_ids
		#			g_lines = lot_lines.mapped('order_line_id')
					
					# validación temporal, por si algún gil quiere entregar cristales q no fueron ingresados a la ubicación origen de éste alb.
					# Hace hacer un mejor análisis para ver en qué casos no aplicaría ésta validación, pero eso ya es chamba del q retome 
					# éste proyecto xD
		#			if not all(loc_id in gl.stock_move_ids.mapped('location_dest_id').ids or gl.lot_line_id.from_insulado for gl in g_lines):
		#				raise UserError(u'No fue posible realizar la transferencia debido a que existen cristales cuyo ingreso no fue hecho al lugar de origen de ésta entrega.')
					
		#			g_lines.write({'state':'send2partner'})
		#			lot_lines.with_context(force_register=True).register_stage('entregado')
		#			order_lines_ids |= g_lines

		#		move.write({'glass_order_line_ids':[(6,0,order_lines_ids.ids)]})
		#		ops_to_transfer |= pack_operation

		#	(picking.pack_operation_product_ids - ops_to_transfer).unlink()
		#	action = picking.do_new_transfer()
		#	if type(action) is dict and action['res_model']==Sbc._name:
		#		ctx = action['context']
		#		sbc = Sbc.with_context(ctx).create({'pick_id': picking.id})
		#		sbc.process()
		#		backorder_pick = self.env['stock.picking'].search([('backorder_id', '=', picking.id)])
		#		backorder_pick.do_unreserve()
		#	return {'type': 'ir.actions.act_window_close'}
		#else:
		#	raise UserError('No se pudo realizar la reservación de los productos')
