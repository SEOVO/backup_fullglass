# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.float_utils import float_round

class BuildGlassOrderWizard(models.TransientModel):
	_inherit = 'build.glass.order.wizard'

	mtf_requirement_line_ids = fields.Many2many('mtf.requisition.material.line', 
		relation='build_glass_order_mtf_requirement_rel', column1='build_go_wiz_id', column2='mtf_requirement_id',
		string='Líneas de requerimiento')
	
	#mtf_requirement_lines = fields.One2many('build.glass.order.wizard.line', 'wizard_id', string=u'Líneas de requerimiento')

	# def _prepare_glass_line_vals(self, vals):
	def _prepare_glass_line_vals(self, calc_line, crystal_number):
		vals = super(BuildGlassOrderWizard, self)._prepare_glass_line_vals(calc_line, crystal_number)
		#calc_line = self.env['glass.sale.calculator.line'].browse(vals.get('calc_line_id'))
		if calc_line.from_insulado:
			extra_vals = {
				'product_id': calc_line.product_ins_id.id,
				'mtf_template_id': calc_line.parent_id.template_id.id,
			}
		else:
			extra_vals = {'mtf_template_id': calc_line.template_id.id or False,}

		extra_vals['is_service'] = calc_line.calculator_id.product_id.type == 'service' and calc_line.calculator_id.order_id.type_sale == 'services'
		
		vals.update(extra_vals)
		return vals


	def _compute_require_sketch(self):
		prod_perf = self.env['mtf.parameter.config'].search([], limit=1).product_perforacion_id
		for wiz in self:
			if wiz.sale_id.type_sale == 'services':
				wiz.sketch_required = any(prod_perf and sol.product_id.id == prod_perf.id for sol in wiz.sale_id.order_line)
			else:
				super(BuildGlassOrderWizard, wiz)._compute_require_sketch()


# class BuildGlassOrderWizardLine(models.TransientModel):
# 	_name = 'build.glass.order.wizard.line'
	
# 	#requirement_line_id = fields.Many2one('mtf.requisition.material.line', string=u'Línea de requerimiento')
# 	# Ingreso de materiales de clientes (Determinado por ficha maestra)
# 	wizard_id = fields.Many2one('build.glass.order.wizard', string='Wizard', readonly=True, required=True)
# 	product_id = fields.Many2one('product.product', string='Producto', readonly=True)
# 	uom_id = fields.Many2one(related='product_id.uom_id', string=u'Unidad de medida')
# 	quantity = fields.Float('Cantidad', digits=(10, 4), readonly=True)
	

# Debería ser un nuevo modelo?
# usaremos mtf.requisition.material.line hasta definir eso

#class BuildGlassOrderWizard(models.TransientModel):
	#_inherit = 'build.glass.order.'

