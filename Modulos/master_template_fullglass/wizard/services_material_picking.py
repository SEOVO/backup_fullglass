# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from odoo.tools.float_utils import float_round


class MtfServicesMaterialWizard(models.TransientModel):
	_name = 'mtf.services_material.wizard'
	_description = 'Crear picking de Material de terceros'


	sale_order_id = fields.Many2one('sale.order', string='Órden de venta')
	pick_type_id = fields.Many2one('stock.picking.type', string=u'Tipo de operación')
	pick_motive_id = fields.Many2one('einvoice.catalog.12', string='Motivo de traslado')
	mtf_requirement_lines = fields.One2many('mtf.services_material.wizard.line', 
		'wizard_id', string=u'Líneas de requerimiento')

	def action_create_picking(self):
		#res = super(MtfServicesMaterialWizard, self).create_production_order()
		# picking de materias primas
		if self.mtf_requirement_lines:
			pick_type = self.pick_type_id
			pick_motive = self.pick_motive_id
			pick_vals = self._prepare_pick_vals(pick_type, pick_motive)
			move_vals = []
			for r_line in self.mtf_requirement_lines:
				if r_line.use_sides_dims:
					r_line._onchange_sides_dims()
				quantity = r_line.quantity
				if quantity > 0.0:
					move_vals.append((0, 0, self._prepare_move_vals(pick_type, r_line.product_id, quantity)))


			pick_vals['move_lines'] = move_vals
			pick = self.env['stock.picking'].create(pick_vals)
			pick.action_confirm()
			pick.action_assign()
			
			if pick.state != 'assigned':
				pick.force_assign()
			if pick.state == 'assigned':
				for op in pick.pack_operation_ids:
					op.write({'qty_done': op.product_qty})
				pick.action_done()


			# Create 2nd picking:
			# picking de almacén de terceros a ubicación de producción
			conf = self.env['mtf.parameter.config'].search([], limit=1)
			pick_type = conf.pick_type_3ros_prod_id
			pick_motive = conf.pick_traslate_motive_3ros_prod_id
			if not (pick_type and pick_motive):
				raise UserError(u'No se encontró la configuración para el tipo de operación/motivo de traslado para el albarán de ingreso a producción desde almacén de terceros.')
			
			pick_vals = self._prepare_pick_vals(pick_type, pick_motive)
			move_vals = []
			for r_line in self.mtf_requirement_lines:
				if r_line.quantity > 0.0:
					move_vals.append((0, 0, self._prepare_move_vals(pick_type, r_line.product_id, r_line.quantity)))

			pick_vals['move_lines'] = move_vals
			pick2 = self.env['stock.picking'].create(pick_vals)
			pick2.action_confirm()
			pick2.action_assign()
			if pick2.state == 'assigned':
				for op in pick2.pack_operation_ids:
					op.write({'qty_done': op.product_qty})
			# Done pending

			return {
				'name': pick.name,
				'res_id': pick.id,
				'type': 'ir.actions.act_window',
				'res_model': 'stock.picking',
				'view_mode': 'form',
			}
		else:
			raise UserError('No ha agregado líneas con los productos que desea transferir al almacén de terceros.')

	def _prepare_pick_vals(self, pick_type, pick_motive):
		current_date = fields.Date.context_today(self)
		#pick_type = self.pick_type_id
		#pick_motive = self.pick_motive_id
		
		return {
			'partner_id': False,
			'picking_type_id': pick_type.id,
			'date': current_date,
			'fecha_kardex': current_date,
			'location_dest_id': pick_type.default_location_dest_id.id,
			'location_id': pick_type.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': pick_motive.id,
			'sale_services_id': self.sale_order_id.id,
		}

	def _prepare_move_vals(self, pick_type, product, qty):
		current_date = fields.Date.context_today(self)
		pick_type = self.pick_type_id

		return {
			'name': product.name,
			'product_id': product.id,
			'product_uom': product.uom_id.id,
			'date':current_date,
			'date_expected':current_date,
			'picking_type_id': pick_type.id,
			'location_id': pick_type.default_location_src_id.id,
			'location_dest_id': pick_type.default_location_dest_id.id,
			'partner_id': False,
			'move_dest_id': False,
			'state': 'draft',
			'company_id':self.env.user.company_id.id,
			'procurement_id': False,
			'route_ids':pick_type.warehouse_id and [(6, 0, pick_type.warehouse_id.route_ids.ids)] or [],
			'warehouse_id': pick_type.warehouse_id and pick_type.warehouse_id.id or False,
			'product_uom_qty': qty,
		}


class MtfServicesMaterialWizardLine(models.TransientModel):
	_name = 'mtf.services_material.wizard.line'

	wizard_id = fields.Many2one('mtf.services_material.wizard', string='Wizard', readonly=True, required=True)
	product_id = fields.Many2one('product.product', string='Producto', required=True)
	use_sides_dims = fields.Boolean('Ingresar por medidas')
	base1 = fields.Integer('Base (mm)', default=0)
	#base2 = fields.Integer('Base 2 (mm)', default=0)
	height1 = fields.Integer('Altura (mm)', default=0)
	#height2 = fields.Integer('Altura 2 (mm)', default=0)
	uom_id = fields.Many2one(related='product_id.uom_id', string=u'Unidad de medida', readonly=True)
	uom_name = fields.Char(related='uom_id.name', readonly=True)
	quantity = fields.Float('Cantidad', digits=(10, 4), default=0.0)

	@api.onchange('use_sides_dims', 'base1', 'height1', 'uom_id')
	def _onchange_sides_dims(self):
		if self.use_sides_dims:
			rounding = self.uom_id.rounding or 0.0001
			self.quantity = float_round(((self.base1 or 0) * (self.height1 or 0)) / 1000000.0, precision_rounding=rounding)
		else:
			self.update({
				'quantity': 0.0,
				#'base1': 0.0,
				#'height1': 0.0,
				})