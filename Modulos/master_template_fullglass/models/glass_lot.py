# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError


class GlassLotLine(models.Model):
	_inherit = 'glass.lot.line'

	#mtf_temp_process_line_id = fields.Many2one(related='order_line_id.mtf_temp_process_line_id')
	mtf_template_id = fields.Many2one(related='order_line_id.mtf_template_id', store=True)
	from_insulado = fields.Boolean(related='calc_line_id.from_insulado', store=True)
	insulado_search_code = fields.Char(u'Código de Insulado') # campo aparentemente temporal

	def _get_requested_stages(self):
		if self.is_service: # servicios dependen de la plantilla
			return [stg.name for stg in self.mtf_template_id.stage_ids]
		
		res = super(GlassLotLine, self)._get_requested_stages()
		# add stages from template
		# FIXME: Match por el producto para obtener las etapas que le corresponden, no parece ser lo mejor pero funcionará...

		stages = []
		tmpl = self.mtf_template_id
		if self.from_insulado:
			line=tmpl.line_ids.filtered(lambda x: x.product_id==self.product_id and x.pos_ins_crystal)
			if line:
				line = line[0]
				stages = tmpl.ins_cr_1_stage_ids if line.pos_ins_crystal==1 else tmpl.ins_cr_2_stage_ids
				stages = stages.mapped('name')
		else:
			stages = tmpl.stage_ids.mapped('name')
			# agg etapas de servicios vinculados:
			calc = self.calc_line_id
			services_tmpls = (calc.arenado_id.mtf_template_id | calc.biselado_id.mtf_template_id | calc.polished_id.mtf_template_id)
			
			if services_tmpls:
				stages.extend(services_tmpls.mapped('stage_ids.name'))

			# TODO FIXME etapas de entalle y perforaciones, pendientes, los templates deberían jalarse de la configuracíón mtf

		res.extend(stages)
		return list(set(res))

	@api.model
	def make_search_code(self,**kwargs):
		search_code = ''
		if self._context.get('final_insulado_code',False): 
			order_name = kwargs.get('order_name',False)
			total_ins_items = kwargs.get('total_ins_items',False)
			crystal_num = kwargs.get('crystal_number',False)

			if not all((order_name,total_ins_items,crystal_num)):
				raise UserError(u'No se han ingresado todos los parámetros necesarios')

			if '.' in order_name: # factura adelantada:
				splited = order_name.split('.') # debería tener 2 items
				try:
					search_code+=str(splited[1])
					order_name = splited[0]
				except IndexError:
					raise UserError(u'Ocurrió un error al generar el código de cristal debido a un formato incorrecto en el nombre de OP.')
			try:
				search_code += str(int(order_name)).rjust(5,'0')
			except ValueError:
				raise UserError(u'El nombre de OP: %s no corresponde a producción, es posible que se trate de una órden de distribución.'%order_name)
			search_code += str(crystal_num).rjust(4,'0')
			search_code += str(total_ins_items).rjust(6,'0')

		else:
			search_code = super(GlassLotLine, self).make_search_code(**kwargs)
		return search_code.replace('.','')
