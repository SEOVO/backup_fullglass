# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError
from itertools import groupby

class GlassRequisition(models.Model):
	_inherit = 'glass.requisition'

	def action_create_services_transfer(self):
		# Generar un movimiento de almacén para hacer requisición de las materias primas solicitadas para los
		# servicios vnculados a los cristales templados, NOTE sólo se requiere material por ésta vía 
		# para los servicios viculados desde una calculadora de templados.
		
		res = super(GlassRequisition, self).action_create_services_transfer()
		
		# debería haber sólo 1
		if self.move_srv_raw_ids.mapped('picking_id').filtered(lambda p: p.state == 'done'):
			return res
		# Usar los mismos tipos de operación de materias primas
		pick_type, pick_motive, scrap_loc = self._get_pick_params('mat_prima_services')
		wiz = self.env['requisition.material.wizard'].create({
			'mode': 'mat_prima_services',
			'requisition_id': self.id,
		})
		
		req_lines = self.env['mtf.requisition.material.line']

		for lot in self.glass_lot_ids:
			for l_line in lot.line_ids:
				if l_line.calc_line_id.from_insulado: # el requerimiento de insulado va por su wizard
					continue
				req_lines |= l_line.calc_line_id.material_line_ids

		if not req_lines:
			return res

		key = lambda x: x.product_id.id
		req_lines = req_lines.sorted(key=key)
		Product = self.env['product.product']

		move_line_vals = []
		pick_vals = wiz._prepare_pick_vals(pick_type, pick_motive)

		for product_id, groups in groupby(req_lines, key=key):
			product_obj = Product.browse(product_id)
			qty = sum(map(lambda i: i.uom_id._compute_quantity(i.required_quantity, product_obj.uom_id), groups))
			move_line_vals.append((0, 0, wiz._prepare_move_vals(product_obj, qty, pick_type, 'requisition_srv_raw_id')))

		pick_vals['move_lines'] = move_line_vals
		picking = self.env['stock.picking'].create(pick_vals)
		picking.action_confirm()
		picking.action_assign()
		if picking.state != 'assigned':
			picking.force_assign()
		if picking.state == 'assigned':
			for op in picking.pack_operation_ids:
				op.write({'qty_done': op.product_qty})
			picking.action_done()
		return {
			'name': picking.name,
			'res_id': picking.id,
			'type': 'ir.actions.act_window',
			'res_model': picking._name,
			'view_mode': 'form',
		}
