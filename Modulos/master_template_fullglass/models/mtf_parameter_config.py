# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError


class MtfParameterConfig(models.Model):
	_name = 'mtf.parameter.config'

	name = fields.Char('Nombre',default=u"Parámetros de Fichas maestras")

	mat_prima_location_id = fields.Many2one('stock.location',string='Ubicación de Materia Prima',help=u'Ubicación que determinará si una línea de ficha maestra será materia prima')
	category_insulados_id = fields.Many2one('product.category',string='Categ. Insulados') # cristales insulados

	requisition_seq_id = fields.Many2one('ir.sequence',string='Secuencia para requisiciones')
	
	# para requisiciones de templado/insulados
	req_default_pick_type_id = fields.Many2one('stock.picking.type',string=u'Operación de Requisición por defecto')
	req_default_traslate_motive_id = fields.Many2one('einvoice.catalog.12',string=u'Motivo de traslado por defecto')
	
	# para servicios
	# el tipo de picking vendrá de los parámetros de producción
	#req_default_srv_pick_type_id = fields.Many2one('stock.picking.type',string=u'Operación de Req. de Servicio por defecto')
	#req_default_srv_traslate_motive_id = fields.Many2one('einvoice.catalog.12',string=u'Motivo de trasl. de Servicio por defecto')
	#gastos_insumos_location_id = fields.Many2one('stock.location',string='Ubicación de Gastos Insumos',help=u'Ubicación que determinará si una línea de ficha maestra será un Gasto/Insumo')

	discount_categ_a = fields.Float(u'Descuento categoría A', default=0.0)
	discount_categ_b = fields.Float(u'Descuento categoría B', default=0.0)
	discount_categ_c = fields.Float(u'Descuento categoría C', default=0.0)
	discount_categ_d = fields.Float(u'Descuento categoría D', default=0.0)

	# Importante: los sufijos son usados en el template : cr_tmp,mt,cr_lam 
	# cristales templados
	manpower_cr_tmp = fields.Float('Mano de Obra Crist. Temp.', default=0.0)
	services_cr_tmp = fields.Float('Servicio de terceros Crist. Temp.', default=0.0)
	depreciacion_cr_tmp = fields.Float(u'Depreciación Crist. Temp.', default=0.0)
	cost_safe_cr_tmp = fields.Float('Seguros Crist. Temp.', default=0.0)
	tributes_cr_tmp = fields.Float('Tributos Crist. Temp.', default=0.0)
	cost_adm_sales_cr_tmp = fields.Float('Gastos Adm y Ventas Crist. Temp.', default=0.0)
	util_percent_cr_tmp = fields.Float('Margen sobre la venta Crist. Temp.', default=0.0)

	# Ficha maestra
	manpower_cr_ins = fields.Float('Mano de Obra Fichas Maestras', default=0.0)
	services_cr_ins = fields.Float('Servicio de terceros Fichas Maestras', default=0.0)
	depreciacion_cr_ins = fields.Float(u'Depreciación Fichas Maestras', default=0.0)
	cost_safe_cr_ins = fields.Float('Seguros Fichas Maestras', default=0.0)
	tributes_cr_ins = fields.Float('Tributos Fichas Maestras', default=0.0)
	cost_adm_sales_cr_ins = fields.Float('Gastos Adm y Ventas Fichas Maestras', default=0.0)
	util_percent_cr_ins = fields.Float('Margen sobre la venta Fichas Maestras', default=0.0)

	# Cristales laminados
	manpower_cr_lam = fields.Float('Mano de Obra Crist. Laminado', default=0.0)
	services_cr_lam = fields.Float('Servicio de terceros Crist. Laminado', default=0.0)
	depreciacion_cr_lam = fields.Float(u'Depreciación Crist. Laminado', default=0.0)
	cost_safe_cr_lam = fields.Float('Seguros Crist. Laminado', default=0.0)
	tributes_cr_lam = fields.Float('Tributos Crist. Laminado', default=0.0)
	cost_adm_sales_cr_lam = fields.Float('Gastos Adm y Ventas Crist. Laminado', default=0.0)
	util_percent_cr_lam = fields.Float('Margen sobre la venta Crist. Laminado', default=0.0)

	# Carpintería metálica
	manpower_carp_met = fields.Float('Mano de Obra Carp. metálica', default=0.0)
	services_carp_met = fields.Float('Servicio de terceros Carp. metálica', default=0.0)
	depreciacion_carp_met = fields.Float(u'Depreciación Carp. metálica', default=0.0)
	cost_safe_carp_met = fields.Float('Seguros Carp. metálica', default=0.0)
	tributes_carp_met = fields.Float('Tributos Carp. metálica', default=0.0)
	cost_adm_sales_carp_met = fields.Float('Gastos Adm y Ventas Carp. metálica', default=0.0)
	util_percent_carp_met = fields.Float('Margen sobre la venta Carp. metálica', default=0.0)

	# Pegado de Silicona
	manpower_peg_sil = fields.Float('Mano de Obra Peg. de Silicona', default=0.0)
	services_peg_sil = fields.Float('Servicio de terceros Peg. de Silicona', default=0.0)
	depreciacion_peg_sil = fields.Float(u'Depreciación Peg. de Silicona', default=0.0)
	cost_safe_peg_sil = fields.Float('Seguros Peg. de Silicona', default=0.0)
	tributes_peg_sil = fields.Float('Tributos Peg. de Silicona', default=0.0)
	cost_adm_sales_peg_sil = fields.Float('Gastos Adm y Ventas Peg. de Silicona', default=0.0)
	util_percent_peg_sil = fields.Float('Margen sobre la venta Peg. de Silicona', default=0.0)

	# Armado de paneles (pendiente)
	# manpower_arm_pan = fields.Float('Mano de Obra Arm. de paneles', default=0.0)
	# services_arm_pan = fields.Float('Servicio de terceros Arm. de paneles', default=0.0)
	# depreciacion_arm_pan = fields.Float(u'Depreciación Arm. de paneles', default=0.0)
	# cost_safe_arm_pan = fields.Float('Seguros Arm. de paneles', default=0.0)
	# tributes_arm_pan = fields.Float('Tributos Arm. de paneles', default=0.0)
	# cost_adm_sales_arm_pan = fields.Float('Gastos Adm y Ventas Arm. de paneles', default=0.0)
	# util_percent_arm_pan = fields.Float('Margen sobre la venta Arm. de paneles', default=0.0)
	

	# SERVICIOS
	# Serv. insulado
	manpower_srv_insulado = fields.Float('Mano de Obra Serv. Insulado', default=0.0)
	services_srv_insulado = fields.Float('Servicio de terceros Serv. Insulado', default=0.0)
	depreciacion_srv_insulado = fields.Float(u'Depreciación Serv. Insulado', default=0.0)
	cost_safe_srv_insulado = fields.Float('Seguros Serv. Insulado', default=0.0)
	tributes_srv_insulado = fields.Float('Tributos Serv. Insulado', default=0.0)
	cost_adm_sales_srv_insulado = fields.Float('Gastos Adm y Ventas Serv. Insulado', default=0.0)
	util_percent_srv_insulado = fields.Float('Margen sobre la venta Serv. Insulado', default=0.0)

	# Serv. Corte laminado
	manpower_srv_corte_lam = fields.Float('Mano de Obra Serv. Corte-Laminado', default=0.0)
	services_srv_corte_lam = fields.Float('Servicio de terceros Serv. Corte-Laminado', default=0.0)
	depreciacion_srv_corte_lam = fields.Float(u'Depreciación Serv. Corte-Laminado', default=0.0)
	cost_safe_srv_corte_lam = fields.Float('Seguros Serv. Corte-Laminado', default=0.0)
	tributes_srv_corte_lam = fields.Float('Tributos Serv. Corte-Laminado', default=0.0)
	cost_adm_sales_srv_corte_lam = fields.Float('Gastos Adm y Ventas Serv. Corte-Laminado', default=0.0)
	util_percent_srv_corte_lam = fields.Float('Margen sobre la venta Serv. Corte-Laminado', default=0.0)

	# Serv. Corte lavado laminado
	manpower_srv_corte_lav_lam = fields.Float('Mano de Obra Serv. Corte-Lavado-Laminado', default=0.0)
	services_srv_corte_lav_lam = fields.Float('Servicio de terceros Serv. Corte-Lavado-Laminado', default=0.0)
	depreciacion_srv_corte_lav_lam = fields.Float(u'Depreciación Serv. Corte-Lavado-Laminado', default=0.0)
	cost_safe_srv_corte_lav_lam = fields.Float('Seguros Serv. Corte-Lavado-Laminado', default=0.0)
	tributes_srv_corte_lav_lam = fields.Float('Tributos Serv. Corte-Lavado-Laminado', default=0.0)
	cost_adm_sales_srv_corte_lav_lam = fields.Float('Gastos Adm y Ventas Serv. Corte-Lavado-Laminado', default=0.0)
	util_percent_srv_corte_lav_lam = fields.Float('Margen sobre la venta Serv. Corte-Lavado-Laminado', default=0.0)

	# Serv. Pegado / producido
	manpower_srv_pegado = fields.Float('Mano de Obra Serv. Pegado/Producido', default=0.0)
	services_srv_pegado = fields.Float('Servicio de terceros Serv. Pegado/Producido', default=0.0)
	depreciacion_srv_pegado = fields.Float(u'Depreciación Serv. Pegado/Producido', default=0.0)
	cost_safe_srv_pegado = fields.Float('Seguros Serv. Pegado/Producido', default=0.0)
	tributes_srv_pegado = fields.Float('Tributos Serv. Pegado/Producido', default=0.0)
	cost_adm_sales_srv_pegado = fields.Float('Gastos Adm y Ventas Serv. Pegado/Producido', default=0.0)
	util_percent_srv_pegado = fields.Float('Margen sobre la venta Serv. Pegado/Producido', default=0.0)

	# Serv. Arenado
	manpower_srv_arenado = fields.Float('Mano de Obra Serv. Arenado', default=0.0)
	services_srv_arenado = fields.Float('Servicio de terceros Serv. Arenado', default=0.0)
	depreciacion_srv_arenado = fields.Float(u'Depreciación Serv. Arenado', default=0.0)
	cost_safe_srv_arenado = fields.Float('Seguros Serv. Arenado', default=0.0)
	tributes_srv_arenado = fields.Float('Tributos Serv. Arenado', default=0.0)
	cost_adm_sales_srv_arenado = fields.Float('Gastos Adm y Ventas Serv. Arenado', default=0.0)
	util_percent_srv_arenado = fields.Float('Margen sobre la venta Serv. Arenado', default=0.0)

	# Serv. Perforaciones
	manpower_srv_perforacion = fields.Float('Mano de Obra Serv. Perforaciones', default=0.0)
	services_srv_perforacion = fields.Float('Servicio de terceros Serv. Perforaciones', default=0.0)
	depreciacion_srv_perforacion = fields.Float(u'Depreciación Serv. Perforaciones', default=0.0)
	cost_safe_srv_perforacion = fields.Float('Seguros Serv. Perforaciones', default=0.0)
	tributes_srv_perforacion = fields.Float('Tributos Serv. Perforaciones', default=0.0)
	cost_adm_sales_srv_perforacion = fields.Float('Gastos Adm y Ventas Serv. Perforaciones', default=0.0)
	util_percent_srv_perforacion = fields.Float('Margen sobre la venta Serv. Perforaciones', default=0.0)

	perforacion_extra_gt_one = fields.Float(u'Margen adicional perforación', default=0.0, 
		help=u'Margen adicional aplicado cuando es una perforación y la cantidad de cristales por línea de calculadora es mayor a 1')

	# Serv. Canto pulido brillante
	manpower_srv_canto_pulido = fields.Float('Mano de Obra Serv. Canto Pulido', default=0.0)
	services_srv_canto_pulido = fields.Float('Servicio de terceros Serv. Canto Pulido', default=0.0)
	depreciacion_srv_canto_pulido = fields.Float(u'Depreciación Serv. Canto Pulido', default=0.0)
	cost_safe_srv_canto_pulido = fields.Float('Seguros Serv. Canto Pulido', default=0.0)
	tributes_srv_canto_pulido = fields.Float('Tributos Serv. Canto Pulido', default=0.0)
	cost_adm_sales_srv_canto_pulido = fields.Float('Gastos Adm y Ventas Serv. Canto Pulido', default=0.0)
	util_percent_srv_canto_pulido = fields.Float('Margen sobre la venta Serv. Canto Pulido', default=0.0)

	# Serv. Biselado
	manpower_srv_biselado = fields.Float('Mano de Obra Serv. Biselados', default=0.0)
	services_srv_biselado = fields.Float('Servicio de terceros Serv. Biselados', default=0.0)
	depreciacion_srv_biselado = fields.Float(u'Depreciación Serv. Biselados', default=0.0)
	cost_safe_srv_biselado = fields.Float('Seguros Serv. Biselados', default=0.0)
	tributes_srv_biselado = fields.Float('Tributos Serv. Biselados', default=0.0)
	cost_adm_sales_srv_biselado = fields.Float('Gastos Adm y Ventas Serv. Biselados', default=0.0)
	util_percent_srv_biselado = fields.Float('Margen sobre la venta Serv. Biselados', default=0.0)

	# Serv. Entalles
	manpower_srv_entalle = fields.Float('Mano de Obra Serv. Entalles', default=0.0)
	services_srv_entalle = fields.Float('Servicio de terceros Serv. Entalles', default=0.0)
	depreciacion_srv_entalle = fields.Float(u'Depreciación Serv. Entalles', default=0.0)
	cost_safe_srv_entalle = fields.Float('Seguros Serv. Entalles', default=0.0)
	tributes_srv_entalle = fields.Float('Tributos Serv. Entalles', default=0.0)
	cost_adm_sales_srv_entalle = fields.Float('Gastos Adm y Ventas Serv. Entalles', default=0.0)
	util_percent_srv_entalle = fields.Float('Margen sobre la venta Serv. Entalles', default=0.0)

	entalle_extra_gt_one = fields.Float(u'Margen adicional entalle', default=0.0, 
		help=u'Margen adicional aplicado cuando es un entalle y la cantidad de cristales por línea de calculadora es mayor a 1')

	# Parámetros para generación de picking por materias primas:
	pick_type_raw_material_id = fields.Many2one('stock.picking.type', 
		string=u'Tipo de picking Clientes->Terceros', 
		help=u'Tipo de operación para generar el picking de ingreso de material de clientes a almacén de terceros.')
	pick_traslate_motive_raw_id = fields.Many2one('einvoice.catalog.12', 
		string=u'Motivo de tras. Clientes->Terceros',
		help=u'Tipo de de operación para el picking de clientes a almacén de terceros.')

	# parámetos para crear pickign de ingreso a producción (desde almacén de terceros)
	pick_type_3ros_prod_id = fields.Many2one('stock.picking.type', 
		string=u'Tipo de picking Terceros->Producción', 
		help=u'Tipo de operación para generar el picking de ingreso de material de clientes a producción.')
	pick_traslate_motive_3ros_prod_id = fields.Many2one('einvoice.catalog.12', 
		string=u'Motivo de tras. Terceros->Producción',
		help=u'Tipo de de operación para el picking de almacén de terceros a producción.')


	# Fichas maestras para los servicios que pueden ser agregados a un cristal templado:
	#tmpl_arenado_id = fields.Many2one('mtf.template', 'Ficha para servicio de arenado')
	tmpl_perforacion_id = fields.Many2one('mtf.template', 'Ficha para servicio de perforación')
	#tmpl_biselado_id = fields.Many2one('mtf.template', 'Ficha para servicio de biselado')
	tmpl_entalle_id = fields.Many2one('mtf.template', 'Ficha para servicio de entalle')
	# Las plantillas para pulidos, biselados y arenados irán dentro de cada modelo relacionado
	
	free_limit_entalle = fields.Integer(string=u'Límite para entalles libres', default=4,
		help='Cantidad máxima de entalles por cristal libres de pago')

	free_limit_perforacion = fields.Integer(string=u'Límite para perforaciones libres', default=4,
		help='Cantidad máxima de perforaciones por cristal libres de pago')

	# A soilicitud de Nicolás Castillo
	charge_extra_measures = fields.Float('Cargo extra por medidas', 
		help='Porcentaje sumado al márgen sobre la venta para cristales que superen 120 cm')

	# Productos de servicio que necesitan nro de ítems, ésta vaina se puede mejorar, pero
	# por cuestiones de tiempo así nomás.
	# TODO debería ser sólo un producto por cada uno, caso contrario estamos en problemas
	product_perforacion_id = fields.Many2one('product.product', 
		string=u'Prod. serv. de perforación')
	# product_biselado_id = fields.Many2one('product.product', 
	# 	string=u'Prod. serv. biselado')
	product_entalle_id = fields.Many2one('product.product', 
		string=u'Prod. serv. entalle')

	_sql_constraints = [
		(
			'check_discounts',
			"""CHECK (
				discount_categ_a >= 0 and discount_categ_a <= 100 and discount_categ_b >= 0 and discount_categ_b <= 100 and 
				discount_categ_c >= 0 and discount_categ_c <= 100 and discount_categ_d >= 0 and discount_categ_d <= 100)""",
			u'Los porcentajes de descuento deben estar en el rango de 0 a 100.'
		)
	]

	@api.model
	def create(self, values):
		if self.search_count([]) != 0:
			raise UserError(u'Ya existe un registro de parámetros de configuración')
		return super(MtfParameterConfig, self).create(values)

	def unlink(self):
		if self.env['mtf.template'].search_count([('config_id', 'in', self.ids)]) > 0:
			raise UserError(u'No es posible eliminar los parámetros de configuración de debido a que hay fichas maestras que los utilizan')
		return super(MtfParameterConfig, self).unlink()


	@api.constrains('tmpl_perforacion_id', 'tmpl_entalle_id')
	def _check_uniq_service_templates(self):
		for conf in self:
			tmplids = [i.id for i in (conf.tmpl_perforacion_id | conf.tmpl_entalle_id) if i]
			if len(tmplids) != len(set(tmplids)):
				raise UserError('Todas las fichas asignadas a los servicios deben ser diferentes.')
