# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError, ValidationError

class PulidoProforma(models.Model):
	_inherit = 'sale.pulido.proforma'

	mtf_template_id = fields.Many2one('mtf.template', string='Ficha Maestra')

class GlassTypeBiselado(models.Model):
	_inherit = 'glass.type.biselado'

	mtf_template_id = fields.Many2one('mtf.template', string='Ficha Maestra')

class GlassTypeArenado(models.Model):
	_inherit = 'glass.type.arenado'

	mtf_template_id = fields.Many2one('mtf.template', string='Ficha Maestra')