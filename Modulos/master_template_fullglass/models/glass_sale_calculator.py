# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError,ValidationError
from odoo.tools.float_utils import float_round, float_compare, float_is_zero
import re

class GlassSaleCalculator(models.Model):
	_inherit = 'glass.sale.calculator'

	type_calculator = fields.Selection(selection_add=[('insulado_calculator', 'Calculadora de Insulados')])
	template_id = fields.Many2one('mtf.template', string='Plantilla Maestra', readonly=True)




	
	def save_calculator(self):
		# update quantity and unit price
		self._check_template_consistence()
		return super(GlassSaleCalculator, self).save_calculator()

	def get_order_line_write_vals(self):
		vals = super(GlassSaleCalculator, self).get_order_line_write_vals()
		
		if self.order_id.before_invoice:
			return vals

		# Update line values from templates
		tmpl_vals = self.line_ids.compute_saleprice_from_template() # compute  from template
		self.line_ids.with_context(tmpl_vals=tmpl_vals).action_update_from_template()
		
		total = sum(map(lambda v: v.get('mtf_sale_price', 0.0), tmpl_vals.values()))
		qty_to_sale = vals['product_uom_qty']
		
		if self.type_calculator == 'insulado_calculator':
			vals['price_unit'] = total / qty_to_sale
		elif self.type_calculator == 'common_product':
			self.order_line_id.product_uom_change() # update original price unit
			price_unit = self.order_line_id.price_unit
			if not float_is_zero(total, 5):
				price_unit += total / qty_to_sale
			vals['price_unit'] = price_unit
		elif self.type_calculator == 'service':
			vals['price_unit'] = total / qty_to_sale
		#raise UserError('xd')
		# FIX agregar el porcentaje de descuento de la línea, para evitar doble descuento
		# FIXME call onchange methods on sale.order for changed values?
		return vals

	# estandarizar validación
	def _check_template_consistence(self):
		# 1. Si la faturación es adelantada, no será posible agregar el precio según ficha maestra, 
		#    ya que al ser entregas parciales, es imposible determinar un precio unitario, ya que la ficha 
		#    puede determinar un precio distinto para cada entrega parcial (el costeo de 100M2 no siempre será el mismo,
		#    ya que influye la cantidad de cristales, las métricas de ficha maestra etc); éste comportamiento podría mutar el 
		#    precio unitario de la línea de pedido de venta, y éste, al encontrarse facturado, traer problemas de 
		#    inconsistencia.
		# 1. Si pertenece a la categoría de insulados, debe tener una ficha vinculada:
		# 2. Si es una calculadora de templado con servicios asociados a servicios, se suma el exceso unitario por el servicio al 
		#     precio unitario base según tarifas (ésto no aplica para pedido don fact. adelentada).
		# 3. Si es un pedido por servicios , es lo mismo , el precio unitario se mantiene según tarifa, se obvia el coste de la ficha
		#    maestra, ya que todos los servicios tendrán uan ficha asociada.
		# 4. Sin en una línea de calc. se elige pulido, entalles, bilesados o perforaciones, éstos 
		#    deben tener una ficha maestra asociada; sólo los servicios mencionados deben aparecer en la calculadora de templados

		for rec in self:
			if rec.type_calculator in ('insulado_calculator', 'service') and not rec.template_id:
				raise ValidationError(u'Las calculadoras de cristales insulados y de servicios deben tener una ficha maestra vinculada.\nSolicite a su administrador de fichas maestras crear una para el producto %s.' % rec.product_id.name)



	def _get_glass_order_ids(self):
		ops = super(GlassSaleCalculator, self)._get_glass_order_ids()
		#add op's for child_ids
		return ops | self.line_ids.mapped('child_ids.glass_order_id')

	def _get_lines_to_produce(self):
		if self.type_calculator == 'insulado_calculator':
			return self.line_ids.mapped('child_ids').filtered(lambda l: not l.glass_order_id)
		
		elif self.type_calculator == 'service':
			# sólo generan OS si está asociados a una ficha maestra
			if not self.template_id:
				return self.env['glass.sale.calculator.line']
			else:
				return super(GlassSaleCalculator, self)._get_lines_to_produce()
		else:
			return super(GlassSaleCalculator, self)._get_lines_to_produce()


	def _get_dict_crystals(self, extra_domain=[], only_exists=False, limit=False):
		# return a dict {'calc_line_id':{'crystal_number':[lot_line_ids]}}
		if self.type_calculator == 'insulado_calculator':
			# only_exists considera sólo insulados termninados
			return self.line_ids._get_insulados_dict_crystals(extra_domain, only_exists, limit)
		else:
			return super(GlassSaleCalculator, self)._get_dict_crystals(extra_domain, only_exists, limit)

	@api.constrains('product_id', 'template_id')
	def _verify_product_consistence(self):
		for rec in self:
			if not rec.template_id:
				continue
			if rec.product_id.id != rec.template_id.product_id.id:
				raise ValidationError(u'¡El producto de la calculadora no coincide con el producto de la ficha maestra asociada!')


	def _prepare_clone_calc_items_vals(self, calc_line):
		vals = super(GlassSaleCalculator, self)._prepare_clone_calc_items_vals(calc_line)
		GlassLine = self.env['glass.sale.calculator.line'].with_context(
			default_product_id=self.product_id.id, 
			default_type_calculator=self.type_calculator,
			default_order_line_id=self.order_line_id.id,
			)
		vals.update({
			'service_item_helper': GlassLine._default_service_item_helper(),
			})
		return vals


	# En un principio la ficha maestra sólo era para insulados 
	# @api.multi
	# def write(self, values):
	# 	if 'type_calculator' in values and values['type_calculator']!='insulado_calculator':
	# 		values['template_id'] = False
	# 	return super(GlassSaleCalculator,self).write(values)
	
class GlassSaleCalculatorLine(models.Model):
	_inherit = 'glass.sale.calculator.line'

	parent_id = fields.Many2one('glass.sale.calculator.line', 
		string='Parent id', ondelete='cascade', help=u'Cristal Insulado Padre')
	template_id = fields.Many2one(related='calculator_id.template_id')
	child_ids = fields.One2many('glass.sale.calculator.line','parent_id', 
		string='Childs', help=u'Cristales que conforman un Cristal Insulado')
	insulado = fields.Boolean('Insulado', 
		default=False, help=u'Define si es un cristal insulado')
	from_insulado = fields.Boolean('Insulado', 
		default=False, help=u'Define si forma parte de un cristal insulado')
	material_line_ids = fields.One2many('mtf.requisition.material.line', 'calculator_line_id', 
		string=u'Líneas de requisición')

	"""Importante: Este campo representa al cristal-producto (ejm Templado incoloro 8mm) 
	base para fabricar un insulado, no representa al producto Insulado en si, 
	solo debe ser establecido en registros de tipo child_ids."""
	product_ins_id = fields.Many2one('product.product',string=u'Cristal base de Insulado')
	default_code_ins = fields.Char(related='product_ins_id.default_code',string=u"Código")

	mtf_sale_price = fields.Float('Precio Total')

	# Campo auxiliar par afiltrar los campo a mostrar que requieren llenar un nro de ítems
	# biselados, entalles, perforaciones, aplica sólo para las calculadoras de servicios
	
	def _default_service_item_helper(self):
		ctx = self._context
		if 'default_product_id' in ctx:
			config = self.env['mtf.parameter.config'].sudo().search([], limit=1)
			
			map_vals = {
				config.product_perforacion_id.id: 'perforacion',
				#config.product_biselado_id.id: 'biselado',
				config.product_entalle_id.id: 'entalle',
			}
			return ctx.get('default_type_calculator') == 'service' and map_vals.get(ctx['default_product_id'], False) or False
		return False
	
	service_item_helper = fields.Selection([
		('perforacion', u'Perforación'),
		#('biselado', u'Biselado'),
		('entalle', u'Entalle'),
	], string=u'Helper tipo de servicio', readonly=True, default=_default_service_item_helper)


	# def _get_service_item_helper(self):
	# 	config = self.env['mtf.parameter.config'].sudo().search([], limit=1)
		
	# 	map_vals = {
	# 		config.product_perforacion_id.id: 'perforacion',
	# 		config.product_biselado_id.id: 'biselado',
	# 		config.product_entalle_id.id: 'entalle',
	# 	}

	# 	for line in self:
	# 		line.service_item_helper = line.type_calculator == 'service' and map_vals.get(line.product_id.id, False) or False


	_sql_constraints = [('check_insulado_is_not_child','CHECK ((insulado AND from_insulado)=false)',u'Un cristal no puede ser un insulado y proceder de un insulado a la vez!')]

	def _get_ref_order_id(self):
		"""IMPORTANTE: éste método devuelve la OP de la línea de calculadora,NO se debe usar 
		en procesos que no sean para fines de reportes u operaciones referenciales."""
		self.ensure_one()
		if self.insulado:
			ops = self.child_ids.mapped('glass_order_id')
			return ops and ops[0] or ops #FIXME podría estar sus cristales en diferentes OP'S?
		else:
			return super(GlassSaleCalculatorLine, self)._get_ref_order_id()

	def action_update_from_template(self):
		values = self._context.get('tmpl_vals') or self.compute_saleprice_from_template()
		for line in self:
			line.write(values.get(line.id, {}))
		return {"type": "ir.actions.do_nothing",} 

	def validate_crystal_number(self,string,qty):
		# si es insulado se hace una verificación simple, ya que no es ingresada manualmente
		if self.from_insulado:
			numbers = []
			if re.match(r'\b[0-9]{1,}.[0-9]{1,}-[0-9]{1,}.[0-9]{1,}\b', string):
				nums = string.split('-')
				start,end = float(nums[0]),float(nums[1])
				if start>=end:
					return u'Formato de número de cristal insulado incorrecto: '+string
				while start<=end:
					numbers.append(start)
					start+=1
			elif re.match(r'(?<=^|)\d+\.\d+(?=$|)', string):
				numbers = [float(string)]
			else:
				return u'Formato de número de cristal insulado incorrecto: '+string
			if len(numbers) != qty:
				return u"El número de cristales debe corresponder a la cantidad indicada (%d):\nValor ingresado: %s" % (qty, string)
			return numbers
		else:
			return super(GlassSaleCalculatorLine, self).validate_crystal_number(string, qty)
			

	
	def show_template_details(self):
		self.ensure_one()
		#if self.calculator_id.type_calculator!='insulado_calculator' or not self.calculator_id.template_id:
		if not self.template_id:
			raise UserError(u'Ésta calculadora no tiene una ficha maestra para el producto procesado.')
		module = __name__.split('addons.')[1].split('.')[0]
		return {
			'name':'Detalles de Ficha Maestra',
			'res_id':self.id,
			'type': 'ir.actions.act_window',
			'res_model': self._name,
			'view_id': self.env.ref('%s.calculator_line_template_details_form'%module).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			}

	def _get_insulados_dict_crystals(self, extra_domain=[], only_completes=False, limit=False):
		"""Obtener cristales para formar un insulado
			-la cantidad de cristales deberia corresponder a la cantidad de child_ids de la línea de calculadora
			TODO: en el caso de laminados (q se piden por requisición pero forma parte del insulado, solo
			tendría un child_ids)"""
			
		# dict = {34: {1: [345, 676], 2: [2, 3, 4, 5]}}
		# return un dicc de {calc_line_id: {cris_q: [lot_line_ids]...}
		LotLine = self.env['glass.lot.line']
		items = self.filtered('insulado')
		dict_vals = dict.fromkeys(items.ids)
		for line in items:
			child_nums = [i.get_crystal_numbers() for i in line.child_ids]
			child_nums = zip(*child_nums)
			sub_dict = {}
			for num in child_nums: # child_nums: [(1.2, 1.2),(2.2, 2.2)]
				num = list(map(str, num))
				domain = [('nro_cristal', 'in', num), ('calc_line_id.parent_id', '=', line.id)] + extra_domain
				#FIXME por ahora solo tomo en cuenta q son 2 cristales de insulado, esa cantidad debería salir de la ficha
				lot_lines = LotLine.search_read(domain, ['id']) 
				if only_completes and len(lot_lines) < len(num): 
					continue
				sub_dict.update({num[0].split('.')[0]: [l['id'] for l in lot_lines]})
			dict_vals[line.id] = sub_dict
		return dict_vals # FIXME habilitar el control de limit

	def _get_not_costed_lines(self):
		return self.material_line_ids.filtered('not_cost') 

	def compute_saleprice_from_template(self):
		"""Tengo en cuenta que los cristales insulados no tienen descuadre y son regulares
		PD: si ya existe un cristal con el mismo nro no es necesario crearlo de nuevo
		PD: si ya existe una línea de material con el mismo template_id no es necesario crearlo de nuevo
		PD: Si hay menos nuevos records que los existentes, se eliminan los sobrantes"""

		config = self.env['mtf.parameter.config'].search([], limit=1)
		#tmpl_arenado = config.tmpl_arenado_id
		tmpl_perforacion = config.tmpl_perforacion_id
		#tmpl_biselado = config.tmpl_biselado_id
		tmpl_entalle = config.tmpl_entalle_id
		calc_line_values = {}
		
		# Campo usado para el detalle de servicios vinculados a un cristal templado

		#srv_templates = [st_id for st_id in (tmpl_arenado.id, tmpl_perforacion.id, tmpl_biselado.id, tmpl_entalle.id) if st_id]
		srv_templates = [st_id for st_id in (tmpl_perforacion.id, tmpl_entalle.id) if st_id]

		limit_entalle = max(config.free_limit_entalle, 0) # qty de entalles libres de cobro en crital templado.
		limit_perforacion = max(config.free_limit_perforacion, 0) # qty de perforaciones libres de cobro en cristal templado.

		for rec in self:
			if rec.calculator_id.is_locked:
				raise UserError(u'No es posible grabar una calculadora en estado bloqueado.')
			ctx = dict(self._context or {}, date=rec.calculator_id.order_id.date_order or fields.Date.today())
			not_costed = rec._get_not_costed_lines()
			if not_costed:
				ctx['not_costed_lines'] = not_costed.mapped('template_line_id').ids

			sale_price, line_vals = 0.0, {}
			map_linked_services_amount = dict.fromkeys(['arenado', 'perforacion', 'biselado', 'entalle', 'pulido'], 0.0) 
			
			def update_tmpl_vals(line_vals, template, magnitude=None, perimeter=None):
				tmpl_price, tmpl_line_vals, t_vals = template.with_context(ctx).compute_template(
					base=rec.base1, 
					height=rec.height1, 
					magnitude=magnitude or rec._get_sale_quantity(), # área, perimetro, nro de aplicaciones
					# NOTE quantity debe incluír también la cantidad de cristalesm, en el caso de _get_sale_quantity ya lo hace
					perimeter=perimeter,
					quantity=rec.quantity, # nro de ítems (cristales)
					currency=rec.calculator_id.order_id.currency_id,
					partner=rec.calculator_id.order_id.partner_id)
				#sale_price += tmpl_price
				line_vals.update(tmpl_line_vals)
				return tmpl_price, line_vals

			#calc_template = rec.calculator_id.template_id.with_context(ctx)
			calc_template = rec.calculator_id.template_id
			tmpl_price, line_vals = update_tmpl_vals(line_vals, calc_template)
			sale_price += tmpl_price
			tmpl_pulido = rec.polished_id.mtf_template_id
			tmpl_arenado = rec.arenado_id.mtf_template_id
			tmpl_biselado = rec.biselado_id.mtf_template_id

			# Agregar costos por servicios en calculadora de templados/insulados
			if rec.type_calculator in ('common_product', 'insulado_calculator'):
				all_tmpls = srv_templates + [st for st in [tmpl_pulido.id, calc_template.id] if st]
				if len(all_tmpls) != len(set(all_tmpls)):
					raise UserError('La ficha maestra del producto no puede estar contenida en las fichas de los servicios')

				# # TODO definir si todos los servicios van a ser cobrados, lo cual implica que deberían tener una ficha asociada,
				#        en caso contrario, obviar y seguir.
				# TODO parámettos para
				#if rec.arenado:
				if rec.arenado_id:
					tmpl_price, line_vals = update_tmpl_vals(line_vals, tmpl_arenado, rec.sold_area)
					sale_price += tmpl_price
					map_linked_services_amount['arenado'] += tmpl_price

				# perforacion_qty => catidad excedente de perforaciones solicitadas sobre las perforaciones libres de cobro
				perforacion_qty = rec.perforacion - limit_perforacion
				if perforacion_qty > 0:
					tmpl_price, line_vals = update_tmpl_vals(line_vals, tmpl_perforacion, perforacion_qty * rec.quantity)
					sale_price += tmpl_price
					map_linked_services_amount['perforacion'] += tmpl_price

				if rec.biselado_id:
					perimeter = rec.get_perimeter_biselado()
					tmpl_price, line_vals = update_tmpl_vals(line_vals, tmpl_biselado, perimeter, perimeter)
					sale_price += tmpl_price
					map_linked_services_amount['biselado'] += tmpl_price
				
				# entalle_qty => catidad excedente de entalles solicitados sobre los entalles libres de cobro
				entalle_qty = rec.entalle - limit_entalle
				if entalle_qty > 0:
					tmpl_price, line_vals = update_tmpl_vals(line_vals, tmpl_entalle, entalle_qty * rec.quantity)
					sale_price += tmpl_price
					map_linked_services_amount['entalle'] += tmpl_price

				if tmpl_pulido:
					# cualquier pulido se hace siempre en base al perímetro
					tmpl_price, line_vals = update_tmpl_vals(line_vals, tmpl_pulido, rec.perimeter, rec.perimeter)
					sale_price += tmpl_price
					map_linked_services_amount['pulido'] += tmpl_price

			ins_to_produce, materials, child_vals, material_vals = [], [], [], []

			# Sólo los cristales insulados deberían tener child_ids
			for item in line_vals.items():
				if item[1]['pos_ins_crystal'] and item[1]['to_produce']:
					ins_to_produce.append(item)
				#else:
				materials.append(item)

			ins_to_produce = sorted(ins_to_produce, key=lambda x: x[1]['pos_ins_crystal'])

			nums = rec.get_crystal_numbers()
			mx = max(nums)
			mn = min(nums)

			# FIXME controlar que ésto se realice sólo si el producto de la calculadora pertenece a la categoría de insulados
			# establecida en lso parámetros de configuración y además q la calculadora sea de insulados.
			for i, item in enumerate(ins_to_produce, 1):
				key, value = item
				crystal_num = '%d.%d' % (mn, i) if mn == mx else '%d.%d-%d.%d' % (mn, i, mx, i)
				exist = rec.child_ids.filtered(lambda x: x.crystal_num == crystal_num)
				vals = {
					'product_ins_id': value['product_id'],
					'base1': value['base'],
					'base2': value['base'],
					'height1': value['height'],
					'height2': value['height'],
					'insulado': False,
					'from_insulado': True,
					'descuadre': rec.descuadre,
					'page_number': rec.page_number,
					'lavado': rec.lavado,
					#'arenado': rec.arenado, # TODO clean
					'arenado_id': rec.arenado_id.id or False,
					'polished_id': rec.polished_id.id or False,
					'packed': rec.packed,
					'template': rec.template,
					'entalle': rec.entalle or False,
					'quantity': rec.quantity,
					'crystal_num': crystal_num,
					}
				if exist:
					child_vals.append((1, exist.id, vals))
				else:
					child_vals.append((0, 0, vals))

			if rec.child_ids:
				map_nums = set(map(lambda x: x[2]['crystal_num'], child_vals))
				to_remove = rec.child_ids.filtered(lambda x: x.crystal_num not in map_nums)
				to_remove.unlink()
			
			# Add raw material ítems
			for item in materials:
				key, value = item
				exist = rec.material_line_ids.filtered(lambda x: x.template_line_id.id == key)
				vals = {
					'template_line_id': key,
					'product_id': value['product_id'],
					'required_quantity': value['total_units'],
					}
				if exist:
					material_vals.append((1, exist.id, vals))
				else:
					material_vals.append((0, 0, vals))

			if rec.material_line_ids:
				tmpl_line_ids = set(map(lambda x: x[2]['template_line_id'], material_vals))
				# remover los sobrantes, pero no los que se marcaron como no costeados
				to_remove = rec.material_line_ids.filtered(lambda x: x.template_line_id.id not in tmpl_line_ids and not x.not_cost)
				to_remove.unlink()

			calc_line_values[rec.id] = {
				'child_ids': child_vals or [(5, 0, 0)],
				'material_line_ids': material_vals or [(5, 0, 0)],
				'mtf_sale_price': sale_price,
				'services_amount': sum(map_linked_services_amount.values())
			}
		return calc_line_values


	def _get_sale_quantity(self):
		# Obtiene la cantidad a la venta, sea por perímetro, área o por aplicación de servicio
		calc = self.calculator_id
		magnitude = 0.0
		
		if self.service_item_helper and calc.product_id.allow_pz_sale and calc.product_id.type == 'service':
			if self.service_item_helper == 'perforacion':
				if self.perforacion <= 0:
					raise UserError('Todas las líneas de calculadora deben tener al menos una perforación')
				magnitude = self.perforacion
			elif self.service_item_helper == 'biselado':
				# TODO mover ésta validación a un constraint?
				if self.biselado_id and not self.biselado_sides:
					raise UserError('Debe asignar los lados donde se aplicarán los biselados.')
				#magnitude = self.biselado
			elif self.service_item_helper == 'entalle':
				if self.entalle <= 0:
					raise UserError('Todas las líneas de calculadora deben tener al menos un entalle')
				magnitude = self.entalle
			else:
				raise UserError('Unsupported service_item_helper: ' + self.service_item_helper)
		else:
			return super(GlassSaleCalculatorLine, self)._get_sale_quantity()

		return magnitude * self.quantity

class MtfRequisitionMaterialLine(models.Model):
	_name = 'mtf.requisition.material.line'
	_description = u'Línea de requerimiento de plantilla maestra'
	
	calculator_line_id = fields.Many2one('glass.sale.calculator.line', 
		string=u'Línea de calculadora asociada', required=True, ondelete='cascade')
	#requisition_id = Many2one('mtf.requisition',string=u'Requisición')
	template_line_id = fields.Many2one('mtf.template.line', 
		help=u'Línea de ficha maestra asociada', readonly=True)
	product_id = fields.Many2one(related='template_line_id.product_id', readonly=True)
	default_code = fields.Char(related='template_line_id.default_code', readonly=True)
	model  = fields.Char(related='template_line_id.model', readonly=True)
	uom_id = fields.Many2one(related='template_line_id.uom_id',string='Unidad', readonly=True)
	to_produce = fields.Boolean(related='template_line_id.to_produce', readonly=True)
	required_quantity = fields.Float('Cantidad Requerida', digits=(12,4), 
		help=u'Cantidad de material necesario para fabricar la cantidad de cristal Insulado requerida')
	not_cost = fields.Boolean('No Costear', 
		help=u'Marque ésta opción si no desea omitir esta línea, suele darse cuando el cliente trae materiales propios.')

	def write(self,values):
		for rec in self:
			if rec.calculator_line_id.calculator_id.is_locked:
				raise UserError(u'No es posible editar la línea de requerimiento si la calculadora está bloqueada.')
			# send message:
			if values.get('not_cost',False):
				msg = u'Se estableció el producto de ficha maestra <a href=# data-oe-model=product.product data-oe-id=%d>%s</a> como material propio y no costeado.'%(rec.product_id.id,rec.product_id.name)
				rec.calculator_line_id.calculator_id.order_id.message_post(body=msg)
		return super(MtfRequisitionMaterialLine, self).write(values)