# -*- coding: utf-8 -*-
from odoo import models, fields, api
import odoo.addons.decimal_precision as dp
from odoo.tools import float_compare, float_round
from odoo.exceptions import UserError, ValidationError
from odoo.addons.glass_production_order.models.glass_stage import FORBIDDEN_STAGES

#FREE_WRITE_FIELDS = {'int_code', 'stock', 'measure', 'stage_ids', 'ins_cr_1_stage_ids', 'ins_cr_2_stage_ids', 'active'}

class MasterTemplate(models.Model):
	_name = 'mtf.template'
	_inherit = ['mail.thread']
	_description ='Ficha maestra'
	
	product_id = fields.Many2one('product.product',ondelete='restrict', 
		string='Producto', copy=False, track_visibility='onchange')
	code = fields.Char(u'Código de producto',related='product_id.default_code')
	measure = fields.Float('Medida')
	uom_id = fields.Many2one(related='product_id.uom_id',string='Unidad')
	weight = fields.Float('Peso',related='product_id.weight')
	quantity = fields.Float('Cantidad',default=1.0)
	stock = fields.Float('Stock')
	int_code = fields.Char(u'Código Internacional')

	def _get_default_config(self):
		config = self.env['mtf.parameter.config'].search([],limit=1)
		if not config:
			raise UserError(u'No se han encontrado los parámetros de configuración para Fichas Maestras')
		return config.id

	config_id = fields.Many2one('mtf.parameter.config', 
		string='Config', default=_get_default_config, ondelete='restrict')
	mat_prima_location_id = fields.Many2one(related='config_id.mat_prima_location_id')

	# definir esta mrd...
	# cr_tmp -> Cristal templado
	# cr_ins -> Cristal Insulado
	# cr_lam -> Cristal Insulado
	# carp_met -> Carpintería metálica
	# peg_sil -> Pegado de Silicona
	# arm_pan -> Armado de paneles
	
	# TODO FIXME el ítem de services es provisional, hasta definir el tama de los parámetros de costos, márgenes etc
	applied_on = fields.Selection([
		('cr_tmp','Cristal Templado'), 
		('cr_ins','Cristal Insulado'), 
		('cr_lam','Cristal Laminado'), 
		('carp_met',u'Carpintería metálica'), 
		('peg_sil','Pegado de Silicona'),
		('services', 'Servicios')], string='Aplicación de Ficha', default='cr_tmp')

	# TMP field, no se sabe aún si se mantendrá:
	service_type = fields.Selection([
		('srv_insulado', 'Servicio de insulado'), 
		('srv_corte_lam', 'Corte-laminado'), 
		('srv_corte_lav_lam', 'Corte-lavado-laminado'), 
		('srv_pegado', 'Pegado/Producido'), 
		('srv_arenado', 'Arenado'), 
		('srv_perforacion', 'Perforaciones'), 
		('srv_canto_pulido', 'Canto Pulido Brillante'),
		('srv_biselado', 'Biselado'), 
		('srv_entalle', 'Entalle'), 
		], string='Tipo de servicio')

	# NOTE sujeto a modificación, por ahora se aplica a servicios en específico
	cost_unique = fields.Boolean(u'Costo único', compute='_compute_cost_unique', readonly=True)

	cost_mp = fields.Float('Costo Materia Prima', digits=(10, 6))
	cost_insumos = fields.Float('Gastos insumos rep. y sum.', digits=(10, 6))
	amount_total = fields.Float('Total Costo Materiales', digits=(10, 6)) #
	area_total = fields.Float(u'Area Total', digits=(10, 4))
	perimeter_total = fields.Float(u'Perímetro total', digits=(10, 4))
	manpower = fields.Float('Mano de Obra',default=0.0, digits=(10, 6))
	services = fields.Float('Servicios',default=0.0, digits=(10, 6))
	depreciacion = fields.Float(u'Depreciación',default=0.0, digits=(10, 6))
	cost_safe = fields.Float(u'Seguros',default=0.0, digits=(10, 6))
	tributes = fields.Float(u'Tributos',default=0.0, digits=(10, 6))
	
	total_indirect_costs = fields.Float('Total indirectos de Fabricación', default=0.0, digits=(10, 6))
	total_cost_manufacturing = fields.Float('Costo total de Fabricación', default=0.0, digits=(10, 6))
	cost_adm_sales = fields.Float('Gastos Adm. y ventas', default=0.0, digits=(10, 6))
	total_cost = fields.Float('Costo total', default=0.0, digits=(10, 6))

	util_percent = fields.Float('Margen de ganancia(%)',default=35.00, 
		digits=(10, 6), track_visibility='onchange')
	sale_price  = fields.Monetary('Costo de Venta', currency_field='currency_id', 
		default=0.0, track_visibility='onchange')
	
	# Modelos/Precios esta vaina la dejaré comentada por siaca
	# qty_unit_costed = fields.Float('Unidades costeadas',default=1.0,help=u'Unidades que se costean.')
	# currency_id = fields.Many2one('res.currency',string='Moneda')
	# net_price_avg = fields.Float('Precio Neto Promedio')
	# flete_amount = fields.Float('Flete',default=0.0)
	# uom_price = fields.Float('Precio',default=0.0,help=u'Precio de "Unidades costeadas" en la unidad incluyendo el flete.')
	# min_qty_sale = fields.Float('Venta min',default=0.0,help=u'Venta mínima de éste producto')

	# campos solo referanciales , no utilizar para otras cosas 
	margin_obj = fields.Float('Margen Obj.', track_visibility='onchange')
	# currency_id = fields.Many2one('res.currency', string='Moneda', 
	# 	default=lambda self: self.env.user.company_id.currency_id.id, 
	# 	track_visibility='onchange')
	currency_id = fields.Many2one('res.currency', string='Moneda', 
		required=True, track_visibility='onchange')

	# cost_parameter_id = fields.Many2one('mtf.template.cost.parameter', 
	# 	string='Parámetros de costos', readonly=True)

	# Parámetros por unidad (mostadros en modo wizard)
	manpower_by_unit = fields.Float('Mano de obra', default=0.0)
	services_by_unit = fields.Float('Servicios', default=0.0)
	depreciacion_by_unit = fields.Float(u'Depreciación', default=0.0)
	cost_safe_by_unit = fields.Float('Seguros', default=0.0)
	tributes_by_unit = fields.Float('Tributos', default=0.0)
	cost_adm_sales_by_unit = fields.Float('Gastos Adm. y ventas', default=0.0)
	util_percent_by_unit = fields.Float('Margen de ganancia(%)', default=35.0)
	discount_a = fields.Float(string='Descuento A (%)', default=0.0)
	discount_b = fields.Float(string='Descuento B (%)', default=0.0)
	discount_c = fields.Float(string='Descuento C (%)', default=0.0)
	discount_d = fields.Float(string='Descuento D (%)', default=0.0)
	#END

	discount_applied_a = fields.Float('Desc. aplicado A', compute='_get_discount_amount')
	discount_applied_b = fields.Float('Desc. aplicado B', compute='_get_discount_amount')
	discount_applied_c = fields.Float('Desc. aplicado C', compute='_get_discount_amount')
	discount_applied_d = fields.Float('Desc. aplicado D', compute='_get_discount_amount')
	
	ref_product_id = fields.Many2one(related='product_id', readonly=True)
	ref_uom_id = fields.Many2one(related='uom_id', readonly=True)
	ref_total_cost = fields.Float(related='total_cost', readonly=True)
	ref_util_percent = fields.Float(related='util_percent', readonly=True, track_visibility=False)
	ref_sale_price = fields.Monetary(related='sale_price', readonly=True, track_visibility=False)

	line_ids = fields.One2many('mtf.template.line','template_id',string='Detalle',copy=True)
	stage_ids = fields.Many2many('glass.stage', 
		relation='mtf_template_glass_rel', column1='template_id', column2='stage_id', 
		domain=[('name', 'not in', list(FORBIDDEN_STAGES))], string='Procesos')

	# registra etapas sólo para el cristal de insulado 1
	ins_cr_1_stage_ids = fields.Many2many('glass.stage', 
		relation='mtf_template_stage_cr1_rel', column1='template_id', column2='stage_id', 
		domain=[('name', 'not in', list(FORBIDDEN_STAGES))], string='Procesos Ins. Cristal 1')
	# registra etapas sólo para el cristal de insulado 2
	ins_cr_2_stage_ids = fields.Many2many('glass.stage', 
		relation='mtf_template_stage_cr2_rel', column1='template_id', column2='stage_id', 
		domain=[('name', 'not in', list(FORBIDDEN_STAGES))], string='Procesos Ins. Cristal 2')

	active = fields.Boolean('Activo',default=True)

	# Minimos 
	min_base   = fields.Integer(default=50)
	min_height = fields.Integer(default=50)
	min_movil  = fields.Integer(default=0)
	min_sobre  = fields.Integer(default=0)
	min_alfe   = fields.Integer(default=0)
	# standard
	std_base   = fields.Integer(default=2000, track_visibility='onchange')
	std_height = fields.Integer(default=3000, track_visibility='onchange')
	std_movil  = fields.Integer(default=0)
	std_sobre  = fields.Integer(default=0)
	std_alfe   = fields.Integer(default=0)
	# maximos
	max_base   = fields.Integer(default=3200)
	max_height = fields.Integer(default=3600)
	max_movil  = fields.Integer(default=0)
	max_sobre  = fields.Integer(default=0)
	max_alfe   = fields.Integer(default=0)

	# TODO ordenar:
	_sql_constraints = [(
	'check_measures','CHECK ((max_base>=std_base and std_base>=min_base and min_base>=0) and (max_height>=std_height and std_height>=min_height and min_height>=0) and (max_movil>=std_movil and std_movil>=min_movil and min_movil>=0) and (max_sobre>=std_sobre and std_sobre>=min_sobre and min_sobre>=0) and (max_alfe>=std_alfe and std_alfe>=min_alfe and min_alfe>=0))',u'Las medidas mínimas, standard y máximas deben corresponder a su definición (Longitudes máximas superiores a standard y longitudes standard supreriores a mínimas) y todas deben ser mayores o iguales a cero.')]

	_rec_name = 'product_id'

	@api.depends('sale_price', 'discount_a', 'discount_b', 'discount_c', 'discount_d', 'currency_id')
	def _get_discount_amount(self):
		for rec in self:
			applied_a = rec.sale_price * (1 - ((rec.discount_a or 0.0) / 100.0))
			applied_b = rec.sale_price * (1 - ((rec.discount_b or 0.0) / 100.0))
			applied_c = rec.sale_price * (1 - ((rec.discount_c or 0.0) / 100.0))
			applied_d = rec.sale_price * (1 - ((rec.discount_d or 0.0) / 100.0))

			rec.discount_applied_a = rec.currency_id and rec.currency_id.round(applied_a) or applied_a
			rec.discount_applied_b = rec.currency_id and rec.currency_id.round(applied_b) or applied_b
			rec.discount_applied_c = rec.currency_id and rec.currency_id.round(applied_c) or applied_c
			rec.discount_applied_d = rec.currency_id and rec.currency_id.round(applied_d) or applied_d

	@api.depends('applied_on', 'service_type')
	def _compute_cost_unique(self):
		for tmpl in self:
			tmpl.cost_unique = tmpl.applied_on == 'services' and tmpl.service_type in ('srv_perforacion', 'srv_entalle')


	@api.onchange('product_id')
	def _onchange_product_id(self):
		if self.product_id.type == 'service':
			self.applied_on = 'services'

	@api.constrains('product_id', 'applied_on')
	def _verify_constrains_product_id(self):
		ins_categ = self.config_id.category_insulados_id
		for rec in self:
			if self.search_count([('product_id','=',rec.product_id.id)]) > 1:
				raise ValidationError(u'El producto %s ya fue asignado a una ficha maestra.'%rec.product_id.name)
			if rec.product_id.categ_id != ins_categ and rec.applied_on=='cr_ins':
				raise ValidationError(u'El producto %s no corresponde a la categoría de cristales insulados configurada.'%rec.product_id.name)

			# TODO evaluar si carpintería se cuenta como servicio
			if rec.product_id and rec.product_id.type == 'service' and rec.applied_on != 'services':
				raise ValidationError('En los productos de tipo servicio, el tipo de aplicación debe ser de tipo "Servicios"')

	# verificar que si es insulado, debe tener 2 cristales de insulados
	@api.constrains('line_ids', 'applied_on')
	def _verify_line_ids(self):
		for rec in self:
			if rec.applied_on == 'cr_ins': # en insulados deben haber por lo menos 2 cristales
				crystal_lines = rec.line_ids.filtered('pos_ins_crystal')
				if len(crystal_lines) not in (2, 3): # min 2 max 3 cristales
					raise ValidationError(u'Debe especificar como mínimo 2 cristales en su lista de materiales para cristales insulados.')

	# def write(self, values):
	# 	# HACK 
	# 	if self._context.get('recompute_mode'):
	# 		return True
	# 	force_write = self._context.get('force_write', False)
	# 	if not force_write and any(field not in FREE_WRITE_FIELDS for field in values.keys()):
	# 		raise UserError(u'Para guardar los cambios, se debe recomputar la ficha maestra.')
	# 	return super(MasterTemplate, self).write(values)

	def action_define_template_costs(self):
		self.ensure_one()
		return {
			'name':'Conceptos de costos',
			'type': 'ir.actions.act_window',
			'res_id': self.id,
			'res_model': self._name,
			'view_mode': 'form',
			'view_id': self.env.ref('master_template_fullglass.master_template_cost_details_view_form', False).id,
			'target': 'new',
		}

	def action_duplicate(self):
		module = __name__.split('addons.')[1].split('.')[0]
		wiz = self.env['mtf.confirm.update.template'].create({
			'template_id': self.id,
			'mode': 'clone_template',
			})
		return {
			'name': u'Duplicar ficha maestra',
			'res_id': wiz.id,
			'type': 'ir.actions.act_window',
			'res_model': wiz._name,
			'view_id': self.env.ref('%s.mtf_duplicate_template_view_form' % module).id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			}

	
	def save_dummy(self):
		return {'type': 'ir.actions.act_window_close'}

	def action_compute_template(self):
		self.ensure_one()
		# Computar plantilla con los vlaores de plantilla por defecto
		if self.cost_unique:
			magnitude = 1.0
		else:
			magnitude =  float(self.std_base * 2 + self.std_height * 2) / 1000.0 if self.product_id.allow_metric_sale else float(self.std_base * self.std_height) / 1000000.0# por standard , siempre srá el área


		sale_price, line_vals, template_vals = self.compute_template(
			base=self.std_base, 
			height=self.std_height,
			magnitude=magnitude,
			)
		
		if float_compare(sale_price, self.sale_price, precision_digits=2) != 0:
			module = __name__.split('addons.')[1].split('.')[0]
			wiz = self.env['mtf.confirm.update.template'].create({
				'template_id': self.id,
				'current_sale_price': self.sale_price,
				'new_sale_price': sale_price,
				'mode': 'update_template_values',
				})
			return {
				'name': u'Confirmar actualización de Ficha Maestra',
				'res_id': wiz.id,
				'type': 'ir.actions.act_window',
				'res_model': wiz._name,
				'view_id': self.env.ref('%s.mtf_confirm_update_template_view_form' % module).id,
				'view_mode': 'form',
				'view_type': 'form',
				'target': 'new',
				'context':{'vals': template_vals}
				}
		self.write(template_vals)
		return {}

	def compute_template(self, base, height, magnitude, perimeter=None, quantity=1.0, currency=None, partner=None):
		# Process total lines
		# params:
		# @base : int or float, crystal base in mm, 
		# @height: int or float, crystal height in mm, 
		# @quantity: int of crystal count
		# @currency : Moneda a la en la cual se expresará el precio de venta calculado
		# @partner : Partner al cual se evaluará la categoría (para posibles decuentos según su categoría)
		# NOTE 
		# Para el caso del perímetro no se puede admitir que siempre será la suma de los 4 lados de un cristal,
		# ya que en los servicios se puede aplicar dicho servicio a ciertos lados, e.g. 1,2,3 sólo sumará los lados 1,2 y 3 
		# como perímetro total, para indicar que se debe usar un perímetro personalizado (como en el caso de los servicios), 
		# éste deberá indicarse en el context con la llave 'force_perimeter' (el valor debería ser el mismo que el argumento magnitude),
		# para cualquier otro caso, se asume el perímetro por defecto

		if not self:
			return 0.0, {}, {}
		#quantity = quantity if quantity > 0.0 else 1.0
		
		# TODO Mejorar/ordenar ésta validación
		if self.cost_unique and not self.product_id.allow_pz_sale:
			raise UserError('El producto %s deber tener debe ser un Servicio por cantidad de aplicaciones.' % self.product_id.name)

		if self.product_id.allow_pz_sale and not self.cost_unique:
			raise UserError('La ficha "%s" debe establecerse como un servicio de entalle, biselado o perforaciones.' % self.product_id.name)

		ctx = dict(self._context or {})
		not_costed_lines = ctx.get('not_costed_lines', []) # si no se costea desde calculadora
		lines = self.line_ids.filtered(lambda l: not l.not_costed)
		line_vals = dict.fromkeys(lines.ids)

		base_in = base
		height_in = height

		if base_in > self.max_base or base_in < self.min_base:
			raise UserError('La base ingresada debe estar entre los rangos de %d a %d' % (self.min_base, self.max_base))
		if height_in > self.max_height or height_in < self.min_height:
			raise UserError('La altura ingresada debe estar entre los rangos de %d a %d' % (self.min_height, self.max_height))

		# 1) Compute lines
		for line in lines:
			line_vals[line.id] = line._compute_line(
				base=base_in, 
				height=height_in, 
				perimeter=perimeter, 
				quantity=magnitude if self.cost_unique else quantity,
				special_price=line.id in not_costed_lines)

		values = line_vals.values()
		mp, insumos = [], []
		for val in values:
			if val['raw_material']:
				mp.append(val['amount_untaxed'])
			else:
				insumos.append(val['amount_untaxed'])
		
		area_total = float(base_in * height_in) / 1000000.0 * quantity# ? FIX?
		cost_mp = sum(mp)
		cost_insumos = sum(insumos)
		amount_total = cost_mp + cost_insumos
		
		# a) aplicar conceptos de gastos
		app_on = self.applied_on if self.applied_on != 'services' else self.service_type
		# manpower     = getattr(self.config_id, 'manpower_' + app_on, 0.0) * magnitude
		# services     = getattr(self.config_id, 'services_' + app_on, 0.0) * magnitude # FIXME ?
		# depreciacion = getattr(self.config_id, 'depreciacion_' + app_on, 0.0) * magnitude
		# cost_safe    = getattr(self.config_id, 'cost_safe_' + app_on, 0.0) * magnitude
		# tributes     = getattr(self.config_id, 'tributes_' + app_on, 0.0) * magnitude
		# cost_adm_sales = getattr(self.config_id, 'cost_adm_sales_' + app_on, 0.0) * magnitude
		# util_percent = getattr(self.config_id, 'util_percent_' + app_on, 0.0)

		manpower     = (self.manpower_by_unit or 0.0) * magnitude
		services     = (self.services_by_unit or 0.0) * magnitude # FIXME ?
		depreciacion = (self.depreciacion_by_unit or 0.0) * magnitude
		cost_safe    = (self.cost_safe_by_unit or 0.0) * magnitude
		tributes     = (self.tributes_by_unit or 0.0) * magnitude
		cost_adm_sales = (self.cost_adm_sales_by_unit or 0.0) * magnitude
		util_percent = (self.util_percent_by_unit or 0.0)

		total_indirect_costs = (manpower + services + depreciacion + cost_safe + tributes)
		total_cost_manufacturing = total_indirect_costs + (amount_total)
		total_cost = total_cost_manufacturing + cost_adm_sales
		
		# Agregar como márgen de ganancia el monto adicional por contar con más de 1 ítem de servicio de 
		# entalle o perforaciones en la misma línea. 
		if self.cost_unique and quantity > 1:
			if self.service_type == 'srv_perforacion':
				util_percent += self.config_id.perforacion_extra_gt_one or 0.0
			elif self.service_type == 'srv_entalle':
				util_percent += self.config_id.entalle_extra_gt_one or 0.0
			
		# TODO A solicitud de N. Castillo , cuando el lado mayor de un biselado o arenado supere los 
		# 120 cm , se le cobreará un adicional, ésto será expresado en un porcentaje adicional al márgen de venta
		if app_on == 'services' and self.service_type in ('srv_arenado', 'srv_biselado') and max(base_in, height_in) > 1200:
			util_percent += self.config_id.charge_extra_measures or 0.0

		sale_price = (total_cost / (1.0 - (util_percent / 100.0)))

		#sale_price = total_cost*(1 + (util_percent/100)) # al parecer es esto xD
		
		# 3) aplicar descuento por categoría
		partner_category = partner and partner.property_product_pricelist.categ_client or False
		if partner_category:
			# NOTE 
			# El descuento aún no se aplicará desde aquí, ya que al mandar el precio descontado a la línea de pedido
			# de venta, se le aplicará un doble descuento (con el descuento propio de la línea de pedido).
			# Entonces, se omitirá el descuento de los parámetros de ficha maestra y prevalecerá el descuento 
			# que aplique la línea de pedido de venta. Esto debe ser tomado en cuenta cuando se animen a aplicar 
			# el descuento desde éste cómputo de ficha maestra. 
			
			# apply discounts:
			# discount = getattr(self, 'discount_' + partner_category.lower(), 0.0)
			# sale_price *= (1 - (discount / 100.0))
			pass

		# 4) conversión a moneda y redondeo
		if currency and currency.id != self.currency_id.id:
			# TODO FIXME es correcto usar el tipode cambio comercial?
			ex_type = self.currency_id.with_context(use_special_extype=True)._get_conversion_rate(self.currency_id, currency)
			sale_price = currency.round(sale_price * ex_type)
		else:
			sale_price = self.currency_id.round(sale_price)

		template_vals = {
			'cost_mp': cost_mp,
			'cost_insumos': cost_insumos,
			'amount_total': amount_total,
			'manpower': manpower,
			'services': services,
			'depreciacion': depreciacion,
			'cost_safe': cost_safe,
			'tributes': tributes,
			'cost_adm_sales': cost_adm_sales,
			'util_percent': util_percent,
			'total_indirect_costs': total_indirect_costs,
			'total_cost_manufacturing': total_cost_manufacturing,
			'total_cost': total_cost,
			'sale_price': sale_price,
			'area_total': area_total,
			'line_ids':[(1, k, v) for k, v in line_vals.items()]
		}
		
		return sale_price, line_vals, template_vals

	def update_process_lines(self):
		line_ids = self.line_ids.ids
		to_remove = self.line_process_ids.filtered(lambda x: x.template_line_id.id not in line_ids)
		to_remove.unlink()
		existing = self.line_process_ids.mapped('template_line_id').ids
		new_values = [(0, 0, {'template_line_id':l}) for l in line_ids if l not in existing]
		if any(new_values):
			self.line_process_ids = new_values

	@api.model
	def get_template_by_product(self, product, raise_if_not_found=True, exclude_archive=True):
		template = self.with_context(active_test=exclude_archive).search([('product_id', '=', product.id)], limit=1)
		if not template and raise_if_not_found:
			raise UserError(u'No se encontró una ficha maestra para el producto %s, solicite a su administrador la creación de una.')
		return template

class MasterTemplateLine(models.Model):
	_name = 'mtf.template.line'
	_description = u'Línea de ficha maestra'

	template_id  = fields.Many2one('mtf.template', ondelete='cascade')
	pos_ins_crystal = fields.Selection([
		(1, 'Cristal 1'), 
		(2, 'Cristal 2'), 
		(3, 'Cristal 3')], string=u'Posición cristal', help=u'Posición en cristal insulado')
	to_produce = fields.Boolean(u'Producción', 
		help="Determina si el cristal debe ser producido")
	not_costed = fields.Boolean('No Costeado', 
		help=u'Destermina el ésta línea de ficha será costeada o no') 
	product_id = fields.Many2one('product.product', string='Producto')
	default_code = fields.Char(related='product_id.default_code', string=u'Cód. producto')
	model  = fields.Char(string='Modelo',compute='_compute_model')
	uom_id = fields.Many2one(related='product_id.uom_id', string='Unidad')
	unit_price = fields.Float('Precio Unitario',digits=dp.get_precision('Product Price'),default=0.0)
	un_price_special = fields.Float('Precio Unitario Mat. propio', 
		digits=dp.get_precision('Product Price'), default=0.0, 
		help=u'Costo unitario si el material es propiedad del cliente')
	uom_quantity = fields.Float('Cantidad', digits=(12, 6), default=0.0)
	fixed  = fields.Boolean('Fijo')
	base   = fields.Integer(string='Base')
	inc_b = fields.Float('IncB',default=0.0, 
		help=u'Descuento en la medida de la base.')
	height = fields.Integer(string='Altura')
	inc_h = fields.Float('IncA', 
		default=0.0, 
		help=u'Descuento en la medida de la altura.')
	cut = fields.Char('Corte') # free text
	raw_material = fields.Boolean('Es materia Prima')
	depending_on = fields.Selection(selection=[
		('b', 'Base'),('h','Altura'), 
		('bxh', 'Base x Altura'), 
		('p',u'Perímetro'), 
		('vxp',u'Volumen x Perímetro')], string=u'En función de')
	location = fields.Selection(selection=[
		('marco', 'Marco'), 
		('hoja_fija', 'Hoja Fija'),
		('hoja_movil', u'Hoja Móvil'),
		('sobreluz', 'Sobreluz'),
		('alteizar', 'Alteizar'),
		('cristal', 'Cristal')], string=u'Ubicación')
	position = fields.Selection(selection=[
		('left', 'Izquierda'), 
		('right', 'Derecha'), 
		('t_horizontal', u'Travesaño Horizontal'), 
		('t_verical', u'Travesaño Vertical'), 
		('top_bottom', 'Arriba/Abajo'), 
		('left_right', 'Izquierda/Derecha'), 
		('4_sides', 'Cuatro Lados')], string='Posición')
	cut_type = fields.Selection(selection=[
		('revestimiento', 'Material de Revestimiento'), 
		('accesorios_panel', 'Accesorios para Panel'), 
		('instalacion_insumos', u'Insumos de Instalación'), 
		('mano_obra', 'Mano de Obra'), 
		('instalacion_equipos', u'Equipos de Instalación'), 
		('servicios_terceros', 'Servicios de Terceros'), 
		('insumos_proceso', 'Insumos con proceso')], string='Tipo')
	waste_percent= fields.Float('% Merma',default=0.0)

	# es producto spacer
	is_spacer = fields.Boolean('Separador')
	application = fields.Char(u'Aplicación') # free text
	total_units    = fields.Float('Total Unidades', digits=(12,4), default=0.0)
	amount_untaxed = fields.Float('Total Costo sin IGV', digits=(12,4), default=0.0)

	# si es fijo va en función a nada
	@api.onchange('fixed')
	def _onchange_fixed(self):
		if self.fixed: self.depending_on = False
			
	@api.onchange('product_id')
	def _onchange_product_id(self):
		# por mientras...
		std_price = self.product_id.standard_price
		self.unit_price = std_price 
		self.un_price_special = std_price

	@api.onchange('depending_on')
	def _onchange_depending_on(self):
		if self.depending_on:
			self.base = self.template_id.std_base or 0
			self.height = self.template_id.std_height or 0

	@api.constrains('base', 'height', 'depending_on')
	def _verify_base_height(self):
		for rec in self:
			if rec.depending_on and (not rec.base or not rec.height):
				raise ValidationError(u'Error de validación!\nCuando agrega una línea en función de alguna dimensión (perímetro,base por altura, etc) debe asignar una base y una altura a dicha línea.')
			if not rec.depending_on and (rec.base or rec.height):
				raise ValidationError(u'Error de validación!\nUd agregó una base y/o altura a una línea que no depende de dichas dimensiones.\nQuite dichos valores para poder continuar.')

	@api.one
	@api.depends('product_id')
	def _compute_model(self):
		attr = self.product_id.atributo_ids.filtered(lambda x: x.atributo_id.id==4)
		self.model = attr[0].valor_id.name if any(attr) else ''

	def _compute_line(self, base, height, perimeter=None, quantity=1.0, special_price=False):
		self.ensure_one()
		# params
		# base & height -> para critales cuya venta sea por área.
		# perímeter , -> Magnitud en la que se basará el costeo, puede ser área, perímetro o cantidad de ítems
		# (base & height) & perimeter con excluyentes.
		# quantity -> Cantidad de cristales a costear.
		
		total_units = 0.0
		depending_on = self.depending_on

		#base = (base-self.inc_b) if depending_on else False
		base = base if depending_on else False
		height = (height - self.inc_h) if depending_on else False
		perimeter = float(base * 2 + height * 2) / 1000.0 if perimeter is None else perimeter 
		
		# NOTE TODO Para las fichas de costo por aplicación de servicio, 
		# todos los ítems deben ser fijos, ya que no dependen de las dimensiones del cristal
		# por ejemplo, para 4 entalles, no importa la dimensión del cristal; 
		# de todas formas validar con Nicolás ésta premisa
		# if self.template_id.cost_unique and not self.fixed:
		# 	raise UserError('La fichas con costo único deben tener todas las líneas en "Fijo".')
		
		if self.fixed:
			total_units = self.uom_quantity
		elif depending_on == 'bxh' and base and height:
			total_units = float(base * height) / 1000000.0 * self.uom_quantity
		elif depending_on == 'b' and base:
			total_units = float(base) / 1000.0 * self.uom_quantity
		elif depending_on == 'h' and height:
			total_units = float(height) / 1000.0 * self.uom_quantity
		elif depending_on == 'p' and base and height:
			# TODO FIXME validar con Nicolás si éste cálculo es correcto:
			total_units = perimeter * self.uom_quantity
			#total_units = float(base * 2 + height * 2) / 1000.0 * self.uom_quantity

		elif depending_on == 'vxp' and base and height:
			spacer = self.template_id.line_ids.filtered(lambda x: x.is_spacer and x.model and not x.not_costed)
			separator = 0.0
			if spacer:
				spacer = spacer[0]
				try:
					separator = float(''.join([i for i in spacer.model if i.isnumeric() or i == '.']))
				except TypeError:
					pass
			
			# CLEANME
			#total_units = separator * self.inc_b * float(base * 2 + height * 2) / 1000000.0 * self.uom_quantity
			total_units = separator * self.inc_b * perimeter / 1000.0 * self.uom_quantity
		
		# tmp merma le agrego al total de unidades la cantidad de merma o desperdicio:
		total_units = total_units * (1 + (self.waste_percent / 100.0)) * quantity
		# total_units = float_round(total_units * (1 + (self.waste_percent / 100.0)) * quantity, precision_rounding=0.0001)
		#total_units = total_units/(1-(self.waste_percent/100)) # revisar esta vaina
		mp_loc = self.template_id.config_id.mat_prima_location_id
		# por ahora todo lo q no esta en materia primas va a gastos e insumos
		#ig_loc = config.gastos_insumos_location_id 
		if not mp_loc:
			raise UserError(u'No se ha encontrado la configuración para las ubicaciones de productos de Ficha Maestra.')
		unit_price = self.un_price_special if special_price else self.unit_price
		return {
			'product_id':self.product_id.id,
			'base': base,
			'height': height,
			'fixed': self.fixed,
			'total_units': total_units,
			#'amount_untaxed': (total_units * unit_price)/1.18,
			'amount_untaxed': total_units * unit_price,
			'pos_ins_crystal': self.pos_ins_crystal,
			'to_produce': self.to_produce,
			#'raw_material':bool(self.product_id.property_stock_inventory==mp_loc)
			# TODO: provisionalmente se define un producto como materia prima si va a producción o si forma parte de un insulado: 
			'raw_material': bool(self.to_produce or self.pos_ins_crystal)
		}

