# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError
from itertools import groupby

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	@api.constrains('before_invoice')
	def _verify_templates_in_calc_lines(self):
		# NOTE La característica de factura adelantada no es aplicable a ningún pedido con ficha maestra.
		for order in self:
			if not order.before_invoice:
				continue
			bad_lines = order.order_line.mapped('calculator_id').filtered('template_id')
			if bad_lines:
				raise ValidationError(u'Las siguientes líneas de pedido tienen calculadoras asociadas a fichas maestras:\n%s\nSi desea hacer un pedido con factura adelantada debe remover dichas líneas'%'\n- '.join(bad_lines.mapped('product_id.name')))

	def action_create_services_material_picking(self):
		conf = self.env['mtf.parameter.config'].search([], limit=1)
		pick_type = conf.pick_type_raw_material_id
		pick_motive = conf.pick_traslate_motive_raw_id
		if not pick_type:
			raise UserError('No se encontró la configuración para el tipo de operación de materias primas.')
	
		if not pick_motive:
			raise UserError('No se encontró la configuración para el motivo de traslado del picking de material de terceros.')


		wiz = self.env['mtf.services_material.wizard'].sudo().create({
			'pick_type_id': pick_type.id,
			'pick_motive_id': pick_motive.id,
			'sale_order_id': self.id,
		})

		return {
			'name': 'Crear ingreso a Alm. de terceros',
			'res_id': wiz.id,
			'type': 'ir.actions.act_window',
			'res_model': 'mtf.services_material.wizard',
			'view_mode': 'form',
			'target': 'new',
		}


	def action_confirm(self):
		# Check type order consistence:
		for order in self:
			for l in order.order_line:
				type_calc = l.calculator_id.type_calculator

				if type_calc and type_calc != order.order_line[0].calculator_id.type_calculator:
					raise UserError('Todas las líneas de pedido de compra deben tener el mismo tipo de calculadora.')
				
				if order.type_sale == 'production' and type_calc and type_calc not in ('common_product', 'insulado_calculator'):
					raise UserError('Si el tipo de venta es "Producción", todas las calculadoras de pedido deben ser de tipo insulado o de cristales templados')

				if order.type_sale == 'services' and type_calc and type_calc != 'service':
					raise UserError('Si el tipo de venta es "Servicios", todas las calculadoras de pedido deben ser de tipo servicios.')

		return super(SaleOrder, self).action_confirm()



	# Entrega de servicios:
	def delivery_crystals_services(self):
		self.ensure_one()
		if self.type_sale != 'services':
			return {}
		
		self.validate_sale_pay_terms('delivery_products')
		ops = self.mapped('op_ids').filtered(lambda o: o.state != 'returned' and o.type_sale == 'services')
		seq = self.env['glass.order.config'].search([], limit=1).seq_def_delivery_services_id
		
		if not seq:
			raise UserError('No ha configurado la secuencia por defecto para las guías de remisión de servicios.')

		wiz = self.env['glass.delivery.picking.crystals'].create({
			'glass_order_ids': [(6, 0, ops.ids)],
			'picking_id': False,
			'serie_guia_id': seq.id,
			'sale_order_id': self.id,
		})
		if ops: 
			wiz.get_crystals_list()
		return {
			'name': 'Entrega de Servicios',
			'res_id': wiz.id,
			'type': 'ir.actions.act_window',
			'res_model': wiz._name,
			'view_id': self.env.ref('glass_production_order.glass_delivery_services_crystals_view_form').id,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': dict(self._context or {}, ops_domain=[('id', 'in', ops.ids)]),
			}


class SaleOrderLine(models.Model):
	_inherit = 'sale.order.line'
	
	def _prepare_calculator_vals(self):
		self.ensure_one()
		res = super(SaleOrderLine, self)._prepare_calculator_vals()
		product = self._context.get('defined_product', self.product_id)
		categ_ins = self.env['mtf.parameter.config'].search([], limit=1).category_insulados_id
		template = self.env['mtf.template'].search([('product_id', '=', product.id)], limit=1)
		before_invoice = self.order_id.before_invoice
		
		# Cambios, ahora los servicios pueden ser hechos desde Ficha Maestra
		# Si el producto tiene vinculada una ficha maestra, se prioriza la ficha
		# 
		
		# Cetegoría de insulados
		if categ_ins and product.categ_id == categ_ins:
			if not template: # si es insulado debe tener ficha
				raise UserError(u'No se ha encontrado una ficha maestra para el producto %s.\nPor favor solicite a su administrador crear una para dicho producto y así poder calcular el precio de su pedido.' % product.name)
			if before_invoice:
				raise UserError(u'No es posible generar un pedido con factura adelantada para un Cristal Insulado.')
			module = __name__.split('addons.')[1].split('.')[0]
			module = self._context.get('module_name', module)
			return {
				'vals': {
						'order_line_id': self.id,
						'type_calculator': 'insulado_calculator',
						'template_id': template.id
					},
				'view': '%s.glass_calculator_insulados_form_view' % module,
				'act_name':'Calculadora de Insulados',
			}
		if template and not self.calculator_id: #si hay template y la calculadora recién será creada
			if before_invoice:
				raise UserError(u'El producto %s tiene asociada una Ficha Maestra.\nNo es posible generar calculadora si el pedido de venta es de factura adelantada y si dicho producto tiene asociada una Ficha Maestra' % product.name)
			else:
				res['vals']['template_id'] = template.id
		return res
		
		# if product.categ_id == conf.category_insulados_id:
		# 	template = self.env['mtf.template'].search([('product_id','=',product.id)],limit=1)
		# 	if not template:
		# 		raise UserError(u'No se ha encontrado una ficha maestra para el producto %s.\nPor favor solicite a su administrador crear una para dicho producto y así poder calcular el precio de su pedido.'%product.name)
		# 	module = __name__.split('addons.')[1].split('.')[0]
		# 	module = self._context.get('module_name',module)
		# 	res = {
		# 		'vals': {
		# 			'order_line_id':self.id,
		# 			'type_calculator':'insulado_calculator',
		# 			'template_id':template.id
		# 			},
		# 		'view': '%s.glass_calculator_insulados_form_view'%module,
		# 		'act_name':'Calculadora de Insulados',
		# 	}

		#	calculate = self.env['mtf.sale.calculate']
		# 	if self.mtf_calcutale_id:
		# 		calculate = self.mtf_calcutale_id
		# 	else:
		# 		calculate = calculate.create({'order_line_id':self.id,'template_id':template.id})
		# 		self.mtf_calcutale_id = calculate.id
		# 	module = __name__.split('addons.')[1].split('.')[0]
		# 	view = self.env.ref('%s.mtf_sale_calculate_view_form'%module)
		# 	return {
		# 		'name':'Calculadora de Insulados',
		# 		'res_id': calculate.id,
		# 		'type': 'ir.actions.act_window',
		# 		'res_model': calculate._name,
		# 		'view_id': view.id,
		# 		'view_mode': 'form',
		# 		'view_type': 'form',
		# 		'target': 'new',
		# 		}
		# return super(SaleOrderLine,self).show_calculator()

		