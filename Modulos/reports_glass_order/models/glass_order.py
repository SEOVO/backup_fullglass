# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import UserError
from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import  cm
from reportlab.lib.utils import ImageReader
from reportlab.lib.enums import TA_CENTER,TA_RIGHT,TA_LEFT,TA_JUSTIFY
from reportlab.lib.styles import ParagraphStyle,getSampleStyleSheet
from reportlab.platypus import Paragraph, Table ,TableStyle ,SimpleDocTemplate
from reportlab.lib import colors
from datetime import datetime, timedelta
import base64,StringIO
from decimal import Decimal

pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))

p1 = ParagraphStyle('p1',alignment=TA_CENTER,fontSize=10,fontName="Calibri-Bold")
p2 = ParagraphStyle('p2',alignment=TA_CENTER,fontSize=10,fontName="Calibri")
p3 = ParagraphStyle('p3',alignment=TA_LEFT,fontSize=10,fontName="Calibri")
p4 = ParagraphStyle('p4',alignment=TA_RIGHT,fontSize=10,fontName="Calibri")
p5 = ParagraphStyle('p5',alignment=TA_RIGHT,fontSize=10,fontName="Calibri-Bold")
p6 = ParagraphStyle('p6',alignment=TA_LEFT,fontSize=10,fontName="Calibri-Bold")
p7 = ParagraphStyle('p7',alignment=TA_LEFT,fontSize=11,fontName="Calibri")
style = getSampleStyleSheet()["Normal"]

class GlassOrder(models.Model):
	_inherit='glass.order'

	def build_report(self):
		self.ensure_one()
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		wPage,hPage = A4  # 595 , 842
		path = self.env['main.parameter'].search([])[0].dir_create_file
		file_name = u'Órden de Producción %s.pdf'%self.name
		path+=file_name
		c = canvas.Canvas(path, pagesize= A4)
		pos = hPage-250
		pos_left = 20
		wUtil = wPage-2*pos_left
		size_widths = [70,104,78,16,16,16,16,16,70,70]
		col_widths = [float(i)/100*wUtil for i in (16,20,7,7,4,4,4,4,4,15,15)]
		bottom = 40
		separator = 12
		
		company = self.env['res.company']._company_default_get(self._name)
		# cabezera_op para el segundo reporte
		def cabezera_op(c):
			wReal = wPage- 30
			hReal = hPage - 40
			if company.logo:
				file = base64.b64decode(company.logo)
				c.drawImage(ImageReader(StringIO.StringIO(file)),pos_left,792,width=75,height=40,mask=None)
			else:
				c.setFont("Calibri", 12)
				c.drawString(pos_left,800,company.name or '')

			invoice = ''
			if self.invoice_ids:
				invoice = self.invoice_ids[0].number or ''

			# Datos de Conpañía para la cabecera
			c.setFont("Calibri", 7)
			c.drawString(pos_left,784,company.street or '')
			c.drawString(pos_left,778,u'Teféfono: '+(company.phone or ''))
			c.drawString(pos_left+70,778,u'Fax: '+(company.fax or ''))
			c.drawString(pos_left,770,company.website or '')
			c.drawString(pos_left+100,770,company.email or '')
			c.drawString(pos_left,762,'RUC: '+company.partner_id.nro_documento or '')
			
			pos = hReal-83
			posicion_indice = 1
			c.roundRect(375,750,205,50,5)
			c.setFont("Calibri", 13)
			c.drawString( 410 , 785,u'ÓRDEN DE ' + u'PRODUCCIÓN' if self.type_sale == 'production' else 'SERVICIO')
			c.drawString( 470 , 765, self.name)
			for i in range(0,2):
				c.line(pos_left,750-i,365,750-i)
			c.line(pos_left,745,365,745)
			c.setFont("Calibri", 8) 
			c.drawString(pos_left,730,'Documento:')
			c.drawString(pos_left+70,730,invoice)
			c.drawString(pos_left,722,'Cliente:')
			c.drawString(pos_left+70,722,self.partner_id.name)
			c.drawString(pos_left,714,'Obra:')
			c.drawString(pos_left+70,714, self.obra or '')
			c.drawString(pos_left,706,u'Dirección:')
			street   = (self.partner_id.street or '').strip()
			province = self.partner_id.province_id.name or ''
			district = self.partner_id.district_id.name or ''
			street  += '/'+province+'/'+district
			c.drawString(pos_left+70,706,street)
			c.drawString(pos_left,698,'Pto.Llegada:')
			llegada   = (self.sale_order_id.partner_shipping_id.street or '').strip()
			province  = self.sale_order_id.partner_shipping_id.province_id.name or ''
			district  = self.sale_order_id.partner_shipping_id.district_id.name or ''
			llegada  += '/'+province+'/'+district
			c.drawString(pos_left+70,698,llegada)	
			c.drawString( 380 ,722,u'Fecha Emisión:')
			c.drawString( 450 ,722,self.date_sale_order)
			c.drawString( 380 ,714,'Fecha Entrega:')
			c.drawString( 450 ,714,self.date_delivery)
			c.drawString( 380 ,706,'Vendedor:')
			c.drawString( 450 ,706,self.seller_id.partner_id.name)
			c.line(pos_left,690,580,690)
			
			aux = pos_left
			for i in col_widths:
				c.rect(aux,600,i,80)
				aux+=i
			c.setFont("Calibri", 9)
			c.drawString( 45 ,640,'Nro. Cristal')
			c.drawString( 140 ,640,'Medidas (mm)')
			c.drawString( 271 ,640,'Nro.')
			c.drawString( 271 ,630,u'Pág.')
			c.drawString( 440 ,640,'Metros')
			c.drawString( 437 ,630,'Cuadrados')
			c.drawString( 515 ,640,'Peso (Kg.)')
			c.rotate(90)
			c.drawString(22*cm, -240, "Descuadre")
			c.drawString(22*cm, -312, "Pulido")
			c.drawString(22*cm, -335, "Entalle")
			c.drawString(22*cm, -357, "Plantilla")
			c.drawString(22*cm, -379, "Arenado")
			c.drawString(22*cm, -400, "Embalado")
			c.rotate(-90)

		cabezera_op(c)

		for item in self.sale_lines:
			op_lines = item.calculator_id.line_ids.filtered(lambda l: l._get_ref_order_id().id==self.id)
			if not op_lines: continue
			
			prod = item.product_id
			weight_pro = prod.weight or 0.0
			tot_area = 0.0
			tot_weight = 0.0

			data = [[Paragraph('[%s] %s'%(prod.default_code or '',prod.name),p6)],]
			
			t=Table(data,colWidths=(wUtil,),rowHeights=(separator,))
			t.setStyle(TableStyle([
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('LINEBELOW', (0,-1), (-1,-1), 1, colors.black),
			]))
			w_table,h_table=t.wrap(0,0)
			t.wrapOn(c,120,500)
			pos-=h_table
			if pos < bottom:
				c.showPage()
				cabezera_op(c)
				pos = hPage-250
				pos-=h_table
				t.drawOn(c,pos_left,pos)
			else: t.drawOn(c,pos_left,pos)

			qty_cristales = 0
			for line in op_lines:
				area = line.area
				weight = weight_pro * area
				tot_area+=area
				tot_weight+=weight
				qty_cristales += line.quantity
				data = [
					[
						Paragraph(line.crystal_num,p3),
						Paragraph(line.measures,p3),
						Paragraph(line.descuadre or '',p2),
						Paragraph(str(line.page_number or ''),p2),
						Paragraph('X' if line.polished_id else '',p2),
						Paragraph(str(line.entalle or ''),p2),
						Paragraph('X' if line.template else '',p2),
						Paragraph('X' if line.arenado_id else '',p2),
						Paragraph('X' if line.packed else '',p2),
						Paragraph('{:,.4f}'.format(Decimal("%0.4f" % area)) ,p4),
						Paragraph('{:,.4f}'.format(Decimal("%0.4f" % weight)),p4)],
					]
				
				t=Table(data,colWidths=col_widths)
				t.setStyle(TableStyle([
					('VALIGN',(0,0),(0,0),'MIDDLE'),
					('VALIGN',(1,0),(-1,0),'TOP'),
					('TOPPADDING',(0,0),(-1,-1),0),
					('BOTTOMPADDING',(0,0),(-1,-1),0),
					('INNERGRID', (0,0), (-1,-1), 1, colors.black),
					('LINEBEFORE',(0,0),(0,0),1,colors.black),
					('LINEAFTER',(-1,0),(-1,0),1,colors.black),
				]))
				w_table,h_table=t.wrap(0,0)
				t.wrapOn(c,120,500)
				pos-=h_table

				if pos < bottom:
					c.showPage()
					cabezera_op(c)
					pos = hPage-250
					pos-=h_table
					t.drawOn(c,pos_left,pos)
				else: t.drawOn(c,pos_left,pos)
				
			data = [
				[
					Paragraph(u'Nro. de Piezas: '+str(qty_cristales),p3),'','','','','','','','',
					Paragraph('{:,.4f}'.format(Decimal("%0.4f" % tot_area)) ,p4),
					Paragraph('{:,.4f}'.format(Decimal("%0.4f" % tot_weight)),p4)],
				]
			
			t=Table(data,colWidths=col_widths)
			t.setStyle(TableStyle([
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('SPAN',(0,0),(8,0)),
				('LINEABOVE',(0,0),(-1,-1),1,colors.black),
			]))
			w_table,h_table=t.wrap(0,0)
			t.wrapOn(c,120,500)
			pos-=h_table
			if pos < bottom:
				c.showPage()
				cabezera_op(c)
				pos = hPage-250
				pos-=h_table
				t.drawOn(c,pos_left,pos)
			else: t.drawOn(c,pos_left,pos)

		now = fields.Datetime.context_timestamp(self,datetime.now())
		c.drawString(pos_left+10, pos-36,'Usuario: %s %s'%(self.env.user.name,str(now)[:19]))
		c.setAuthor(company.name)
		c.setTitle(file_name)
		c.setSubject('Reportes')
		c.save()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path)


	def build_report_lots(self):
		# no uso ensure_one xq se puede intentar imprimir desde la vista tree
		if len(self)>1: 
			raise UserError(u'Este reporte sólo puede exportarse para una OP a la vez.')
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
		pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))
		path = self.env['main.parameter'].search([])[0].dir_create_file+''
		file_name = 'Reporte OP-'+self.name+'.pdf'
		path+=file_name
		c = canvas.Canvas(path, pagesize= A4)
		width ,height  = A4  # 595 , 842
		wReal = width- 30
		hReal = height - 40
		pos = hReal-140
		pos_left = 20
		size_widths = [25,180,55,30,40,40,40,40,90]
		total_width_size = sum(size_widths)
		totals = [0,0,0,0]
		aux_array = [0,0,0,0,0] # para almacenar auxiliares
		qty_cristales = 0
		pagina = 1
		textPos = 0

		company = self.env['res.company']._company_default_get(self._name)

		# header para el segundo reporte
		def header(c,wReal,hReal,pos_left,size_widths=None):
			# company = self.env['res.company'].search([])[0]
			# if len(company) == 0:
			# 	raise exceptions.Warning(u"No ha confugurado su Compañía.\n Configure los datos de su compañía para poder mostrarlos en este reporte.")
			if company.logo:
				file = base64.b64decode(company.logo)
				c.drawImage(ImageReader(StringIO.StringIO(file)),pos_left,792,width=75,height=40,mask=None)
			else:
				c.setFont("Calibri", 12)
				c.drawString(pos_left,800,company.name if company.name else 'Company')

			## Codigo temporal:
			invoice = ''
			if self.invoice_ids:
				invoice = self.invoice_ids[0].number or ''

			# Datos de Conpañía para la cabecera
			c.setFont("Calibri", 6)
			c.drawString(pos_left,784,company.street or '')
			c.drawString(pos_left,778,(u'Teféfono: '+str(company.phone)) if company.phone else '')
			c.drawString(pos_left+70,778,(u'Fax: '+str(company.fax))if company.fax else '')
			c.drawString(pos_left,770,company.website if company.website else 'website')
			c.drawString(pos_left+100,770,company.email if company.email else 'email')
			ruc = company.partner_id.nro_documento if company.partner_id.nro_documento else ''
			c.drawString(pos_left,762,'RUC: '+str(ruc))		
			
			pos = hReal-83
			posicion_indice = 1
			c.line(375,800,580,800)
			c.line(375,750,580,750)
			c.line(375,800,375,750)
			c.line(580,800,580,750)
			c.setFont("Calibri", 13)
			c.drawString( 410 , 785,u'ÓRDEN DE ' + u'PRODUCCIÓN' if self.type_sale == 'production' else 'SERVICIO')
			c.drawString( 470 , 765, self.name)
			for i in range(0,2):
				c.line(pos_left,750-i,365,750-i)
			c.line(pos_left,745,365,745)
			c.setFont("Calibri", 8) 
			c.drawString(pos_left,730,'Documento:')
			c.drawString(pos_left+70,730,str(invoice))
			c.drawString(pos_left,722,'Cliente:')
			c.drawString(pos_left+70,722,self.partner_id.name[:78])
			c.drawString(pos_left,714,'Obra:')
			c.drawString(pos_left+70,714, self.obra or '')
			c.drawString(pos_left,706,u'Dirección:')
			street   = self.partner_id.street or ''
			province = self.partner_id.province_id.name or ''
			district = self.partner_id.district_id.name or ''
			street  += '/'+province+'/'+district
			c.drawString(pos_left+70,706,street[:78])
			c.drawString(pos_left,698,'Pto.Llegada:')
			llegada   = self.sale_order_id.partner_shipping_id.street or ''
			province  = self.sale_order_id.partner_shipping_id.province_id.name or ''
			district  = self.sale_order_id.partner_shipping_id.district_id.name or ''
			llegada  += '/'+province+'/'+district
			c.drawString(pos_left+70,698,llegada[:78])	
			c.drawString( 380 ,730,u'Fecha Emisión:')
			c.drawString( 460 ,730,str(datetime.strptime(self.date_order,"%Y-%m-%d %H:%M:%S")-timedelta(hours=5)))
			c.drawString( 380 ,722,u'Fecha Producción:')
			c.drawString( 460 ,722,self.date_production)
			c.drawString( 380 ,714,'Fecha Despacho:')
			c.drawString( 460 ,714,self.date_send)
			c.drawString( 380 ,706,'Fecha Entrega:')
			c.drawString( 460 ,706,self.date_delivery)
			c.drawString( 380 ,698,'Vendedor:')
			c.drawString( 460 ,698,self.seller_id.partner_id.name)
			c.line(pos_left,690,580,690)
			
			total_width = sum(size_widths) + pos_left
			c.line(pos_left,680,total_width,680)
			c.line(pos_left,660,total_width,660)
			c.line(pos_left,680,pos_left,660)
			c.line(total_width,680,total_width,660)
			headers = ['Cristal','Detalle','Medida','Lote','Embalado','Estado OP','M2','Peso',u'Ubicación']
			if len(headers) != len(size_widths):
				return
			pos = pos_left
			for i,item in enumerate(headers):
				c.drawCentredString(pos +int(size_widths[i]/2),667,item)
				pos += size_widths[i]

		def verify_linea(c,wReal,hReal,posactual,valor,pagina,pos_left,size_widths):
			if posactual <40:
				c.showPage()
				header(c,wReal,hReal,pos_left,size_widths)
				c.setFont("Calibri-Bold", 8)
				#c.drawCentredString(300,25,'Pág. ' + str(pagina+1))
				return pagina+1,hReal-160
			else:
				return pagina,posactual-valor

		header(c,wReal,hReal,pos_left,size_widths)
		glass_order_lines = self.line_ids
		
		pagina, pos = verify_linea(c,wReal,hReal,pos,12,pagina,pos_left,size_widths)

		for line in glass_order_lines:
			c.setFont("Calibri", 8)
			pagina, pos = verify_linea(c,wReal,hReal,pos,12,pagina,pos_left,size_widths)

			tmp_pos = pos_left
			c.drawRightString(tmp_pos+size_widths[0]-10,pos,line.crystal_number or '')
			tmp_pos += size_widths[0]

			c.drawString(tmp_pos+2,pos,line.product_id.name[:51] if line.product_id.name else '')
			tmp_pos += size_widths[1]

			c.drawString(tmp_pos+2, pos, line.measures)
			tmp_pos += size_widths[2]

			c.drawString(tmp_pos+2, pos,line.lot_line_id.lot_id.name or '')
			tmp_pos += size_widths[3]
			
			c.drawString(tmp_pos+2, pos, 'EM' if line.embalado else '')
			tmp_pos += size_widths[4]
			
			state= dict(self._fields['state'].selection).get(line.order_id.state)
			c.drawString(tmp_pos+2,pos,state)
			aux_array[0] = tmp_pos+2
			tmp_pos += size_widths[5]

			area = line.area or 0
			c.drawRightString(tmp_pos+size_widths[6]-2,pos,'{:,.4f}'.format(Decimal ("%0.4f" % area)))
			aux_array[1] = tmp_pos+size_widths[6]-2
			totals[0] += area
			tmp_pos += size_widths[6]

			peso = line.peso or 0
			c.drawRightString(tmp_pos+size_widths[7]-2,pos,'{:,.2f}'.format(Decimal ("%0.2f" % peso)))
			totals[1] += peso
			aux_array[2] = tmp_pos+size_widths[7]-2
			tmp_pos += size_widths[7]
			location = line.order_id.warehouse_id.name if (line.order_id.warehouse_id and line.lot_line_id.ingresado) else ''
			c.drawString(tmp_pos+10,pos,location)
			
		c.line(pos_left,pos-2,pos_left+total_width_size,pos-2)
		c.drawString(pos_left, pos-12,'Nro Pzs.'+str(len(glass_order_lines)))
		c.drawRightString(aux_array[0], pos-12,'Totales:')
		c.drawRightString(aux_array[1], pos-12, '{:,.4f}'.format(Decimal ("%0.4f" % totals[0])))
		c.drawRightString(aux_array[2], pos-12, '{:,.2f}'.format(Decimal ("%0.2f" % totals[1])))

		now = fields.Datetime.context_timestamp(self,datetime.now())
		c.drawString(pos_left+10, pos-48,'Usuario: %s %s'%(self.env.user.name,str(now)[:19]))
		
		c.setAuthor(company.name)
		c.setTitle(file_name)
		c.setSubject('Reportes')
		c.save()

		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path)
	