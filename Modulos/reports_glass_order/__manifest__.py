# -*- encoding: utf-8 -*-
{
	'name': 'Glass Order Reports',
	'category': 'etiquetas',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['glass_production_order','export_file_manager_it',],
	'version': '1.0.0',
	'description':"""
		Reportes de Órden de producción
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'views/glass_order.xml',
		'wizard/reporte_orden_produccion_wizard.xml',
		],
	'installable': True
}
