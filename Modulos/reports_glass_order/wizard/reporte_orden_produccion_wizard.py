# -*- coding: utf-8 -*-
from odoo import api, fields, models
from odoo.exceptions import UserError

class ReporteOrdenProduccionAlmacen(models.TransientModel):
	_name='reporte.orden.produccion.wizard'
	
	order_id = fields.Many2one('glass.order')

	def print_report_op(self):
		return self.order_id.build_report_lots()