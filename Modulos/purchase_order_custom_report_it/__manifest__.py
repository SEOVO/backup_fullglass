# -*- encoding: utf-8 -*-
{
	'name': u'Purchase Order Custom Report - Full Glass',
	'category': 'customize',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['purchase','purchase_carrier_info_it','export_file_manager_it'],
	'version': '1.0.0',
	'description':u"""
	FUNCIÓN:
		-> Módulo que reemplaza el reporte de cotización/Pedido de compra por defecto de Odoo por uno personalizado. 
		-> Incluye reemplazar el pdf generado al enviar la P.O. por email.
		-> Considerar los siguiente:
			- Este módulo debe ser versionado para cada empresa/instancia donde se desee usar, modificando el método build_report del modelo print.purchase.order ajustando el formato de reporte de acuerdo a las necesidades de cada empresa.
			- Si en el reporte se requieren campos/vistas declarados en módulos de personalización de cada empresa, tener en cuenta incluir dichos módulos en el apartado 'depends' de éste fichero para evitar problemas de dependencias.
			- Por una razón de estándar se coloca el nombre de la empresa/instancia en el nombre del módulo, ejm: Purchase Order Custom Report - Empresa de Ejemplo, para evitar confusiones.
	FUNCIONAL: Jorge Cardenas.
	DESRROLLADOR: Rodrigo Dueñas.
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'security/ir.model.access.csv',
		'views/purchase_order.xml',
		],
	'installable': True
}
