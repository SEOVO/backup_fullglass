# -*- coding: utf-8 -*-
from odoo import models, fields, api,tools
from odoo.exceptions import UserError

class PurchaseOrder(models.Model):
	_inherit = 'purchase.order'

	# @ Reporte personalizado: 
	@api.multi
	def print_custom_quotation(self):
		return self.env['print.purchase.order.it'].create({'order_id': self.id}).build_report()


	# Override
	@api.multi
	def print_quotation(self):
		return self.print_custom_quotation()