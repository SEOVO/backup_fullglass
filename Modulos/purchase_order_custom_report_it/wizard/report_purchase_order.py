# -*- coding: utf-8 -*-
from odoo import models, fields, api,tools
from odoo.exceptions import UserError
import codecs, pprint, pytz,base64
from decimal import Decimal
from StringIO import StringIO
from datetime import datetime
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Paragraph, Table ,TableStyle ,SimpleDocTemplate,Image
from reportlab.lib.utils import simpleSplit,ImageReader
from reportlab.lib.enums import TA_CENTER,TA_RIGHT,TA_LEFT,TA_JUSTIFY
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle,getSampleStyleSheet
from reportlab.pdfgen import canvas 

# global variables
wPage,hPage = A4  # 595 , 842
pos_left = 15 # padding
bottom = 55
wUtil = wPage-2*pos_left # Util width : Real Width - left margins
middle = wPage/2 # Center page
pos_right = middle+pos_left
separator = 12 # separador entre lineas
col_widths = [float(i)/100*wUtil for i in [6,10,11,11,43,9.5,9.5]]

pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))

p1 = ParagraphStyle('p1',alignment=TA_CENTER,fontSize=9,fontName="Calibri-Bold")
p2 = ParagraphStyle('p2',alignment=TA_CENTER,fontSize=10,fontName="Calibri")
p3 = ParagraphStyle('p3',alignment=TA_LEFT,fontSize=9,fontName="Calibri")
p4 = ParagraphStyle('p4',alignment=TA_RIGHT,fontSize=10,fontName="Calibri")
p5 = ParagraphStyle('p5',alignment=TA_RIGHT,fontSize=10,fontName="Calibri-Bold")
p6 = ParagraphStyle('p6',alignment=TA_LEFT,fontSize=9,fontName="Calibri-Bold")

# normal style
style = getSampleStyleSheet()['Normal']
style.alignment = TA_JUSTIFY
style.fontSize = 8

style2 = getSampleStyleSheet()['Normal']
style2.alignment = TA_RIGHT
style2.fontSize = 9

# Creamos un modelo nuevo para evitar psibles conflictos con sale.order
class PrintPurchaseOrderIt(models.TransientModel):
	_name = 'print.purchase.order.it'
	
	order_id = fields.Many2one('purchase.order', string='Purchase Order')
	company_id = fields.Many2one('res.company',related='order_id.company_id')

	@api.multi
	def build_report(self):
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		order = self.order_id
		partner = order.partner_id
		path = self.env['main.parameter'].search([])[0].dir_create_file
		dt = fields.Datetime
		now = dt.context_timestamp(self,datetime.now())
		file_name = u'P.O. %s %s.pdf'%(order.name,str(now)[:19].replace(':','_'))
		path += file_name
		c = canvas.Canvas(path , pagesize= A4)
		pos = hPage-140 # dynamic vertical position
		currency = self.env['account_currency_name_it.currency_name'].search([('currency_id','=',order.currency_id.id)],limit=1)
		symbol = order.currency_id.symbol
		date_planned_tz = str(dt.context_timestamp(self,dt.from_string(order.date_planned)))
		
		self.header(c)
		data=[
			[Paragraph(u"<b>SEÑORES:</b> %s"%partner.name,style),Paragraph(u"<b>RUC:</b> %s"%(partner.nro_documento or ''),style)],
			[Paragraph(u"<b>DIRECCIÓN:</b> %s"%(partner.street or ''),style),Paragraph(u"<b>REQUERIMIENTO:</b> %s"%(order.requisition_id.name or ''),style)],
			[Paragraph(u"<b>MONEDA:</b> %s (%s)"%(currency.name_plural or '',symbol),style),Paragraph(u"<b>FORMA DE PAGO:</b> %s"%(order.payment_term_id.name or ''),style)]]
		t=Table(data,colWidths=[float(i)/100*wUtil for i in [45,45]])
		t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'MIDDLE'),
			('BOX',(0,0),(-1,-1),1,colors.black),]))
		t.wrapOn(c,120,500)
		w_table,h_table=t.wrap(0,0)
		pos-=h_table
		t.drawOn(c,pos_left+30,pos-120)
		# header table
		pos-=separator*15
		self.header_table(c,symbol,pos)
		for i,line in enumerate(order.order_line,1):
			quantity   = '{:,.2f}'.format(Decimal("%0.2f"%line.product_qty))
			price_unit = '{:,.2f}'.format(Decimal("%0.2f"%line.price_unit))
			price_sub  = '{:,.2f}'.format(Decimal("%0.2f"%line.price_subtotal))			
			description = line.name.replace('\n','<br/>')
			data = [[
				Paragraph(str(i),p2),
				Paragraph(quantity,p4),
				Paragraph(line.product_uom.name,p2),
				Paragraph(line.product_id.default_code or '',p2),
				Paragraph(description,p3),
				Paragraph(price_unit,p4),
				Paragraph(price_sub,p4)],]
			t=Table(data,colWidths=col_widths)
			t.setStyle(TableStyle([
			('VALIGN',(0,0),(6,0),'TOP'),
			]))
			w_table,h_table=t.wrap(0,0)
			t.wrapOn(c,120,500)
			pos-=h_table
			if pos < bottom:
				c.showPage()
				self.header(c)
				pos = hPage-170
				self.header_table(c,symbol,pos)
				pos-=h_table
				t.drawOn(c,pos_left,pos)
			else: t.drawOn(c,pos_left,pos)

		c.line(pos_left+459,pos-5,pos_left+wUtil,pos-5)
		pos-=10
		# totales
		sub_total    = '{:,.2f}'.format(Decimal("%0.2f"%order.amount_untaxed))
		amount_tax   = '{:,.2f}'.format(Decimal("%0.2f"%order.amount_tax))
		amount_total = '{:,.2f}'.format(Decimal("%0.2f"%order.amount_total))
		
		data = [[Paragraph('<b>Subtotal</b>',style2),Paragraph(sub_total,style2),],
				[Paragraph('<b>Impuestos</b>',style2),Paragraph(amount_tax,style2),],
				[Paragraph('<b>Total (%s)</b>'%symbol,style2),Paragraph(amount_total,style2),],]
		
		t=Table(data,colWidths=(75,55),rowHeights=3*(separator,))
		t.setStyle(TableStyle([('VALIGN',(0,0),(-1,-1),'MIDDLE'),]))
		w_table,h_table=t.wrap(0,0)
		t.wrapOn(c,120,500)
		pos-=h_table
		aux = wPage-pos_left-w_table
		if pos < bottom:
			c.showPage()
			self.header(c)
			pos = hPage-280-h_table
			t.drawOn(c,aux,pos)
		else: t.drawOn(c,aux,pos)
 
		
		# Info sub-footer
		
		data=[
			[Paragraph(u"<b>Transportista: Telef. Ruc:</b>",style),Paragraph(u"<b>Observaciones:</b>",style)],
			[Paragraph(u"%s"%((order.transporter_id.name or '')+u', DIRECCIÓN: '+(order.transporter_street or '')+u', TELÉFONO: '+(order.transporter_id.phone or '')+', RUC: '+(order.transporter_id.nro_documento or '')),style),Paragraph(u"%s"%(order.notes or ''),style)]
			]
		t=Table(data,colWidths=[float(i)/100*wUtil for i in [45,45]])
		t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'MIDDLE'),]))
		
		
		pos2=pos-30
		c.line(pos_left+30,pos2,pos_left+wUtil-30,pos2)
		
		t.wrapOn(c,120,500)
		w_table,h_table=t.wrap(0,0)
		pos-=h_table
		if pos < bottom:
			c.showPage()
			pos = hPage-30-h_table
			t.drawOn(c,pos_left+30,pos-30)
		else: 
			t.drawOn(c,pos_left+30,pos-30)

		c.setFont("Calibri-Bold",10)
		c.drawString(pos_left+50,pos-50, self.env['res.users'].search([('id','=',self.env.uid)]).name or '')

		#Signatures:
		pos-=separator
		data = []
		user1 = order.requisition_id.user_id
		#user2 = order.create_uid

		Employee = self.env['hr.employee'].sudo()


		data = [data[i:i+3] for i in range(0,len(data),3)]

		if any(data):
			t=Table(data,colWidths=[wUtil/len(data[0]) for x in data[0]])
			t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'MIDDLE'),
			('ALIGN',(0,0),(-1,-1),'CENTER')
			]))
			w_table,h_table=t.wrap(0,0)
			t.wrapOn(c,120,500)
			pos-=h_table
			if pos < bottom:
				c.showPage()
				self.header(c)
				pos = hPage-140-h_table
				t.drawOn(c,pos_left,pos)
			else: t.drawOn(c,pos_left,pos)

		c.save()
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'file': base64.b64encode(file.read()),
				'content_type':'application/pdf'
			})
		return export.export_file(clear=True,path=path)
		
	@api.multi
	def header(self,c):
		company = self.company_id
		if company.logo:
			image = base64.b64decode(company.logo)
			c.drawImage(ImageReader(StringIO(image)),pos_left,hPage-140,width=200,height=80,mask='auto')

		

		aux = hPage-50
		c.setFont("Calibri-Bold", 16)
		title=u'ÓRDEN DE COMPRA' if self.order_id.state in ('purchase','sent') else u'COTIZACIÓN'
		c.drawCentredString(middle,aux,title)
		aux-=separator*2
		c.setFont("Calibri-Bold",10)
		c.drawCentredString(middle,aux,'Nro:  %s'%(self.order_id.name or ''))
		aux-=separator*7
		c.line(pos_left,aux,pos_left+wUtil,aux)
		aux-=separator*2
		c.setFont("Calibri",10)
		c.drawString(pos_left,aux,company.name or '')
		c.setFont("Calibri-Bold",10)
		c.drawString(pos_left+470,aux,'Fecha:')
		c.setFont("Calibri",10)
		if self.order_id.date_order != False:
			date = str(self.order_id.date_order)
			fecha = date[8:10]+'/'+date[5:7]+'/'+date[:4]
		else:
			fecha = ''
		c.drawString(pos_left+510,aux,fecha or '')


		aux-=separator
		c.setFont("Calibri",10)
		c.drawString(pos_left,aux,company.street or '')
		aux-=separator
		c.setFont("Calibri",10)
		c.drawString(pos_left,aux,company.state_id.name or '')

		aux-=separator
		c.setFont("Calibri-Bold", 10)
		c.drawString(pos_left,aux,'Teléfono: ')
		c.drawString(pos_left+200,aux,'Fax: ')
		c.setFont("Calibri",10)
		c.drawString(pos_left+50,aux,company.phone or '')
		c.drawString(pos_left+230,aux,company.fax or '')
		aux-=separator
		c.setFont("Calibri-Bold", 10)
		c.drawString(pos_left+200,aux,'Email: ')
		c.setFont("Calibri",10)
		c.drawString(pos_left,aux,company.website or '')
		c.drawString(pos_left+230,aux,company.email or '')

		

	@api.multi
	def header_table(self,c,symbol,pos):
		data=[[
			Paragraph(u"Ítem",p1),
			Paragraph(u"Cantidad",p1),
			Paragraph(u"Unidad",p1),
			Paragraph(u"Código",p1),
			Paragraph(u"Descripción",p1),
			Paragraph(u"Precio Unit. (%s)"%symbol,p1),
			Paragraph(u"Precio Total (%s)"%symbol,p1)],]
		hTable=Table(data,colWidths=col_widths,rowHeights=(32))
		hTable.setStyle(TableStyle([('VALIGN',(0,0),(-1,-1),'MIDDLE'),]))
		hTable.wrapOn(c,120,500)
		hTable.drawOn(c,pos_left,pos)
		c.line(pos_left,pos,pos_left+wUtil,pos)

	def get_Image(self,image):
		I = Image(StringIO(base64.b64decode(image)))
		I.drawHeight = 60.0
		I.drawWidth = 180.0
		return I