# -*- encoding: utf-8 -*-
{
	'name': 'Guía de remisión de servicios',
	'category': 'sale',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['print_guia_remision_it_poliglass', 'master_template_fullglass'],
	'version': '1.0',
	'description':"""
	Imprime guia en remision para servicios de fullglass
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'views/sale_order.xml',
		#'wizard/print_pick_guides_wizard.xml',
	],
	'installable': True
}
