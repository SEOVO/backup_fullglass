# -*- coding: utf-8 -*-
from odoo import models, fields, api, _
import base64,decimal
import sys
from odoo.exceptions import UserError
import pprint
from odoo.exceptions import ValidationError

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	service_remission_guide_ids = fields.One2many('glass.service.remission_guide', 'sale_order_id', 
		string=u'Guías de remisión de servicios', readonly=True)
	service_remission_guide_count = fields.Integer(u'Nro de guías de remisión', 
		compute='_count_service_remission_guide', readonly=True)

	@api.depends('service_remission_guide_ids')
	def _count_service_remission_guide(self):
		for sale in self:
			sale.service_remission_guide_count = len(sale.service_remission_guide_ids)


	def action_view_remission_guides(self):
		action = self.env.ref('glass_production_order.sale_remission_guides_services_action').read()[0]
		
		# TODO get default picking_type from glass parameters
		action['context'] = dict(self.env.context or {}, default_sale_order_id=self.id)

		guides = self.mapped('service_remission_guide_ids')
		if len(guides) > 1:
			action['domain'] = [('id', 'in', guides.ids)]
		elif guides:
			action['views'] = [(self.env.ref('glass_production_order.glass_service_remission_guide_view_form').id, 'form')]
			action['res_id'] = guides.id
		return action


	# send_agency = fields.Boolean(string=u'Envío a Agencia')
	# first_route = fields.Char('Primer Tramo')
	# second_route = fields.Char('Segundo Tramo')

	# # campos de guía de remisión
	# marca = fields.Char('Marca', size=12)
	# placa = fields.Char('Placa', size=12)
	# nro_const = fields.Char(u'Número de constancia de inscripción', size=12)
	# licencia = fields.Char('Licencia de Conducir N°(5)', size=12)
	# nombre = fields.Char('Nombre')
	# ruc = fields.Char('Ruc', size=100)
	# tipo = fields.Char('Tipo', size=12)
	# nro_comp = fields.Char(u'Número de comprobante', size=12)
	# nro_guia = fields.Char(u'Número de guía', size=12)
	# fecha_traslado = fields.Datetime(string='Fecha de traslado')
	# punto_partida = fields.Char('Punto de partida', size=100)
	# punto_llegada = fields.Char('Punto de llegada', size=100)

	
	# @api.onchange('send_agency')
	# def on_change_send_agency(self):
	# 	if self.partner_id and self.send_agency:
	# 		street_partner = self.partner_id.street or ''
	# 		province = '/' + self.partner_id.province_id.name if self.partner_id.province_id else ''
	# 		district = '/' + self.partner_id.district_id.name if self.partner_id.district_id else ''
	# 		self.second_route = street_partner + district + province

	# def get_print_wizard(self):
	# 	module = __name__.split('addons.')[1].split('.')[0]
	# 	wizard = self.env['print.picking.guides.wizard'].create({'sale_order_id': self.id})
	# 	return {
	# 		'name': 'Elegir Sede',
	# 		'type': 'ir.actions.act_window',
	# 		'res_id': wizard.id,
	# 		'res_model': wizard._name,
	# 		'view_id': self.env.ref('%s.print_picking_guides_wizard_view_form' % module).id,
	# 		'view_mode': 'form',
	# 		'view_type': 'form',
	# 		'target': 'new',
	# 		'context': dict(self._context or {}, called_method='action_print_rem_guide_services')
	# 		}
