# -*- coding: utf-8 -*-
from odoo import models, fields, api,tools
from odoo.exceptions import UserError
import codecs, pprint, pytz,base64
from decimal import Decimal
from StringIO import StringIO
from datetime import datetime
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import A4
from reportlab.platypus import Paragraph, Table ,TableStyle ,SimpleDocTemplate
from reportlab.lib.utils import simpleSplit,ImageReader
from reportlab.lib.enums import TA_CENTER,TA_RIGHT,TA_LEFT,TA_JUSTIFY
from reportlab.lib import colors
from reportlab.lib.styles import ParagraphStyle,getSampleStyleSheet
from reportlab.pdfgen import canvas 
from num2words import num2words

# global variables
wPage,hPage = A4  # 595 , 842
pos_left = 10 # padding
bottom = 30
pos = hPage-96 # dynamic vertical position
wUtil = wPage-2*pos_left # Util width : Real Width - left margins
middle = wPage/2 # Center page
pos_right = middle+pos_left
col_widths = [float(i)/100*wUtil for i in (4, 8, 8, 61, 8, 10.5)] # size of columns (list sum = 100%)
separator = 12 # separador entre lineas

pdfmetrics.registerFont(TTFont('Calibri', 'Calibri.ttf'))
pdfmetrics.registerFont(TTFont('Calibri-Bold', 'CalibriBold.ttf'))

p1 = ParagraphStyle('p1',alignment=TA_CENTER,fontSize=10,fontName="Calibri-Bold")
p2 = ParagraphStyle('p2',alignment=TA_CENTER,fontSize=10,fontName="Calibri")
p3 = ParagraphStyle('p3',alignment=TA_LEFT,fontSize=10,fontName="Calibri")
p4 = ParagraphStyle('p4',alignment=TA_RIGHT,fontSize=10,fontName="Calibri")
p5 = ParagraphStyle('p5',alignment=TA_RIGHT,fontSize=10,fontName="Calibri-Bold")
p6 = ParagraphStyle('p6',alignment=TA_LEFT,fontSize=10,fontName="Calibri-Bold")
p7 = ParagraphStyle('p7',alignment=TA_LEFT,fontSize=11,fontName="Calibri")
style = getSampleStyleSheet()["Normal"]

gray = colors.Color(red=(200.0/255),green=(200.0/255),blue=(200.0/255))

class SaleOrder(models.Model):
	_inherit = 'sale.order'

	# @ Reporte personalizado: 
	def print_custom_quotation(self):
		if len(self)>1:
			raise UserError(u'Este reporte sólo puede ser emitido en un pedido a la vez.')
		if self.state == 'draft':
			self.write({'state': 'sent'})
		#return self.env['print.sale.order.it'].create({'order_id':self.id}).build_report()
		import sys
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		path = self.env['main.parameter'].search([],limit=1).dir_create_file
		now = fields.Datetime.context_timestamp(self,datetime.now())
		file_name = u'S.O. %s %s.pdf'%(self.name,str(now)[:19].replace(':','_'))
		path += file_name
		c = canvas.Canvas(path , pagesize= A4)
		#pos = hPage - 150
		pos = hPage - 128
		currency = self.env['account_currency_name_it.currency_name'].search([('currency_id','=',self.pricelist_id.currency_id.id)],limit=1)
		if not currency:
			raise UserError('No ha configurado la moneda '+self.pricelist_id.currency_id.name)
		symbol = currency.currency_id.symbol
		title=u'PEDIDO DE VENTA N° ' if self.state in ('sale','done') else u'COTIZACIÓN N° '
		dt = fields.Datetime
		def header(c,title):
			com = self.company_id
			if com.logo:
				image = base64.b64decode(com.logo)
				c.drawImage(ImageReader(StringIO(image)),pos_left,hPage-70,width=140,height=65,mask='auto')
			c.setFont("Calibri-Bold", 16)
			c.drawCentredString(middle,hPage-50,title+self.name)
			
			street = ' - '.join((com.street or '',com.partner_id.district_id.name or '',com.partner_id.province_id.name or '',com.state_id.name or '',))
			# data = [
			# 	[Paragraph(com.name,p7),''],
			# 	[Paragraph(street,p7),''],
			# 	[Paragraph(u'RUC:',p7),Paragraph(com.partner_id.nro_documento or '',p7)],
			# 	[Paragraph(u'Teléfono:',p7),Paragraph(com.phone or '',p7)],
			# 	[Paragraph(u'Sitio Web:',p7),Paragraph(com.website or '',p7)],]
			data = [
				[Paragraph(com.name,p7),'', '', '', '', ''],
				[Paragraph(street,p7),'', '', '', '', ''],
				[Paragraph(u'RUC:',p7), Paragraph(com.partner_id.nro_documento or '',p7), Paragraph(u'Teléfono:',p7), Paragraph(com.phone or '',p7), Paragraph(u'Sitio Web:',p7), Paragraph(com.website or '',p7)],]
			
			t=Table(data,colWidths=[float(i)/100*wUtil for i in (10,15,10,15,12,38)],rowHeights=(separator))
			t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'MIDDLE'),
			('SPAN',(0,0),(5,0)),
			('SPAN',(0,1),(5,1)),
			]))
			t.wrapOn(c,120,500)
			t.drawOn(c,pos_left,hPage-115)

			aux = bottom
			c.setFont("Calibri",9)
			c.line(pos_left,aux,wPage - pos_left,aux)
			aux -= separator
			phones = u'Teléfono: %s Celular: %s'%(com.phone or '', com.partner_id.mobile or '')
			street_company = ' '.join([i for i in [com.street, com.city, com.country_id.name] if i])
			c.drawCentredString(wPage/2,aux, '%s - %s' % (street_company, phones))
			#aux -= separator
			#c.drawCentredString(wPage/2,aux,)
			aux -= separator
			c.drawCentredString(wPage/2,aux,' '.join([i for i in [com.email,com.website] if i]))

		def header_table(c,pos):
			data=[[Paragraph(u"#",p1),Paragraph(u"CANT.",p1),Paragraph(u"U.M.",p1),Paragraph(u"DESCRIPCIÓN",p1),Paragraph(u"P. UNIT.",p1),Paragraph(u"TOTAL",p1)],]
			hTable=Table(data,colWidths=col_widths,rowHeights=(32))
			hTable.setStyle(TableStyle([
				('VALIGN',(0,0),(-1,-1),'MIDDLE'),
				('BOX',(0,0),(-1,-1),1,colors.black),
				('BACKGROUND',(0,0),(-1,-1),gray)
				]))
			hTable.wrapOn(c,120,500)
			hTable.drawOn(c,pos_left,pos)

		header(c,title)
		partner = self.partner_id
		date_order = dt.to_string(dt.context_timestamp(self,dt.from_string(self.date_order)))
		data = [
		[Paragraph('CLIENTE:',p6),Paragraph(partner.name,p3),'',''],
		[Paragraph(u'DIRECCIÓN:',p6),Paragraph(partner.street or '',p3),'',''],
		[Paragraph('DNI/RUC:',p6),Paragraph(partner.nro_documento or '',p3),Paragraph('FECHA:',p6),Paragraph(date_order,p3)],
		[Paragraph(u'CIUDAD:',p6),Paragraph(partner.state_id.name or '',p3),Paragraph(u'DISTRITO:',p6),Paragraph(partner.district_id.name or '',p3)],
		[Paragraph('MONEDA:',p6),Paragraph('%s (%s)'%(currency.name_plural,symbol),p3),Paragraph('COND. DE PAGO:',p6),Paragraph(self.payment_term_id.name or '',p3)],
		#[Paragraph(u'TELÉFONO:',p6),Paragraph(partner.phone or '',p3),Paragraph(u'CELULAR:',p6),Paragraph(partner.mobile or '',p3)]
		]
		
		t=Table(data,colWidths=[float(i)/100*wUtil for i in (12,38,15,35)],rowHeights=(separator))
		t.setStyle(TableStyle([
		('VALIGN',(0,0),(-1,-1),'MIDDLE'),
		('SPAN',(1,0),(3,0)),
		('SPAN',(1,1),(3,1)),
		]))
		w_table,h_table=t.wrap(0,0)
		t.wrapOn(c,120,500)
		pos-=h_table
		t.drawOn(c,pos_left,pos)
		pos-=separator
		c.setFont("Calibri", 10)
		c.drawString(pos_left,pos,u'Mediante la presente nos es muy grato cotizar a continuación lo siguiente:')
		pos-=44
		header_table(c,pos)

		total_discount = total_sub = 0.0
		for i, line in enumerate(self.order_line,1):
			quantity   = '{:,.2f}'.format(Decimal("%0.2f" % line.product_uom_qty))
			price_unit = '{:,.2f}'.format(Decimal("%0.2f" % line.price_unit))
			#discount   = '{:,.2f}'.format(Decimal("%0.2f"%line.discount))
			price_sub  = line.price_subtotal/(1-line.discount/100.0)
			
			total_sub+=price_sub
			total_discount+=(line.price_subtotal-price_sub)
			
			price_sub  = '{:,.2f}'.format(Decimal("%0.2f"%price_sub))
			description = line.name.replace('\n','<br/>')
			calc_lines = line.calculator_id.line_ids
			if calc_lines:
				#desc_crystals = ' '.join(['%d(%s)' % (10, '1200x1200') for xd in range(250)]) # test
				desc_crystals = ' '.join(calc_lines.mapped(lambda l: '%d(%s)'%(l.quantity,l.measures)))
				description = '<b>%s</b><br/><font size=8>%s</font>'%(description,desc_crystals)

			data = [[Paragraph(str(i),p2),Paragraph(quantity,p4),Paragraph(line.product_uom.name,p2),Paragraph(description,style),Paragraph(price_unit,p4),Paragraph(price_sub,p4)],]
			t=Table(data,colWidths=col_widths)
			t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'TOP'),
			]))
			w_table,h_table=t.wrap(0,0)
			t.wrapOn(c,120,500)
			pos-=h_table
			if pos < bottom:
				c.showPage()
				header(c,title)
				pos = hPage-155
				header_table(c,pos)
				pos-=h_table
				t.drawOn(c,pos_left,pos)
			else: t.drawOn(c,pos_left,pos)

		pos-=3
		c.line(pos_left,pos,pos_left+wUtil,pos)
		pos-=3
		total_sub      = '{:,.2f}'.format(Decimal("%0.2f"%total_sub))
		total_discount = '{:,.2f}'.format(Decimal("%0.2f"%total_discount))
		sub_total    = '{:,.2f}'.format(Decimal("%0.2f"%self.amount_untaxed))
		amount_tax   = '{:,.2f}'.format(Decimal("%0.2f"%self.amount_tax))
		amount_total = '{:,.2f}'.format(Decimal("%0.2f"%self.amount_total))
		data = [[Paragraph('BASE',p5),Paragraph(total_sub,p5),],
				[Paragraph('DESCUENTO',p5),Paragraph(total_discount,p5),],
				[Paragraph('SUB-TOTAL',p5),Paragraph(sub_total,p5),],
				[Paragraph('IMPUESTOS',p5),Paragraph(amount_tax,p5),],
				[Paragraph('TOTAL %s'%symbol,p5),Paragraph(amount_total,p5),],]
		#print(col_widths[4], col_widths[5])
		t=Table(data,colWidths=(70.0, 65.0),rowHeights=5*(separator,))
		t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'MIDDLE'),
			('BACKGROUND',(0,4),(1,4),gray)
			]))
		w_table,h_table=t.wrap(0,0)
		t.wrapOn(c,120,500)
		pos-=h_table
		aux = wPage-pos_left-w_table
		if pos < bottom:
			c.showPage()
			header(c,title)
			pos = hPage-180-h_table
			t.drawOn(c,aux,pos)
		else: t.drawOn(c,aux,pos)

		integ = str(self.amount_total).split('.')
		try:
			number_label = num2words(int(integ[0]), lang='es')
		except NotImplementedError:
			number_label = num2words(int(integ[0]), lang='en')
		total_string = u'%s CON %s/100 %s'%(number_label.upper(),integ[1].ljust(2,'0'),currency.name_plural)

		user = self.user_id or self.env.user
		user_name = '%s - %s'%(user.name,user.function) if user.function else user.name
		data = [[Paragraph(u'TOTAL ÓRDEN',p6),''],
				[Paragraph('SON:',p6),Paragraph(total_string,p3)],
				[Paragraph('ATTE.:  %s'%user_name,p3),'']]
		t=Table(data,colWidths=(40,450,))
		t.setStyle(TableStyle([
			('VALIGN',(0,0),(-1,-1),'MIDDLE'),
			('SPAN',(0,0),(1,0)),
			('SPAN',(0,2),(1,2)),
			]))
		w_table,h_table=t.wrap(0,0)
		t.wrapOn(c,120,500)
		pos-=h_table
		if pos < bottom:
			c.showPage()
			header(c,title)
			pos = hPage-180-h_table
			t.drawOn(c,pos_left,pos)
		else: t.drawOn(c,pos_left,pos)
		
		c.showPage()
		c.setAuthor(self.company_id.name)
		c.setTitle(file_name)
		c.setSubject('Reportes')
		c.save()
		options = {'msg_success_notify':'El archivo pdf de %s Nro. %s se ha generado correctamente.'%(title,self.name)}
		with open(path,'rb') as file:
			export = self.env['export.file.manager'].create({
				'file_name': file_name,
				'content_type':'application/pdf',
				'file': base64.b64encode(file.read()),	
			})
		return export.export_file(clear=True,path=path,options=options)


	# Extend Method
	@api.multi
	def action_quotation_send(self):
		"""Reemplazamos el attachment por defecto 
			al generar el wizard de envío de pedido/cotización 
			de venta"""
		self.ensure_one()
		res = super(SaleOrder,self).action_quotation_send()
		template_id = res['context'].get('default_template_id')
		if template_id:
			template = self.env['mail.template'].browse(template_id).exists()
			# set report_template on False
			if template and template.report_template:
				template.write({'report_template':False})
		export = self.print_custom_quotation()
		file  = self.env['export.file.manager'].browse(export['res_id'])[0].file
		title = u'%s de venta Nro. %s.pdf'%(('Pedido' if self.state == 'sale' else  u'Cotización'),self.name)
		attachment = self.env['ir.attachment'].create({
						'name': title,
						'datas': file,
						'datas_fname': title,
						})
		res['context'].update({
			'default_attachment_ids': [(6,0,[attachment.id])],
		})
		return res