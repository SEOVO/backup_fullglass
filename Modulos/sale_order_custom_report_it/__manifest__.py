# -*- encoding: utf-8 -*-
{
	'name': u'Sale Order Custom Report Fullglass',
	'category': 'customize',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['sale_calculadora_it','export_file_manager_it'],
	'version': '1.0.0',
	'description':"""
	-> Módulo que reemplaza el reporte de cotización por defecto de Odoo por uno personalizado. 
	-> Incluye reemplazar el pdf generado al enviar la cotización por email.
	-> Considerar los siguiente:
		- Este módulo debe ser versionado para cada empresa/instancia donde se desee usar, modificando el método build_report del modelo print.sale.order ajustando el formato de reporte de acuerdo a las necesidades de cada empresa.
		- Si en el reporte se requieren campos/vistas declarados en módulos de personalización de cada empresa, tener en cuenta incluir dichos módulos en el apartado 'depends' de éste fichero para evitar problemas de dependencias.
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		#'security/ir.model.access.csv',
		'views/sale_order.xml',
		],
	'external_dependencies': {'python': ['num2words']},
	'installable': True
}
