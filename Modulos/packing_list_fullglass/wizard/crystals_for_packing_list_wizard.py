from odoo import fields,models,api,exceptions, _
from odoo.exceptions import UserError

# Wizard contenedor para agragar cristales al packing list:
class Crystals_For_Packinglist_Wizard(models.TransientModel):
	_name = 'crystals.for.packinglist.wizard'
	detail_lines = fields.One2many('crystals.for.packinglist.wizard.line','main_id')
	packing_id = fields.Many2one('packing.list')
	show_button = fields.Boolean(string='Show button', compute='_get_show_button')
	warning_message = fields.Char()
	
	@api.depends('detail_lines')
	def _get_show_button(self):
		for item in self:
			if len(item.detail_lines) > 0:
				item.show_button = True
			else:
				item.show_button = False

	def add_selected(self):
		items = self.detail_lines.filtered(lambda x: x.selected)
		if len(items)==0:
			raise UserError('No ha seleccionado cristales')
		return self.add_items(items)

	def add_all(self):
		if len(self.detail_lines)==0:
			raise UserError('No hay cristales disponibles para el Packing List')
		return self.add_items(self.detail_lines)

	def add_items(self,items):
		delivered = items.filtered(lambda x: x.order_line_id.lot_line_id.entregado)
		packing = items.filtered(lambda x: x.order_line_id.in_packing_list)
		msg = ''
		for item in delivered:
			msg+=str(item.origen)+'/'+str(item.crystal_number)+' : Entregado'+'\n'
		for item in packing:
			msg+=str(item.origen)+'/'+str(item.crystal_number)+' : En Packing List'+'\n'
		if msg != '':
			raise UserError('No es posible agregar los siguientes cristales:\n'+msg)
		location = self.packing_id.location and self.packing_id.location.id or False
		self.packing_id.write({
			'selected_line_ids':[(0,0,{
				'order_line_id':item.order_line_id.id,
				'order_id':item.order_id.id,
				'product_id':item.product_id.id,
				#'picking_id': item.picking_id.id,
				'invoice_id': item.invoice_id.id,
				'location_tmp': location,
				'measures': item.measures,
			}) for item in items],
		})
		self.packing_id.refresh_resume_lines()
		return True

		# for item in items:

		# 	self.env['glass.order.line.packing.list'].create({
		# 		'order_line_id':item.order_line_id.id,
		# 		'packing_id':obj_main.id,
		# 		'order_id':item.order_id.id,
		# 		'product_id':item.product_id.id,
		# 		#'picking_id': item.picking_id.id,
		# 		'invoice_id': item.invoice_id.id,
		# 		'location_tmp': obj_main.location.id,
		# 		'measures': item.measures,
		# 	})

 # Cristales a seleccionar para el packing list
class Crystals_For_Packinglist_Wizard_Line(models.TransientModel):
	_name = 'crystals.for.packinglist.wizard.line'
	
	main_id = fields.Many2one('crystals.for.packinglist.wizard')
	selected = fields.Boolean('Selected')
	order_line_id = fields.Many2one('glass.order.line',string='Order Line')
	order_id = fields.Many2one(related='order_line_id.order_id',string='Ord. de Prod.')
	product_id = fields.Many2one(related='order_line_id.product_id',string='Producto')
	#picking_id = fields.Many2one('stock.picking')
	invoice_id = fields.Many2one('account.invoice')
	crystal_number = fields.Char(related='order_line_id.crystal_number',string='Nro Cristal')
	partner_id = fields.Many2one(related='order_line_id.partner_id',string='Cliente')
	measures = fields.Char(related='order_line_id.measures',string='Medidas')