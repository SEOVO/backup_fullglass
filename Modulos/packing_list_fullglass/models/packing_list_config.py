# -*- coding: utf-8 -*-
from odoo import fields, models,api,exceptions, _
from odoo.exceptions import UserError

class PackingListConfig(models.Model):
	_name = 'packing.list.config'
	
	name = fields.Char(u'Parámetros',default=u"Parámetros")
	seq_packing_list=fields.Many2one('ir.sequence',u'Secuencia para los Packing List')
	picking_type_pl = fields.Many2one('stock.picking.type',u'Tipo Picking - Packing List')
	traslate_motive_pl = fields.Many2one('einvoice.catalog.12','Motivo traslado Packing List')
	location_id = fields.Many2one('stock.location',string=u'Ubicación Destino por defecto',domain=[('usage','in',('internal',))])
	custom_location = fields.Many2one('custom.glass.location',u'Ubicación por defecto')
	guide_series = fields.Many2one('ir.sequence',string='Serie de guia')
	path_remission_guides = fields.Char('Path')
	default_start_point = fields.Char(u'Punto de partida por defecto')
	default_end_point = fields.Char(u'Punto de salida por defecto')






