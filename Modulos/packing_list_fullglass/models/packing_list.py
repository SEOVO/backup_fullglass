# -*- coding: utf-8 -*-
from odoo import api, fields, models 
from datetime import datetime,timedelta
from odoo.exceptions import UserError
from odoo.tools.float_utils import float_round
import decimal,sys

class PackingList(models.Model):
	_name = 'packing.list'

	name = fields.Char(string="Nombre", default='/',copy=False)
	#warehouse_id = fields.Many2one('stock.warehouse',string=u'Almacén Destino')
	picking_type_id = fields.Many2one('stock.picking.type',string='Tipo de Picking para transf.',help=u'Tipo de picking empleado para la tranferencia de éste Packing List')
	location_id = fields.Many2one(related='picking_type_id.default_location_dest_id',string=u'Ubicación Destino',readonly=True)
	traslate_motive_id = fields.Many2one('einvoice.catalog.12',u'Motivo de traslado para transf.',help=u'Motivode traslado empleado para la tranferencia de éste Packing List')
	#stock_location = fields.Many2one('stock.location',string=u'Ubicación destino')
	type_pk = fields.Selection([('to_customer','A Cliente'),('to_transfer',u'Transf. a otra ubicación')],default='to_customer',required=True)
	guide_series = fields.Many2one('ir.sequence','Serie de guia')
	numberg = fields.Char(u'Serie de guía')
	order_filter = fields.Many2one('glass.order',string='Filtro O.P.',
		domain="[('state', 'in', ('process', 'ended')), ('type_sale', '=', 'production')]")
	selected_line_ids = fields.One2many('glass.order.line.packing.list','packing_id',string="Cristales Seleccionados",copy=False)
	grouped_lines = fields.One2many('packing.list.grouped.line','parent_id',string='Lineas resumen',copy=False)
	state = fields.Selection([('draft','Borrador'),('process','En proceso'),('done','Finalizada')],default='draft',copy=False)
	date = fields.Date('Fecha',default=fields.Date.today)
	location = fields.Many2one('custom.glass.location',string='Caballete') ## Ver cómo se va a gestionar esta mrd
	picking_id = fields.Many2one('stock.picking',string="Picking") # pick por transf almacen
	picking_ids = fields.Many2many('stock.picking') # picking por entrega a cliente
	picking_count = fields.Integer(compute='_get_count_picking', default=0)
	# estas huevadas son para el albarán:
	transporter_id = fields.Many2one('res.partner',string=u'Transportista')
	driver_delivery = fields.Char('Conductor')
	marca   = fields.Char('marca')
	placa   = fields.Char('placa')
	nro_const = fields.Char('nro_const')
	licencia  = fields.Char('licencia')
	nombre = fields.Char('nombre')
	ruc    = fields.Char('ruc')
	tipo   = fields.Char('tipo')
	nro_comp = fields.Char('nro_comp')
	nro_guia = fields.Char('nro_guia')
	fecha_traslado = fields.Date('fecha_traslado')
	punto_partida  = fields.Char('punto_partida')
	punto_llegada  = fields.Char('punto_llegada')

	@api.depends('picking_ids')
	def _get_count_picking(self):
		for rec in self:
			rec.picking_count = len(rec.picking_ids)

	# para mostrar los albaranes movidos en packing list
	@api.multi
	def show_picking_ids(self):
		if self.picking_count == 1:
			return {
				'name':self.picking_ids[0].name,
				'res_id':self.picking_ids[0].id,
				'type': 'ir.actions.act_window',
				'res_model': 'stock.picking',
				'view_id': self.env.ref('stock.view_picking_form').id,
				'view_mode': 'form',
				'view_type': 'form',
				}
		elif self.picking_count > 1:
			return {
					'name':'Operaciones de Packing List',
					'view_type':'form',
					'view_mode':'tree,form',
					'res_model':'stock.picking',
					'type':'ir.actions.act_window',
					'domain': [['id', 'in', self.picking_ids.ids]],
					}

	@api.onchange('guide_series')
	def changed_guide_series(self):
		if self.guide_series:
			self.refres_numg()

	@api.model
	def default_get(self,default_fields):
		res = super(PackingList,self).default_get(default_fields)
		conf = self.env['packing.list.config'].search([],limit=1)
		if not conf:
			raise UserError(u'No se encontraron los valores de configuración para Packing List')
		res.update({
			'location':conf.custom_location.id,
			#'location_id':conf.location_id.id,
			'guide_series':conf.guide_series.id,
			'picking_type_id':conf.picking_type_pl.id,
			'traslate_motive_id':conf.traslate_motive_pl.id,
			'punto_partida': conf.default_start_point or '',
			'punto_llegada': conf.default_end_point or '',
		})
		return res

	@api.multi
	def unlink(self):
		for rec in self:
			if rec.state in ('process','done'):
				raise UserError('No es posible eliminar un Packing List cuando ha sido procesado o tiene estado finalizado.')
		return super(PackingList,self).unlink()
	
	def get_crystals_for_op(self):
		self.ensure_one()
		if self.order_filter:
			sql_ext = ''
			added  = self.selected_line_ids.mapped('order_line_id').ids
			if added:
				added_str = ','.join(map(str,added))
				sql_ext = ' AND gol.id NOT IN (%s)'%added_str
			query = """
			SELECT
			gol.id AS glass_line_id
			FROM glass_order_line gol 
			JOIN glass_lot_line gll ON gll.id = gol.lot_line_id
			WHERE
			gll.ingresado = true
			AND (gll.entregado = false OR gll.entregado is null)
			AND (gll.is_break = false OR gll.is_break is null)
			AND (gll.from_insulado = false OR gll.from_insulado is null)
			AND (gol.in_packing_list = false OR gol.in_packing_list is null)
			AND gol.state = 'instock'
			AND COALESCE(gol.is_service, false) = false 
			AND gol.order_id = %d 
			%s 
			ORDER BY gll.nro_cristal_fl"""%(self.order_filter.id,sql_ext)
			self._cr.execute(query)
			results = self._cr.dictfetchall()
			invoice = self.order_filter.invoice_ids.filtered(lambda i: i.state!='cancel')
			invoice_id = invoice and invoice[0].id or False
			wizard = self.env['crystals.for.packinglist.wizard'].create({
				'packing_id':self.id,
				'detail_lines':[(0,0,{
					'invoice_id':invoice_id,
					'order_line_id': l['glass_line_id'],
				}) for l in results]
			})
			name = self.order_filter.name
			self.order_filter = False
			module = __name__.split('addons.')[1].split('.')[0]
			return {
				'name':'Cristales en ATP de OP: '+name,
				'res_id':wizard.id,
				'type': 'ir.actions.act_window',
				'res_model': wizard._name,
				'view_id': self.env.ref('%s.crystals_for_packinglist_wizard_form'%module).id,
				'view_mode': 'form',
				'view_type': 'form',
				'target': 'new',
				}
		else:
			raise UserError('No ha seleccionado ninguna OP')

	@api.one
	@api.depends('numberg')
	def refres_numg(self):
		if self.state not in ('done','cancel') and self.guide_series:
			res = ''
			if self.guide_series.prefix:
				res = self.guide_series.prefix
			number = str(self.guide_series.number_next_actual)
			if self.guide_series.padding:
				number = number.rjust(self.guide_series.padding, '0')
			res = res + number
			self.numberg = res

	def execute_packing_list(self):
		self.ensure_one()
		seq_packing_list = self.env['packing.list.config'].search([],limit=1).seq_packing_list
		if not seq_packing_list:
			raise UserError(u'No ha configurado la secuencia de Packing List en sus parámetros de configuración.')
		# lines = self.selected_line_ids
		if not self.selected_line_ids:
			raise UserError(u'No ha seleccionado ningún cristal para el Packing List.')	
		self.confirm_lines()
		self.write({
			'name': seq_packing_list.next_by_id(),
			'state':'process'
		})

	def confirm_lines(self):
		lines = self.selected_line_ids.filtered(lambda l: l.state=='draft')
		if not lines: return
		delivered = lines.filtered('order_line_id.lot_line_id.entregado')
		packing   = lines.filtered('order_line_id.in_packing_list')
		breaks    = lines.filtered(lambda l: not l.order_line_id.lot_line_id)
		msg = ''
		for item in delivered:
			msg+='%s / %s : Entregado\n'%(item.order_id.name,item.crystal_number)
		for item in packing:
			msg+='%s / %s : En otro Packing List\n'%(item.order_id.name,item.crystal_number)
		for item in breaks:
			msg+='%s / %s : Roto\n'%(item.order_id.name,item.crystal_number)
		if msg != '':
			raise UserError(u'No es posible hacer Packing List a los siguientes cristales:\n'+msg)
		# lines.mapped('order_line_id').write({
		# 		'in_packing_list':True,
		# 		'locations':[(4,item.location_tmp.id)]
		# 	})
		for item in lines:
			item.order_line_id.write({
				'in_packing_list':True,
				'locations':[(4,item.location_tmp.id)]
			})
		lines.write({'state':'confirmed'})

	def action_process_pk_to_customer(self):
		self.ensure_one()
		#self.confirm_lines() 
		#conf = self.env['packing.list.config'].search([],limit=1)		
		to_done_lines = self._get_to_done_lines()
		if self.type_pk == 'to_customer':
			sales = to_done_lines.mapped('order_id.sale_order_id')
			for sale in sales:
				lines = to_done_lines.filtered(lambda x: x.order_id.sale_order_id == sale)
				picking = sale.mapped('picking_ids').filtered(lambda x: x.state not in ('done','cancel'))
				if not picking:
					raise UserError(u'No se ha encontrado una entrega pendiente en la órden de venta: '+sale.name)
				picking = picking[0]
				ops = picking.move_lines.mapped('procurement_id.sale_line_id.order_id.op_ids')
				wiz = self.env['glass.delivery.picking.crystals'].create({
					'picking_id':picking.id,
					'glass_order_ids':[(6,0,ops.ids)],
				})
				g_lines = lines.mapped('order_line_id')
				g_lines.write({'in_packing_list':False})
				wiz.with_context(extra_domain=[('order_line_id','in',g_lines.ids)]).get_crystals_list()
				wiz.delivery()
				self.picking_ids |= picking
				lines.write({'state':'done'})
			self.state = 'done'
		return True

	def action_process_pk_to_location(self):
		self.ensure_one()
		if self.type_pk != 'to_transfer':
			raise UserError(u'La entrega a otra ubicación está sólo disponible para los packing list de tipo "Transf. a otra ubicación"')
		#self.confirm_lines()
		to_done_lines = self._get_to_done_lines()
		if not to_done_lines:
			return

		pick_type = self.picking_type_id
		pick_vals = self._prepare_pick_vals(pick_type)
		move_vals = []
		products = to_done_lines.mapped('product_id')

		for product in products:
			filt = to_done_lines.filtered(lambda l: l.product_id==product)
			g_lines = filt.mapped('order_line_id')
			qty = float_round(sum(g_lines.mapped('area')),precision_rounding=0.0001)
			move_vals.append((0,0,self._prepare_move_vals(pick_type,product,qty,g_lines.ids)))
			g_lines.write({'in_packing_list':False})
			g_lines.mapped('lot_line_id').write({'location_transfer_id':pick_type.default_location_dest_id.id})
			filt.write({'state':'done'})

		pick_vals['move_lines'] = move_vals
		picking = self.env['stock.picking'].create(pick_vals)

		#Sit = self.env['stock.immediate.transfer']
		picking.action_confirm()
		picking.action_assign()
		if picking.state!='assigned':
			picking.force_assign() # :'v
		
		for op in picking.pack_operation_ids:
			op.write({'qty_done':op.product_qty})
		picking.action_done()
		self.picking_ids |= picking
		self.state = 'done'
		return {
			'name':picking.name,
			'res_id':picking.id,
			'type': 'ir.actions.act_window',
			'res_model': picking._name,
			'view_mode': 'form',
		}

	def _get_to_done_lines(self):
		"""heredable"""
		self.confirm_lines() # Confirmar líneas agregadas a última hora GRRRR
		return self.selected_line_ids.filtered(lambda x: x.state=='confirmed')

	def _prepare_pick_vals(self,pick_type):
		current_date = fields.Date.context_today(self)
		#pick_type = self.picking_type_id
		return {
			'partner_id': False,
			'picking_type_id':pick_type.id,
			'date':current_date,
			'fecha_kardex':current_date,
			'location_dest_id':pick_type.default_location_dest_id.id,
			'location_id': pick_type.default_location_src_id.id,
			'company_id': self.env.user.company_id.id,
			'einvoice_12': self.traslate_motive_id.id,
			'serie_guia': self.guide_series.id,
			'numberg': self.numberg or '',
			'transporter_id':self.transporter_id and self.transporter_id.id or False,
			'driver_delivery': self.driver_delivery or '',
			'marca': self.marca or '',
			'placa': self.placa or '',
			'nro_const': self.nro_const or '',
			'licencia': self.licencia or '',
			'nombre': self.nombre or '',
			'ruc': self.ruc or '',
			'tipo': self.tipo or '',
			'nro_comp': self.nro_comp or '',
			'nro_guia': self.nro_guia or '',
			'fecha_traslado': self.fecha_traslado or False,
			'punto_partida': self.punto_partida or '',
			'punto_llegada': self.punto_llegada or '',
		}

	def _prepare_move_vals(self,pick_type,product,qty,glass_ids):
		current_date = fields.Date.context_today(self)
		#pick_type = self.picking_type_id
		return {
		'name': product.name,
		'product_id': product.id,
		'product_uom': product.uom_id.id,
		'date':current_date,
		'date_expected':current_date,
		'picking_type_id': pick_type.id,
		'location_id': pick_type.default_location_src_id.id,
		'location_dest_id': pick_type.default_location_dest_id.id,
		'partner_id': False,
		'move_dest_id': False,
		'state': 'draft',
		'company_id':self.env.user.company_id.id,
		'procurement_id': False,
		'route_ids':pick_type.warehouse_id and [(6,0,pick_type.warehouse_id.route_ids.ids)] or [],
		'warehouse_id': pick_type.warehouse_id and pick_type.warehouse_id.id or False,
		'product_uom_qty':qty,
		'glass_order_line_ids':[(6,0,glass_ids or [])]
		}

	def print_remission_guide(self):
		self.ensure_one()

		#copy paste de la guía de albarán csm

		if self.state not in ('process','done'):
			raise UserError(u'La guía de remisión sólo puen imprimirse cuando en los estados prcesado y finalizado.')
		# if self.picking_type_id.code != 'internal':
		# 	partner_id = self.partner_id.parent_id or self.partner_id
		# else:
		# 	#loc_partner = self.location_dest_id.location_id.get_warehouse().partner_id
		# 	#partner_id = loc_partner or self.env['res.company']._company_default_get(self.name).partner_id
		com = self.env['res.company']._company_default_get(self.name)
		if self.type_pk == 'to_customer':
			partner_id = self.selected_line_ids and self.selected_line_ids[0].partner_id or com.partner_id
		elif self.type_pk == 'to_transfer':
			partner_id = com.partner_id

		document = partner_id.nro_documento or partner_id.parent_id.nro_documento
		if not (partner_id.street and document):
			raise UserError(u'El partner %s no tiene dirección y/o número de documento'%partner_id.name)

		if not self.numberg:
			raise UserError(_(u'Debe ingresar el número de guía'))

		# if not self.punto_partida and start_point:
		# 	self.punto_partida = start_point

		
		# conf = self.env['glass.order.config'].search([],limit=1)
		# if type(path) not in (str,unicode):
		# 	path = conf.path_remission_guides
		# path+=name_file

		name_file = '%s.txt'%self.numberg.replace('/','-')
		path = self.env['packing.list.config'].search([],limit=1).path_remission_guides
		path += name_file
		
		data = ''

		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		txt = chr(27) + chr(15) + chr(27) + chr(48)
		data+=txt
		#file.write(txt)
		data+=(6 * "\n")
		# change

		def replace_chr(string):
			return string.replace('\xe1',chr(160)).replace('\xe9',chr(130)).replace('\xed',chr(161)).replace('\xf3',chr(162)).replace('\xfa',chr(163)).replace('\xc1',chr(160)).replace('\xc9',chr(144)).replace('\xcd',chr(161)).replace('\xd3',chr(162)).replace('\xda',chr(163)).replace('\xd1',chr(164)).replace('\xf1',chr(165))

		def get_line(data,widths=None,style='columns',space=2):
			position,line,styles = 'left','',['consecutive','columns'] # por defecto en left
			if style not in styles:
				return ''
			for i,item in enumerate(data):
				if type(item) is dict:
					value = str(item.get('value','undefined'))
					position = item.get('position','left')
				else:
					value = str(item).encode('iso-8859-1','ignore')
				if style == 'consecutive':
					line += value + space * ' '
					continue
				# Quitamos espacios en blanco y si si el item es mayor al ancho se recorta a la longitud del ancho dejando 2 espacios:
				value = value.strip()[:widths[i]-1] 
				count = widths[i] - len(value)
				if position == 'right':
					line += count * ' ' + value
					continue
				if position == 'center':
					count = int(count / 2)
					line += count * ' ' + value + count * ' '
					continue
				line += value + count * ' '
			return replace_chr(line)+'\n'
		
		date_guide = fields.Date.context_today(self)
		#data+=get_line(('',date_guide,self.invoice_id.number or ''),(14,55,30))
		data+=get_line(('',date_guide,''),(14,55,30)) # qué factura va acá??
		data+=get_line(('',partner_id.name,'',document),(14,42,13,30))
		# end change
		phone = partner_id.phone if partner_id.phone else ''
		partner_province = [partner_id.district_id.name,partner_id.province_id.name]
		partner_province = ' - '.join([item for item in partner_province if item])

		street_partner = partner_id.street+' '+partner_province

		data+=get_line(('',street_partner[:55],phone),(14,55,30))

		data+=get_line(('','','',self.numberg),(14,55,43,18)) # dicen q no lo usarán por lo dejo por siaca xD
		data+=(1 * "\n")

		nombre = self.transporter_id.name or 'EL MISMO'
		ruc = self.transporter_id.nro_documento or self.ruc or ''
		trans_street = self.transporter_id.street or ''
		trans_phone = self.transporter_id.phone or self.transporter_id.mobile or ''
		
		data+=get_line(('',nombre,'',ruc),(14,46,6,20))
		data+=get_line(('',trans_street,'',trans_phone),(10,50,6,20))
		data+=(1*"\n")
		
		#partida = self.punto_partida or self.picking_type_id.warehouse_id.partner_id.street or ''
		partida = self.punto_partida or ''
		
		data+=get_line(('',partida),(24,73))

		#wh_street =  self.location_dest_id.location_id.get_warehouse().partner_id.street
		#end_point = self.punto_llegada or ((wh_street or partner_id.street or '') + partner_province)
		end_point = (self.punto_llegada or '') + partner_province

		data+=get_line(('',end_point),(24,73))

		# left = 0
		# if self.einvoice_12.code != '01':
		# 	left+=18
		# if self.send_agency:
		# 	data+=get_line(('',self.first_route or ''),(24,73))
		# 	data+=get_line(('',self.second_route or '','X'),(24,73+left,3))
		# 	data+=(4 * "\n")
		# else:
		# 	data+=(6 * "\n")
		data+=(6 * "\n")


		acum_weight = 0
		total_weight = 0
		aux = 0
		headers_widths = (1,3,18,10,5,90,10) # anchos para datos de producto

		for cont,item in enumerate(self.grouped_lines,1):
			#detail_lines = move.glass_order_line_ids
			detail_lines = self.selected_line_ids.filtered(lambda x: x.product_id.id == item.product_id.id)
			container = []
			description = item.product_id.name
			acum_weight = 0.0
			for detail in detail_lines:
				try:
					op_name = str(int(detail.order_id.name))
				except ValueError:
					op_name = detail.order_id.name
				show_data = '%s-%s(%s)'%(op_name,detail.crystal_number,detail.measures)
				acum_weight += detail.weight
				container.append(show_data)
			description+=' (%d Pzs)'%(len(detail_lines))
			# else:
			# 	acum_weight = move.product_uom_qty * move.product_id.weight
			
			srt_weight = '{:,.2f}'.format(decimal.Decimal ("%0.2f"%acum_weight))
			data+=get_line(('',cont,item.product_id.default_code or '',item.area,item.product_id.uom_id.name,description,{'value':srt_weight,'position':'right'}),headers_widths)

			container = [container[i:i+5] for i in range(0,len(container), 5)]
			for sub_array in container:
				line = ''.join([x.ljust(25,' ') for x in sub_array])
				data+=get_line(('',line),(4,125))
				aux += 1
			
			total_weight += acum_weight
			aux += 1
		data+=((32-aux) * '\n')
		extra_info = u'Conductor: %s Marca: %s Placa: %s Licencia: %s'%(self.driver_delivery or '',self.marca or '',self.placa or '',self.licencia or '')
		data+=get_line(('',extra_info,'','Total Peso:',{'value':round(total_weight,4),'position':'right'}),(4,108,1,12,12))
		data+=(10*'\n')
		with open(path,'w') as file:
			file.write(data)

		##END


		# try:
		# 	conf = self.env['packing.list.config'].search([])[0]		
		# except IndexError:
		# 	raise UserError(u'No se encontraron los valores de configuración para Packing List')
		# if self.type_pk == 'to_warehouse':
		# 	self.picking_id.print_guide(path=conf.path_remission_guides)
		# 	return True
		# try:
		# 	company = self.env['res.company'].search([])[0]
		# except IndexError:
		# 	raise UserError(u"No ha configurado su compañía.\n Configure los datos de su compañía para emitir la guí de remisión.")
		
	# 	file = open(conf.path_remission_guides+self.numberg.replace('/','-')+'.txt','wb')
	# 	file.write(5 * "\n")
	# 	data = ['',self.date if self.date else '']
	# 	self.get_line(file,data,[14,55],style='columns')
		
	# 	ruc = company.partner_id.nro_documento if company.partner_id.nro_documento else ''
	# 	data = ['',company.name,ruc]
	# 	self.get_line(file,data,[14,55,30],style='columns')
	# 	phone = company.phone if company.phone else ''
	# 	street = company.partner_id.street if company.partner_id.street else ''
	# 	data = ['',street,phone]
	# 	self.get_line(file,data,[14,55,30],style='columns')
	# 	file.write(2 * "\n")
	# 	nombre = self.nombre if self.nombre else 'EL MISMO'
	# 	ruc = self.ruc if self.ruc else ''
	# 	self.get_line(file,['',nombre,ruc],[14,50,30],style='columns')
	# 	file.write(2*"\n")
	# 	self.get_line(file,['',self.punto_partida if self.punto_partida else ''],[22,60],style='columns')

	# 	self.get_line(file,['',self.punto_llegada if self.punto_llegada else ''],[22,60],style='columns')
	# 	file.write(6 * "\n")
	# 	cont = 1
	# 	weight = total_weight = aux = 0
	# 	container = []
	# 	widths = [1,5,20,10,5,80,12] # anchos para datos de producto


	# 	for item in self.grouped_lines:
	# 		lines = self.selected_line_ids.filtered(lambda x: x.product_id.id == item.product_id.id)
	# 		prod = item.product_id
	# 		for line in lines:
	# 			show_data = str(line.order_id.name)+'-'+str(line.crystal_number)+ '(' +line.measures + ')'
	# 			container.append(show_data)
	# 		weight = '{:,.2f}'.format(decimal.Decimal ("%0.2f" %  item.weight))
	# 		prod.name += ' ('+str(len(lines))+' Pzs)'
	# 		data = ['',cont,prod.default_code,item.area,prod.uom_id.name,prod.name,{'value':weight,'position':'right'}]
	# 		self.get_line(file,data,widths)
	# 		container = [container[i:i+5] for i in range(0, len(container), 5)]
	# 		for sub_array in container:
	# 			self.get_line(file,[5*' ']+sub_array,style='consecutive',space=1)
	# 			aux += 1
	# 		total_weight += float(weight)
	# 		container = []
	# 		cont += 1
	# 		aux += 1


	# 	file.write((32-aux) * '\n')
	# 	self.get_line(file,['','Total Peso:',{'value':total_weight,'position':'right'}],[109,12,12])
	# 	file.write(10*'\n')
	# 	file.close()

	# @api.model
	# def get_line(self,file,data,widths=None,style='columns',space=2):
	# 	position = 'left' # por defecto en left
	# 	line = ''
	# 	styles = ['consecutive','columns']
	# 	if style not in styles:
	# 		print('Invalid style value: set style as column or consecutive')
	# 		return
	# 	for i,item in enumerate(data):
	# 		if type(item) is unicode:
	# 			item = item.encode('ascii','ignore').decode('ascii')
	# 		if type(item) is dict:
	# 			value = str(item.get('value','undefined'))
	# 			position = item.get('position','left')
	# 		else:
	# 			value = str(item)
	# 		if style == 'consecutive':
	# 			line += value + space * ' '
	# 			continue
	# 		value = value.strip()[:widths[i]-1] 
	# 		count = widths[i] - len(value)
	# 		if position == 'right':
	# 			line += count * ' ' + value
	# 			continue
	# 		if position == 'center':
	# 			count = int(count / 2)
	# 			line += count * ' ' + value + count * ' '
	# 			continue
	# 		line += value + count * ' '
	# 	file.write(line+'\n')

	@api.multi
	def refresh_resume_lines(self):
		self.grouped_lines.unlink()
		lines = self.selected_line_ids.mapped('order_line_id')
		for prod in lines.mapped('product_id'):
			fil_list = lines.filtered(lambda x:x.product_id.id == prod.id)
			new = self.env['packing.list.grouped.line'].create({
			'parent_id':self.id,
			'product_id':prod.id, 
			'count_crystals':len(fil_list), 
			'area':float(sum(fil_list.mapped('area'))), 
			'weight':float(sum(fil_list.mapped('peso'))), 
			})
		return True

	def action_cancel(self):
		if self.picking_ids:
			raise UserError(u'El packing List ya tiene restrados albaranes y no puede ser cancelado')
		self.selected_line_ids.mapped('order_line_id').write({'in_packing_list':False})
		self.selected_line_ids.write({'state':'draft'})
		self.state = 'draft'

class Packing_List_Grouped_Line(models.Model):
	_name='packing.list.grouped.line'

	parent_id = fields.Many2one('packing.list',string='Parent')
	product_id = fields.Many2one('product.product')
	count_crystals = fields.Integer('Nro de cristales')
	area = fields.Float(string='Area', digits=(20,4))
	weight = fields.Float('Peso', digits=(20,4))

class GlassOrderLinePackingList(models.Model):
	_name = 'glass.order.line.packing.list'

	packing_id = fields.Many2one('packing.list',string="Packing List")
	order_line_id = fields.Many2one('glass.order.line',string='Order Line')
	lot_line_id = fields.Many2one(related='order_line_id.lot_line_id',string='Lot line')
	order_id = fields.Many2one('glass.order','OP')
	product_id = fields.Many2one('product.product','Producto')
	state = fields.Selection(string='Estado',selection=[('draft','Borrador'),('confirmed','Confirmado'),('done','Realizado')],default='draft')
	#picking_id = fields.Many2one('stock.picking')
	invoice_id = fields.Many2one('account.invoice')
	crystal_number = fields.Char(related='order_line_id.crystal_number',string='Nro Cristal')
	lot_id = fields.Many2one(related='lot_line_id.lot_id',string='Lote')
	requisition_id = fields.Many2one(related='lot_id.requisition_id',string='Requisicion')
	partner_id = fields.Many2one(related='order_line_id.partner_id',string='Cliente')
	location_tmp = fields.Many2one('custom.glass.location',string=u'Ubicación')
	warehouse_tmp = fields.Many2one(related='location_tmp.location_code',string='Almacen') # ??? 
	measures = fields.Char(related='order_line_id.measures')
	weight = fields.Float(related ='order_line_id.peso',string='Peso', digits=(20,4))
	area = fields.Float(related='order_line_id.area',string="Area",digits=(12,4))	