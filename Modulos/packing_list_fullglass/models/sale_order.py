
## NO SE SI ESTA WADA SEGUIRÁ VIGENTE, LO DEJO POR SI ACASO PERO LO RETIRO DEL __init__

from odoo import fields, models,api, _
from odoo.exceptions import UserError
from datetime import datetime

class SaleOrder(models.Model):
	_inherit = 'sale.order'
	packing_pick_ids = fields.Many2many('stock.picking')
	packing_count = fields.Integer(compute='_get_glass_packing', default=0)

	@api.depends('packing_pick_ids')
	def _get_glass_packing(self):
		for rec in self:
			rec.packing_count = len(rec.packing_pick_ids)

	# para mostrar los albaranes hechos por packing lista de esta SO
	@api.multi
	def show_packing_list(self):
		if len(self.packing_pick_ids) == 1:
			return {
				'name':self.packing_pick_ids[0].name,
				'res_id':self.packing_pick_ids[0].id,
				'type': 'ir.actions.act_window',
				'res_model': 'stock.picking',
				'view_id': self.env.ref('stock.view_picking_form').id,
				'view_mode': 'form',
				'view_type': 'form',
				}
		elif len(self.packing_pick_ids) > 1:
			return {
				'name':'Operaciones de Packing List',
				'type': 'ir.actions.act_window',
				'res_model': 'stock.picking',
				'view_id': self.env.ref('stock.vpicktree').id,
				'view_type': 'tree',
				'domain': [['id', 'in', self.packing_pick_ids.ids]],
				}