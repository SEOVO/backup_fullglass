# -*- encoding: utf-8 -*-
{
	'name': 'Reporte vale entrega',
	'category': 'reporte',
	'author': 'ITGRUPO-POLIGLASS',
	'depends': ['import_base_it','stock','print_guia_remision_it_poliglass','kardex_it'],
	'version': '1.0.0',
	'ITGRUPO_VERSION': 2,
	'description':"""
	Reportes poliglass
	""",
	'auto_install': False,
	'demo': [],
	'data':	[
		'stock_picking.xml',
		#'reporte_vale_entrega.xml',
		],
	'installable': True
}
