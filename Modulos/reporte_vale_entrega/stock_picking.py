# -*- encoding: utf-8 -*-
from odoo import api, fields, models, exceptions
from datetime import datetime,timedelta
import sys,decimal
class StockPicking(models.Model):
	_inherit = 'stock.picking'
	
	@api.multi
	def print_ticket(self,path,start_point=False): # no quitar el param start_point
		if not self.partner_id:
			raise exceptions.Warning('La entrega no tiene un partner disponible')
		name_file = 'vale_entrega_'+self.name.replace('/','-')+'.txt'
		if type(path) in (str,unicode):
			file = open(path+name_file, "w")
		else:
			path = self.env['glass.order.config'].search([])[0].path_remission_guides
			file = open(path+name_file, "w")
		reload(sys)
		sys.setdefaultencoding('iso-8859-1')
		txt = chr(27) + chr(15) + chr(27) + chr(48)
		file.write(txt)
		file.write(3*"\n")
		# change
		def change_chr(string):
			return string.replace('\xe1',chr(160)).replace('\xe9',chr(130)).replace('\xed',chr(161)).replace('\xf3',chr(162)).replace('\xfa',chr(163)).replace('\xc1',chr(181)).replace('\xc9',chr(144)).replace('\xcd',chr(214)).replace('\xd3',chr(224)).replace('\xda',chr(233)).replace('\xd1',chr(164)).replace('\xf1',chr(165))
		now = datetime.now()-timedelta(hours=5)
		inv = self.invoice_id.number if self.invoice_id.number else ''
		data = [inv,str(now)[:19],'Entrega # '+self.name]
		self.print_line(file,data,[18,45,25],style='columns')
		file.write("\n")
		data = [change_chr(self.partner_id.name),str(now)[:19]]
		self.print_line(file,data,[63,25],style='columns')
		# end change
		line = 85 * '_'
		self.print_line(file,[line],[101],style='columns')
		data = ['',change_chr(u'Código'),change_chr(u'Descripción'),'Unidad','Cantidad','Peso']
		self.print_line(file,data,[6,20,33,10,12,5],style='columns')
		self.print_line(file,[line],[101],style='columns')
		
		for move in self.move_lines:
			headers_widths = [16,45,9,7,8]
			total_weight = 0
			code = move.product_id.default_code  or ''
			name = move.product_id.name
			uom  = move.product_uom.name
			qty  = move.product_uom_qty
			qty  = '{:,.2f}'.format(decimal.Decimal ("%0.2f" % qty))
			weight = move.product_id.weight * move.product_uom_qty
			weight = '{:,.2f}'.format(decimal.Decimal ("%0.2f" %  weight))
			data = [code,change_chr(name),uom,{'value':qty,'position':'right'},{'value':weight,'position':'right'}]
			self.print_line(file,data,headers_widths,style='columns')
		space = 24 - len(self.move_lines)
		file.write(space*'\n')
		file.write('---')
		file.close()

	def print_line(self,file,data,widths=None,style='columns',space=2):
		position = 'left' # por defecto en left
		line = ''
		styles = ['consecutive','columns']
		if style not in styles:
			print('Invalid style value: set style as column or consecutive')
			return
		for i,item in enumerate(data):
			if type(item) is dict:
				value = str(item.get('value','undefined'))
				position = item.get('position','left')
			else:
			   value = str(item)

			if style == 'consecutive':
				line += value + space * ' '
				continue
			# Quitamos espacios en blanco y si si el item es mayor al ancho se recorta a la longitud del ancho dejando 2 espacios:
			value = value.strip()[:widths[i]-1] 
			count = widths[i] - len(value)
			if position == 'right':
				line += count * ' ' + value
				continue
			if position == 'center':
				count = int(count / 2)
				line += count * ' ' + value + count * ' '
				continue
			line += value + count * ' '

		file.write(line+'\n')
			

