from odoo import models,fields, api,exceptions, _
from openerp.osv import osv
import base64
import codecs
import pprint
import math
from reportlab.pdfgen import canvas
from reportlab.lib.units import inch
from reportlab.lib.colors import magenta, red , black , blue, gray, Color, HexColor
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib.pagesizes import letter, A4
from reportlab.platypus import SimpleDocTemplate, Table, TableStyle
from reportlab.lib import colors
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.platypus import Paragraph, Table
from reportlab.lib.units import  cm,mm
from reportlab.lib.utils import simpleSplit
from cgi import escape
import decimal


class ProductPricelist(models.Model):
	_inherit = 'product.pricelist'
	prod_category = fields.Many2one('product.category','Categoria')
	mode_selector = fields.Selection([('variant','Producto'),('category','Categoria')],string='Agregar por:', default='variant')


	@api.multi
	def get_lista_items(self):
		action = self.env.ref('custom_functionaity_rates_it.action_sale_tarifa').read()[0]

		items = self.mapped('item_ids')
		if len(items) > 1:
			action['domain'] = [('id', 'in', items.ids)]
		elif items:
			action['views'] = [(self.env.ref('product.product_pricelist_item_form_view').id, 'form')]
			action['res_id'] = items.id
		return action


	@api.multi
	def export_excel(self):
		direccion = self.env['main.parameter'].search([])[0].dir_create_file
		self.env.cr.execute("""
				copy (select * from public.get_rep_tarifa(('"""+str(self.id)+"""'))) to
				'"""+direccion+u"""reporte_elementos_tarifa.csv' WITH DELIMITER ',' CSV HEADER
				"""
			)

		f = open(direccion + 'reporte_elementos_tarifa.csv', 'rb')


		sfs_obj = self.pool.get('repcontab_base.sunat_file_save')
		vals = {
			'output_name': 'reporte_elementos_tarifa.csv',
			'output_file': base64.encodestring(''.join(f.readlines())),
		}

		sfs_id = self.env['export.file.save'].create(vals)

		return {
			"type": "ir.actions.act_window",
			"res_model": "export.file.save",
			"views": [[False, "form"]],
			"res_id": sfs_id.id,
			"target": "new",
		}

		# import io
		# from xlsxwriter.workbook import Workbook
		# output = io.BytesIO()
		# ########### PRIMERA HOJA DE LA DATA EN TABLA
		# #workbook = Workbook(output, {'in_memory': True})


		# workbook = Workbook(direccion +'elementostarifa.xlsx')
		# worksheet = workbook.add_worksheet("Elementos de Tarifa")
		# #Print Format
		# worksheet.set_landscape() #Horizontal
		# worksheet.set_paper(9) #A-4
		# worksheet.set_margins(left=0.75, right=0.75, top=1, bottom=1)
		# worksheet.fit_to_pages(1, 0)  # Ajustar por Columna

		# bold = workbook.add_format({'bold': True})
		# normal = workbook.add_format()
		# boldbord = workbook.add_format({'bold': True})
		# boldbord.set_border(style=2)
		# boldbord.set_align('center')
		# boldbord.set_align('vcenter')
		# boldbord.set_text_wrap()
		# boldbord.set_font_size(9)
		# boldbord.set_bg_color('#DCE6F1')
		# numbertres = workbook.add_format({'num_format':'0.000'})
		# numberdos = workbook.add_format({'num_format':'0.00'})
		# numberdosbord = workbook.add_format({'num_format':'0.00','bold': True})
		# numberdosbord.set_border(style=1)
		# bord = workbook.add_format()
		# bord.set_border(style=1)
		# bord.set_text_wrap()
		# bord.set_align('center')
		# bord.set_align('vcenter')
		# numberdos.set_border(style=1)
		# numbertres.set_border(style=1)


		# title = workbook.add_format({'bold': True})
		# title.set_align('center')
		# title.set_align('vcenter')
		# title.set_text_wrap()
		# title.set_font_size(20)
		# worksheet.set_row(0, 30)
		# saldo_inicial = 0
		# saldo_tmp = 0 ## saldo acumulativo
		# x= 6
		# tam_col = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
		# tam_letra = 1.2
		# import sys
		# reload(sys)
		# sys.setdefaultencoding('iso-8859-1')

		# worksheet.merge_range(0,0,0,6,'TARIFA : ' + self.name, title)


		# #worksheet.write(2,0, u" NOMBRE DE TARIFA", boldbord)
		# #worksheet.write(2,1, self.name, boldbord)
		# worksheet.write(2,0, u" ULTIMA ACTUALIZACION: ", bord)
		# worksheet.write(2,1, self.write_date, bord)





		# worksheet.write(5,1, "PRODUCTO", boldbord)
		# worksheet.write(5,2, "PRECIO DE VENTA:", boldbord)
		# worksheet.write(5,3, "COSTO PROMEDIO", boldbord)
		# worksheet.write(5,4, "ULTIMA COMPRA", boldbord)
		# worksheet.write(5,5, "PRECIO(FORMULA)", boldbord)

		# valores=[0,0,0,0]



		# dicc_meses={

		# 	'01':'00',
		# 	'02':'01',
		# 	'03':'02',
		# 	'04':'03',
		# 	'05':'04',
		# 	'06':'05',
		# 	'07':'06',
		# 	'08':'07',
		# 	'09':'08',
		# 	'10':'09',
		# 	'11':'10',
		# 	'12':'11',
		# 	}

		# resultados = [0,0]



		# for item in self.item_ids:
		# 	print('itemcito: ', item)
		# 	worksheet.write(x,1,line.product_id.campo_name_get ,bord )
		# 	worksheet.write(x,2,line.product_id.lst_price if line.applied_on else '' ,bord )
		# 	worksheet.write(x,3,line.standart_price if line.standart_price  else '',bord)
		# 	worksheet.write(x,4,line.last_cost if line.last_cost  else '',bord)
		# 	worksheet.write(x,5,line.price if line.price  else '',bord)

		# 	x = x + 1

		# tam_col = [30,30,20,20,20,20,20,30,7,15,6,15,30,10,10,10,10,10,10,10,10,12,12,12,12]
		# worksheet.set_column('A:A', tam_col[0])
		# worksheet.set_column('B:B', tam_col[1])
		# worksheet.set_column('C:C', tam_col[2])
		# worksheet.set_column('D:D', tam_col[3])
		# worksheet.set_column('E:E', tam_col[4])
		# worksheet.set_column('F:F', tam_col[5])
		# worksheet.set_column('G:G', tam_col[6])
		# worksheet.set_column('H:H', tam_col[7])
		# worksheet.set_column('I:I', tam_col[8])
		# worksheet.set_column('J:J', tam_col[9])
		# worksheet.set_column('K:K', tam_col[10])
		# worksheet.set_column('L:L', tam_col[11])
		# worksheet.set_column('M:M', tam_col[12])
		# worksheet.set_column('N:N', tam_col[13])
		# worksheet.set_column('O:O', tam_col[14])
		# worksheet.set_column('P:P', tam_col[15])
		# worksheet.set_column('Q:Q', tam_col[16])
		# worksheet.set_column('R:R', tam_col[17])
		# worksheet.set_column('S:S', tam_col[18])
		# worksheet.set_column('T:T', tam_col[19])

		# workbook.close()


#PARA AGREGAR ITEMS A LA TARIFA
	@api.multi
	def add_items(self):


		products = None
		print('sel', self.mode_selector)
		if not self.mode_selector:
			raise exceptions.Warning('No ha seleccionado un tipo de Criterio. \n'+'Debe elegir Producto o Categoria')
		if self.mode_selector == 'category':
			cat = self.prod_category[0].id
		if self.mode_selector == 'variant':
			cat = 0



		self.env.cr.execute("""
				select* from get_lista(1,('"""+str(self.id)+"""'),('"""+str(cat)+"""'))
				"""
			)

		verified_products= self.env.cr.fetchall()



		for product in verified_products:
			vals = {
				'pricelist_id': self.id,
				'applied_on': '0_product_variant',
				'compute_price': 'fixed',
				#'standart_price': product[0].standard_price,
				'product_id': product
			}

			self.env['product.pricelist.item'].create(vals)

#PARA REMOVER LOS ITEMS DEPENDIENDO DE LA CATEGORIA O TODOS LOS ITEMS
	@api.multi
	def remove_items(self):

		print('works remove')
		if not self.mode_selector:
			raise exceptions.Warning('No ha seleccionado un tipo de Criterio. \n'+'Debe elegir Producto o Categoria')
		if self.mode_selector == 'variant':
			cat = 0
		if self.mode_selector == 'category':
			cat = self.prod_category[0].id
		self.env.cr.execute("""
				select* from get_lista(2,('"""+str(self.id)+"""'),('"""+str(cat)+"""'))
				"""
			)
		verified_products= self.env.cr.fetchall()

#PARA MOSTRAR LOS COSTOS EN LA TARIFA
	@api.multi
	def show_cost_for_items(self):

		self.env.cr.execute("""
				select * from vst_ultimos_precios_compra
				"""
			)
		query = list(self.env.cr.dictfetchall())
		for que in query:
			result = self.env['product.pricelist.item'].search([('product_id','=', que["product_id"]),('pricelist_id','=', self.id)])
			cadena = str(que['price_unit'])
			cadena1 = que['simbolo']
			cadena2= cadena1 + ' ' + cadena  
			result.write({'last_cost' : cadena2})



	@api.multi
	def show_costo_prom(self):
		self.env.cr.execute("""
				select  cast(substr (res_id, 17) AS INTEGER) as product_id, value_float as costo_promedio
				FROM ir_property
				WHERE name='standard_price'
				"""
			)
		query_costo = list(self.env.cr.dictfetchall())
		for que_costo in query_costo:
			que_costo
		for que_costo in query_costo:
			result = self.env['product.pricelist.item'].search([('product_id','=', que_costo["product_id"]),('pricelist_id','=', self.id)])
			result.write({'standart_price' : que_costo["costo_promedio"]})



class ProductPricelistItem(models.Model):
	_inherit = 'product.pricelist.item'
	# standard price del producto de del product_list_item
	standart_price = fields.Char(u'Precio Estandar')
	# last_cost del producto de del product_list_item
	last_cost = fields.Char('Ultimo costo')
